var cluster = require('cluster');
var http = require('http');
//var numCPUs = require('os').cpus().length;
var numWorkers = 3;
var singleThreadApp = require(__dirname+'/singleThreadApp');

if (cluster.isMaster) {
  // Fork workers.
  console.log('Master cluster setting up ' + numWorkers + ' workers...');

  for(var i = 0; i < numWorkers; i++) {
      cluster.fork();
  }

  cluster.on('online', function(worker) {
      console.log('Worker ' + worker.process.pid + ' is online');
  });

  cluster.on('exit', function(worker, code, signal) {
      console.log('Worker ' + worker.process.pid + ' died with code: ' + code + ', and signal: ' + signal);
      console.log('Starting a new worker');
      cluster.fork();
  });
} else {
  // Workers can share any TCP connection
  // In this case it is an HTTP server
  // http.createServer(function(req, res) {
  //   res.writeHead(200);
  //   res.end("hello world\n");
  // }).listen(8000);
  singleThreadApp.start();
}