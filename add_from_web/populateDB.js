const request = require('request');
const util = require('util');

var urls=["http://otteny.com/en/jia-jia-embroidered-top.html","http://otteny.com/en/scarlet-kid-suede-heels.html","http://otteny.com/en/slip-on-platform-sneaker.html","http://otteny.com/en/tide-sailor-pant.html","http://otteny.com/en/slip-on-platform-sneaker-otteny-65283.html","http://otteny.com/en/tsague-suede-metallic-sneaker.html","http://otteny.com/en/tsague-suede-flower-sneaker.html","http://otteny.com/en/francy-suede-hightop-sneakers.html","http://otteny.com/en/roses-fringe-scarf.html","http://otteny.com/en/roses-leopard-fringe-scarf.html","http://otteny.com/en/16-9oz-a-rose-by-any-other-name-body-cleanser.html","http://otteny.com/en/velvet-tonal-scarf-otteny-65501.html"];




function parseData(x)
{
	var requestJSON = {
		"request":{
			"url":''
		}
	};
	if(process.env.NODE_ENV.toLowerCase()=='development')
	{
		var scrapeURL = 'http://192.168.56.101:7772/v2/scrape/request/';
	}else{
		var scrapeURL = 'http://104.199.138.98:7772/api/v2/scrape/request/';//staging only
		//var scrapeURL = 'http://localhost:7772/api/v2/scrape/request/';//staging only
	}
	console.log('scrapeURL: '+scrapeURL);
	requestJSON.request.url=urls[x];
	request.post(
	    scrapeURL,
	    { form: { data: requestJSON } },
	    function (error, response, body) {
	        if (!error && response.statusCode == 200) {
	        	console.log('processing '+urls[x]);
	        	//console.log(body);
	        	//var dataJSON = eval("("+body+")");
	        	var bodyHolder = body;
	        	var dataJSON = JSON.parse(bodyHolder);
	        	//console.log(dataJSON);
	        	//console.log(util.inspect(dataJSON,{showHidden: false, depth: null}));
	        	//console.log(dataJSON.data[0]);
	        	if(typeof(dataJSON)!='undefined')
	        	{
	        		if(dataJSON.status=="Success" && typeof(dataJSON.data)!='undefined')
		        	{
		        		console.log(dataJSON.status+" : "+urls[x]);
		        		//console.log(dataJSON);
		        		// saveData(x,dataJSON);
		        		var data = dataJSON.data[0];
		        		/*
							"product_id": < int > (if product already exists),
	                        "owner_owner_shop_id":<int> (if there is already a store else default 0),
	                        "owner_user_id":<int> (user_id of the one saving the data),
	                        "category_id":<int> (if category_id exists, else default 0),
		        		*/
		        		data.owner_shop_id=0;
		        		data.owner_user_id=1;
		        		data.category_id=0;
		        		console.log(data);
						var saveJSON = {
							"request": {
								"type": "Save Scraped from Web",
								"data": data
							}
						};
						
						if(process.env.NODE_ENV.toLowerCase()=='development')
						{
							var saveUrl = 'http://192.168.56.101:7772/v2/scrape/save/';
						}else{
							var saveUrl = 'http://104.199.138.98:7772/api/v2/scrape/save/';//staging only
						}
						console.log('saveUrl: '+saveUrl);
						console.log('saving data');
						request.post(
						    saveUrl,
						    { form: { data: saveJSON } },
						    function (error, response, body) {
						        if (!error && response.statusCode == 200) {
						        	// console.log('processing '+urls[x]);
						         //    saveData(x);
						         	console.log(body);
						        }else{
						        	console.log("ERROR:");
						        	console.log(error);
						        }
						    }
						);	
		        	}else{
		        		console.log("skipping :"+urls[x]);
		      			console.log(util.inspect(dataJSON,{showHidden: false, depth: null}));
		        	}
	        	}
	        }else{
	        	console.log("ERROR:");
	        	console.log(error);
	        }
	    }
	);
}


function saveData(x,dataJSON)
{
	//dataJSON = JSON.parse(dataJSON);
	console.log('saveData ======================== ');
	console.log(dataJSON);
	console.log(util.inspect(dataJSON,{showHidden: false, depth: null}));
	console.log(dataJSON.data[0]);
	var data = dataJSON.data[0];
	var saveJSON = {
		"request": {
			"type": "Save Scraped from Web",
			"data": data
		}
	};
	var saveUrl = 'http://192.168.56.101:7772/v2/scrape/save/';
	request.post(
	    saveUrl,
	    { form: { data: saveJSON } },
	    function (error, response, body) {
	        if (!error && response.statusCode == 200) {
	        	// console.log('processing '+urls[x]);
	         //    saveData(x);
	        }else{
	        	console.log("ERROR:");
	        	console.log(error);
	        }
	    }
	);
}


for(var x = 0; x<urls.length;x++)
{
	parseData(x);
}
