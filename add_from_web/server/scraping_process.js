const Crawler = require ('crawler');
const parseUri = require(__dirname + '/helpers/parseUri');
const txtman = require(__dirname + '/helpers/text_manipulation');
const mysql = require(__dirname + '/helpers/mysql_helpers');
const imageFilters = ['.jpg'];
const util = require('util');
const respond = require(GLOBAL.server_location+'/EventListeners/errorListener');
//var JSONstructs = require(__dirname + '/helpers/standardJSONstruct');
//var dataSend = JSONstructs.dataSend;

module.exports.scrapeSite = function(url,myres,myreq){
	var dataSend = {
		"product_name":"",
		"mainDescription":"",
		"price":"",
		"image":"",
		"images":[],
		"otherTitle":[],
		"otherDescription":[],
		"otherPrices":[],
		"site_name":"",
		"web_url":"",
		"canonical_url":"",
		"meta_keywords":""
	};
	//check DB first
	data={
		"url":url,
		"searchby":"url" 
	};
	var ret = false;
	// query = mysql.getExternalProducts(data);
	// GLOBAL.db_cs_search_results.query(query,function(err,rows, fields){
	// 	if(err==null)
	// 	{
	// 		if(rows.length > 0)
	// 		{
	// 			//populate dataSend
	// 			var images = eval("("+rows[0].image_urls+")");
	// 			dataSend = {
	// 				"product_name":rows[0].name,
	// 				"mainDescription":"",
	// 				"price":rows[0].price,
	// 				"image":rows[0].image_url,
	// 				"images": images,
	// 				"otherTitle":[],
	// 				"otherDescription":[],
	// 				"otherPrices":[],
	// 				"site_name":rows[0].domain,
	// 				"web_url":rows[0].url,
	// 				"canonical_url":rows[0].canonical_url,
	// 				"meta_keywords":rows[0].meta_keywords
	// 			};
	// 			myres.json(dataSend);
	// 		}else{
				//if no result then scrape
				console.log("setting crawler");
				var c = new Crawler({
					"maxConnections":40,
					"userAgent":"Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2490.86 Safari/537.36",
					"jQuery": true,
					"callback":function(err,result,$){
						parsePage(err,result,$,myres,myreq,dataSend);
					},
					"forceUTF8": true
				});
				console.log("calling url");
				c.setMaxListeners(20);
				c.queue(url);
	// 		}
	// 	}else{
	// 		console.log("MySQL Error:");
	// 		console.log(error);
	// 	}

	// });
}

function parsePage(err,result,$,res,req,dataSend){
	console.log("parsing page");
	if(err==null)
	{
		//result.uri;//this is the original URL sent
		//result.body;//this is the read information
		//console.log(result.body);
		//get domain
		var domain = parseUri.parseUri(result.uri).host;
		//remove www from domain
		dataSend.site_name = domain.replace("www.","");
		//get original url
		dataSend.web_url = result.uri;
		//get canonical
		if((dataSend.canonical_url = txtman.getCanonical($,result.body))=='')
		{
			dataSend.canonical_url = "";
		}
		//get keywords
		if((dataSend.meta_keywords = txtman.getKeywords($,result.body))=='')
		{
			dataSend.meta_keywords = "";
		}
		//use domain as the function
		try{
			domain = domain.toLowerCase();
			
			// var asos = /asos.com/i;
			// var shopify = /shopify.com/i;
			// if(asos.test(domain))
			// {
			// 	GLOBAL.modules["wc.asos.com"](err,result,$,dataSend,result.uri,domain,imageFilters,res,req);
			// }else if(shopify.test(domain)){
			// 	GLOBAL.modules["wc.shopify.com"](err,result,$,dataSend,result.uri,domain,imageFilters,res,req);
			// }else{

				var testFlag=false;
				var x = 0;
				for(x=0;x<GLOBAL.wodubNames.length;x++)
				{
					var testVar = new RegExp(GLOBAL.wodubNames[x].toString(),"i");
					if(testVar.test(domain)==true)
					{
						testFlag=true;
						break;
					}
				}
				if(testFlag==true)
				{
					var newDomain = GLOBAL.wdubNames[x];
					console.log("executing "+domain+" through "+newDomain);
					GLOBAL.modules[newDomain](err,result,$,dataSend,result.uri,domain,imageFilters,res,req);
				}else{
					GLOBAL.modules["generic_scraping"](err,result,$,dataSend,result.uri,domain,imageFilters,res,req);
				}

				// if(GLOBAL.wdubNames.indexOf(domain)!=-1)
				// {
				// 	console.log("executing "+domain);
				// 	GLOBAL.modules[domain](err,result,$,dataSend,result.uri,domain,imageFilters,res,req);
				// }else if(GLOBAL.wodubNames.indexOf(domain)!=-1)
				// {
				// 	var newDomain = GLOBAL.wdubNames[GLOBAL.wodubNames.indexOf(domain)];
				// 	console.log("executing "+domain+" through "+newDomain);
				// 	GLOBAL.modules[newDomain](err,result,$,dataSend,result.uri,domain,imageFilters,res,req);
				// }else{
				// 	console.log("executing generic_scraping for "+domain);
				// 	GLOBAL.modules["generic_scraping"](err,result,$,dataSend,result.uri,domain,imageFilters,res,req);
				// }			
			// }
		}catch(error){
			console.log(error);
			console.log(util.inspect(error,{showHidden: false, depth: null}));
			var msg = {
				"success":false, "status":"Error",
				"desc":"[scraping_process:parsePage]Error Processing Data",
				"err":error,
				"details":dataSend,
				"code":1
			};
			respond.respondError(msg,res,req);
			//GLOBAL.modules["generic_scraping"](err,result,$,dataSend,result.uri,domain,imageFilters,res,req);
			// do nothing for now
		}
	}else{
		
		var msg = {
			"success":false, "status":"Error",
			"desc":"[scraping_process:parsePage]Error parsing page",
			"err":err,
			"code":0
		};
		respond.respondError(msg,res,req);
		/*
		No internet connection
		{
			code: "ENOTFOUND",
			errno: "ENOTFOUND",
			syscall: "getaddrinfo"
		}
		*/
	}
}

