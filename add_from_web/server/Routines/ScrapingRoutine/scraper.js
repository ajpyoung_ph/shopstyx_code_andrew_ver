const Crawler = require ('crawler');
const parser = require(__dirname+'/parsePage');
//var phantom=require('node-phantom');

const parseUri = require(GLOBAL.server_location+'/helpers/parseUri');
const util = require('util');
const respond = require(GLOBAL.server_location+'/EventListeners/errorListener');
						//dataJSON,req,res,simpleRes
var scrapeSite = function(url,req,res,cb)
{
	//console.log("calling the scrapeSite");
	var problematic_sites=['sears.com','tigerdirect.com','walmart.com'];
	var imageFilters = ['.jpg'];
	var dataSend = { //edit this to conform to new format
		"product_name":"",
		"mainDescription":"",
		"price":"",
		"image":"",
		"images":[],
		"otherTitle":[],
		"otherDescription":[],
		"otherPrices":[],
		"site_name":"",
		"web_url":"",
		"canonical_url":"",
		"meta_keywords":"",
		"seller_url":""
	};
	var problem_flag = false;
	// for(var x = 0;x<problematic_sites.length;x++)
	// {
	// 	var testReg = new RegExp(problematic_sites[x].toString(),"i");
	// 	if(testReg.test(url)==true)
	// 	{
	// 		problem_flag=true;
	// 		break;
	// 	}
	// }
	if(util.isFunction(parseUri)==false)
	{
		var domain = simple_domain(url);
	}else{
		var domain = parseUri(url).host;
	}
	if(util.isNullOrUndefined(domain)==true)
	{
		var msg = {
			"success":false, "status":"Error",
			"desc":"[scraper:scrapeSite]Error unknown url format",
			"err":"URL sent is : "+url
		};
		respond.respondError(msg,res,req);
	}else{
		dataSend.site_name = domain.replace("www.","");
		//get original url
		dataSend.web_url = url;
		domain = domain.toLowerCase()
		// if(problem_flag==false)
		// {
			//console.log("setting crawler");
			var c = new Crawler({
				"maxConnections":40,
				"userAgent":"Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2490.86 Safari/537.36",
				"jQuery": true,
				"callback":function(err,result,$){
					//console.log("inside crawler callback");
					parser.parsePage(err,result,$,res,req,imageFilters,dataSend,cb);
				},
				"forceUTF8": true
			});
			//console.log("calling url");
			c.setMaxListeners(20);
			c.queue(url);
	}
	
	// }else{
	// 	//phantomjs crawler
	// 	//console.log("using phantomjs crawler");
	// 	// var domain = parseUri(url).host;
	// 	// dataSend.site_name = domain.replace("www.","");
	// 	// //get original url
	// 	// dataSend.web_url = url;
	// 	// domain = domain.toLowerCase();

	// 	var x = 0;
	// 	for(x=0;x<GLOBAL.wodubNames.length;x++)
	// 	{
	// 		var testVar = new RegExp(GLOBAL.wodubNames[x].toString(),"i");
	// 		if(testVar.test(domain)==true)
	// 		{
	// 			testFlag=true;
	// 			break;
	// 		}
	// 	}
	// 	GLOBAL.modules[GLOBAL.wdubNames[x]](dataSend,imageFilters,url,res,cb);

		
		// phantom.create(function(err,ph) {
		// 	if(err==null)
		// 	{
		// 		//console.log("Success phantom.create");
		// 		return ph.createPage(function(err,page) {
		// 			if(err==null){
		// 				//console.log("ph.createPage");
		// 				return page.open(url, function(err,status) {
		// 					if(err==null)
		// 					{
		// 						//console.log("opened site? ", status);
		// 						page.includeJs('http://localhost:7772/v2/jquery/request/jquery-2.1.4.min.js', function(err) {
		// 							//jQuery Loaded. 
		// 							//Wait for a bit for AJAX content to load on the page. Here, we are waiting 8 seconds. 
		// 							//console.log("insertion of jQuery done");
		// 							setTimeout(function() {
		// 								var domain = parseUri(url).host;
		// 								dataSend.site_name = domain.replace("www.","");
		// 								//get original url
		// 								dataSend.web_url = url;
		// 								domain = domain.toLowerCase();

		// 								var x = 0;
		// 								for(x=0;x<GLOBAL.wodubNames.length;x++)
		// 								{
		// 									var testVar = new RegExp(GLOBAL.wodubNames[x].toString(),"i");
		// 									if(testVar.test(domain)==true)
		// 									{
		// 										testFlag=true;
		// 										break;
		// 									}
		// 								}
		// 								return page.evaluate(GLOBAL.modules[GLOBAL.wdubNames[x]](dataSend,imageFilters,url), function(err,dataSend) {
		// 									//console.log(dataSend);
		// 									ph.exit();
		// 								});
		// 								// return page.evaluate(function() {
		// 								// 	//Get what you want from the page using jQuery. A good way is to populate an object with all the jQuery commands that you need and then return the object. 
		// 								// 	var h2Arr = [],
		// 								// 	pArr = [];
		// 								// 	$('h2').each(function() {
		// 								// 		h2Arr.push($(this).html());
		// 								// 	});

		// 								// 	$('p').each(function() {
		// 								// 		pArr.push($(this).html());
		// 								// 	});

		// 								// 	return {
		// 								// 		h2: h2Arr,
		// 								// 		p: pArr
		// 								// 	};
		// 								// }, function(err,dataSend) {
		// 								// 	//console.log(dataSend);
		// 								// 	ph.exit();
		// 								// });
		// 							}, 8000);
		// 						});
		// 					}else{
		// 						var msg = {
		// 							"success":false, "status":"Error",
		// 							"desc":"[scraper:phantom.create]Error Processing Data",
		// 							"err":err,
		// 							"status":status,
		// 							"code":3
		// 						};
		// 						process.emit('Shopstyx:resError',msg,res);
		// 					}
							
		// 				});
		// 			}else{
		// 				var msg = {
		// 					"success":false, "status":"Error",
		// 					"desc":"[scraper:phantom.create]Error Processing Data",
		// 					"err":err,
		// 					"code":2
		// 				};
		// 				process.emit('Shopstyx:resError',msg,res);
		// 			}
					
		// 		});
		// 	}else{
		// 		var msg = {
		// 			"success":false, "status":"Error",
		// 			"desc":"[scraper:phantom.create]Error Processing Data",
		// 			"err":err,
		// 			"code":1
		// 		};
		// 		process.emit('Shopstyx:resError',msg,res);
		// 	}
		// });
	// }
	
};
var simple_domain=function(url)
{
	var matches = url.match(/^https?\:\/\/([^\/?#]+)(?:[\/?#]|$)/i);
	if(util.isNullOrUndefined(matches)==true)
	{
		return null;
	}else{
		return matches[1];
	}
};

module.exports={
	scrapeSite:scrapeSite
};