const parseUri = require(GLOBAL.server_location+'/helpers/parseUri');
const txtman = require(GLOBAL.server_location+'/helpers/text_manipulation');
const util = require('util');
const respond = require(GLOBAL.server_location+'/EventListeners/errorListener');

var parsePage=function(err,result,$,res,req,imageFilters,dataSend,cb){
	
	try{
		//console.log("parsing page");
		if(err==null)
		{
			//result.uri;//this is the original URL sent
			//result.body;//this is the read information
			////console.log(result.body);
			//get domain
			try{
				var domain = parseUri.parseUri(result.uri).host;
			}catch(err){
				
				var domain = simple_domain(result.uri);
			}
			
			//console.log("domain: "+domain);
			//remove www from domain
			dataSend.site_name = domain.replace("www.","");
			//get original url
			dataSend.web_url = result.uri;
			//get canonical
			if((dataSend.canonical_url = txtman.getCanonical($,result.body))=='')
			{
				dataSend.canonical_url = "";
			}
			//get keywords
			if((dataSend.meta_keywords = txtman.getKeywords($,result.body))=='')
			{
				dataSend.meta_keywords = "";
			}
			//use domain as the function
			try{
				domain = domain.toLowerCase();

				var testFlag=false;
				var x = 0;
				for(x=0;x<GLOBAL.wodubNames.length;x++)
				{
					var testVar = new RegExp(GLOBAL.wodubNames[x].toString(),"i");
					if(testVar.test(domain)==true)
					{
						testFlag=true;
						break;
					}
				}
				if(testFlag==true)
				{
					//console.log("calling the page scraper");
					var newDomain = GLOBAL.wdubNames[x];
					//console.log("executing "+domain+" through "+newDomain);
					try{						//err,result,$,dataSend,url       ,domain,imageFilters,res,req,cb
						GLOBAL.modules[newDomain](err,result,$,dataSend,result.uri,domain,imageFilters,res,req,cb);
					}catch(err2){
						GLOBAL.modules["generic_scraping"](err,result,$,dataSend,result.uri,domain,imageFilters,res,req,cb);
					}
					
				}else{
					GLOBAL.modules["generic_scraping"](err,result,$,dataSend,result.uri,domain,imageFilters,res,req,cb);
				}

			}catch(error){
				//console.log(error);
				//console.log(util.inspect(error,{showHidden: false, depth: null}));
				var msg = {
					"success":false, "status":"Error",
					"desc":"[scraping_process:parsePage]Error Processing Data",
					"err":error,
					"details":dataSend,
					"code":1
				};
				if(util.isNullOrUndefined(error.stack)==false)
				{
					msg.stack=error.stack;
				}
				respond.respondError(msg,res,req);
				//GLOBAL.modules["generic_scraping"](err,result,$,dataSend,result.uri,domain,imageFilters,res,req);
				// do nothing for now
			}
		}else{
			
			var msg = {
				"success":false, "status":"Error",
				"desc":"[scraping_process:parsePage]Error parsing page",
				"err":err,
				"code":0
			};
			if(util.isNullOrUndefined(error.stack)==false)
				{
					msg.stack=error.stack;
				}
			respond.respondError(msg,res,req);
			/*
			No internet connection
			{
				code: "ENOTFOUND",
				errno: "ENOTFOUND",
				syscall: "getaddrinfo"
			}
			*/
		}
	}catch(overall_error){
		//console.log(overall_error);
		//console.log(util.inspect(overall_error,{showHidden: false, depth: null}));
		var msg = {
			"success":false, "status":"Error",
			"desc":"[scraping_process:parsePage]Possible Too Many Requests",
			"err":overall_error
		};
		respond.respondError(msg,res,req);
	}
};

var simple_domain=function(url)
{
	var matches = url.match(/^https?\:\/\/([^\/?#]+)(?:[\/?#]|$)/i);
	return matches[1];
};

module.exports={
	parsePage:parsePage
};