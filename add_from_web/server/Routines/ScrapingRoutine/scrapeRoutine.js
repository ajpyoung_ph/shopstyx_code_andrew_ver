
const respond = require(GLOBAL.server_location+'/EventListeners/errorListener');
const scraper = require(__dirname+'/scraper');
const parseDataJSON = require(GLOBAL.server_location+'/helpers/parseDataJSON');
const mysql = require(GLOBAL.server_location+'/helpers/mysql_helpers');
const mysqlConv = require(GLOBAL.server_location+'/helpers/mysql_convertions');
const util = require('util');

var processRequest=function(dataJSON,dataStruct,req,res)
{
	//console.log("processRequest 1");
	var url = dataJSON.request.url;
	var query_url = mysqlConv.mysql_real_escape_string(url);
	var query = 'SELECT afw.*, p.*, pseo.* FROM `product_added_from_web_info` afw LEFT JOIN `products` p ON afw.product_id = p.product_id LEFT JOIN `product_seo` pseo ON p.product_id = pseo.product_id WHERE (afw.url_source = "'+query_url+'" OR afw.canonical_url="'+query_url+'") LIMIT 1;';
	
	dataStruct['replyRes']={
		"success":true, "status":"Success",
		"data":[]
	};

	var mysql = new GLOBAL.mysql.mysql_connect(query);
	mysql.results_ss_common().then(function(results){
		if(util.isError(results)==true){
			var msg = {
				"success":false, "status":"Error",
				'desc':'[scrapeRoutine:processRequest]DB Error Connection',
				'query':query,
				'message':results.message,
				'stack':results.stack||null
			};
			respond.respondError(msg,res,req);
		}else{
			if(results.length>0)
			{
				findImages(dataStruct,results,0,req,res);
			}else{
				dataJSON['backup_res']=res;
				scraper.scrapeSite(dataJSON.request.url,req,res,simpleRes);
			}
		}
	});
};


function findImages(dataStruct,rows,x,req,res)
{
	if(x<rows.length)
	{
		if(util.isNullOrUndefined(rows[x])==false)
		{
			var row=rows[x];

			var query = "SELECT * FROM `product_images` WHERE `product_id`="+parseInt(row.product_id)+" ORDER BY product_default_image DESC, product_sort_order ASC;";
			var mysql = new GLOBAL.mysql.mysql_connect(query);
			mysql.results_ss_common().then(function(results){
				if(util.isError(results)==true){
					var msg = {
						"success":false, "status":"Error",
						'desc':'[scrapeRoutine:findImages]DB Error Connection',
						'query':query,
						'message':results.message,
						'stack':results.stack||null
					};
					process.emit('Shopstyx:logError',msg);
					x=x+1;
					findImages(dataStruct,rows,x,req,res);
				}else{
					var mainImage = '';
					var images = [];
					mainImage = (results[0].product_image_url).toString()+(results[0].product_image_filename).toString();
					results.shift();
					var prefixes='';
					replyRes(row,results,rows,dataStruct,x,req,res);
				}
			});
		}else{
			x=x+1;
			findImages(dataStruct,rows,x,req,res);
		}
	}else{
		if(x==rows.length)
		{
			respond.respondError(dataStruct.replyRes,res,req);
		}
	}
}

function replyRes(row,rows,original_rows,dataStruct,x,req,res)
{
	row['product_images']=[];
	row['additional_info']=[];
	row.product_images=rows;
	var additional_info={
		"url_source": row.url_source,
		"canonical_url": row.canonical_url,
		"domain_name": row.domain_name,
		"date_updated": row.date_updated
	};
	delete row.url_source;
	delete row.canonical_url;
	delete row.domain_name;
	delete row.date_updated;
	row.additional_info.push(additional_info);
	if(parseInt(row.department_id)>0)
	{
		var query = "SELECT * FROM `default_departments` WHERE `department_id`="+parseInt(row.department_id)+";";
		var mysql = new GLOBAL.mysql.mysql_connect(query);
		mysql.results_ss_common().then(function(results){
			if(util.isError(results)==true){
				row['department_name']="NONE";
				dataStruct.replyRes['err']=err;
				dataStruct.replyRes.data.push(row);
				x=x+1;
				setImmediate(findImages,dataStruct,original_rows,x,req,res);
			}else{
				row['department_name']=results.department_title;
				dataStruct.replyRes.data.push(row);
				x=x+1;
				setImmediate(findImages,dataStruct,original_rows,x,req,res);
			}
		});
	}else{
		row['department_name']="NONE";
		dataStruct.replyRes.data.push(row);
		x=x+1;
		setImmediate(findImages,dataStruct,original_rows,x,req,res);
	}
}

function simpleRes(dataSend,res,req)
{
	/*
	dataSend.product_name
	dataSend.product_name
	dataSend.price
	dataSend.image
	dataSend.mainDescription
	dataSend.otherPrices
	dataSend.seller_url
	*/
	/*
	"data": {
			"product_id": < int > (
				if product already exists),
			"owner_shop_id": < int > (
				if product already exists),
			"owner_user_id": < int > (user_id of the one saving the data),
			"product_name": "INSPIRE Q Giselle",
                        "product_description":"product description here \nsomething something",
			"product_price": "16681.62",
			"product_images": [{
				"product_default_image": 1,
				"product_image_filename": "INSPIRE-Q-Giselle-Antique-Dark-Bronze-Graceful-Lines-Victorian-Iron-Metal-Bed-86d153ee-735f-4fbf-8251-2a11316fd5cd_600.jpg",
				"product_image_url": "http://ak1.ostkcdn.com/images/products/7720291/"
			}, {
				"product_default_image": 0,
				"product_image_filename": 20150917 _Header - Dropdown - Images - Worldstock.jpg ",
				"product_image_url": "http://ak1.ostkcdn.com/img/mxc/"
			}],
			"additional_info": [{
				"url_source": "",
				"canonical_url": "",
				"domain_name": "",
				"date_updated": ""
			}],
			"meta_keywords": "",
			"length": < float > ,
			"width": < float > ,
			"height": < float > ,
			"lwh_unit": < int > ,
			"weight": < float > ,
			"weight_unit": < int > ,
			'category_id': < int > ,
			'department_id': < int > ,
			'price_unit': < int > , //1-USD (default)
			"product_grouping_id":<int>,
			"color":<string>,
	*/
	var newDataForm = {
		"product_name": dataSend.product_name,
		"product_description":dataSend.mainDescription,
		"product_price": dataSend.price,
		"product_images":[],
		"additional_info":[],
		"meta_keywords":dataSend.meta_keywords,
		"seller_url":''

	};
	var mainImageFound=false;
	if(util.isNullOrUndefined(dataSend.image)==false && dataSend.image!='')
	{
		var tempFile = dataSend.image.split('/');
		var filename = tempFile[tempFile.length-1];
		console.log("MAIN IMAGE ====================");
		console.log('"'+dataSend.image+'"');
		tempFile = dataSend.image.split(filename);
		var url = tempFile[0];
		var product_images ={
			"product_default_image": 1,
			"product_image_filename":filename ,
			"product_image_url": url
		};
		mainImageFound=true;
	}else{
		console.log("no main image found");
	}
	if(util.isNullOrUndefined(dataSend.canonical_url)==true)
	{
		dataSend.canonical_url='';
	}
	if(dataSend.canonical_url=='')
	{
		dataSend.canonical_url=dataSend.web_url;
	}
	if(util.isNullOrUndefined(dataSend.seller_url)==true)
	{
		dataSend.seller_url='';
	}
	newDataForm.seller_url=dataSend.seller_url;
	var additional_info = {
		"url_source": dataSend.web_url,
		"canonical_url": dataSend.canonical_url,
		"domain_name": dataSend.site_name,
		"date_updated": ""
	};
	if(mainImageFound==true)
	{
		newDataForm.product_images.push(JSON.parse(JSON.stringify(product_images)));
	}
	newDataForm.additional_info.push(additional_info);
	//convert images to product images
	for(var x=0;x<dataSend.images.length;x++)
	{
		tempFile = dataSend.images[x].split('/');
		filename = tempFile[tempFile.length-1];
		tempFile = dataSend.images[x].split(filename);
		url = tempFile[0];
		product_images ={
			"product_default_image": (mainImageFound==false && x==0)?1:0,
			"product_image_filename":filename ,
			"product_image_url": url
		};
		// console.log(dataSend.images[x]);
		// console.log(product_images);
		newDataForm.product_images.push(JSON.parse(JSON.stringify(product_images)));
	}
	var msg = {
		"success":true, "status":"Success",
		"desc":"[scrapeRoutine:simpleRes] Scraped Data Success",
		"data":[]
	};
	msg.data.push(newDataForm);
	respond.respondError(msg,res,req);
}

module.exports={
	processRequest:processRequest
};

/*
dataSend structure

{
            "product_id":<int>(if product already exists),
            "owner_shop_id":0,
            "product_name": "",
            "mainDescription": "",
            "price": "",
            "image": "",
            "images": [
                "http://ak1.ostkcdn.com/img/mxc/20150917_Header-Dropdown-Images-Worldstock.jpg",
                "http://ak1.ostkcdn.com/images/products/9116507/P16301475.jpg",
                "http://ak1.ostkcdn.com/images/products/10101947/P17242902.jpg",
                "http://ak1.ostkcdn.com/images/products/7894644/P15275577.jpg",
                "http://ak1.ostkcdn.com/images/products/10395757/P17498599.jpg"
            ],
            "otherTitle": [],
            "otherDescription": [],
            "otherPrices": [
                "036.20",
                "798.14",
                "472.20",
                "608.14",
                "134.20",
                "622.14",
                "798.14",
                "636.12",
                "755.20",
                "550.14"
            ],
            "site_name": "overstock.com",
            "web_url": "",
            "canonical_url": "",
            "meta_keywords": ""
        }
*/
/*
{
    "product_id":<int>(if product already exists),
    "owner_shop_id":<int>(if product already exists),
    "product_name": "INSPIRE Q Giselle Antique Dark Bronze Graceful Lines Victorian Iron Metal Bed | Overstock.com Shopping - The Best Deals on Beds",
    "product_description": "\r\n                        \r\n                        \r\n                            ITEM#: 15123764\r\n                        \r\n                        \r\n\r\n                        \r\n                            \r\n                        \r\n\r\n                        The retro styling of this classic iron bed lets you pay homage to\nthe past as you appreciate its graceful and airy design. Pair this\nantique bronze bed with a mattress and box spring for full support,\nand appreciate the finished look that the matching headboard and\nfootboard bring to your bedroom. The set comes with one headboard, one footboard, three metal\nslats, and one set of rails, giving you everything you need to\ncreate a comfortable sleeping space. Constructed from metal, the\nbed offers the strength and durability needed to support a mattress\nand boxspring. The bed's combination of straight and curved lines\nprovides flowing elegance that complements nearly any style of\nbedroom decor.  Set includes: One (1) headboard, one (1) footboard, one (1)\n  set of rails, three (3) metal slats  Materials: Metal  Finish: Antique dark bronze  Bed designs for the use of box spring         Dimensions:  Overall twin bed: 82.25 inches long x 43.5 inches wide x 51.5\n  inches high  Overall full bed: 82.25 inches long x 58.5 inches wide x\n  51.75 inches high  Overall queen bed: 87.25 inches long x 64.5 inches wide x\n  51.5 inches high  Headboard height: 51.5 inches  Footboard height: 36.49 inches  Leg height: 6.7 inches  Accommodates boxsprings and mattress (NOT included)\r\n                    ",
    "product_price": "PHP 16681.62",
    "product_images": [
        {
	"product_default_image": 1,
	"product_image_filename": "INSPIRE-Q-Giselle-Antique-Dark-Bronze-Graceful-Lines-Victorian-Iron-Metal-Bed-86d153ee-735f-4fbf-8251-2a11316fd5cd_600.jpg",
	"product_image_url": "http://ak1.ostkcdn.com/images/products/7720291/"
},
        {
	"product_default_image": 0,
	"product_image_filename": 20150917_Header-Dropdown-Images-Worldstock.jpg",
	"product_image_url": "http://ak1.ostkcdn.com/img/mxc/"
}
    ],
    "otherTitle": [],
    "otherDescription": [],
    "otherPrices": [
        "036.20",
        "798.14",
        "472.20",
        "608.14",
        "134.20",
        "622.14",
        "798.14",
        "636.12",
        "755.20",
        "550.14"
    ],
"additional_info": [{
	"url_source": "http://www.overstock.com/tl/Home-Garden/INSPIRE-Q-Giselle-Antique-Dark-Bronze-Graceful-Lines-Victorian-Iron-Metal-Bed/7720291/product.html?refccid=HE4F5SBWEAV2ZSCGFJCGAKMX2I&searchidx=0",
	"canonical_url": "http://www.overstock.com/tl/Home-Garden/INSPIRE-Q-Giselle-Antique-Dark-Bronze-Graceful-Lines-Victorian-Iron-Metal-Bed/7720291/product.html",
	"domain_name": "overstock.com",
	"date_updated": "2015-12-14T08:43:24.000Z"
}],
    "meta_keywords": "INSPIRE Q Giselle Antique Dark Bronze Graceful Lines Victorian Iron Metal Bed,INSPIRE Q,11E411B221W(3A)[BED],compare INSPIRE Q Giselle Antique Dark Bronze Graceful Lines Victorian Iron Metal Bed,best price on INSPIRE Q Giselle Antique Dark Bronze Graceful Lines Victorian Iron Metal Bed,discount INSPIRE Q Giselle Antique Dark Bronze Graceful Lines Victorian Iron Metal Bed"
}
*/