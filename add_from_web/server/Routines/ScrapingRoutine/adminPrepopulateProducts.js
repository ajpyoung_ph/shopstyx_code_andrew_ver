
const respond = require(GLOBAL.server_location+'/EventListeners/errorListener');
const scraper = require(__dirname+'/scraper');
const request = require('request');
const parseDataJSON = require(GLOBAL.server_location+'/helpers/parseDataJSON');
const util = require('util');

var processRequest=function(dataStruct,req,res)
{
	var dataJSON = parseDataJSON.parseJSON(dataStruct,req,res);
	if(util.isNullOrUndefined(dataJSON)==false)
	{
		//pull url array from request
		var max = dataJSON.request.urls.length;
		var counter = 0;
		scrapeitems(max,counter,dataJSON.request.urls,req,res);
		var msg = {
			"success":true, "status":"Success",
			"desc":"[adminPrepopulateProducts:processRequest]Processing Data"
		};
		respond.respondError(msg,res,req);

	}else{
		var msg = {
			"success":false, "status":"Error",
			"desc":"[adminPrepopulateProducts:processRequest]No data to process"
		};
		respond.respondError(msg,res,req);
	}
};

function scrapeitems(max_pages,current_page,url_array,res,req)
{
	var url = url_array[current_page];
	scraper.scrapeSite(url,res,req,saveData);
}

function saveData(replyJSON,res)
{
	/*
	INSERT [LOW_PRIORITY | DELAYED | HIGH_PRIORITY] [IGNORE]
    [INTO] tbl_name
    [PARTITION (partition_name,...)]
    SET col_name={expr | DEFAULT}, ...
    [ ON DUPLICATE KEY UPDATE
      col_name=expr
        [, col_name=expr] ... ]
	*/
	var query="INSERT INTO `products` SET `owner_user_id`=1, `owner_shop_id`=0, `category_id`=0, `is_added_from_web`=1";
	//check if product_name is available
	if(typeof(replyJSON.product_name)=='undefined')
	{
		//no product name
		query = query +"`product_name`='unknown scraped product', ";
	}else{
		query = query +"`product_name`='"+replyJSON.product_name"', ";
	}
	if(typeof(replyJSON.mainDescription)=='undefined')
	{
		if(replyJSON.otherDescription.length>0)
		{
			query = query +"`product_description`='"+replyJSON.otherDescription[0]"', ";
		}
	}else{
		query = query +"`product_description`='"+replyJSON.mainDescription"', ";
	}
	var mysql = new GLOBAL.mysql.mysql_connect(query);
	mysql.results_ss_common().then(function(results){
		if(util.isError(results)==true){
			var msg = {
				"success":false, "status":"Error",
				'desc':'[adminPrepopulateProducts:saveData]DB Error Connection',
				'query':query,
				'message':results.message,
				'stack':results.stack||null
			};
			process.emit('Shopstyx:logError',msg);
		}else{
			// result.insertId - inserted Id
			// call Clint's save Image API
			saveImage(replyJSON,results.insertId);
			// update product_seo
			//seo_title=product_name
			//
			saveSEO(replyJSON,results.insertId);
			// update product_added_from_web_info
			saveAddedInfo(replyJSON,results.insertId);
		}
	});
}

function saveImage(replyJSON,product_id)
{
	var url = "URL of Clint";
	var image_url='';
	if(util.isNullOrUndefined(replyJSON.image)==false)
	{
		image_url=replyJSON.image;
	}
	if(image_url==''||image_url==null)
	{
		image_url=replyJSON.images[0];
	}
	if(image_url!='' || image_url!=null)
	{
		request.post({"url":url, "form": {"image_url":image_url,"product_id":parseInt(product_id)}}, function(err,response,body){
			if (error!=null) 
			{
				var msg = {
					"success":false, "status":"Error",
					"desc":"[adminPrepopulateProducts:saveImage]Error in DB",
					"err":err
				};
				process.emit('Shopstyx:logError',msg);
			}
		});
	}else{
		var msg = {
			"success":false, "status":"Error",
			"desc":"[adminPrepopulateProducts:saveImage]image_url is null or empty",
			"replyJSON":replyJSON
		};
		process.emit('Shopstyx:logError',msg);
	}
	
}

function saveSEO(replyJSON,product_id)
{
	var query = "INSERT INTO `product_seo` SET `meta_keywords`='"+replyJSON.meta_keywords+"', product_id="+parseInt(product_id)+";";
	var mysql = new GLOBAL.mysql.mysql_connect(query);
	mysql.results_ss_common().then(function(results){
		if(util.isError(results)==true){
			var msg = {
				"success":false, "status":"Error",
				'desc':'[adminPrepopulateProducts:saveSEO]DB Error Connection',
				'query':query,
				'message':results.message,
				'stack':results.stack||null
			};
			process.emit('Shopstyx:logError',msg);
			updateSEO(replyJSON,product_id);
		}else{
	
		}
	});
}

function updateSEO(replyJSON,product_id)
{
	/*
	UPDATE [LOW_PRIORITY] [IGNORE] table_reference
    SET col_name1={expr1|DEFAULT} [, col_name2={expr2|DEFAULT}] ...
    [WHERE where_condition]
    [ORDER BY ...]
    [LIMIT row_count]
	*/
	var query = "UPDATE `product_seo` SET `meta_keywords`='"+replyJSON.meta_keywords+"' WHERE product_id="+parseInt(product_id)+";";
	var mysql = new GLOBAL.mysql.mysql_connect(query);
	mysql.results_ss_common().then(function(results){
		if(util.isError(results)==true){
			var msg = {
				"success":false, "status":"Error",
				'desc':'[adminPrepopulateProducts:updateSEO]DB Error Connection',
				'query':query,
				'message':results.message,
				'stack':results.stack||null
			};
			process.emit('Shopstyx:logError',msg);
		}else{
	
		}
	});
}

function saveAddedInfo(replyJSON,product_id)
{
	var query = "INSERT INTO `product_added_from_web_info` SET `product_id`="+parseInt(product_id)+", `added_by_user_id`=1, `url_source`='"+replyJSON.web_url+"', `canonical_url`='"+replyJSON.canonical_url+"', `domain_name`='"+replyJSON.site_name+"';";
	var mysql = new GLOBAL.mysql.mysql_connect(query);
	mysql.results_ss_common().then(function(results){
		if(util.isError(results)==true){
			var msg = {
				"success":false, "status":"Error",
				'desc':'[adminPrepopulateProducts:saveAddedInfo]DB Error Connection',
				'query':query,
				'message':results.message,
				'stack':results.stack||null
			};
			process.emit('Shopstyx:logError',msg);
		}else{
	
		}
	});
}

module.exports={
	processRequest:processRequest
};

/*
{
    "success":true, "status": "Success",
    "data": [
        {
            "product_name": "INSPIRE Q Giselle Antique Dark Bronze Graceful Lines Victorian Iron Metal Bed | Overstock.com Shopping - The Best Deals on Beds",
            "mainDescription": "\r\n                        \r\n                        \r\n                            ITEM#: 15123764\r\n                        \r\n                        \r\n\r\n                        \r\n                            \r\n                        \r\n\r\n                        The retro styling of this classic iron bed lets you pay homage to\nthe past as you appreciate its graceful and airy design. Pair this\nantique bronze bed with a mattress and box spring for full support,\nand appreciate the finished look that the matching headboard and\nfootboard bring to your bedroom. The set comes with one headboard, one footboard, three metal\nslats, and one set of rails, giving you everything you need to\ncreate a comfortable sleeping space. Constructed from metal, the\nbed offers the strength and durability needed to support a mattress\nand boxspring. The bed's combination of straight and curved lines\nprovides flowing elegance that complements nearly any style of\nbedroom decor.  Set includes: One (1) headboard, one (1) footboard, one (1)\n  set of rails, three (3) metal slats  Materials: Metal  Finish: Antique dark bronze  Bed designs for the use of box spring         Dimensions:  Overall twin bed: 82.25 inches long x 43.5 inches wide x 51.5\n  inches high  Overall full bed: 82.25 inches long x 58.5 inches wide x\n  51.75 inches high  Overall queen bed: 87.25 inches long x 64.5 inches wide x\n  51.5 inches high  Headboard height: 51.5 inches  Footboard height: 36.49 inches  Leg height: 6.7 inches  Accommodates boxsprings and mattress (NOT included)\r\n                    ",
            "price": "PHP 16681.62",
            "image": "http://ak1.ostkcdn.com/images/products/7720291/INSPIRE-Q-Giselle-Antique-Dark-Bronze-Graceful-Lines-Victorian-Iron-Metal-Bed-86d153ee-735f-4fbf-8251-2a11316fd5cd_600.jpg",
            "images": [
                "http://ak1.ostkcdn.com/img/mxc/20150917_Header-Dropdown-Images-Worldstock.jpg",
                "http://ak1.ostkcdn.com/images/products/9116507/P16301475.jpg",
                "http://ak1.ostkcdn.com/images/products/10101947/P17242902.jpg",
                "http://ak1.ostkcdn.com/images/products/7894644/P15275577.jpg",
                "http://ak1.ostkcdn.com/images/products/10395757/P17498599.jpg"
            ],
            "otherTitle": [],
            "otherDescription": [],
            "otherPrices": [
                "036.20",
                "798.14",
                "472.20",
                "608.14",
                "134.20",
                "622.14",
                "798.14",
                "636.12",
                "755.20",
                "550.14"
            ],
            "site_name": "overstock.com",
            "web_url": "http://www.overstock.com/tl/Home-Garden/INSPIRE-Q-Giselle-Antique-Dark-Bronze-Graceful-Lines-Victorian-Iron-Metal-Bed/7720291/product.html?refccid=HE4F5SBWEAV2ZSCGFJCGAKMX2I&searchidx=0",
            "canonical_url": "http://www.overstock.com/tl/Home-Garden/INSPIRE-Q-Giselle-Antique-Dark-Bronze-Graceful-Lines-Victorian-Iron-Metal-Bed/7720291/product.html",
            "meta_keywords": "INSPIRE Q Giselle Antique Dark Bronze Graceful Lines Victorian Iron Metal Bed,INSPIRE Q,11E411B221W(3A)[BED],compare INSPIRE Q Giselle Antique Dark Bronze Graceful Lines Victorian Iron Metal Bed,best price on INSPIRE Q Giselle Antique Dark Bronze Graceful Lines Victorian Iron Metal Bed,discount INSPIRE Q Giselle Antique Dark Bronze Graceful Lines Victorian Iron Metal Bed"
        }
    ]
}

*/