
const respond = require(GLOBAL.server_location+'/EventListeners/errorListener');
const parseUri = require(GLOBAL.server_location+'/helpers/parseUri');
const mysql_convertions = require(GLOBAL.server_location+'/helpers/mysql_convertions');
const etsyRoutine = require(__dirname+'/domainRoutines/etsyRoutine');
const ebayRoutine = require(__dirname+'/domainRoutines/ebayRoutine');
const util = require('util');

var start = function(dataJSON,dataStruct,images,cb,req,res)
{
		/*if( util.isFunction(res.json)==false)
		{
			//console.log("****************** [addStore:start] res.json NOT A FUNCTION ****************");
		}*/
	//try{
		//check if store is already added
		//if not insert data else do nothing
		var target_url = (dataJSON.request.data.additional_info[0].url_source).toString();
		//console.log("================== TARGET URL ===========================");
		//console.log(target_url);
		var domain = parseUri.parseUri(target_url).host;
		//console.log(domain);

		//data to fill
		var source_site_url = parseUri.parseUri(target_url).protocol+'://'+domain;
		var store_name = '';//*
		var is_external = 1;
		var seo_store_name = '';//*
		var store_name_source = '';//if this was red.amazon.com then the value would be amazon or etsy.com/usr/red then this would be etsy
		var added_by_user_id = 0;//*
		var claimed_by_user_id = 0;
		var social_account = {
			'status':'none'
		};
		var store_status = 1;//0-unpublished;1-published
		var current_store_creation_step = 0;


		//pattern check 
		
		var check=true;
		if(dataJSON.request.data.seller_url!='')
		{
			var pattern = new RegExp('etsy.co','i');//https://www.etsy.com/shop/riricreations
			if(pattern.test(domain)==true)
			{
				//console.log('Parsing ETSY');
				store_name = etsyRoutine.getUserName(dataJSON.request.data.seller_url);
				source_site_url = dataJSON.request.data.seller_url;
				store_name_source = 'etsy';
				check=false;
			}
			pattern = new RegExp('ebay.co','i');//http://www.ebay.com/usr/asicsamerica?_trksid=p2047675.l2559
			if(pattern.test(domain)==true)
			{
				//console.log('Parsing EBAY');
				store_name = ebayRoutine.getUserName(dataJSON.request.data.seller_url);
				source_site_url = ebayRoutine.getSourceSiteURL(dataJSON.request.data.seller_url);
				store_name_source = 'ebay';
				check=false;
			}
		}
		
		if(check==true)
		{
			//continue to check if the URL has a subdomain
			if(parseUri.subDomainExists(domain)==true)
			{
				//extract subdomain
				var holder = domain.split('.');
				store_name=holder[0];
				store_name_source=holder[1];
			}else{
				var holder=domain.split('.');
				if(holder[0].toLowerCase()!='www')
				{
					store_name=holder[0];
				}else{
					store_name=holder[1];
				}
			}
		}
		seo_store_name=store_name.replace(/[^a-z0-9 ]/gmi, "").replace(/\s+/g, "-")
		added_by_user_id=parseInt(dataJSON.request.data.owner_user_id);
		//check if store is already existing

		var query = 'SELECT * FROM `stores` WHERE `store_name` LIKE "'+store_name+'" AND `source_site_url`="'+source_site_url+'" LIMIT 1;'
		var mysql = new GLOBAL.mysql.mysql_connect(query);
		mysql.results_ss_common().then(function(results){
			if(util.isError(results)==true){
				var msg = {
					"success":false, "status":"Error",
					'desc':'[addStore:start]DB Error Connection',
					'query':query,
					'message':results.message,
					'stack':results.stack||null
				};
				respond.respondError(msg,res,req);
			}else{
				if(results.length>0)
				{
					//send id back to callback
					cb(dataJSON,dataStruct,images,results[0].store_id,req,res);
				}else{
					//insert
					var queryInsert = 'INSERT INTO `stores` SET `source_site_url`="'+source_site_url+'", `store_name`="'+store_name+'", `is_external`='+parseInt(is_external)+', `seo_store_name`="'+seo_store_name+'", `store_name_source`="'+store_name_source+'", `added_by_user_id`='+parseInt(added_by_user_id)+', `claimed_by_user_id`='+parseInt(claimed_by_user_id)+', social_account="'+mysql_convertions.mysql_real_escape_string(JSON.stringify(social_account))+'", `store_status`='+parseInt(store_status)+', `current_store_creation_step`='+parseInt(current_store_creation_step)+';';
					var mysql2 = new GLOBAL.mysql.mysql_connect(queryInsert);
					mysql2.results_ss_common().then(function(results2){
						if(util.isError(results2)==true){
							var msg = {
								"success":false, "status":"Error",
								'desc':'[addStore:start]DB Error Connection2',
								'query':query,
								'message':results2.message,
								'stack':results2.stack||null
							};
							respond.respondError(msg,res,req);
						}else{
							cb(dataJSON,dataStruct,images,results2.insertId,req,res);
						}
					});
				}
			}
		});
};

module.exports={
	start:start
};