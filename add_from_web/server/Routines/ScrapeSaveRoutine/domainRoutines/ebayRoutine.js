const parseUri = require(GLOBAL.server_location+'/helpers/parseUri');

var getUserName = function(full_url)
{
	//http://www.ebay.com/usr/asicsamerica?_trksid=p2047675.l2559
	var path = parseUri.parseUri(full_url).path;
	var pathArray = path.split('/');
	//dataJSON.request.type.join("`").toLowerCase().split("`").indexOf("search")>-1
	var targetString = pathArray[pathArray.length-1];
	var targetArray = targetString.split('?');
	return targetArray[0];
}

var getSourceSiteURL = function(full_url)
{
	//http://www.ebay.com/usr/asicsamerica?_trksid=p2047675.l2559
	////console.log(full_url);
	//var path = parseUri.parseUri(full_url).path;
	var pathArray = full_url.split('?');
	return pathArray[0];
}

module.exports={
	getUserName:getUserName,
	getSourceSiteURL:getSourceSiteURL
};