const parseUri = require(GLOBAL.server_location+'/helpers/parseUri');

var getUserName = function(full_url)
{
	//https://www.etsy.com/shop/riricreations
	var path = parseUri.parseUri(full_url).path;
	var pathArray = path.split('/');
	//dataJSON.request.type.join("`").toLowerCase().split("`").indexOf("search")>-1
	return pathArray[pathArray.length-1];
}

// var getSourceSiteURL = function(site_url)
// {

// }

module.exports={
	getUserName:getUserName
};