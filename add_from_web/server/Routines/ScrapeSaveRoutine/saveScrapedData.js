const respond = require(GLOBAL.server_location+'/EventListeners/errorListener');
const request = require('request');
const mkdirp = require('mkdirp');
const uuid = require('node-uuid');
const util = require('util');
if(typeof(process.env.NODE_USER)!='undefined')
{
	const gm = require('gm').subClass({imageMagick: true});
}else{
	const sharp = require('sharp');
}

const saveProductImage = require(__dirname+'/saveProductImageRecord');
const uploadProductImage = require(__dirname+'/uploadProductImage');
const saveAddedFromWeb = require(__dirname+'/saveAddedFromWeb');
const saveSEO = require(__dirname+'/saveSEO');
const addStore = require(__dirname+'/addStore');

const fs = require('fs');
const textMan = require(GLOBAL.server_location+'/helpers/text_manipulation');
const parseUri = require(GLOBAL.server_location+'/helpers/parseUri');
//finding the commonnodeconfig
// var upload_dir_array = (__dirname).toString().split("server");
// var upload_location = upload_dir_array[0]+"uploads";
// delete upload_dir_array;

const mysqlConv = require(GLOBAL.server_location+'/helpers/mysql_convertions');

const gcloud = require('gcloud');

var start = function(dataJSON,dataStruct,req,res)
{
	/*if( util.isFunction(res.json)==false)
	{
		//console.log("****************** [start] res.json NOT A FUNCTION ****************");
	}*/
	/*
	"security": {
		"type": "web/app",
		"token": "JWT token (logged in/non logged in depends on the API call)"
	},
	"dev_identity": {
		"geometry": {
			"type": "Point",
			"coordinates": [
				123.8897752761841, (longitude; I think)
				10.31576371999243(latitude; I think)
			]
		},
		(optional
			if web browser allows; mobile mandatory)
		"ip_address": < string > ,
		"referrer": "http://domain",
		(web only)
	},
	"request": {
		"type": "Save Scraped from Web/Overwrite Scraped from Web",
		(save = new record, overwrite = update)
		"data": {
			"product_id": < int > (
				if product already exists),
			"owner_shop_id": < int > (
				if product already exists),
			"owner_user_id": < int > (user_id of the one saving the data),
			"product_name": "INSPIRE Q Giselle",
                        "product_description":"product description here \nsomething something",
			"product_price": "16681.62",
			"product_images": [{
				"product_default_image": 1,
				"product_image_filename": "INSPIRE-Q-Giselle-Antique-Dark-Bronze-Graceful-Lines-Victorian-Iron-Metal-Bed-86d153ee-735f-4fbf-8251-2a11316fd5cd_600.jpg",
				"product_image_url": "http://ak1.ostkcdn.com/images/products/7720291/"
			}, {
				"product_default_image": 0,
				"product_image_filename": 20150917 _Header - Dropdown - Images - Worldstock.jpg ",
				"product_image_url": "http://ak1.ostkcdn.com/img/mxc/"
			}],
			"additional_info": [{
				"url_source": "",
				"canonical_url": "",
				"domain_name": "",
				"date_updated": ""
			}],
			"meta_keywords": "",
			"length": < float > ,
			"width": < float > ,
			"height": < float > ,
			"lwh_unit": < int > ,
			"weight": < float > ,
			"weight_unit": < int > ,
			'category_id': < int > ,
			'department_id': < int > ,
			'price_unit': < int > , //1-USD (default)
			"product_grouping_id":<int>,
			"color":<string>,
		}
	}
	*/
	
	
	
	var images = [];
	if(util.isNullOrUndefined(dataJSON.request.data)==false)
	{
		//populate list of images
		//console.log(dataJSON.request.data);
		if(typeof(dataJSON.request.data.product_images)=='object' && dataJSON.request.data.product_images.length>0)
		{
			//console.log("dataJSON.request.data.product_images found");
			//console.log(dataJSON.request.data.product_images);
			var images_list = dataJSON.request.data.product_images.slice();
			//console.log('images_list:');
			//console.log(images_list);
			for(var x=0;x<images_list.length;x++)
			{
				if(parseInt(images_list[x].product_default_image)==1)
				{
					if(util.isNullOrUndefined(images_list[x].product_image_url)==false)
					{
						if(images_list[x].product_image_url.trim().toLowerCase()!='none')
						{
							images.push(images_list[x].product_image_url+images_list[x].product_image_filename);
						}else{
							images.push(images_list[x].product_image_filename);
						}
					}
					
					delete images_list[x];
				}
			}
			for(var x=0;x<images_list.length;x++)
			{
				if(util.isNullOrUndefined(images_list[x])==false)
				{
					if(images_list[x]!=null)
					{
						if(images_list[x].product_image_url.trim().toLowerCase()!='none')
    					{
    						images.push(images_list[x].product_image_url+images_list[x].product_image_filename);
    					}else{
    						images.push(images_list[x].product_image_filename);
    					}
					}
				}
			}
		}
	}else{
		//console.log("No Data Found");
	}
	////console.log(images);
	if(typeof(images)=='object' && images.length>0)
	{
		//save the information first in MySQL then scrape the images, resample, and save in Cloud and MySQL
		dataJSON.tempStoreImages = images.slice();
		addStore.start(dataJSON,dataStruct,images,saveInfo,req,res);
		//saveInfo(dataJSON,dataStruct,images);
	}else{
		var msg = {
			"success":false, "status":"Error",
			"desc":"[saveScrapeData:start]No images found"
		};
		respond.respondError(msg,res,req);
	}
	  
};

function saveInfo(dataJSON,dataStruct,images,store_id,req,res)
{
	/*if( util.isFunction(res.json)==false)
	{
		//console.log("****************** [saveInfo] res.json NOT A FUNCTION ****************");
	}*/
	/*
	INSERT [LOW_PRIORITY | DELAYED | HIGH_PRIORITY] [IGNORE]
    [INTO] tbl_name
    [PARTITION (partition_name,...)]
    SET col_name={expr | DEFAULT}, ...
    [ ON DUPLICATE KEY UPDATE
      col_name=expr
        [, col_name=expr] ... ]
	*/

	var owner_shop_id = parseInt(store_id);
	// if(util.isNullOrUndefined(dataJSON.request.data.store_id)==false)
	// {
	// 	if(dataJSON.request.data.store_id>0)
	// 	{
	// 		owner_shop_id=parseInt(dataJSON.request.data.owner_shop_id);
	// 	}
	// }
	// if(util.isNullOrUndefined(store_id)==false && owner_shop_id==0)
	// {
	// 	owner_shop_id=store_id;
	// }
	var category_id=0;
	// if(util.isNullOrUndefined(dataJSON.request.data.category_id)==false)
	// {
	// 	category_id=parseInt(dataJSON.request.data.category_id);
	// }
	if(util.isNullOrUndefined(dataJSON.request.data.category_id)==false)
	{
		category_id=parseInt(dataJSON.request.data.category_id);
	}
	//check price
	//console.log("*********************************************");
	//console.log("Product Price recieved (dataJSON.request.data.product_price): "+dataJSON.request.data.product_price);
	//console.log("*********************************************");
	if(isNaN(dataJSON.request.data.product_price)==true)
	{
		var price=0;
	}else{
		var price=parseFloat(dataJSON.request.data.product_price);
	}

	if(util.isNullOrUndefined(dataJSON.request.data.price_unit)==true)
	{
		var price_unit=1;
	}else{
		var price_unit=parseInt(dataJSON.request.data.price_unit);
	}
	//check for weight
	/*
	"weight": < float > ,
	"weight_unit": < int >
	*/
	
	if(isNaN(dataJSON.request.data.weight)==true)
	{
		var weight=0;
	}else{
		var weight = parseFloat(dataJSON.request.data.weight);
	}
	if(util.isNullOrUndefined(dataJSON.request.data.weight_unit)==true)
	{
		var weight_unit_id=0;
	}else{
		var weight_unit_id=parseInt(dataJSON.request.data.weight_unit);
	}
	//check for dimension
	/*
	"length": < float > ,
	"width": < float > ,
	"height": < float > ,
	"lwh_unit": < int > ,
	*/
	if(isNaN(dataJSON.request.data['length'])==true)
	{
		var length=0;
	}else{
		var length = parseFloat(dataJSON.request.data['length']);
	}
	if(isNaN(dataJSON.request.data.width)==true)
	{
		var width=0;
	}else{
		var width = parseFloat(dataJSON.request.data.width);
	}
	if(isNaN(dataJSON.request.data.height)==true)
	{
		var height=0;
	}else{
		var height = parseFloat(dataJSON.request.data.height);
	}
	if(util.isNullOrUndefined(dataJSON.request.data.lwh_unit)==true)
	{
		var lwh_unit=0;
	}else{
		var lwh_unit=parseInt(dataJSON.request.data.lwh_unit);
	}

	//check for monetary unit
	var insert_me=true;
	if(util.isNullOrUndefined(dataJSON.request.data.product_id)!=true)
	{
		if(parseInt(dataJSON.request.data.product_id)>0)
		{
			var msg = {
				"success":false, "status":"Error",
				"desc":"[saveScrapeData:saveInfo]Error in DB",
				"err":err,
				"query":query
			};
			respond.respondError(msg,res,req);
			insert_me=false;
		}
	}
	var err_msg=[];

	if(util.isNullOrUndefined(dataJSON.request.data.color)==true)
	{
		insert_me=false;
		err_msg.push('No Color Information');
	}
	// if(util.isNullOrUndefined(dataJSON.request.data.product_grouping_id)==true)
	// {
	// 	insert_me=false;
	// 	err_msg.push('No Collection Information');
	// }
	if(util.isNullOrUndefined(dataJSON.request.data.department_id)==true)
	{
		insert_me=false;
		err_msg.push('No Department Information');
	}
	if(util.isNullOrUndefined(dataJSON.request.data.seller_url)==true)
	{
		insert_me=false;
		err_msg.push('No Seller URL Information');
	}
	if(util.isNullOrUndefined(dataJSON.request.data.meta_keywords)==true)
	{
		// insert_me=false;
		// err_msg.push('No Meta Key Words Information');
		dataJSON.request.data.meta_keywords=dataJSON.request.data.product_name;
	}
	if(util.isNullOrUndefined(dataJSON.request.data.additional_info)==true)
	{
		insert_me=false;
		err_msg.push('No Additional Information Found');
	}
	if(util.isNullOrUndefined(dataJSON.request.data.additional_info)==false)
	{
		if(util.isNullOrUndefined(dataJSON.request.data.additional_info[0].url_source)==true)
		{
			insert_me=false;
			err_msg.push('No URL Source');
		}
		if(util.isNullOrUndefined(dataJSON.request.data.additional_info[0].canonical_url)==true)
		{
			insert_me=false;
			err_msg.push('No Canonical URL');
		}
		if(util.isNullOrUndefined(dataJSON.request.data.additional_info[0].domain_name)==true)
		{
			insert_me=false;
			err_msg.push('No Domain Name');
		}
	}
	if(util.isNullOrUndefined(dataJSON.request.data.product_images)==true)
	{
		insert_me=false;
		err_msg.push('No Product Images Found');
	}
	if(util.isNullOrUndefined(dataJSON.request.data.product_price)==true)
	{
		insert_me=false;
		err_msg.push('No Product Images Found');
	}
	if(util.isNullOrUndefined(dataJSON.request.data.product_price)==false)
	{
		if(isNaN(dataJSON.request.data.product_price)==true)
		{
			insert_me=false;
			err_msg.push('Price is not a Number');
		}
	}
	if(util.isNullOrUndefined(dataJSON.request.data.product_description)==true)
	{
		insert_me=false;
		err_msg.push('No Product Description Found');
	}
	if(util.isNullOrUndefined(dataJSON.request.data.product_name)==true)
	{
		insert_me=false;
		err_msg.push('No Product Name Found');
	}
	if(util.isNullOrUndefined(dataJSON.request.data.owner_user_id)==true)
	{
		insert_me=false;
		err_msg.push('No Owner ID Found');
	}
	if(insert_me==true)
	{
		var query = 'INSERT INTO `products` SET `owner_user_id`='+parseInt(dataJSON.request.data.owner_user_id)+', `owner_shop_id`='+parseInt(owner_shop_id)+', `category_id`='+category_id+', `department_id`='+parseInt(dataJSON.request.data.department_id)+', `product_name`="'+mysqlConv.mysql_real_escape_string(dataJSON.request.data.product_name)+'", `product_description`="'+mysqlConv.mysql_real_escape_string(dataJSON.request.data.product_description)+'", `product_price`='+price+', `monetary_unit_id`='+price_unit+',`length`='+length+',`width`='+width+',`height`='+height+',`lwh_unit_id`='+lwh_unit+',`weight`='+weight+',`weight_unit_id`='+weight_unit_id+',`is_added_from_web`=1, `product_color`="'+dataJSON.request.data.color+'"';
		if(util.isNullOrUndefined(dataJSON.request.data.product_grouping_id)==false)
		{
			query=query+', `product_grouping_id`='+parseInt(dataJSON.request.data.product_grouping_id);
		}
		query=query+';';
		
		//console.log('==========================================================');
		//console.log(query);
		//console.log('==========================================================');
		var mysql = new GLOBAL.mysql.mysql_connect(query);
		mysql.results_ss_common().then(function(results){
			if(util.isError(results)==true){
				var msg = {
					"success":false, "status":"Error",
					'desc':'[saveScrapeData:saveInfo]DB Error Connection',
					'query':query,
					'message':results.message,
					'stack':results.stack||null
				};
				respond.respondError(msg,res,req);
			}else{
				var resultDoc=results;
				dataJSON.tempStoreImagesCounter =0;
				var files = req.files;
				if(util.isNullOrUndefined(files)==true)
				{
					replySuccess(dataJSON,dataStruct,parseInt(resultDoc.insertId),owner_shop_id,req,res);
				}
				saveSEO.start(dataJSON,parseInt(resultDoc.insertId));
				saveAddedFromWeb.start(dataJSON,parseInt(resultDoc.insertId));
				//                 (dataJSON,dataStruct,product_id                  ,owner_shop_id,req,res)
				identify_image_type(dataJSON,dataStruct,parseInt(resultDoc.insertId),owner_shop_id,req,res);
			}
		});
	}else{
		if(err_msg.length>0)
		{
			var msg = {
				"success":false, "status":"Error",
				"desc":"[saveScrapeData:saveInfo]Error in Request",
				"err":err_msg
			};
			respond.respondError(msg,res,req);
		}
		
	}
}

function replySuccess(dataJSON,dataStruct,product_id,owner_shop_id,req,res)
{
	var replyJSON = JSON.parse(JSON.stringify(dataJSON.request.data));
	replyJSON.owner_shop_id=parseInt(owner_shop_id);
	replyJSON.product_id=parseInt(product_id);
	var msg = {
		"success":true, "status":"Success",
		"desc":"[saveScrapeData:replySuccess]Saved Data",
		"data":[]
	};
	msg.data.push(replyJSON);
	respond.respondError(msg,res,req);
}

var identify_image_type = function(dataJSON,dataStruct,product_id,owner_shop_id,req,res)
{
	/*if( util.isFunction(res.json)==false)
	{
		//console.log("****************** [identify_image_type:"+dataJSON.tempStoreImagesCounter+"] res.json NOT A FUNCTION ****************");
	}*/
	var images = dataJSON.tempStoreImages;
	var x = dataJSON.tempStoreImagesCounter;
	if(util.isNullOrUndefined(images[x])==false)
	{
		/*var images_array = images[x].split('/');
		if(images_array.length>1)
		{
			scrape_image(dataJSON,dataStruct,product_id,owner_shop_id,req,res);
		}else{
			read_image(dataJSON,dataStruct,product_id,owner_shop_id,req,res);
		}*/
		if(parseUri.isUrl(images[x])==true)
		{
		  //                         (dataJSON,dataStruct,product_id,owner_shop_id,req,res)
			setImmediate(scrape_image,dataJSON,dataStruct,product_id,owner_shop_id,req,res);
		}else{
		  //                       (dataJSON,dataStruct,product_id,owner_shop_id,req,res)
			setImmediate(read_image,dataJSON,dataStruct,product_id,owner_shop_id,req,res);
		}
	}else{
		//do nothing since there are no more images to be found
	}
	
}
var scrape_image=function(dataJSON,dataStruct,product_id,owner_shop_id,req,res)
{
	/*
		headers:
	   { server: 'ATS/5.3.0',
	     'last-modified': 'Thu, 17 Sep 2015 21:58:36 GMT',
	     etag: '"1b81c-51ff88453fb00"',
	     'accept-ranges': 'bytes',
	     'content-length': '112668',
	     'cache-control': 'max-age=604800, public',
	     'content-type': 'image/jpeg',
	     date: 'Tue, 08 Dec 2015 07:04:28 GMT',
	     connection: 'close' }
	*/
	/*
	if( util.isFunction(res.json)==false)
	{
		//console.log("****************** [scrape_image:start of function] res.json NOT A FUNCTION ****************");
	}*/
	var images = dataJSON.tempStoreImages;
	var x = dataJSON.tempStoreImagesCounter;
	//console.log('IMAGES =============');
	//console.log(dataJSON.tempStoreImages);
	//console.log('REQUESTING IMAGE '+images[x]);
	request.head(images[x], function(err, resRet, body){
		if(err==null)
		{
			if(resRet.statusCode==200)
			{
				dataJSON.contentType=resRet.headers['content-type'];
				var ext = resRet.headers['content-type'].split('/');
				ext = ext[1];
				var filename = uuid.v1()+'.'+ext;
				//scrape data
				var x = dataJSON.tempStoreImagesCounter;
				
				//old code
				request(images[x]).pipe(fs.createWriteStream(GLOBAL.temp_dir_location+'/'+filename)).on('close', function(){
					//saving of file is done
					//resample file here
					var targetWidth=GLOBAL.product_sizes;
					var imageInfo = {
						'product_id':parseInt(product_id),
						'product_default_image':0,
						'product_image_filename':'',
						'product_image_url':'',
						'product_sort_order':0,
						'product_prefixes':targetWidth.join(',')
					};
					
					x= dataJSON.tempStoreImagesCounter;
					if(x==0)
					{
						imageInfo.product_default_image=1;
					}

					imageInfo.product_image_filename=filename;
					imageInfo.product_image_url=GLOBAL.storage_url+GLOBAL.storage_names.products+'/images/';
					imageInfo.product_sort_order=dataJSON.tempStoreImagesCounter+1;
					
					//setTimeout(uploadProductImage.start, 7000,GLOBAL.temp_dir_location,targetValue+'_'+filename);
					saveProductImage.start(imageInfo);
					
					
					for(var tIndex=0;tIndex<targetWidth.length;tIndex++)
					{
						var targetValue = targetWidth[tIndex];
						/*if( util.isFunction(res.json)==false)
						{
							//console.log("****************** [scrape_image:for tIndex Loop] res.json NOT A FUNCTION ****************");
						}*/
					  //resampleImage(imageInfo,targetValue,upload_location         ,filename,dataJSON,dataStruct,images,product_id,x,owner_shop_id,req,res)
						setImmediate(resampleImage,imageInfo,targetValue,GLOBAL.temp_dir_location,filename,dataJSON,dataStruct,images,product_id,x,owner_shop_id,req,res);
					}
				});
				//end old code			
			}else{
				//skip scrape 
			}
		}else{
			//console.log('Error in request');
			//console.log(err);
		}
		
	});
};

var read_image = function(dataJSON,dataStruct,product_id,owner_shop_id,req,res)
{
	var images = dataJSON.tempStoreImages;
	var x = dataJSON.tempStoreImagesCounter;
	//console.log('IMAGES =============');
	//console.log(dataJSON.tempStoreImages);
	//console.log('READING UPLOADED IMAGE '+images[x]);
	var files = req.files;
	var filename = '';
	for(var y = 0;y<files.src.length;y++)
	{
		if(util.isNullOrUndefined(files.src[y]['already_used'])==true)
		{
			var testPattern = new RegExp(files.src[y].originalname,'gi');
			if(testPattern.test(images[x])==true)
			{
				var ext = files.src[y].mimetype.split('/');
				ext = ext[1];
				filename = uuid.v1()+'.'+ext;
				fs.renameSync(files.src[y].path, files.src[y].destination+'/'+filename);
				
				files.src[y]['already_used']=true;
				break;
			}
		}		
	}

	if(filename!='')
	{
		var targetWidth=GLOBAL.product_sizes;
		var imageInfo = {
			'product_id':parseInt(product_id),
			'product_default_image':0,
			'product_image_filename':'',
			'product_image_url':'',
			'product_sort_order':0,
			'product_prefixes':targetWidth.join(',')
		};
		
		x= dataJSON.tempStoreImagesCounter;
		if(x==0)
		{
			imageInfo.product_default_image=1;
		}

		imageInfo.product_image_filename=filename;
		imageInfo.product_image_url=GLOBAL.storage_url+GLOBAL.storage_names.products+'/images/';
		imageInfo.product_sort_order=dataJSON.tempStoreImagesCounter+1;
		
		//setTimeout(uploadProductImage.start, 7000,GLOBAL.temp_dir_location,targetValue+'_'+filename);
		saveProductImage.start(imageInfo);
		for(var tIndex=0;tIndex<targetWidth.length;tIndex++)
		{
			var targetValue = targetWidth[tIndex];
			/*if( util.isFunction(res.json)==false)
			{
				//console.log("****************** [readImage:for tIndex Loop] res.json NOT A FUNCTION ****************");
			}*/
		  //resampleImage(imageInfo,targetValue,upload_location         ,filename,dataJSON,dataStruct,images,product_id,x,owner_shop_id,req,res)
			setImmediate(resampleImage,imageInfo,targetValue,GLOBAL.temp_dir_location,filename,dataJSON,dataStruct,images,product_id,x,owner_shop_id,req,res);
		}
	}else{
		dataJSON.tempStoreImagesCounter=dataJSON.tempStoreImagesCounter+1;
		//                 (dataJSON,dataStruct,product_id,owner_shop_id,req,res)
		identify_image_type(dataJSON,dataStruct,product_id,owner_shop_id,req,res);
	}
	
}

function resampleImage(imageInfo,targetValue,upload_location,filename,dataJSON,dataStruct,images,product_id,x,owner_shop_id,req,res)
{
	if(typeof(process.env.NODE_USER)!='undefined')
	{
		gm(GLOBAL.temp_dir_location+'/'+filename)
		.resize(targetValue)
		.write(GLOBAL.temp_dir_location+'/'+targetValue+'_'+filename,function(err){
			if(err==null)
			{
				fs.access(GLOBAL.temp_dir_location+'/'+targetValue+'_'+filename, fs.R_OK | fs.W_OK, function (err) {
					if(err==null)
					{
						uploadFile(imageInfo,targetValue,upload_location,filename,dataJSON,dataStruct,images,product_id,x,owner_shop_id,req,res);
					}else{
						//console.log('no access to '+GLOBAL.temp_dir_location+'/'+targetValue+'_'+filename);
					}
				});
				
			}else{
				var msg = {
					"success":false, "status":"Error",
					"desc":"[saveSrapeData:resampleImage]Error Writing",
					"err":err
				};
				process.emit('Shopstyx:logError',msg);
				checkDelete(targetValue,filename,GLOBAL.temp_dir_location,dataJSON,dataStruct,product_id,owner_shop_id,req,res);
				//deleteSourceFile(GLOBAL.temp_dir_location+'/'+targetValue+'_'+filename);
			}
		});
	}else{
		sharp(GLOBAL.temp_dir_location+'/'+filename)
		.resize(targetValue)
		.toFile(GLOBAL.temp_dir_location+'/'+targetValue+'_'+filename, function(err) {
			// output.jpg is a 300 pixels wide and 200 pixels high image
			// containing a scaled and cropped version of input.jpg
			fs.access(GLOBAL.temp_dir_location+'/'+targetValue+'_'+filename, fs.R_OK | fs.W_OK, function (err) {
				if(err==null)
				{
					uploadFile(imageInfo,targetValue,upload_location,filename,dataJSON,dataStruct,images,product_id,x,owner_shop_id,req,res);
				}else{
					//console.log('no access to '+GLOBAL.temp_dir_location+'/'+targetValue+'_'+filename);
				}
			});
		});
	}
	
	// fs.access(GLOBAL.temp_dir_location+'/'+filename,fs.F_OK,function(err){
	// 	if(err==null)
	// 	{
			// gm(GLOBAL.temp_dir_location+'/'+filename)
			// .resize(targetValue)
			// .write(GLOBAL.temp_dir_location+'/'+targetValue+'_'+filename,function(err){
			// 	if(err==null)
			// 	{
			// 		//upload saved data
			// 		// var gcs = GLOBAL.gcloud.storage();
			// 		// var bucket = gcs.bucket(GLOBAL.storage_names.products);
			// 		// var file = bucket.file('images/'+targetValue+'_'+filename);
			// 		// fs.createReadStream(GLOBAL.temp_dir_location+'/'+targetValue+'_'+filename)
			// 		// 	.pipe(file.createWriteStream())
			// 		// 	.on('error', function(err) {
			// 		// 		var msg = {
			// 		// 			"success":false, "status":"Error",
			// 		// 			"desc":"[saveSrapeData:resampleImage]Error in Upload",
			// 		// 			"err":err
			// 		// 		};
			// 		// 		process.emit('Shopstyx:logError',msg);
			// 		// 		checkDelete(targetValue,filename,GLOBAL.temp_dir_location,dataJSON,dataStruct,product_id,owner_shop_id,req,res);
			// 		// 		//deleteSourceFile(GLOBAL.temp_dir_location+'/'+targetValue+'_'+filename);
			// 		// 	})
			// 		// 	.on('finish', function() {
			// 		// 		// The file upload is complete.
			// 		// 		// The file upload is complete.
			// 		// 		//console.log("Successfully uploaded "+targetValue+'_'+filename);
			// 		// 		checkDelete(targetValue,filename,GLOBAL.temp_dir_location,dataJSON,dataStruct,product_id,owner_shop_id,req,res);
			// 		// 		//deleteSourceFile(GLOBAL.temp_dir_location+'/'+targetValue+'_'+filename);
			// 		// 	});
			// 		fs.access(GLOBAL.temp_dir_location+'/'+targetValue+'_'+filename, fs.R_OK | fs.W_OK, function (err) {
			// 			if(err==null)
			// 			{
			// 				uploadFile(imageInfo,targetValue,upload_location,filename,dataJSON,dataStruct,images,product_id,x,owner_shop_id,req,res);
			// 			}else{
			// 				//console.log('no access to '+GLOBAL.temp_dir_location+'/'+targetValue+'_'+filename);
			// 			}
			// 		});
					
			// 	}else{
			// 		var msg = {
			// 			"success":false, "status":"Error",
			// 			"desc":"[saveSrapeData:resampleImage]Error Writing",
			// 			"err":err
			// 		};
			// 		process.emit('Shopstyx:logError',msg);
			// 		checkDelete(targetValue,filename,GLOBAL.temp_dir_location,dataJSON,dataStruct,product_id,owner_shop_id,req,res);
			// 		//deleteSourceFile(GLOBAL.temp_dir_location+'/'+targetValue+'_'+filename);
			// 	}
			// });


			// .stream(function (err, stdout, stderr) {
			// 	//var writeStream = fs.createWriteStream('/path/to/my/reformatted.png');
			// 	//stdout.pipe(writeStream);
			// 	var gcs = GLOBAL.gcloud.storage();
			// 	var bucket = gcs.bucket(GLOBAL.storage_names.products);
			// 	var file = bucket.file('images/'+targetValue+'_'+filename);
			// 	stdout.pipe(file.createWriteStream())
			// 		.on('error', function(err) {
			// 			var msg = {
			// 				"success":false, "status":"Error",
			// 				"desc":"[saveSrapeData:resampleImage]Error in Upload",
			// 				"err":err
			// 			};
			// 			process.emit('Shopstyx:logError',msg);
			// 			/*if( util.isFunction(res.json)==false)
			// 			{
			// 				//console.log("****************** [resampleImage:error] res.json NOT A FUNCTION ****************");
			// 			}*/
			// 		  //checkDelete(targetValue,filename,upload_location         ,dataJSON,dataStruct,product_id,owner_shop_id,req,res)
			// 			setImmediate(checkDelete,targetValue,filename,GLOBAL.temp_dir_location,dataJSON,dataStruct,product_id,owner_shop_id,req,res);
			// 		})
			// 		.on('finish', function() {
			// 			// The file upload is complete.
			// 			//console.log("Successfully uploaded "+targetValue+'_'+filename);
			// 			/*if( util.isFunction(res.json)==false)
			// 			{
			// 				//console.log("****************** [resampleImage:finish] res.json NOT A FUNCTION ****************");
			// 			}*/
			// 		  //checkDelete(targetValue,filename,upload_location         ,dataJSON,dataStruct,product_id,owner_shop_id,req,res)
			// 			setImmediate(checkDelete,targetValue,filename,GLOBAL.temp_dir_location,dataJSON,dataStruct,product_id,owner_shop_id,req,res);
			// 		});
				
			// });
	// 	}else{
	// 		//console.log("FILE NOT FOUND FOR RESAMPLING: "+GLOBAL.temp_dir_location+'/'+filename);
	// 	}
	// });
	
}
function uploadFile(imageInfo,targetValue,upload_location,filename,dataJSON,dataStruct,images,product_id,x,owner_shop_id,req,res)
{
	//upload saved data
	var gcs = GLOBAL.gcloud.storage();
	var bucket = gcs.bucket(GLOBAL.storage_names.products);
	var file = bucket.file('images/'+targetValue+'_'+filename);//dataJSON.contentType
	fs.createReadStream(GLOBAL.temp_dir_location+'/'+targetValue+'_'+filename)
		.pipe(file.createWriteStream({
		    metadata: {
		      contentType: dataJSON.contentType
		    }
		  }))
		.on('error', function(err) {
			var msg = {
				"success":false, "status":"Error",
				"desc":"[saveSrapeData:resampleImage]Error in Upload",
				"err":err
			};
			process.emit('Shopstyx:logError',msg);
			checkDelete(targetValue,filename,GLOBAL.temp_dir_location,dataJSON,dataStruct,product_id,owner_shop_id,req,res);
			//deleteSourceFile(GLOBAL.temp_dir_location+'/'+targetValue+'_'+filename);
		})
		.on('finish', function() {
			// The file upload is complete.
			// The file upload is complete.
			//console.log("Successfully uploaded "+targetValue+'_'+filename);
			checkDelete(targetValue,filename,GLOBAL.temp_dir_location,dataJSON,dataStruct,product_id,owner_shop_id,req,res);
			//deleteSourceFile(GLOBAL.temp_dir_location+'/'+targetValue+'_'+filename);
		});
}
function checkDelete(targetValue,filename,upload_location,dataJSON,dataStruct,product_id,owner_shop_id,req,res)
{
	/*if( util.isFunction(res.json)==false)
	{
		//console.log("****************** [checkDelete] res.json NOT A FUNCTION ****************");
	}*/
	if(targetValue==GLOBAL.product_sizes[GLOBAL.product_sizes.length-1])//based on the last value of targetWidth array
	{
		//console.log('Deleting file:'+filename);
		//setImmediate(deleteSourceFile,GLOBAL.temp_dir_location+'/'+filename);
		setTimeout(deleteSourceFile,60000,GLOBAL.temp_dir_location+'/'+filename);
		for(var xx=0;xx<GLOBAL.product_sizes.length;xx++)
		{
			var iteratedValue = GLOBAL.product_sizes[xx];
			var default_delay = 60000;
			if(iteratedValue==GLOBAL.product_sizes[GLOBAL.product_sizes.length-1])
			{
				default_delay = 120000;
			}
			//setImmediate(deleteSourceFile,GLOBAL.temp_dir_location+'/'+iteratedValue+'_'+filename);
			setTimeout(deleteSourceFile,default_delay,GLOBAL.temp_dir_location+'/'+iteratedValue+'_'+filename);
		}
		if(/*dataJSON.tempStoreImagesCounter==0*/ util.isNullOrUndefined(dataJSON['finished_response'])==true && util.isNullOrUndefined(req.files)==false)
		{
			dataJSON['finished_response']=true;
			var replyJSON = JSON.parse(JSON.stringify(dataJSON.request.data));
			replyJSON.owner_shop_id=parseInt(owner_shop_id);
			replyJSON.product_id=parseInt(product_id);
			var files = {
				"product_default_image": 1,
				"product_image_filename": filename,
				"product_image_url": GLOBAL.storage_url+GLOBAL.storage_names.products+'/images/'
			};
			replyJSON.product_images=[files];
			var msg = {
				"success":true, "status":"Success",
				"desc":"[saveScrapeData:checkDelete]Saved Data",
				"data":[]
			};

			msg.data.push(replyJSON);
			respond.respondError(msg,res,req);
		}
		dataJSON.tempStoreImagesCounter=dataJSON.tempStoreImagesCounter+1;
		//                 (dataJSON,dataStruct,product_id,owner_shop_id,req,res)
		setImmediate(identify_image_type,dataJSON,dataStruct,product_id,owner_shop_id,req,res);
	}
}
function deleteSourceFile(file)
{
	try
	{
		var fs=require('fs');
		fs.access(file,fs.F_OK,function(err){
			if(err==null)
			{
				//console.log('file found '+file);
				fs.access(file, fs.R_OK | fs.W_OK, function (err) {
					if(err==null)
					{
						//console.log('can delete file'+file);
						//setTimeout(fs.unlinkSync,10000,GLOBAL.temp_dir_location+'/'+filename);
						fs.unlink(file, function (err) {
							if(err!=null){
								//console.log('Error Deleting '+file);
							}else{
								//console.log('successfully deleted '+file);
							}
						});
					}else{
						//console.log('no access to '+file);
					}
				});
			}else{
				//console.log('not found file: '+file);
			}
		});
	}catch(err){
		//console.log("FS NO LONGER VIABLE");
	}
	
}

module.exports={
	start:start,
	scrape_image:scrape_image
};