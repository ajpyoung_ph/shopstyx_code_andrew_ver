const util = require('util');
const mysqlConv = require(GLOBAL.server_location+'/helpers/mysql_convertions');

var start = function(dataJSON,product_id)
{
	var query = 'INSERT INTO `product_seo` SET `product_id`='+parseInt(product_id)+', `seo_title`="'+mysqlConv.mysql_real_escape_string(dataJSON.request.data.product_name)+'", `seo_description`="'+mysqlConv.mysql_real_escape_string(dataJSON.request.data.product_description)+'", `seo_url`="'+mysqlConv.mysql_real_escape_string(dataJSON.request.data.product_name.replace(/[^a-z0-9 ]/gmi, "").replace(/\s+/g, "-"))+'", `meta_keywords`="'+mysqlConv.mysql_real_escape_string(dataJSON.request.data.meta_keywords)+'";';
	// //console.log('==========================================================');
	// //console.log(query);
	// //console.log('==========================================================');
	var mysql = new GLOBAL.mysql.mysql_connect(query);
	mysql.results_ss_common().then(function(results){
		if(util.isError(results)==true){
			var msg = {
				"success":false, "status":"Error",
				'desc':'[saveSEO:start]DB Error Connection',
				'query':query,
				'message':results.message,
				'stack':results.stack||null
			};
			process.emit('Shopstyx:logError',msg);
		}else{
			//resultDoc.insertId - product_id
			//no response
		}
	});
};

module.exports={
	start:start
};