const fs = require('fs');
const gcloud = require('gcloud');

var start = function(upload_location,filename)
{
	//push to cloud storage
	//console.log('saving '+upload_location+'/'+filename+' to '+GLOBAL.storage_names.products);
	// var storage = GLOBAL.gcloud.storage();
	// var bucket = storage.bucket(GLOBAL.storage_names.products);
	var gcs = GLOBAL.gcloud.storage();
	var bucket = gcs.bucket(GLOBAL.storage_names.products);
	var options = {
	  destination: 'images/'+filename,
	  resumable: true
	};
	fs.access(upload_location+'/'+filename,fs.F_OK,function(err){
		if(err==null)
		{
			bucket.upload(upload_location+'/'+filename, options, function(err, file){
				if(err!=null)
				{
					
					var msg = {
						"success":false, "status":"Error",
						"desc":"[uploadProductImage:start]Error in Uploading Image",
						"err":err
					};
					process.emit('Shopstyx:logError',msg);
					/*
					{ errors: [ { domain: 'global', reason: 'forbidden', message: 'Forbidden' } ],
				     code: 403,
				     message: 'Forbidden' } }
					*/
				}else{
					file.makePublic(function(err, apiResponse) {});
					//console.log("uploaded file "+filename);
					fs.access(upload_location+'/'+filename,fs.F_OK,function(err){
						if(err==null)
						{
							//console.log('file found '+upload_location+'/'+filename);
							fs.access(upload_location+'/'+filename, fs.R_OK | fs.W_OK, function (err) {
								if(err==null)
								{
									setTimeout(deleteFile,120000,upload_location+'/'+filename);
								}else{
									//console.log('no access to '+upload_location+'/'+filename);
								}
							});
						}else{
							//console.log('not found file: '+file);
						}
					});
				}
			});
		}else{
			//console.log('no file found!');
			//console.log('waiting for 4 seconds');
			setTimeout(start, 3000,upload_location,filename);
		}
	});
};

function deleteFile(file)
{
	//console.log('can delete file'+file);
	//setTimeout(fs.unlinkSync,10000,upload_location+'/'+filename);
	fs.unlink(file, function (err) {
		if(err!=null){
			//console.log('Error Deleting '+file);
		}else{
			//console.log('successfully deleted '+file);
		}
	});
}
module.exports={
	start:start
};