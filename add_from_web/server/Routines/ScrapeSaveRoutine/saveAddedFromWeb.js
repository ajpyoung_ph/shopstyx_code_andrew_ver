const util = require('util');
const mysqlConv = require(GLOBAL.server_location+'/helpers/mysql_convertions');

var start = function(dataJSON,product_id)
{
	var query = 'INSERT INTO `product_added_from_web_info` SET `product_id`='+parseInt(product_id)+', `added_by_user_id`='+parseInt(dataJSON.request.data.owner_user_id)+', `url_source`="'+mysqlConv.mysql_real_escape_string(dataJSON.request.data.additional_info[0].url_source)+'", `canonical_url`="'+mysqlConv.mysql_real_escape_string(dataJSON.request.data.additional_info[0].canonical_url)+'", `domain_name`="'+mysqlConv.mysql_real_escape_string(dataJSON.request.data.additional_info[0].domain_name)+'";';
	var mysql = new GLOBAL.mysql.mysql_connect(query);
	mysql.results_ss_common().then(function(results){
		if(util.isError(results)==true){
			var msg = {
				"success":false, "status":"Error",
				'desc':'[saveAddedFromWeb:start]DB Error Connection',
				'query':query,
				'message':results.message,
				'stack':results.stack||null
			};
			process.emit('Shopstyx:logError',msg);
		}else{
			//results.insertId - product_id
			//no response
		}
	});
};

module.exports={
	start:start
};