
const respond = require(GLOBAL.server_location+'/EventListeners/errorListener');
const mysqlConv = require(GLOBAL.server_location+'/helpers/mysql_convertions');
const saveScrapedData = require(__dirname+'/saveScrapedData');
const parseDataJSON = require(GLOBAL.server_location+'/helpers/parseDataJSON');
const util = require('util');


var processRequest=function(dataJSON,dataStruct,req,res)
{
	var error=false;
	var error_msg=[];

	if(util.isNullOrUndefined(dataJSON.request)==false)
	{
		if(util.isNullOrUndefined(dataJSON.request.type)==true)
		{
			error=true;
			error_msg.push('No Request Type Found');
		}
	}
	if(util.isNullOrUndefined(dataJSON.request)==true)
	{
		error=true;
		error_msg.push('No Request Found');
	}
	if(error==false)
	{
		//console.log(dataJSON.request.type.toLowerCase().replace(/\s/g, ''));
		switch(dataJSON.request.type.toLowerCase().replace(/\s/g, ''))
		{
			case 'savescrapedfromweb':
				//console.log('saving information');
				setImmediate(saveScrapedData.start,dataJSON,dataStruct,req,res);
				break;
			// case 'overwrite scraped from web':
			// 	break;
			default:
				var msg = {
					"success":false, "status":"Error",
					"desc":"[saveRoutine:processRequest]Invalid Request"
				};
				respond.respondError(msg,res,req);
				break;
		}
	}else{
		var msg = {
			"success":false, "status":"Error",
			"desc":"[saveRoutine:processRequest]No Request Found",
			"err":error_msg.join(',')
		};
		respond.respondError(msg,res,req);
	}
};

module.exports={
	processRequest:processRequest
};