const util = require('util');
const mysqlConv = require(GLOBAL.server_location+'/helpers/mysql_convertions');
const encdec = require(GLOBAL.server_location+'/helpers/encodeDecode');

var start = function(imageInfo)
{
	/*
	imageInfo = {
		'product_id':parseInt(product_id),
		'product_default_image':0,
		'product_image_filename':'',
		'product_image_url':GLOBAL.storage_url+GLOBAL.storage_names.products+'/images/',
		'product_sort_order':0
	};
	*/
	//console.log(imageInfo);
	var query = 'INSERT INTO `product_images` SET `product_id`='+imageInfo.product_id+', `product_default_image`='+imageInfo.product_default_image+', `product_image_filename`="'+mysqlConv.mysql_real_escape_string(imageInfo.product_image_filename)+'", `product_image_url`="'+mysqlConv.mysql_real_escape_string(imageInfo.product_image_url)+'", `product_sort_order`='+imageInfo.product_sort_order+',`product_prefixes`="'+imageInfo.product_prefixes+'";';
	var mysql = new GLOBAL.mysql.mysql_connect(query);
	mysql.results_ss_common().then(function(results){
		if(util.isError(results)==true){
			var msg = {
				"success":false, "status":"Error",
				'desc':'[saveProductImageRecord:start]DB Error Connection',
				'query':query,
				'message':results.message,
				'stack':results.stack||null
			};
			process.emit('Shopstyx:logError',msg);
		}else{
			//results.insertId - product_id
			//no response
		}
	});
};

module.exports={
	start:start
};