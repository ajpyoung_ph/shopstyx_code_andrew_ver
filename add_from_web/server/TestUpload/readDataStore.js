const util = require('util');

const kind = 'TestTable';

var start = function()
{
	list(1,null);
};

function list(limit, token) {
	var datastore = GLOBAL.gcloud.datastore;
	var ds = datastore.dataset();
	console.log("limit: "+limit);
	console.log("token: "+token);
	console.log("kind: "+kind);
	var q = ds.createQuery([kind])
	  .limit(limit)
	  //.order('title')
	  .start(token);

	ds.runQuery(q, function(err, entities, cursor) {
		if(err)
		{
			console.log("Error in query");
			console.log(util.inspect(err,{showHidden: false, depth: null}));
		}else{
			console.log("listing data");
			console.log("length:"+entities.length);
			console.log("util.entities:");
			console.log(util.inspect(entities,{showHidden: false, depth: null}));
			//console.log("entities.map")
			console.log("entities");
			console.log(entities);
			//var data = parseObj(entities);//entities.map(fromDatastore);
			//console.log(util.inspect(data,{showHidden: false, depth: null}));
		}
		//cb(null, entities.map(fromDatastore), entities.length === limit ? cursor : false);
	});
}

module.exports={
	start:start
};