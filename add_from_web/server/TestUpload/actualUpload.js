const fs = require('fs');
const util = require('util');
var start = function()
{
	var filename = '960_b1e00e50-9f4c-11e5-b32a-d7f40049b2dd.jpeg';
	console.log('setting gcs');
	var gcs = GLOBAL.gcloud.storage();
	//var storage = GLOBAL.gcloud.storage();
	console.log('opening bucket');
	if(process.env.NODE_ENV.toLowerCase()=='development')
	{
		var bucket = gcs.bucket('shopstyx-products-local');
	}else{
		var bucket = gcs.bucket('shosptyx-products-staging');
	}
	
	var options = {
	  destination: 'images/'+filename,
	  resumable: true
	};
	// console.log('list files');
	// bucket.getFiles(function(err, files) {
	//   if (!err) {
	//     console.log(files);
	//     console.log(util.inspect(files,{showHidden: false, depth: null}));
	//   }else{
	//   	console.log('Error');
	//   	console.log(err);
	//   }
	// });
	console.log('checking file access');
	fs.access(GLOBAL.upload_location+'/'+filename,fs.F_OK,function(err){
		if(err==null)
		{
			console.log('file found and uploading '+GLOBAL.upload_location+'/'+filename);
			bucket.upload(GLOBAL.upload_location+'/'+filename, options, function(err, file){
				if(err!=null)
				{
					
					var msg = {
						"success":false, "status":"Error",
						"desc":"[uploadProductImage:start]Error in Uploading Image",
						"err":err
					};
					console.log(util.inspect(msg,{showHidden: false, depth: null}));
					/*
					{ errors: [ { domain: 'global', reason: 'forbidden', message: 'Forbidden' } ],
				     code: 403,
				     message: 'Forbidden' } }
					*/
				}else{
					file.makePublic(function(err, apiResponse) {});
					console.log("uploaded file "+filename);
				}
			});
		}else{
			console.log('no file found at '+GLOBAL.upload_location+'/'+filename);
			// console.log('waiting for 4 seconds');
			// setTimeout(start, 3000,GLOBAL.upload_location,filename);
		}
	});
};

module.exports={
	start:start
};