const test = require(__dirname+'/testRoute');
const healthcheck = require(__dirname+'/healthcheck');
const scraping = require(__dirname+'/client/scrapingRoute');
const jqueryRequest = require(__dirname+'/client/jqueryRequests');

var mainRoute = function(dataStruct)
{
	dataStruct = scraping.configRoutes(dataStruct);
	dataStruct = jqueryRequest.configRoutes(dataStruct);
	dataStruct = healthcheck.configRoutes(dataStruct);
	dataStruct = test.configRoutes(dataStruct);
	
	// REGISTER OUR ROUTES -------------------------------
	// all of our routes will be from root /
	if(process.env.NODE_ENV.toLowerCase()==='development' || process.env.NODE_ENV.toLowerCase()==='local')
	{
		console.log('registering route for '+process.env.NODE_ENV.toLowerCase())
		dataStruct.app.use('/', dataStruct.router);
	}else{
		console.log('registering route for '+process.env.NODE_ENV.toLowerCase())
		dataStruct.app.use('/api/', dataStruct.router);
	}
	return dataStruct;
}

module.exports = {
	mainRoute:mainRoute
};