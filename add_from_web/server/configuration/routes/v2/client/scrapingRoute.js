const scrapeRoutine = require('../../../../Routines/ScrapingRoutine/scrapeRoutine');

const scrapeSaveRoutine = require('../../../../Routines/ScrapeSaveRoutine/saveRoutine');
const securityListener = require(GLOBAL.server_location+'/EventListeners/securityListener');

const multer  = require('multer');
console.log("temp_dir_location: "+GLOBAL.temp_dir_location);
const upload = multer({ dest: GLOBAL.temp_dir_location });
const mkdirp = require('mkdirp');
const cpUpload = upload.fields([{ name: 'prevpic', maxCount: 10 }, { name: 'src', maxCount: 10 }]);
const respond = require(GLOBAL.server_location+'/EventListeners/errorListener');
const util=require('util');

var configRoutes = function(dataStruct)
{   
    var router = dataStruct.router;

    router.route('/v2/scrape/request/')
        .post(cpUpload,function(req,res){
            mkdirp(GLOBAL.temp_dir_location, function (err) {
                if (err){
                    console.log("Error creating folder");
                    var msg = {
                        'status':'Error',
                        'desc':'Error creating folder'
                    };
                    respond.respondError(msg,res,req);  
                }else{//,req,res
                    console.log('Starting: '+req.protocol + '://' + req.get('host') + req.originalUrl + " -------- "+Date());
                    securityListener.start(dataStruct,req).spread(function(dataJSON,dataStruct){
                        try{
                            console.log('executing: '+req.protocol + '://' + req.get('host') + req.originalUrl + " -------- "+Date());
                            setImmediate(scrapeRoutine.processRequest,dataJSON,dataStruct,req,res);
                        }catch(err){
                            var msg={
                                "success":false, "status":"Error",
                                "err":"Error Code Root 1",
                                "url":"",
                                "error_report":err,
                                "stack":err.stack
                            }
                            respond.respondError(err,res,req);
                        }
                        
                    }).catch(function(err){
                        if(util.isError(err))
                        {
                            try{
                                try{
                                    var msg = JSON.parse(err.message);
                                }catch(dont_care){
                                    var msg = {
                                        message:err.message
                                    };
                                }
                                if(util.isNullOrUndefined(err.stack)==false)
                                {
                                    msg.stack = err.stack;
                                    console.error(err.stack);
                                }
                                respond.respondError(msg,res,req);
                            }catch(somethingHappened){
                                console.log(err);
                                respond.respondError(err,res,req);
                                //res.json(err);
                            }
                        }else{
                            if(util.isNullOrUndefined(err.stack)==false)
                            {
                                console.error(err.stack);
                            }
                            throw err;
                        }
                    });
                }
            });
        });

    router.route('/v2/scrape/save/')
        .post(cpUpload,function(req,res){
            mkdirp(GLOBAL.temp_dir_location, function (err) {
                if (err){
                    console.log("Error creating folder");
                    var msg = {
                        'status':'Error',
                        'desc':'Error creating folder'
                    };
                    respond.respondError(msg,res,req);  
                }else{
                    console.log('Starting: '+req.protocol + '://' + req.get('host') + req.originalUrl + " -------- "+Date());
                    securityListener.start(dataStruct,req).spread(function(dataJSON,dataStruct){
                        //scrapeRoutine.processRequest asynchronous process, results are ignored
                        
                        try{
                            console.log('executing: '+req.protocol + '://' + req.get('host') + req.originalUrl + " -------- "+Date());
                            setImmediate(scrapeSaveRoutine.processRequest,dataJSON,dataStruct,req,res);
                        }catch(err){
                            var msg={
                                "success":false, "status":"Error",
                                "err":"Error Code Root 1",
                                "url":"",
                                "error_report":err,
                                "stack":err.stack
                            }
                            respond.respondError(err,res,req);
                        }
                    }).catch(function(err){
                        if(util.isError(err))
                        {
                            try{
                                var msg = JSON.parse(err.message);
                            }catch(dont_care){
                                var msg = {
                                    message:err.message
                                };
                            }
                            if(util.isNullOrUndefined(err.stack)==false)
                            {
                                msg.stack = err.stack;
                                console.error(err.stack);
                            }
                            respond.respondError(msg,res,req);
                        }else{
                            if(util.isNullOrUndefined(err.stack)==false)
                            {
                                console.error(err.stack);
                            }
                            throw err;
                        }
                    });
                }
            });
        });

    return dataStruct;
}


module.exports = {
    configRoutes:configRoutes
};