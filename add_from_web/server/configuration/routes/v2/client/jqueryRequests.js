const multer  = require('multer');
console.log("temp_dir_location: "+GLOBAL.temp_dir_location);
const upload = multer({ dest: GLOBAL.temp_dir_location });
const mkdirp = require('mkdirp');
const cpUpload = upload.fields([{ name: 'prevpic', maxCount: 10 }, { name: 'src', maxCount: 10 }]);
const respond = require(GLOBAL.server_location+'/EventListeners/errorListener');
const util = require('util');

var configRoutes = function(dataStruct)
{   
    var router = dataStruct.router;

    router.route('/v2/jquery/request/:filename')
        .get(cpUpload,function(req,res){
            mkdirp(GLOBAL.temp_dir_location, function (err) {
                if (err){
                    console.log("Error creating folder");
                    var msg = {
                        'status':'Error',
                        'desc':'Error creating folder'
                    };
                    respond.respondError(msg,res,req);  
                }else{
                    var rootPath = __dirname;
        			var holder = rootPath.split('server');
        			rootPath = holder[0];
        			res.sendFile(req.params.filename,{root:rootPath+'/server/jslibs/'});
                }
            });
        });
    
    return dataStruct;
}


module.exports = {
    configRoutes:configRoutes
};