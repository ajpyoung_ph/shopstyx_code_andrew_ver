//asos.com has multiple subdomains but use the same scraping method
const txtman = require (GLOBAL.server_location+'/helpers/text_manipulation');
const grphman = require (GLOBAL.server_location+'/helpers/image_manipulation');
const jsonwrt = require (GLOBAL.server_location+'/helpers/writescrapeJSON');

module.exports=function(err,result,$,dataSend,url,domain,imageFilters,res,req,cb){
	var html_results = $('html').html();
	//get canonical
	if((dataSend.canonical_url = txtman.getCanonical($,html_results))=='')
	{
		dataSend.canonical_url = "";
	}
	//get keywords
	if((dataSend.meta_keywords = txtman.getKeywords($,html_results))=='')
	{
		dataSend.meta_keywords = "";
	}
	var dataSource = [];
	dataSource[0] = $('meta[itemprop="name"]').attr("content");
	dataSource[1] = txtman.getPrice($('meta[itemprop="price"]').attr("content"));
	dataSource[2] = $('img[itemprop="image"]').attr("src");

	if(dataSource[1]=='')
	{
		dataSource[5]=txtman.getPrice(result.body);
	}
	dataSource[3] = [];
	grphman.getAllImages($,imageFilters,dataSend,dataSource,res,req,cb,retImgList); //list of images found
}
var retImgList=function($,imgList,dataSend,dataSource,res,req,cb)
{
	dataSource[3] = imgList
	dataSource[4] = $('div.product-description').find('ul').text();
	jsonwrt(dataSend,dataSource);
	cb(dataSend,res,req);
}