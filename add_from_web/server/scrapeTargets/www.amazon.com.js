const txtman = require (GLOBAL.server_location+'/helpers/text_manipulation');
const grphman = require (GLOBAL.server_location+'/helpers/image_manipulation');
const jsonwrt = require (GLOBAL.server_location+'/helpers/writescrapeJSON');

module.exports=function(err,result,$,dataSend,url,domain,imageFilters,res,req,cb){
	var html_results = $('html').html();
	//get canonical
	if((dataSend.canonical_url = txtman.getCanonical($,html_results))=='')
	{
		dataSend.canonical_url = "";
	}
	//get keywords
	if((dataSend.meta_keywords = txtman.getKeywords($,html_results))=='')
	{
		dataSend.meta_keywords = "";
	}
	var dataSource = [];

	dataSource[0] = $("span#btAsinTitle").text();
	if(dataSource[0]==null || dataSource[0]=='' || dataSource[0]==undefined)
	{
		dataSource[0] = $("span#productTitle").text();
	}
	dataSource[1] = $("b.priceLarge").text();
	if(dataSource[1]==null || dataSource[1]=='' || dataSource[1]==undefined)
	{
		dataSource[1] = $("span#priceblock_ourprice").text();
		if(dataSource[1]==null || dataSource[1]=='' || dataSource[1]==undefined)
		{
			dataSource[1] = $("span#current-price").text();
			if(dataSource[1]==null || dataSource[1]=='' || dataSource[1]==undefined)
			{
				dataSource[1] = $("span.a-color-price.offer-price").text();
				try{
					if(dataSource[1].length>1)
					{
						dataSource[1] = dataSource[1][1];
					}
				}catch(error){
					//do nothing?
				}
			}
		}
	}
	dataSource[1]=txtman.getPrice(dataSource[1]);
	dataSource[2] = $("img.kib-ma").attr("src");//ask red about this
	//$image = doParsing($index, '/data-a-dynamic-image.*?quot;(.*?)&quot/s');
	//$image = doParsing($index, '/"large":"(.*?)"/s');
	if(dataSource[2]==null || dataSource[2]=='' || dataSource[2]==undefined)
	{
		dataSource[2] = $("img#imgBlkFront").attr("src");
		if(dataSource[2]==null || dataSource[2]=='' || dataSource[2]==undefined)
		{
			dataSource[2] = $("img[data-a-dynamic-image]").attr("data-a-dynamic-image");
			if(dataSource[2]==null || dataSource[2]=='' || dataSource[2]==undefined)
			{
				var pattern = /"large":"(.*?)"/;
				dataSource[2] = txtman.getFirstInstance(pattern,result.body);
			}else{
				dataSource[2] = dataSource[2].split('"');
				dataSource[2] = dataSource[2][1];
			}
		}
	}
	if(dataSource[1]=='')
	{
		dataSource[5]=txtman.getPrice(result.body);
	}
	dataSource[3] = [];
	grphman.getAllImages($,imageFilters,dataSend,dataSource,res,req,cb,retImgList); //list of images found
}
var retImgList=function($,imgList,dataSend,dataSource,res,req,cb)
{
	dataSource[3] = imgList;
	dataSource[4] = $('#featurebullets_feature_div').find('#feature-bullets').text();
	jsonwrt(dataSend,dataSource);
	cb(dataSend,res,req);
}