
const imagesize = require('request-image-size');
//var async = require('synchronize');
const txtman = require (GLOBAL.server_location+'/helpers/text_manipulation');
const grphman = require (GLOBAL.server_location+'/helpers/image_manipulation');
const util = require('util');

module.exports=function(err,result,$,dataSend,url,domain,imageFilters,res,req,cb){
	//result.uri;//this is the original URL sent
	//result.body;//this is the read information
	//get price
	//debugger;
	var html_results = $('html').html();
	//get canonical
	if((dataSend.canonical_url = txtman.getCanonical($,html_results))=='')
	{
		dataSend.canonical_url = "";
	}
	//get keywords
	if((dataSend.meta_keywords = txtman.getKeywords($,html_results))=='')
	{
		dataSend.meta_keywords = "";
	}
	var dataSource=[];
	dataSend.otherPrices = txtman.getPrice(result.body);
	if(dataSend.otherPrices.length>0)
	{
		dataSend.price = dataSend.otherPrices[0];
	}
	//getting all images($,imageFilters,dataSend,dataSource,res,req,cb_orig,cb)
	grphman.getAllImages($,imageFilters,dataSend,dataSource,res,req,cb,retImgList); //list of images found
}
var retImgList=function($,imgList,dataSend,dataSource,res,req,cb)
{
	dataSend.images = imgList;
	//getting all elements whose id has title as a value
	var idTitle = $("[id]").filter(function() {
		try{
			return(this.id.match(/.*title.*/i) != null);
		}catch(error){
			return false;
		}
	});
	var nameTitle = $("[name]").filter(function() {
	    try{
			return(this.name.match(/.*title.*/i) != null);
		}catch(error){
			return false;
		}
	});
	//collect all titles
	var titleList = [];
	titleList = titleList.concat(txtman.getALLtext(idTitle));
	titleList = titleList.concat(txtman.getALLtext(nameTitle));
	delete idTitle;
	delete nameTitle;
	dataSend.otherTitle=titleList;
	//match $(title).text() if trim(nameTitle.eq(index).text()) matches
	try{
		var titleBarValue = $("title").text();
		dataSend.product_name=titleBarValue;
	}catch(error){
		var titleBarValue = null;
	}
	if(titleBarValue!=null && titleBarValue!=undefined)
	{
		dataSend.product_name = txtman.getText(titleList,titleBarValue);
		console.log("product_name :"+dataSend.product_name);
		if(dataSend.product_name==false)
		{
			//if still did not find any title then we use the value in the title bar
			dataSend.product_name=titleBarValue;
		}
	}else{
		//if there's no titleBarValue then this must be the worst site ever so I'll just use the first text value available
		var dummyvar = [""];
		dataSend.product_name = txtman.findtext(titleList);
	}
	//getting all elements whose name has title as a value
	//pulling image dimensions
	// async.mapSeries(imgList, imagesize, function(err,dim){
	// 	//console.log(err,dim);
	// 	//getting the biggest picture
	// 	var currentDim = 0;
	// 	var maxDim = 0;
	// 	for(var x=0;x<dim.length;x++)
	// 	{
	// 		currentDim = parseFloat(dim[x].height * dim[x].width);
	// 		if(currentDim>maxDim)
	// 		{
	// 			maxDim=currentDim;
	// 			dataSend.mainPic.url = imgList[x];
	// 			dataSend.mainPic.height = dim[x].height;
	// 			dataSend.mainPic.width = dim[x].width;
	// 		}
	// 	}
	// 	//console.log(dataSend);
	// 	GLOBAL.res[domain].json(dataSend);
	// 	delete GLOBAL.res[domain];
	// 	delete GLOBAL.req[domain];
	// });
	try{
		cb(dataSend,res,req);
	}catch(err){
		console.log("generic_scraping:failed to response");
		console.log( util.inspect(err,{showHidden: false, depth: null}));
		console.log( util.inspect(dataSend,{showHidden: false, depth: null}));
	}
	
};