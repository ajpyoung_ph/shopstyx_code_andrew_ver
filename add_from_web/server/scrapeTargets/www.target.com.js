const txtman = require (GLOBAL.server_location+'/helpers/text_manipulation');
const grphman = require (GLOBAL.server_location+'/helpers/image_manipulation');
const jsonwrt = require (GLOBAL.server_location+'/helpers/writescrapeJSON');

module.exports=function(err,result,$,dataSend,url,domain,imageFilters,res,req,cb)
{
	var html_results = $('html').html();
	//get canonical
	if((dataSend.canonical_url = txtman.getCanonical($,html_results))=='')
	{
		dataSend.canonical_url = "";
	}
	//get keywords
	if((dataSend.meta_keywords = txtman.getKeywords($,html_results))=='')
	{
		dataSend.meta_keywords = "";
	}	
	var dataSource = [];

	dataSource[0] = $('meta[property="og:title"]').attr("content");
	//dataSource[1] = $('span.offerPrice[itemprop="price"]').text();
	console.log($('[itemprop="price"]').attr('data-bfx'));
	dataSource[1] = $('.offerPrice[itemprop="price"]').attr('data-bfx');
	if(dataSource[1]==''||dataSource[1]==null)
	{
		dataSource[1]=$('span.price').text();
	}
	dataSource[1]=txtman.getPrice(dataSource[1]);
	if(typeof(dataSource[1])!="string")
	{
		dataSource[1]=dataSource[1][0];
	}
	dataSource[2] = $('img[itemprop="image"]').attr("src");

	dataSource[3] = [];
	grphman.getAllImages($,imageFilters,dataSend,dataSource,res,req,cb,retImgList); //list of images found
}
var retImgList=function($,imgList,dataSend,dataSource,res,req,cb)
{
	dataSource[3] = imgList;
	dataSource[4] = $('[itemprop="description"]').parents('.content').text();
	if(dataSource[1]=='' || dataSource[1]==null)
	{
		dataSource[5]=txtman.getPrice(result.body);
	}
	jsonwrt(dataSend,dataSource);
	cb(dataSend,res,req);
}