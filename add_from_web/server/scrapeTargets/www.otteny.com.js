const txtman = require (GLOBAL.server_location+'/helpers/text_manipulation');
const grphman = require (GLOBAL.server_location+'/helpers/image_manipulation');
const jsonwrt = require (GLOBAL.server_location+'/helpers/writescrapeJSON');
const util = require('util');

module.exports=function(err,result,$,dataSend,url,domain,imageFilters,res,req,cb)
{
	var html_results = $('html').html();
	//get canonical
	if((dataSend.canonical_url = txtman.getCanonical($,html_results))=='')
	{
		dataSend.canonical_url = "";
	}
	//get keywords
	if((dataSend.meta_keywords = txtman.getKeywords($,html_results))=='')
	{
		dataSend.meta_keywords = "";
	}
	console.log('start of datascrape');
	var dataSource = [];
	//console.log("executing otteny scrape");
	//dataSource[0] = $('meta[property="og:title"]').attr("content");
	dataSource[0] = $('div.product-shop').find('p.product-name').text();
	//console.log("Product Name: "+dataSource[0]);
	var price= txtman.getPrice($('div.price-wrapper').find('span.price').text());
	if(typeof(price)=='object')
	{
		dataSource[1]=price[0].toString();
	}
	if(typeof(price)=='string')
	{
		dataSource[1]=price;	
	}
	//console.log("Product Price: "+dataSource[1]);
	//dataSource[2] = $('p.product-image.product-image-zoom').children("a").attr("href");
	dataSource[2] = $('div.product-image.product-image-zoom').find('img').attr("src");
	//console.log("Image: "+dataSource[2]);

	
	dataSource[3] = [];
	console.log('end of datascrape 1');
	grphman.getAllImages($,imageFilters,dataSend,dataSource,res,req,cb,retImgList); //list of images found
}
var retImgList=function($,imgList,dataSend,dataSource,res,req,cb)
{
	console.log('end of datascrape 2');
	dataSource[3] = imgList;
	// dataSource[3]= $('img').map(function() {
	//     //returning all image tags src
	//     if(this.src!='')
	//     {
	//     	for(var x=0;x<imageFilters.length;x++)
	//     	{
	//     		var pattern = new RegExp(imageFilters[x],"i");
	//     		if(pattern.test(this.src))
	//     		{
	//     			return this.src;
	//     		}
	//     	}
	//     }
	// }).get();
	dataSource[4] = $('div.product-shop').find('.short-description').text();
	// if(dataSource[0] == '' || dataSource[1] == '')
	// {
	// 	throw new Error({"message":'Error Pulling Data From '+domain+' using generic scraping'});
	// }else{
	console.log('end of datascrape 3');
		dataSend=jsonwrt(dataSend,dataSource);
		cb(dataSend,res,req);
	// }
	
}