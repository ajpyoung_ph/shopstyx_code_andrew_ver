const txtman = require (GLOBAL.server_location+'/helpers/text_manipulation');
const grphman = require (GLOBAL.server_location+'/helpers/image_manipulation');
const jsonwrt = require (GLOBAL.server_location+'/helpers/writescrapeJSON');

module.exports=function(err,result,$,dataSend,url,domain,imageFilters,res,req,cb)
{
	var html_results = $('html').html();
	//get canonical
	if((dataSend.canonical_url = txtman.getCanonical($,html_results))=='')
	{
		dataSend.canonical_url = "";
	}
	//get keywords
	if((dataSend.meta_keywords = txtman.getKeywords($,html_results))=='')
	{
		dataSend.meta_keywords = "";
	}
	
	var dataSource = [];

	dataSource[0] = $('meta[property="og:title"]').attr("content");
	dataSource[1] = $('span[itemprop="price"]').text();
	dataSource[2] = $('meta[property="og:image"]').attr("content");

	
	dataSource[3] = [];
	grphman.getAllImages($,imageFilters,dataSend,dataSource,res,req,cb,retImgList); //list of images found
}
var retImgList=function($,imgList,dataSend,dataSource,res,req,cb)
{
	dataSource[3] = imgList;
	// if(dataSource[0] == '' || dataSource[1] == '')
	// {
		
	// }else{
		jsonwrt(dataSend,dataSource);
		cb(dataSend,res,req);
	//	}
	
}