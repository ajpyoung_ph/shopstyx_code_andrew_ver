const txtman = require (GLOBAL.server_location+'/helpers/text_manipulation');
const grphman = require (GLOBAL.server_location+'/helpers/image_manipulation');
const jsonwrt = require (GLOBAL.server_location+'/helpers/writescrapeJSON');

module.exports=function(err,result,$,dataSend,url,domain,imageFilters,res,req,cb){
	var html_results = $('html').html();
	//get canonical
	if((dataSend.canonical_url = txtman.getCanonical($,html_results))=='')
	{
		dataSend.canonical_url = "";
	}
	//get keywords
	if((dataSend.meta_keywords = txtman.getKeywords($,html_results))=='')
	{
		dataSend.meta_keywords = "";
	}
	var dataSource = [];

	//dataSource[0] = $('meta[name="twitter:title"]').attr("value");
	dataSource[0] = $('div#listing-page-cart-inner').find('span[itemprop=name]').text();
	//dataSource[1] = $('meta[itemprop="price"').attr("content");
	dataSource[1] = txtman.getPrice($('.currency-value').text());
	if(typeof(dataSource[1])!='string')
	{
		dataSource[1]=dataSource[1][0];
	}
	//dataSource[2] = $('meta[name="twitter:image"]').attr("content");
	dataSource[2] = $('div#image-main').find('img').attr('src');

	
	if(dataSource[1]=='')
	{
		dataSource[5]=txtman.getPrice(result.body);
	}
	dataSource[3] = [];
	grphman.getAllImages($,imageFilters,dataSend,dataSource,res,req,cb,retImgList); //list of images found
}
var retImgList=function($,imgList,dataSend,dataSource,res,req,cb)
{
	dataSource[3] = imgList;
	dataSource[4] = $('#item-overview').find('.properties').text();
	if(typeof(dataSource[5])=='undefined')
	{
		dataSource[5]=[];
	}
	dataSource[6]=$('meta[property="etsymarketplace:shop"]').attr("content");//
	if(dataSource[0] == '' || dataSource[1] == '')
	{
		throw new Error({"message":'Error Pulling Data From '+domain+' using generic scraping'});
	}else{
		jsonwrt(dataSend,dataSource);
		cb(dataSend,res,req);
	}
	
}