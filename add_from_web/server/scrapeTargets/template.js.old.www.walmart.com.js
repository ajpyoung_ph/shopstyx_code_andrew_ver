const txtman = require (GLOBAL.server_location+'/helpers/text_manipulation');
const grphman = require (GLOBAL.server_location+'/helpers/image_manipulation');
const jsonwrt = require (GLOBAL.server_location+'/helpers/writescrapeJSON');

module.exports=function(err,result,$,dataSend,url,domain,imageFilters,res,req,cb){
	var html_results = $('html').html();
	//get canonical
	if((dataSend.canonical_url = txtman.getCanonical($,html_results))=='')
	{
		dataSend.canonical_url = "";
	}
	//get keywords
	if((dataSend.meta_keywords = txtman.getKeywords($,html_results))=='')
	{
		dataSend.meta_keywords = "";
	}
	var dataSource = [];

	dataSource[0] = $('meta[name="twitter:title"]').attr("content"); //mainTitle or product_name
	dataSource[1] = $('meta[itemprop="price"]').attr("content"); //price
	dataSource[2] = $('img[name="twitter:image"]').attr("src"); //image
	//dataSource[2] = dataSource[2].replace('215X215', '500X500');

	
	dataSource[3] = [];
	dataSource[3] = grphman.getAllImages($,imageFilters); //list of images found
	if(dataSource[0] == '' || dataSource[1] == '')
	{
		throw new Error({"message":'Error Pulling Data From '+domain+' using generic scraping'});
	}else{
		jsonwrt(dataSend,dataSource);
		cb(dataSend,res,req);
	}
	
}