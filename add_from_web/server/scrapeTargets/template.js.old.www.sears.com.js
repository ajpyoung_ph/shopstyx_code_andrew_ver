const txtman = require (GLOBAL.server_location+'/helpers/text_manipulation');
const grphman = require (GLOBAL.server_location+'/helpers/image_manipulation');
const jsonwrt = require (GLOBAL.server_location+'/helpers/writescrapeJSON');
const phantom = require('phantom');

module.exports=function(dataSend,imageFilters,url,res,cb){
	console.log("executing sears routine");
	phantom.create(function (ph) {
		ph.createPage(function (page) {
	    	page.open(url, function (status) {
	      		console.log("opened google? ", status);
		      	page.includeJs('http://localhost:7772/v2/jquery/request/jquery-2.1.4.min.js', function(err) {
					//jQuery Loaded. 
					//Wait for a bit for AJAX content to load on the page. Here, we are waiting 8 seconds. 
					console.log("insertion of jQuery done");
					setTimeout(function() {
						return page.evaluate(function(){
							var html_results = $('html').html();
							//get canonical
							if((dataSend.canonical_url = txtman.getCanonical($,html_results))=='')
							{
								dataSend.canonical_url = "";
							}
							//get keywords
							if((dataSend.meta_keywords = txtman.getKeywords($,html_results))=='')
							{
								dataSend.meta_keywords = "";
							}
							var dataSource = [];

							dataSource[0] = $('div[property="gr:name"]').attr("content");
							dataSource[1] = $('span[itemprop="price"]').text();
							dataSource[2] = $('img[itemprop="image"]').attr("src");

							dataSource[3] = [];
							dataSource[3] = grphman.getAllImages($,imageFilters); //list of images found
							if(dataSource[1]=='' || dataSource[1]==null)
							{
								dataSource[5]=txtman.getPrice(result.body);
							}
							jsonwrt(dataSend,dataSource);
							console.log(dataSend);
							console.log("returning dataSend");
							return dataSend;

						}, function(err,dataSend) {
							cb(dataSend,res,req);
							ph.exit();
						});
					}, 8000);
				});
	    	});
	  	});
	});
	
	

}

// module.exports=function(err,result,$,dataSend,url,domain,imageFilters,res,req){
// 	var dataSource = [];

// 	dataSource[0] = $('div[property="gr:name"]').attr("content");
// 	dataSource[1] = $('span[itemprop="price"]').text();
// 	dataSource[2] = $('img[itemprop="image"]').attr("src");

// 	dataSource[3] = [];
// 	dataSource[3] = grphman.getAllImages($,imageFilters); //list of images found
// 	if(dataSource[1]=='' || dataSource[1]==null)
// 	{
// 		dataSource[5]=txtman.getPrice(result.body);
// 	}
// 	jsonwrt(dataSend,dataSource);
// 	res.json(dataSend);
// }