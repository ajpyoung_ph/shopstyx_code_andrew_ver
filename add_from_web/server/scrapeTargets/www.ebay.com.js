const txtman = require (GLOBAL.server_location+'/helpers/text_manipulation');
const grphman = require (GLOBAL.server_location+'/helpers/image_manipulation');
const jsonwrt = require (GLOBAL.server_location+'/helpers/writescrapeJSON');

module.exports=function(err,result,$,dataSend,url,domain,imageFilters,res,req,cb){
	var html_results = $('html').html();
	//get canonical
	if((dataSend.canonical_url = txtman.getCanonical($,html_results))=='')
	{
		dataSend.canonical_url = "";
	}
	//get keywords
	if((dataSend.meta_keywords = txtman.getKeywords($,html_results))=='')
	{
		dataSend.meta_keywords = "";
	}
	var dataSource = [];

	//dataSource[0] = $('meta[property="og:title"]').attr("content");
	dataSource[0] = $('#itemTitle[itemprop=name]').text();
	//dataSource[1] = $('meta[property="og:description"]').attr("content");
	dataSource[1] = txtman.getPrice($('#prcIsum').text());
	// if(dataSource[1]==null || dataSource[1] == undefined || dataSource[1] == '')
	// {
	// 	dataSource[1] = $('span.notranslate[itemprop="price"]').text();
	// 	dataSource[1] = dataSource[1].split("US");
	// 	dataSource[1] = dataSource[1][0];
	// }else{
	// 	dataSource[1] = dataSource[1].split("US");
	// 	dataSource[1] = dataSource[1][0];
	// }
	dataSource[2] = $('img#icImg[itemprop="image"]').attr("src");
	
	if(dataSource[1]=='')
	{
		dataSource[5]=txtman.getPrice(result.body);
	}
	dataSource[3] = [];
	grphman.getAllImages($,imageFilters,dataSend,dataSource,res,req,cb,retImgList); //list of images found
}
var retImgList=function($,imgList,dataSend,dataSource,res,req,cb)
{
	dataSource[3] = imgList
	dataSource[4] = $('div.itemAttr').text();
	if(typeof(dataSource[5])=='undefined')
	{
		dataSource[5]=[];
	}
	dataSource[6]=$('div[class="mbg vi-VR-margBtm3"]').find('a').attr('href');
	jsonwrt(dataSend,dataSource);
	cb(dataSend,res,req);
}