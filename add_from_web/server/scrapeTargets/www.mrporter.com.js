const txtman = require (GLOBAL.server_location+'/helpers/text_manipulation');
const grphman = require (GLOBAL.server_location+'/helpers/image_manipulation');
const jsonwrt = require (GLOBAL.server_location+'/helpers/writescrapeJSON');

module.exports=function(err,result,$,dataSend,url,domain,imageFilters,res,req,cb){
	var html_results = $('html').html();
	//get canonical
	if((dataSend.canonical_url = txtman.getCanonical($,html_results))=='')
	{
		dataSend.canonical_url = "";
	}
	//get keywords
	if((dataSend.meta_keywords = txtman.getKeywords($,html_results))=='')
	{
		dataSend.meta_keywords = "";
	}
	var dataSource = [];

	dataSource[0] = $('meta[property="og:title"]').attr("content");
	dataSource[0] = dataSource[0].replace('&nbsp;|&nbsp;MR PORTER','');
	dataSource[1] = $('span.price-value').text();
	if(dataSource[1]=='' || dataSource[1]==null)
	{
		dataSource[1] = $('.product-details__price--value').text();
	}
	//dataSource[2] = $('meta#medium-image').attr("src");
	dataSource[2] = $('#medium-image-container').find('img').attr('src');
	if(dataSource[2]==''||dataSource[2]==null)
	{
		dataSource[2]=$('div.product-image').find('img').attr('src');
	}
	//test if http: is in the string
	var tester = /http/i;
	if(tester.test(dataSource[2])==false)
	{
		dataSource[2]='http:'+dataSource[2];
	}
	if(dataSource[1]=='' || dataSource[1]==null)
	{
		dataSource[5]=txtman.getPrice(result.body);
	}
	dataSource[3] = [];
	grphman.getAllImages($,imageFilters,dataSend,dataSource,res,req,cb,retImgList); //list of images found
}
var retImgList=function($,imgList,dataSend,dataSource,res,req,cb)
{
	//check all images if it has http
	var tester = /http/i;
	var clone = imgList;
	clone.forEach(function(data){
		var temp = data;
		if(tester.test(data)==false)
		{
			temp = 'http:'+data;
		}
		dataSource[3].push(temp);
	});
	dataSource[4]=$('#tab1').find('div.productDescription.product-description').text();
	if(dataSource[4]==''||dataSource[4]==null)
	{
		dataSource[4]=$('section.product-accordion__item.product-accordion__item--detailsAndCare').find('ul').text()
	}
	jsonwrt(dataSend,dataSource);
	cb(dataSend,res,req);
}