const txtman = require (GLOBAL.server_location+'/helpers/text_manipulation');
const grphman = require (GLOBAL.server_location+'/helpers/image_manipulation');
const jsonwrt = require (GLOBAL.server_location+'/helpers/writescrapeJSON');

module.exports=function(err,result,$,dataSend,url,domain,imageFilters,res,req,cb)
{
	var html_results = $('html').html();
	//get canonical
	if((dataSend.canonical_url = txtman.getCanonical($,html_results))=='')
	{
		dataSend.canonical_url = "";
	}
	//get keywords
	if((dataSend.meta_keywords = txtman.getKeywords($,html_results))=='')
	{
		dataSend.meta_keywords = "";
	}
	
	var dataSource = [];

	//dataSource[0] = $('td[align="center"]').children('p').children('img').attr("alt"); //mainTitle or product_name
	dataSource[0] = $('div.ProductMain').find('h1').text();
	//dataSource[1] = $('span.prod-detail-cost-value').text(); //price
	dataSource[1]=$('div.ProductMain').find('.ProductPrice.VariationProductPrice').text();
	//dataSource[2] = $('td[align="center"]').children('p').children('img').attr("src"); //image
	dataSource[2] = $('img[alt="'+dataSource[0]+'"]').attr('src');
	if(dataSource[2]==''||dataSource[2]==null)
	{
		dataSource[2]=$('div.ProductThumbImage').find('img').attr('src');
	}
	dataSource[3] = [];
	grphman.getAllImages($,imageFilters,dataSend,dataSource,res,req,cb,retImgList); //list of images found
}
var retImgList=function($,imgList,dataSend,dataSource,res,req,cb)
{
	dataSource[3] = imgList;
	if(dataSource[1]=='' || dataSource[1]==null)
	{
		dataSource[5]=txtman.getPrice(result.body);
	}
	dataSource[4]=$('.ProductDescriptionContainer.prodAccordionContent').text();
	jsonwrt(dataSend,dataSource);
	cb(dataSend,res,req);
}