const txtman = require (GLOBAL.server_location+'/helpers/text_manipulation');
const grphman = require (GLOBAL.server_location+'/helpers/image_manipulation');
const jsonwrt = require (GLOBAL.server_location+'/helpers/writescrapeJSON');

module.exports=function(err,result,$,dataSend,url,domain,imageFilters,res,req,cb){
	var html_results = $('html').html();
	//get canonical
	if((dataSend.canonical_url = txtman.getCanonical($,html_results))=='')
	{
		dataSend.canonical_url = "";
	}
	//get keywords
	if((dataSend.meta_keywords = txtman.getKeywords($,html_results))=='')
	{
		dataSend.meta_keywords = "";
	}
	var dataSource = [];

	dataSource[0]  = $('meta[property="og:title"]').attr("content");
	 
	var tempPrice = txtman.getPrice(($('form.pdp-form').find('p.price').text()).toString());
	dataSource[1] = tempPrice[0];
	if(dataSource[1] == null || dataSource[1] == undefined || dataSource[1] == '')
	{
		var pattern = /<li class="item all trigger">.*?<span class="italic">from<\/span>\s{1,}?<em>\s{1,}?(.*?)\s{1,}?<\/em>/;
		dataSource[1] = txtman.getFirstInstance(pattern,result.body);
	}
	dataSource[3] = [];
	dataSource[3] = $('[itemprop="image"]').attr("src");

	if(dataSource[1]=='')
	{
		dataSource[5]=txtman.getPrice(result.body);
	}
	dataSource[3] = [];
	grphman.getAllImages($,imageFilters,dataSend,dataSource,res,req,cb,retImgList); //list of images found
}
var retImgList=function($,imgList,dataSend,dataSource,res,req,cb)
{
	dataSource[3] = imgList;
	dataSource[4] = $('div.format-content').find('p').text();
	jsonwrt(dataSend,dataSource);
	cb(dataSend,res,req);
}