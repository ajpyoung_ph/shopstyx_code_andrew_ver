const txtman = require (GLOBAL.server_location+'/helpers/text_manipulation');
const grphman = require (GLOBAL.server_location+'/helpers/image_manipulation');
const jsonwrt = require (GLOBAL.server_location+'/helpers/writescrapeJSON');

module.exports=function(err,result,$,dataSend,url,domain,imageFilters,res,req,cb){
	var html_results = $('html').html();
	//get canonical
	if((dataSend.canonical_url = txtman.getCanonical($,html_results))=='')
	{
		dataSend.canonical_url = "";
	}
	//get keywords
	if((dataSend.meta_keywords = txtman.getKeywords($,html_results))=='')
	{
		dataSend.meta_keywords = "";
	}
	var dataSource = [];

	dataSource[0] = $('meta[property="og:title"]').attr("content");
	var pattern = /PROD_Price:(.*?),/;
	dataSource[1] = txtman.getFirstInstance(pattern,result.body);
	dataSource[2] = $('img#detailImg').attr("src");

	dataSource[3] = [];
	dataSource[3] = grphman.getAllImages($,imageFilters); //list of images found
	dataSource[4] = $('.leftCol.shortDesc').text();
	if(dataSource[1]=='' || dataSource[1]==null)
	{
		dataSource[5]=txtman.getPrice(result.body);
	}
	jsonwrt(dataSend,dataSource);
	cb(dataSend,res,req);
}