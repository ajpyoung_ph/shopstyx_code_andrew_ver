const txtman = require (GLOBAL.server_location+'/helpers/text_manipulation');
const grphman = require (GLOBAL.server_location+'/helpers/image_manipulation');
const jsonwrt = require (GLOBAL.server_location+'/helpers/writescrapeJSON');
const respond = require(GLOBAL.server_location+'/EventListeners/errorListener');

module.exports=function(err,result,$,dataSend,url,domain,imageFilters,res,req){
	var html_results = $('html').html();
	//get canonical
	if((dataSend.canonical_url = txtman.getCanonical($,html_results))=='')
	{
		dataSend.canonical_url = "";
	}
	//get keywords
	if((dataSend.meta_keywords = txtman.getKeywords($,html_results))=='')
	{
		dataSend.meta_keywords = "";
	}
	var dataSource = [];

	dataSource[0] = $('meta[itemprop="name"]').attr("content"); //mainTitle or product_name
	dataSource[1] = $('meta[itemprop="price"]').attr("content"); //price
	dataSource[2] = $('img[itemprop="image"]').attr("src"); //image

	
	dataSource[3] = [];
	grphman.getAllImages($,imageFilters,dataSend,dataSource,res,req,cb,retImgList); //list of images found
}
var retImgList=function($,imgList,dataSend,dataSource,res,req,cb)
{
	dataSource[3] = imgList;
	if(dataSource[0] == '' || dataSource[1] == '')
	{
		throw new Error({"message":'Error Pulling Data From '+domain+' using generic scraping'});
	}else{
		jsonwrt(dataSend,dataSource);
		var msg = dataSend;
        respond.respondError(msg,res,req);
	}
	
}
/*
try{
    var msg = JSON.parse(err.message);
}catch(dont_care){
    var msg = {
        message:err.message
    };
}
respond.respondError(msg,res,req);
*/