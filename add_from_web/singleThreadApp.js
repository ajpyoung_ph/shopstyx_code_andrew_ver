const pmx = require('pmx').init({
  http : true
});
const express = require('express');
const timeout = require('connect-timeout');
const util = require('util');
//finding the commonnodeconfig
var server_dir_array = (__dirname).toString().split("add_from_web");
GLOBAL.server_location = server_dir_array[0]+"commonnodeconfig";
delete server_dir_array;

console.log("setting temp_dir_location");
if(process.env.NODE_ENV.toLowerCase()!='production')
{	
	var temp_dir_array = (__dirname).toString().split("server");
	GLOBAL.temp_dir_location = temp_dir_array[0]+"/uploads";
	delete temp_dir_array;
}else{
	GLOBAL.temp_dir_location = '/opt/temp/add_from_web';
}
console.log("init temp_dir_location: "+GLOBAL.temp_dir_location);
const module_directory = __dirname+"/server/scrapeTargets";

const configureEnvironment = require(GLOBAL.server_location + '/configuration/environment/configureEnvironment');
const haltOnTimedout = require(GLOBAL.server_location+'/configuration/environment/haltOnTimedOut');
const conv = require(GLOBAL.server_location + '/helpers/mysql_convertions');
const loadModules = require(GLOBAL.server_location + '/helpers/loadModules');

//var testCodes = require('./testCodes/initEmitTest');
const initListeners = require(__dirname + '/server/EventListeners/initializeListeners');
const routes = require(__dirname + '/server/configuration/routes/v2/mainRoute');
//const mongodb_config = require(__dirname+'/server/configuration/mongodb/mongodb_config');

initListeners.start(); //initialize all known listeners for the system

var MainDataStruct = {
	app:express(),
	HTTP_location:'',
	mongo_db_location:'',
	mysql_db_init:{
		connectionLimit : 200,
		host     : '',
		user     : '',
		password : '',
		database : ''
	},
	gcloud_db_location:{
		projectId: '',
		keyFilename:''
	},
	router:express.Router(),
	routeData:{
		"user_id":'',
		"token":'',
		"req": '',
		"res": '',
		"new_unix_timestamp":'',
		"new_simple_timestamp":'',
		"timezone":"America/Los_Angeles"
	},
	rootPath:__dirname,
	port:7772
};
//increase timeout to 1200seconds
//MainDataStruct.app.use(timeout('1200s'));

configureEnvironment.start(MainDataStruct).then(function(retVal){
	return retVal;
}).then(function(MainDataStruct){
	//setup routes
	MainDataStruct = routes.mainRoute(MainDataStruct);
	//load all modules
	loadModules.startLoad(module_directory);

	//End Configuration of system
	GLOBAL.number_list_items=50;
	GLOBAL.start_list_page=0;


	// Add the error middleware at the end (after route declaration)
	MainDataStruct.app.use(haltOnTimedout.haltOnTimedout);

	GLOBAL.server = MainDataStruct.app.listen(MainDataStruct.port);
	console.log(MainDataStruct.HTTP_location);
	console.log('port:'+MainDataStruct.port);
	console.log(process.env.NODE_ENV+' mode running');
	console.log("New Log Entry ==== "+conv.convertUnixToMySQL(Math.floor(new Date().getTime()/1000)));
	return MainDataStruct;
}).catch(function(err){
	if(util.isError(err))
	{
		console.log(err.message);
	}else{
		throw err;
	}
});