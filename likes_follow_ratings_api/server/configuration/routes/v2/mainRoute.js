const test = require(__dirname+'/testRoute');
const healthcheck = require(__dirname+'/healthcheck');
const likes = require(__dirname+'/client/likeRoutes');
const follow = require(__dirname+'/client/followRoutes');
const moods = require(__dirname+'/client/moodRoutes');


var mainRoute = function(dataStruct)
{
	dataStruct = likes.configRoutes(dataStruct);
	dataStruct = follow.configRoutes(dataStruct);
	dataStruct = moods.configRoutes(dataStruct);
	dataStruct = healthcheck.configRoutes(dataStruct);
	dataStruct = test.configRoutes(dataStruct);
	
	// REGISTER OUR ROUTES -------------------------------
	// all of our routes will be from root /
	if(process.env.NODE_ENV.toLowerCase()==='development' || process.env.NODE_ENV.toLowerCase()==='local')
	{
		console.log('registering route for '+process.env.NODE_ENV.toLowerCase())
		dataStruct.app.use('/', dataStruct.router);
	}else{
		console.log('registering route for '+process.env.NODE_ENV.toLowerCase())
		dataStruct.app.use('/api/', dataStruct.router);
	}
	return dataStruct;
}

module.exports = {
	mainRoute:mainRoute
};