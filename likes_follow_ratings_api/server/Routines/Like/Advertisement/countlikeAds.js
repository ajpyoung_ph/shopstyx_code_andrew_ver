
const respond = require(GLOBAL.server_location+'/EventListeners/errorListener');
const start = function(dataJSON,dataStruct,req,res)
{
	var query = {
        "ads_id":dataJSON.request.styx_id,
        "type" : "like ads summary"
    };
    GLOBAL.mongodb.likeRecords.findOne(query,function(err,doc){
    	if(err==null)
    	{
    		var msg = {
                "success":true, "status":"Success",
                "desc":"[countlikeAds:start]Found Record",
                "data":[]
            };
    		if(doc!=null)
    		 {
    		 	msg.data.push(doc);
    		 }else{
    		 	msg.desc="[countlikeAds:start]NO Found Record"
    		 }
            
            respond.respondError(msg,res,req);
    	}else{
    		 var msg = {
                "success":false, "status":"Error",
                "desc":"[countlikeAds:start]Error in DB",
                "err":err
            };
            respond.respondError(msg,res,req);
    	}
        
    });
};

module.exports={
	start:start
};