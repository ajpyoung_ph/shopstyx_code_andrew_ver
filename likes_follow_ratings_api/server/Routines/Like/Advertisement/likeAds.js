const respond = require(GLOBAL.server_location+'/EventListeners/errorListener');
const reportMsg = require('../../ReportMsg/reportMsg');
/*
{ 
    "_id" : ObjectId("56a0724f1e30590545825b40"), 
    "ads_id" : String, 
    "user_id" : NumberInt(1), 
    "type" : "like ads history", 
    "date_simple" : "2016-01-21 13:53:18", 
    "date_unix" : NumberInt(1453355598)
}
{ 
    "_id" : ObjectId("56a0724f1e30590545825b41"), 
    "ads_id" : String,
    "type" : "like ads summary", 
    "date_simple" : "2016-01-21 13:53:18", 
    "date_unix" : NumberInt(1453355598),
    "current_count":NumberInt(5)
}

"request":{
        "type":"Like"/"Unlike",
        "owner_id":<int>,(id of the user liking)
        "styx_id":"uuid_v1" <string>
    }

*/
var start = function(dataJSON,dataStruct,req,res)
{
	//check if the person has already liked this
	var query = {
		'ads_id':dataJSON.request.styx_id,
		'user_id':parseInt(dataJSON.request.owner_id),
		'type' : "like ads history"
	};
	GLOBAL.mongodb.likeRecords.findOne(query,function(err,doc){
		if(err==null)
		{
			dataJSON['reportForm']=[];
            dataJSON['reportForm']['type']=[];
            dataJSON['reportForm']['action']=[];
            dataJSON['reportForm']['to']=[];
            dataJSON.reportForm.type='profile';
            dataJSON.reportForm.action='like styx';
            
			if(doc==null)
			{
				//meaning no records found
				//then insert like product history
				insertLikeHistory(dataJSON,dataStruct,req,res);
				//upsert like product summary
				upsertLikeSummary(dataJSON,dataStruct,req,res);
			}else{
				//record found and reply success
				var msg = {
					"success":true, "status":"Success",
					"desc":"[likeAds:start]This ad is already liked by the user"
				};
				respond.respondError(msg,res,req);
				reportMsg.findAdOwner(doc,dataJSON,dataStruct);
			}
			//dataStruct.new_unix_timestamp
			//dataStruct.new_simple_timestamp
		}else{
			var msg = {
				"success":false, "status":"Error",
				"desc":"[likeAds:start]Error in DB",
				"err":err
			};
			respond.respondError(msg,res,req);
		}
		
	});
};
/*
var msg = {
					"success":true, "status":"Success",
					"desc":"[likeAds:start]Like is successful"
				};
				respond.respondError(msg,res,req);
*/
function insertLikeHistory(dataJSON,dataStruct,req,res)
{
	//dataStruct.new_unix_timestamp
	//dataStruct.new_simple_timestamp
	var query = {
		"ads_id" : dataJSON.request.styx_id, 
	    "user_id" : parseInt(dataJSON.request.owner_id), 
	    "type" : "like ads history", 
	    "date_simple" : dataStruct.new_unix_timestamp.toString(), 
	    "date_unix" : dataStruct.new_simple_timestamp
	};
	/*
	"_id" : ObjectId("56a0724f1e30590545825b40"), 
    "ads_id" : String, 
    "user_id" : NumberInt(1), 
    "type" : "like ads history", 
    "date_simple" : "2016-01-21 13:53:18", 
    "date_unix" : NumberInt(1453355598)
	*/
	GLOBAL.mongodb.likeRecords.insert(query,function(err,retVal){
		if(err!=null)
		{
			var msg = {
				"success":false, "status":"Error",
				"desc":"[likeAds:insertLikeHistory]Error in DB",
				"err":err
			};
			respond.respondError(msg,res,req);
			reportMsg.findAdOwner(query,dataJSON,dataStruct)
			//reportMsg.getRobotApiKey(query,dataJSON,dataStruct);
		}
		
	});
}

function upsertLikeSummary(dataJSON,dataStruct,req,res)
{
	/*
	"_id" : ObjectId("56a0724f1e30590545825b41"), 
    "ads_id" : String,
    "type" : "like ads summary", 
    "date_simple" : "2016-01-21 13:53:18", 
    "date_unix" : NumberInt(1453355598),
    "current_count":NumberInt(5)
	*/
	var query = {
		"ads_id":dataJSON.request.styx_id,
   		"type" : "like ads summary"
	};
	var options = {
		'upsert': true,
		'multi': false
	};
	var updatedSet = {
		"product_id":parseInt(dataJSON.request.product_id),
   		"type" : "like product summary",
   		"date_simple" : dataStruct.new_unix_timestamp.toString(), 
	    "date_unix" : dataStruct.new_simple_timestamp
	};
	var updateQ = {
		'$set':updatedSet,
		'$inc':{'current_count':1}
	}
	GLOBAL.mongodb.likeRecords.update(query,updateQ,options,function(err,retVal){
		if(err!=null)
		{
			var msg = {
				"success":false, "status":"Error",
				"desc":"[likeAds:insertLikeHistory]Error in DB",
				"err":err
			};
			respond.respondError(msg,res,req);
		}else{
			var msg = {
				"success":true, "status":"Success",
				"desc":"[likeAds:insertLikeHistory]liked"
			};
			respond.respondError(msg,res,req);
		}
		
	});
}

module.exports={
	start:start
};