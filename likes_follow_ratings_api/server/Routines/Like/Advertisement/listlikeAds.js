
const respond = require(GLOBAL.server_location+'/EventListeners/errorListener');
const start = function (dataJSON,dataStruct,req,res) 
{
	var query = {
        'ads_id':dataJSON.request.styx_id,
        "type" : "like ads history"
    };
    
	var skip = parseInt(dataJSON.request.number_items) * parseInt(dataJSON.request.page_number);
	var limit =  parseInt(dataJSON.request.number_items);
	GLOBAL.mongodb.likeRecords.find(query).sort({"date_unix":-1}).limit(limit).skip(skip,function(err,docs){
		if(err==null)
		{
			var msg = {
                "success":true, "status":"Success",
                "desc":"[listlikeAds:start]Records Found",
                "data":[]
            };
            if(docs.length>0)
            {
            	msg.data=docs;
            }else{
            	msg.desc="[listlikeAds:start]NO Records Found";
            }
            respond.respondError(msg,res,req);
		}else{
			var msg = {
                "success":false, "status":"Error",
                "desc":"[listlikeAds:start]Error in DB",
                "err":err
            };
            respond.respondError(msg,res,req);
		}
        
	});
};

module.exports={
	start:start
};