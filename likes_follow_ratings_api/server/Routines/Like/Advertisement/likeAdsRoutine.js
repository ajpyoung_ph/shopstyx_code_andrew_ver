
const respond = require(GLOBAL.server_location+'/EventListeners/errorListener');
const parseDataJSON = require(GLOBAL.server_location+'/helpers/parseDataJSON');
const likeAds = require(__dirname+'/likeAds');
const unlikeAds = require(__dirname+'/unlikeAds');
const listlikeAds = require(__dirname+'/listlikeAds');
const isliked = require(__dirname+'/isliked');
const countlikeAds = require(__dirname+'/countlikeAds');
const util = require('util');
/*
{ 
    "_id" : ObjectId("56a0724f1e30590545825b40"), 
    "ads_id" : String, 
    "user_id" : NumberInt(1), 
    "type" : "like ads history", 
    "date_simple" : "2016-01-21 13:53:18", 
    "date_unix" : NumberInt(1453355598)
}
{ 
    "_id" : ObjectId("56a0724f1e30590545825b41"), 
    "ads_id" : String,
    "type" : "like ads summary", 
    "date_simple" : "2016-01-21 13:53:18", 
    "date_unix" : NumberInt(1453355598),
    "current_count":NumberInt(5)
}

"request":{
        "type":"Like"/"Unlike",
        "owner_id":<int>,(id of the user liking)
        "styx_id":"uuid_v1" <string>
    }

*/

var processRequest = function(dataJSON,dataStruct,req,res)
{
	var error=false;
	var error_mgs = [];
	if(typeof(dataJSON.request)=='undefined')
	{
		error = true;
		error_mgs.push('no request found');
	}
	if(typeof(dataJSON.request)!='undefined')
	{
		if(typeof(dataJSON.request.type)=='undefined')
		{
			error=true;
			error_mgs.push('no type request found');
		}
		if(typeof(dataJSON.request.owner_id)=='undefined')
		{
			 try{
                if(dataJSON.request.type.toLowerCase().replace(/\s/g, '')!='list' && dataJSON.request.type.toLowerCase().replace(/\s/g, '')!='count')
                {
                    error=true;
                    error_mgs.push('no id of target user found');
                }
            }catch(error_type){
                error=true;
                error_mgs.push('no type request found');
            }
		}else{
			if(isNaN(dataJSON.request.owner_id)==true)
			{
				error=true;
				error_msg.push('Owner ID must be a number');
			}else{
				if(parseInt(dataJSON.request.owner_id<1))
				{
					error=true;
					error_msg.push('Owner must be a registered user');
				}
			}
			if(dataJSON.request.type.toLowerCase().replace(/\s/g, '')=='isliked')
            {
                 if(typeof(dataJSON.request.product_id)=='undefined')
                {
                    error=true;
                    error_mgs.push('no styx to query');
                }
            }
		}
		if(parseInt(dataJSON.security.user_id)<1)
		{
			error=true;
			error_msg.push('User must be logged in');
		}
		if(typeof(dataJSON.request.styx_id)=='undefined')
		{
			error=true;
			error_mgs.push('no advertisement request found');
		}
	}
	if(error==false)
	{
		switch(dataJSON.request.type.toLowerCase().replace(/\s/g, ''))
		{
			case 'like':
				likeAds.start(dataJSON,dataStruct,req,res);
				break;
			case 'unlike':
				unlikeAds.start(dataJSON,dataStruct,req,res);
				break;
			case 'list':
                listlikeAds.start(dataJSON,dataStruct,req,res);
                break;
            case 'count':
                countlikeAds.start(dataJSON,dataStruct,req,res);
                break;
            case 'isliked':
            	isliked.start(dataJSON,dataStruct,req,res);
            	break;
			default:
				var msg = {
					"success":false, "status":"Error",
					"desc":"[likeProdRoutine:processRequest]Unknown requirement type"
				};
				respond.respondError(msg,res,req);
				break;
		}
		
	}else{
		var msg = {
			"success":false, "status":"Error",
			"desc":"[likeProdRoutine:processRequest]Missing requirements",
			"err":error_mgs
		};
		respond.respondError(msg,res,req);
	}
};

module.exports={
	processRequest:processRequest
};