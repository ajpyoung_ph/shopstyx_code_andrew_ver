
const respond = require(GLOBAL.server_location+'/EventListeners/errorListener');
const mongojs = require('mongojs');
/*
{ 
    "_id" : ObjectId("56a0724f1e30590545825b40"), 
    "ads_id" : String, 
    "user_id" : NumberInt(1), 
    "type" : "like ads history", 
    "date_simple" : "2016-01-21 13:53:18", 
    "date_unix" : NumberInt(1453355598)
}
{ 
    "_id" : ObjectId("56a0724f1e30590545825b41"), 
    "ads_id" : String,
    "type" : "like ads summary", 
    "date_simple" : "2016-01-21 13:53:18", 
    "date_unix" : NumberInt(1453355598),
    "current_count":NumberInt(5)
}

"request":{
        "type":"Like"/"Unlike",
        "owner_id":<int>,(id of the user liking)
        "styx_id":"uuid_v1" <string>
    }

*/
var start = function(dataJSON,dataStruct,req,res)
{
	//check if the person has already liked this
	var query = {
		'ads_id':dataJSON.request.styx_id,
		'user_id':parseInt(dataJSON.request.owner_id),
		'type' : "like ads history"
	};
	GLOBAL.mongodb.likeRecords.findOne(query,function(err,doc){
		if(err==null)
		{
			if(doc==null)
			{
				//meaning no records found
				//reply success
				var msg = {
					"success":true, "status":"Success",
					"desc":"[unlikeAds:start]This advertisement is cuurently NOT liked by the user"
				};
				respond.respondError(msg,res,req);
			}else{
				//record found 
				//then delete like product history
				deleteLikeHistory(dataJSON,dataStruct,doc,req,res);
				//upsert unlike product summary
				upsertLikeSummary(dataJSON,dataStruct,req,res);
			}
			//dataStruct.new_unix_timestamp
			//dataStruct.new_simple_timestamp
		}else{
			var msg = {
				"success":false, "status":"Error",
				"desc":"[unlikeAds:start]Error in DB",
				"err":err
			};
			respond.respondError(msg,res,req);
		}
		
	});
};
/*
var msg = {
					"success":true, "status":"Success",
					"desc":"[unlikeAds:start]Like is successful"
				};
				respond.respondError(msg,res,req);
*/

function deleteLikeHistory(dataJSON,dataStruct,record,req,res)
{
	//dataStruct.new_unix_timestamp
	//dataStruct.new_simple_timestamp
	var query = {
		'_id':mongojs.ObjectId((record._id).toString())
	};
	var options = {
		justOne: true
	};
	GLOBAL.mongodb.likeRecords.remove(query,options,function(err,retVal){
		if(err!=null)
		{
			var msg = {
				"success":false, "status":"Error",
				"desc":"[unlikeAds:insertLikeHistory]Error in DB",
				"err":err
			};
			respond.respondError(msg,res,req);
		}
	});
}

function upsertLikeSummary(dataJSON,dataStruct,req,res)
{
	/*
	"_id" : ObjectId("56a0724f1e30590545825b41"), 
    "ads_id" : String,
    "type" : "like ads summary", 
    "date_simple" : "2016-01-21 13:53:18", 
    "date_unix" : NumberInt(1453355598),
    "current_count":NumberInt(5)
	*/
	var query = {
		"ads_id":dataJSON.request.styx_id,
   		"type" : "like ads summary"
	};
	var options = {
		'upsert': true,
		'multi': false
	};
	var updatedSet = {
		"ads_id":dataJSON.request.styx_id,
   		"type" : "like ads summary",
   		"date_simple" : dataStruct.new_unix_timestamp.toString(), 
	    "date_unix" : dataStruct.new_simple_timestamp
	};
	var updateQ = {
		'$set':updatedSet,
		'$inc':{'current_count':-1}
	}
	GLOBAL.mongodb.likeRecords.update(query,updateQ,options,function(err,retVal){
		if(err!=null)
		{
			var msg = {
				"success":false, "status":"Error",
				"desc":"[unlikeAds:insertLikeHistory]Error in DB",
				"err":err
			};
			respond.respondError(msg,res,req);
		}else{
			var msg = {
				"success":true, "status":"Success",
				"desc":"[unlikeAds:insertLikeHistory]Unliked"
			};
			respond.respondError(msg,res,req);
		}
	});
}

module.exports={
	start:start
};