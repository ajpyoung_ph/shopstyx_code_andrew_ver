const respond = require(GLOBAL.server_location+'/EventListeners/errorListener');
const reportMsg = require('../../ReportMsg/reportMsg');
/*
{ 
    "_id" : ObjectId("56a0724f1e30590545825b40"), 
    "product_id" : NumberInt(1), 
    "user_id" : NumberInt(1), 
    "type" : "like product history", 
    "date_simple" : "2016-01-21 13:53:18", 
    "date_unix" : NumberInt(1453355598)
}
{ 
    "_id" : ObjectId("56a0724f1e30590545825b41"), 
    "product_id":NumberInt(1),
    "type" : "like product summary", 
    "date_simple" : "2016-01-21 13:53:18", 
    "date_unix" : NumberInt(1453355598),
    "current_count":NumberInt(5)
}

"request":{
        "type":"Like"/"Unlike",
        "owner_id":<int>,(id of the user liking)
        "product_id":<int>
    }

*/
var start = function(dataJSON,dataStruct,req,res)
{
	//check if the person has already liked this
	var query = {
		'product_id':parseInt(dataJSON.request.product_id),
		'user_id':parseInt(dataJSON.request.owner_id),
		'type' : "like product history"
	};
	GLOBAL.mongodb.likeRecords.findOne(query,function(err,doc){
		if(err==null)
		{
			dataJSON['reportForm']=[];
            dataJSON['reportForm']['type']=[];
            dataJSON['reportForm']['action']=[];
            dataJSON['reportForm']['to']=[];
            dataJSON.reportForm.type='profile';
            dataJSON.reportForm.action='like product';
			if(doc==null)
			{
				//meaning no records found
				//then insert like product history
				insertLikeHistory(dataJSON,dataStruct,req,res);
				//upsert like product summary
				upsertLikeSummary(dataJSON,dataStruct,req,res);
			}else{
				//record found and reply success
				var msg = {
					"success":true, "status":"Success",
					"desc":"[likeProducts:start]This product is already liked by the user"
				};
				respond.respondError(msg,res,req);
				reportMsg.findProdOwner(doc,dataJSON,dataStruct);
				//reportMsg.getRobotApiKey(doc,dataJSON,dataStruct);
			}
			//dataStruct.new_unix_timestamp
			//dataStruct.new_simple_timestamp
		}else{
			var msg = {
				"success":false, "status":"Error",
				"desc":"[likeProducts:start]Error in DB",
				"err":err
			};
			respond.respondError(msg,res,req);
		}
		
	});
};
/*
var msg = {
					"success":true, "status":"Success",
					"desc":"[likeProducts:start]Like is successful"
				};
				respond.respondError(msg,res,req);
*/

function insertLikeHistory(dataJSON,dataStruct,req,res)
{
	//dataStruct.new_unix_timestamp
	//dataStruct.new_simple_timestamp
	var query = {
		"product_id" : parseInt(dataJSON.request.product_id), 
	    "user_id" : parseInt(dataJSON.request.owner_id), 
	    "type" : "like product history", 
	    "date_simple" : dataStruct.new_unix_timestamp.toString(), 
	    "date_unix" : dataStruct.new_simple_timestamp
	};
	GLOBAL.mongodb.likeRecords.insert(query,function(err,retVal){
		if(err!=null)
		{
			var msg = {
				"success":false, "status":"Error",
				"desc":"[likeProducts:insertLikeHistory]Error in DB",
				"err":err
			};
			respond.respondError(msg,res,req);
			reportMsg.findProdOwner(query,dataJSON,dataStruct);
		}
		
	});
}

function upsertLikeSummary(dataJSON,dataStruct,req,res)
{
	var query = {
		"product_id":parseInt(dataJSON.request.product_id),
   		"type" : "like product summary"
	};
	var options = {
		'upsert': true,
		'multi': false
	};
	var updatedSet = {
		"product_id":parseInt(dataJSON.request.product_id),
   		"type" : "like product summary",
   		"date_simple" : dataStruct.new_unix_timestamp.toString(), 
	    "date_unix" : dataStruct.new_simple_timestamp
	};
	var updateQ = {
		'$set':updatedSet,
		'$inc':{'current_count':1}
	}
	GLOBAL.mongodb.likeRecords.update(query,updateQ,options,function(err,retVal){
		if(err!=null)
		{
			var msg = {
				"success":false, "status":"Error",
				"desc":"[likeProducts:insertLikeHistory]Error in DB",
				"err":err
			};
			respond.respondError(msg,res,req);
		}else{
			var msg = {
				"success":true, "status":"Success",
				"desc":"[likeProducts:insertLikeHistory]liked"
			};
			respond.respondError(msg,res,req);
		}
		
	});
}

module.exports={
	start:start
};