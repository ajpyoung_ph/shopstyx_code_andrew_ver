
const respond = require(GLOBAL.server_location+'/EventListeners/errorListener');
const mongojs = require('mongojs');
/*
{ 
    "_id" : ObjectId("56a0724f1e30590545825b40"), 
    "product_id" : NumberInt(1), 
    "user_id" : NumberInt(1), 
    "type" : "like product history", 
    "date_simple" : "2016-01-21 13:53:18", 
    "date_unix" : NumberInt(1453355598)
}
{ 
    "_id" : ObjectId("56a0724f1e30590545825b41"), 
    "product_id":NumberInt(1),
    "type" : "like product summary", 
    "date_simple" : "2016-01-21 13:53:18", 
    "date_unix" : NumberInt(1453355598),
    "current_count":NumberInt(5)
}

"request":{
        "type":"Like"/"Unlike",
        "owner_id":<int>,(id of the user liking)
        "product_id":<int>
    }

*/
var start = function(dataJSON,dataStruct,req,res)
{
	//check if the person has already liked this
	var query = {
		'product_id':parseInt(dataJSON.request.product_id),
		'user_id':parseInt(dataJSON.request.owner_id),
		'type' : "like product history"
	};
	GLOBAL.mongodb.likeRecords.findOne(query,function(err,doc){
		if(err==null)
		{
			if(doc==null)
			{
				//meaning no records found
				//reply success
				var msg = {
					"success":true, "status":"Success",
					"desc":"[ununlikeProduct:start]This product is cuurently NOT liked by the user"
				};
				respond.respondError(msg,res,req);
			}else{
				//record found 
				//then delete like product history
				deleteLikeHistory(dataJSON,dataStruct,doc,req,res);
				//upsert unlike product summary
				upsertLikeSummary(dataJSON,dataStruct,req,res);
			}
			//dataStruct.new_unix_timestamp
			//dataStruct.new_simple_timestamp
		}else{
			var msg = {
				"success":false, "status":"Error",
				"desc":"[ununlikeProduct:start]Error in DB",
				"err":err
			};
			respond.respondError(msg,res,req);
		}
		
	});
};
/*
var msg = {
					"success":true, "status":"Success",
					"desc":"[ununlikeProduct:start]Like is successful"
				};
				respond.respondError(msg,res,req);
*/

function deleteLikeHistory(dataJSON,dataStruct,record,req,res)
{
	//dataStruct.new_unix_timestamp
	//dataStruct.new_simple_timestamp
	var query = {
		'_id':mongojs.ObjectId((record._id).toString())
	};
	var options = {
		justOne: true
	};
	GLOBAL.mongodb.likeRecords.remove(query,options,function(err,retVal){
		if(err!=null)
		{
			var msg = {
				"success":false, "status":"Error",
				"desc":"[ununlikeProduct:insertLikeHistory]Error in DB",
				"err":err
			};
			respond.respondError(msg,res,req);
		}
	});
}

function upsertLikeSummary(dataJSON,dataStruct,req,res)
{
	var query = {
		"product_id":parseInt(dataJSON.request.product_id),
   		"type" : "like product summary"
	};
	var options = {
		'upsert': true,
		'multi': false
	};
	var updatedSet = {
		"product_id":parseInt(dataJSON.request.product_id),
   		"type" : "like product summary",
   		"date_simple" : dataStruct.new_unix_timestamp.toString(), 
	    "date_unix" : dataStruct.new_simple_timestamp
	};
	var updateQ = {
		'$set':updatedSet,
		'$inc':{'current_count':-1}
	}
	GLOBAL.mongodb.likeRecords.update(query,updateQ,options,function(err,retVal){
		if(err!=null)
		{
			var msg = {
				"success":false, "status":"Error",
				"desc":"[ununlikeProduct:insertLikeHistory]Error in DB",
				"err":err
			};
			respond.respondError(msg,res,req);
		}else{
			var msg = {
				"success":true, "status":"Success",
				"desc":"[ununlikeProduct:insertLikeHistory]unliked"
			};
			respond.respondError(msg,res,req);
		}
		
	});
}

module.exports={
	start:start
};