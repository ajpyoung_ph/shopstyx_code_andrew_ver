
const respond = require(GLOBAL.server_location+'/EventListeners/errorListener');
const start = function(dataJSON,dataStruct,req,res)
{
	var query = {
        "product_id":dataJSON.request.product_id,
        "type" : "like product summary"
    };
    GLOBAL.mongodb.likeRecords.findOne(query,function(err,doc){
    	if(err==null)
    	{
    		var msg = {
                "success":true, "status":"Success",
                "desc":"[countlikeProduct:start]Found Record",
                "data":[]
            };
    		if(doc!=null)
    		 {
    		 	msg.data.push(doc);
    		 }else{
    		 	msg.desc="[countlikeProduct:start]NO Found Record"
    		 }
            
            respond.respondError(msg,res,req);
    	}else{
    		 var msg = {
                "success":false, "status":"Error",
                "desc":"[countlikeProduct:start]Error in DB",
                "err":err
            };
            respond.respondError(msg,res,req);
    	}
        
    });
};

module.exports={
	start:start
};