
const respond = require(GLOBAL.server_location+'/EventListeners/errorListener');
/*
{ 
    "_id" : ObjectId("56a0724f1e30590545825b40"), 
    "product_id" : NumberInt(1), 
    "user_id" : NumberInt(1), 
    "type" : "like product history", 
    "date_simple" : "2016-01-21 13:53:18", 
    "date_unix" : NumberInt(1453355598)
}
*/
var start = function(dataJSON,dataStruct,req,res)
{
	//check if the person has already liked this
	var query = {
		'product_id':parseInt(dataJSON.request.product_id),
		'user_id':parseInt(dataJSON.request.owner_id),
		'type' : "like product history"
	};
	GLOBAL.mongodb.likeRecords.findOne(query,function(err,doc){
		if(err==null)
		{
			if(doc==null)
			{
				var msg = {
					"success":true, "status":"Success",
					"desc":"[isliked:start]This product NOT already liked by the user",
					"data":false
				};
				respond.respondError(msg,res,req);
			}else{
				//record found and reply success
				var msg = {
					"success":true, "status":"Success",
					"desc":"[isliked:start]This product is already liked by the user",
					"data":true
				};
				respond.respondError(msg,res,req);
			}
			//dataStruct.new_unix_timestamp
			//dataStruct.new_simple_timestamp
		}else{
			var msg = {
				"success":false, "status":"Error",
				"desc":"[isliked:start]Error in DB",
				"err":err
			};
			respond.respondError(msg,res,req);
		}
		
	});
};

module.exports={
	start:start
};