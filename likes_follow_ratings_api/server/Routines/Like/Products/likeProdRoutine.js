
const respond = require(GLOBAL.server_location+'/EventListeners/errorListener');
const parseDataJSON = require(GLOBAL.server_location+'/helpers/parseDataJSON');
const likeProduct = require(__dirname+'/likeProduct');
const unlikeProduct = require(__dirname+'/unlikeProduct');
const listLikeProduct = require(__dirname+'/listLikeProduct');
const isliked = require(__dirname+'/isliked');
const countLikeProduct = require(__dirname+'/countLikeProduct');
const util = require('util');
/*
{ 
    "_id" : ObjectId("56a0724f1e30590545825b40"), 
    "product_id" : NumberInt(1), 
    "user_id" : NumberInt(1), 
    "type" : "like product history", 
    "date_simple" : "2016-01-21 13:53:18", 
    "date_unix" : NumberInt(1453355598)
}
{ 
    "_id" : ObjectId("56a0724f1e30590545825b41"), 
    "product_id":NumberInt(1),
    "type" : "like product summary", 
    "date_simple" : "2016-01-21 13:53:18", 
    "date_unix" : NumberInt(1453355598),
    "current_count":NumberInt(5)
}

"request":{
        "type":"Like"/"Unlike",
        "owner_id":<int>,(id of the user liking)
        "product_id":<int>
    }

*/

var processRequest = function(dataJSON,dataStruct,req,res)
{
	var error=false;
	var error_mgs = [];
	if(typeof(dataJSON.request)=='undefined')
	{
		error = true;
		error_mgs.push('no request found');
	}
	if(typeof(dataJSON.request)!='undefined')
	{
		if(typeof(dataJSON.request.type)=='undefined')
		{
			error=true;
			error_mgs.push('no type request found');
		}
		if(typeof(dataJSON.request.owner_id)=='undefined')
		{
			try{
                if(dataJSON.request.type.toLowerCase().replace(/\s/g, '')!='list' && dataJSON.request.type.toLowerCase().replace(/\s/g, '')!='count')
                {
                    error=true;
                    error_mgs.push('no id of target user found');
                }
            }catch(error_type){
                error=true;
                error_mgs.push('no type request found');
            }
		}else{
			if(isNaN(dataJSON.request.owner_id)==true)
			{
				error=true;
				error_msg.push('Owner ID must be a number');
			}else{
				if(parseInt(dataJSON.request.owner_id<1))
				{
					error=true;
					error_msg.push('Owner must be a registered user');
				}
			}
			if(dataJSON.request.type.toLowerCase().replace(/\s/g, '')=='isliked')
            {
                 if(typeof(dataJSON.request.product_id)=='undefined')
                {
                    error=true;
                    error_mgs.push('no product to query');
                }
            }
		}
		if(parseInt(dataJSON.security.user_id)<1)
		{
			error=true;
			error_msg.push('User must be logged in');
		}
		if(typeof(dataJSON.request.product_id)=='undefined')
		{
			error=true;
			error_mgs.push('no product request found');
		}
	}
	if(error==false)
	{
		switch(dataJSON.request.type.toLowerCase().replace(/\s/g, ''))
		{
			case 'like':
				likeProduct.start(dataJSON,dataStruct,req,res);
				break;
			case 'unlike':
				unlikeProduct.start(dataJSON,dataStruct,req,res);
				break;
			case 'list':
                listLikeProduct.start(dataJSON,dataStruct,req,res);
                break;
            case 'count':
                countLikeProduct.start(dataJSON,dataStruct,req,res);
                break;
            case 'isliked':
            	isliked.start(dataJSON,dataStruct,req,res);
            	break;
			default:
				var msg = {
					"success":false, "status":"Error",
					"desc":"[likeProdRoutine:processRequest]Unknown requirement type"
				};
				respond.respondError(msg,res,req);
				break;
		}
		
	}else{
		var msg = {
			"success":false, "status":"Error",
			"desc":"[likeProdRoutine:processRequest]Missing requirements",
			"err":error_mgs
		};
		respond.respondError(msg,res,req);
	}
};

module.exports={
	processRequest:processRequest
};