
const respond = require(GLOBAL.server_location+'/EventListeners/errorListener');
/*
{ 
    "_id" : ObjectId("56a0724f1e30590545825b40"), 
    "styx_id" : String, 
    "user_id" : NumberInt(1), 
    "type" : "mood ads history",
    "mood_id":Int,
    "date_simple" : "2016-01-21 13:53:18", 
    "date_unix" : NumberInt(1453355598)
}
{ 
    "_id" : ObjectId("56a0724f1e30590545825b41"), 
    "styx_id" : String,
    "type" : "mood ads summary",
    "mood_id":Int,
    "current_count":NumberInt(5),
    "date_simple" : "2016-01-21 13:53:18", 
    "date_unix" : NumberInt(1453355598)
}


{ 
    "_id" : ObjectId("56a0724f1e30590545825b40"), 
    "product_id" : Int, 
    "user_id" : NumberInt(1), 
    "type" : "mood product history",
    "mood_id": Int, 
    "date_simple" : "2016-01-21 13:53:18", 
    "date_unix" : NumberInt(1453355598)
}
{ 
    "_id" : ObjectId("56a0724f1e30590545825b41"), 
    "product_id" : Int, 
    "type" : "mood product summary",
    "mood_id":int,
    "current_count":NumberInt(5),
    "date_simple" : "2016-01-21 13:53:18", 
    "date_unix" : NumberInt(1453355598)
}


 "request":{
        "type":"Save Mood Product"/"Save Mood Advertisement"/"Read Mood count"/"Read Mood List",
        (if Mood Product,Mood Count)
        "product_id":<int>,
        (if Mood Advertisement, Mood Count)
        "styx_id":"uuid_v1", <string>
        "mood_id":<int>, (required for both Mood Product and Mood Advertisement)
    }
*/

const start = function(dataJSON,dataStruct,req,res)
{
	if(typeof(dataJSON.request.styx_id)!='undefined')
	{
		getAdvertList(dataJSON,dataStruct,req,res);
	}else if(typeof(dataJSON.request.product_id)!='undefined')
	{
		getProdList(dataJSON,dataStruct,req,res);
	}
};

function getAdvertList(dataJSON,dataStruct,req,res)
{
	var skip = parseInt(dataJSON.request.number_items) * parseInt(dataJSON.request.page_number);
	var limit =  parseInt(dataJSON.request.number_items);
	var SummaryQuery = {
		"styx_id" : dataJSON.request.styx_id,
	    "type" : "mood ads summary",
	    "mood_id": parseInt(dataJSON.request.mood_id)
	};
	var HistoryQuery={
		"styx_id" : dataJSON.request.styx_id,
	    "type" : "mood ads history",
	    "mood_id":parseInt(dataJSON.request.mood_id)
	};
	GLOBAL.mongodb.moodRecords.findOne(SummaryQuery,function(err,doc){
		if(err==null)
		{
			var msg = {
                "success":true, "status":"Success",
                "desc":"[readMoodList:start]Records Found",
                "data":[]
            };
            if(doc!=null)
            {
            	msg.data['Summary']=doc;
            }else{
            	msg.data['Summary']={
				    "styx_id" : dataJSON.request.styx_id,
				    "type" : "mood ads summary",
				    "mood_id":parseInt(dataJSON.request.mood_id),
				    "current_count":0
				};
            }
            GLOBAL.mongodb.moodRecords.find(HistoryQuery).sort({"date_unix":-1}).limit(limit).skip(skip,function(err,docs){
				if(err==null)
				{
		            if(docs.length>0)
		            {
		            	msg.data['List']=docs;
		            }else{
		            	msg.data['List']=[];
		            }
		            respond.respondError(msg,res,req);
				}else{
					var msg = {
		                "success":false, "status":"Error",
		                "desc":"[readMoodList:start]Error in DB",
		                "err":err
		            };
		            respond.respondError(msg,res,req);
				}
				
			});
		}else{
			var msg = {
                "success":false, "status":"Error",
                "desc":"[readMoodList:start]Error in DB",
                "err":err
            };
            respond.respondError(msg,res,req);
		}
	});
}

function getProdList(dataJSON,dataStruct,req,res)
{
	var skip = parseInt(dataJSON.request.number_items) * parseInt(dataJSON.request.page_number);
	var limit =  parseInt(dataJSON.request.number_items);
	var SummaryQuery = {
		"product_id" : dataJSON.request.product_id,
	    "type" : "mood product_id summary",
	    "mood_id": parseInt(dataJSON.request.mood_id)
	};
	var HistoryQuery={
		"product_id" : dataJSON.request.product_id,
	    "type" : "mood product_id history",
	    "mood_id":parseInt(dataJSON.request.mood_id)
	};
	GLOBAL.mongodb.followRecords.findOne(SummaryQuery,function(err,doc){
		if(err==null)
		{
			var msg = {
                "success":true, "status":"Success",
                "desc":"[readMoodList:start]Records Found",
                "data":[]
            };
            if(doc!=null)
            {
            	msg.data['Summary']=doc;
            }else{
            	msg.data['Summary']={
				    "product_id" : dataJSON.request.product_id,
				    "type" : "mood product_id summary",
				    "mood_id":parseInt(dataJSON.request.mood_id),
				    "current_count":0
				};
            }
            GLOBAL.mongodb.followRecords.find(HistoryQuery).sort({"date_unix":-1}).limit(limit).skip(skip,function(err,docs){
				if(err==null)
				{
		            if(docs.length>0)
		            {
		            	msg.data['List']=docs;
		            }else{
		            	msg.data['List']=[];
		            }
		            respond.respondError(msg,res,req);
				}else{
					var msg = {
		                "success":false, "status":"Error",
		                "desc":"[readMoodList:start]Error in DB",
		                "err":err
		            };
		            respond.respondError(msg,res,req);
				}
				
			});
		}else{
			var msg = {
                "success":false, "status":"Error",
                "desc":"[readMoodList:start]Error in DB",
                "err":err
            };
            respond.respondError(msg,res,req);
		}
	});
}

module.exports={
	start:start
};