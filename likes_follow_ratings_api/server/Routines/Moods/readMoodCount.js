
const respond = require(GLOBAL.server_location+'/EventListeners/errorListener');
const start = function(dataJSON,dataStruct,req,res)
{
	if(typeof(dataJSON.request.styx_id)!='undefined')
	{
		getAdvertsCounts(dataJSON,dataStruct,req,res);
	}else if(typeof(dataJSON.request.product_id)!='undefined')
	{
		getProductCounts(dataJSON,dataStruct,req,res);
	}
};

function getAdvertsCounts(dataJSON,dataStruct,req,res)
{
	var query = {
		"styx_id":parseInt(dataJSON.request.styx_id),
	    "type" :  "mood ads summary"
	};
	GLOBAL.mongodb.moodRecords.find(query,function(err,docs){
		if(err==null)
		{
			var msg = {
                "success":true, "status":"Success",
                "desc":"[readMoodCount:getAdvertsCount]Records Found",
                "data":docs
            };
            respond.respondError(msg,res,req);
		}else{
			var msg = {
                "success":false, "status":"Error",
                "desc":"[readMoodCount:getAdvertsCount]Error in DB",
                "err":err
            };
            respond.respondError(msg,res,req);
		}
		
	});
}

function getProductCounts(dataJSON,dataStruct,req,res)
{
	var query = {
		"product_id":parseInt(dataJSON.request.product_id),
	    "type" : "mood product history"
	};
	GLOBAL.mongodb.moodRecords.find(query,function(err,docs){
		if(err==null)
		{
			var msg = {
                "success":true, "status":"Success",
                "desc":"[readMoodCount:getProductCounts]Records Found",
                "data":docs
            };
            respond.respondError(msg,res,req);
		}else{
			var msg = {
                "success":false, "status":"Error",
                "desc":"[readMoodCount:getProductCounts]Error in DB",
                "err":err
            };
            respond.respondError(msg,res,req);
		}
		
	});
}

module.exports={
	start:start
};