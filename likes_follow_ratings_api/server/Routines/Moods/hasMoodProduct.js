
const respond = require(GLOBAL.server_location+'/EventListeners/errorListener');
/*
{ 
    "_id" : ObjectId("56a0724f1e30590545825b40"), 
    "styx_id" : String, 
    "user_id" : NumberInt(1), 
    "type" : "mood ads history",
    "mood_id":Int,
    "date_simple" : "2016-01-21 13:53:18", 
    "date_unix" : NumberInt(1453355598)
}
*/

const start = function(dataJSON,dataStruct,req,res)
{
	var query = {
		"styx_id":parseInt(dataJSON.request.styx_id),
		"user_id":parseInt(dataJSON.request.owner_id),
		"type" : "mood ads history"
	};
	GLOBAL.mongodb.moodRecords.findOne(query,function(err,doc){
		if(err==null)
		{
			if(doc==null)//so this is a new record
			{
				var msg = {
					"success":true, "status":"Success",
					"desc":"[hasMoodProduct:start]No Mood from user for this product",
					"data":0
				};
				respond.respondError(msg,res,req);
			}else{
				var msg = {
					"success":true, "status":"Success",
					"desc":"[hasMoodProduct:start]User has a mood for this product",
					"data":doc.mood_id
				};
				respond.respondError(msg,res,req);
			}
		}else{
			var msg = {
                "success":false, "status":"Error",
                "desc":"[hasMoodProduct:start]Error in DB",
                "err":err
            };
            respond.respondError(msg,res,req);
		}
		
	});
};

module.exports={
	start:start
};
