const reportMsg = require('../ReportMsg/reportMsg');
const respond = require(GLOBAL.server_location+'/EventListeners/errorListener');
const mongojs=require('mongojs');
/*
{ 
    "_id" : ObjectId("56a0724f1e30590545825b40"), 
    "product_id" : Int, 
    "user_id" : NumberInt(1), 
    "type" : "mood product history",
    "mood_id": Int, 
    "date_simple" : "2016-01-21 13:53:18", 
    "date_unix" : NumberInt(1453355598)
}
{ 
    "_id" : ObjectId("56a0724f1e30590545825b41"), 
    "product_id" : Int, 
    "type" : "mood product summary",
    "mood_id":int,
    "current_count":NumberInt(5),
    "date_simple" : "2016-01-21 13:53:18", 
    "date_unix" : NumberInt(1453355598)
}
"request":{
        "type":"Save Mood Product"/"Save Mood Advertisement"/"Read Mood count",
        (if Mood Product,Mood Count)
        "product_id":<int>,
        (if Mood Advertisement, Mood Count)
        "styx_id":"uuid_v1", <string>
        "mood_id":<int>, (required for both Mood Product and Mood Advertisement)
    }

*/

const start = function(dataJSON,dataStruct,req,res)
{
	//find if product already is mooded
	//if mooded then compare mood_ids
	//if the same then do nothing and put change flag from old_mood_id to new_mood_id
	//else update mood history
	//update mood summary always
	var query = {
		"product_id":parseInt(dataJSON.request.product_id),
		"user_id":parseInt(dataJSON.request.owner_id),
		"type" : "mood product history"
	};
	GLOBAL.mongodb.moodRecords.findOne(query,function(err,doc){
        dataJSON['reportForm']=[];
        dataJSON['reportForm']['type']=[];
        dataJSON['reportForm']['action']=[];
        dataJSON['reportForm']['to']=[];
        dataJSON.reportForm.type='profile';
        dataJSON.reportForm.action='mood product';
		if(err==null)
		{
			if(doc==null)//so this is a new record
			{
				saveNew(dataJSON,dataStruct,req,res);
				upsertSummary(dataJSON,dataStruct,req,res);
			}else{
				compareValue(dataJSON,dataStruct,doc,req,res);
                reportMsg.findProdOwner(doc,dataJSON,dataStruct);
                //reportMsg.getRobotApiKey(doc,dataJSON,dataStruct);
			}
		}else{
			var msg = {
                "success":false, "status":"Error",
                "desc":"[savemoodproduct:start]Error in DB",
                "err":err
            };
            respond.respondError(msg,res,req);
		}
        
	});
};

function saveNew(dataJSON,dataStruct,req,res)
{
	var query = {
	    "product_id" : parseInt(dataJSON.request.product_id), 
	    "user_id" : parseInt(dataJSON.request.owner_id), 
	    "type" : "mood product history",
	    "mood_id": parseInt(dataJSON.request.mood_id), 
        "date_simple" : dataStruct.new_unix_timestamp.toString(), 
        "date_unix" : dataStruct.new_simple_timestamp
	};
	GLOBAL.mongodb.moodRecords.insert(query,function(err,retVal){
		if(err==null)
		{
			console.log('Saved new mood record');
		}else{
			var msg = {
                "success":false, "status":"Error",
                "desc":"[savemoodproduct:saveNew]Error in DB",
                "err":err
            };
            respond.respondError(msg,res,req);
            reportMsg.findProdOwner(query,dataJSON,dataStruct);
            //reportMsg.getRobotApiKey(query,dataJSON,dataStruct);

		}
        
	});
}
function upsertSummary(dataJSON,dataStruct,req,res)
{
	/*
	{ 
	    "_id" : ObjectId("56a0724f1e30590545825b41"), 
	    "product_id" : Int, 
	    "type" : "mood product summary",
	    "mood_id":int,
	    "current_count":NumberInt(5),
	    "date_simple" : "2016-01-21 13:53:18", 
	    "date_unix" : NumberInt(1453355598)
	}
	*/
	var query = {
		"product_id":parseInt(dataJSON.request.product_id),
	    "type" : "mood product summary",
	    "mood_id":parseInt(dataJSON.request.mood_id)
	};
	var options = {
        'upsert': true,
        'multi': false
    };
    var updatedSet = {
        "product_id" : parseInt(dataJSON.request.product_id), 
	    "type" : "mood product summary",
	    "mood_id":parseInt(dataJSON.request.mood_id),
	    "date_simple" : dataStruct.new_unix_timestamp.toString(), 
        "date_unix" : dataStruct.new_simple_timestamp
    };
    var updateQ = {
        '$set':updatedSet,
        '$inc':{'current_count':1}
    };
    GLOBAL.mongodb.moodRecords.update(query,updateQ,options,function(err,retVal){
        if(err!=null)
        {
            var msg = {
                "success":false, "status":"Error",
                "desc":"[savemoodproduct:upsertSummary]Error in DB",
                "err":err
            };
            respond.respondError(msg,res,req);
        }else{
            var msg = {
                "success":true, "status":"Success",
                "desc":"[savemoodproduct:upsertSummary]Mooded"
            };
            respond.respondError(msg,res,req);
        }
        
    });
}

function compareValue(dataJSON,dataStruct,doc,req,res)
{
	/*
{ 
    "_id" : ObjectId("56a0724f1e30590545825b40"), 
    "product_id" : Int, 
    "user_id" : NumberInt(1), 
    "type" : "mood product history",
    "mood_id": Int, 
    "date_simple" : "2016-01-21 13:53:18", 
    "date_unix" : NumberInt(1453355598)
}
"request":{
        "type":"Save Mood Product"/"Save Mood Advertisement"/"Read Mood count",
        (if Mood Product,Mood Count)
        "product_id":<int>,
        (if Mood Advertisement, Mood Count)
        "styx_id":"uuid_v1", <string>
        "mood_id":<int>, (required for both Mood Product and Mood Advertisement)
    }
	*/
	if(parseInt(doc.mood_id)!=parseInt(dataJSON.request.mood_id))
	{
        if(parseInt(dataJSON.request.mood_id)>0)
        {
            //update history (change mood_id for advertisement)
            updateHistory(dataJSON,dataStruct,doc,req,res);
            //upsert new mood summary (+1 count)
            upsertSummary(dataJSON,dataStruct,req,res);
        }else{
            deleteHistory(dataJSON,dataStruct,doc);
        }
        
        //adjust old mood summary
        adjustOldSummary(dataJSON,dataStruct,doc,req,res);
	}else{
		var msg = {
            "success":true, "status":"Success",
            "desc":"[savemoodproduct:upsertSummary]This product has already been mooded by this user for that mood"
        };
        respond.respondError(msg,res,req);
	}
}

function updateHistory(dataJSON,dataStruct,doc,req,res)
{
	var query = {
	   "_id":mongojs.ObjectId((doc._id).toString())
	};
	// , 
 	//        "date_simple" : dataStruct.new_unix_timestamp.toString(), 
 	//        "date_unix" : dataStruct.new_simple_timestamp
 	 var updatedSet = {
	    "mood_id": parseInt(dataJSON.request.mood_id),
	    "date_simple" : dataStruct.new_unix_timestamp.toString(), 
        "date_unix" : dataStruct.new_simple_timestamp
    };
    var updateQ = {
        '$set':updatedSet
    };
	GLOBAL.mongodb.moodRecords.update(query,updateQ,function(err,retVal){
        if(err!=null)
        {
            var msg = {
                "success":false, "status":"Error",
                "desc":"[savemoodproduct:updateHistory]Error in DB",
                "err":err
            };
            respond.respondError(msg,res,req);
        }else{
            // var msg = {
            //     "success":true, "status":"Success",
            //     "desc":"[savemoodproduct:updateHistory]followed"
            // };
            // respond.respondError(msg,res,req);
            console.log('updated record history');
        }
        
    });
}

function adjustOldSummary(dataJSON,dataStruct,doc,req,res)
{
	var query = {
		"product_id":parseInt(dataJSON.request.product_id),
	    "type" : "mood product summary",
	    "mood_id":parseInt(doc.mood_id)
	};
	var options = {
        'upsert': true,
        'multi': false
    };
    var updatedSet = {
	    "date_simple" : dataStruct.new_unix_timestamp.toString(), 
        "date_unix" : dataStruct.new_simple_timestamp
    };
    var updateQ = {
        '$set':updatedSet,
        '$inc':{'current_count':-1}
    };
    GLOBAL.mongodb.moodRecords.update(query,updateQ,options,function(err,retVal){
        if(err!=null)
        {
            var msg = {
                "success":false, "status":"Error",
                "desc":"[adjustOldSummary:upsertSummary]Error in DB",
                "err":err
            };
            respond.respondError(msg,res,req);
        }
        
    });
}

function deleteHistory(dataJSON,dataStruct,doc)
{
    var query = {
       "_id":mongojs.ObjectId((doc._id).toString())
    };
    GLOBAL.mongodb.moodRecords.remove( query, 1 , function(err,retVal){
        //don't care
        
    });
}

module.exports={
	start:start
};