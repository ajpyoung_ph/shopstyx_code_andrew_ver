const respond = require(GLOBAL.server_location+'/EventListeners/errorListener');
const reportMsg = require('../../ReportMsg/reportMsg');
/*
{ 
    "_id" : ObjectId("56a0724f1e30590545825b40"), 
    "target_user_id" : NumberInt(1), 
    "user_id" : NumberInt(1), 
    "type" : "follow profile history", 
    "date_simple" : "2016-01-21 13:53:18", 
    "date_unix" : NumberInt(1453355598)
}
{ 
    "_id" : ObjectId("56a0724f1e30590545825b41"), 
    "target_user_id":NumberInt(1),
    "type" : "follow profile summary", 
    "date_simple" : "2016-01-21 13:53:18", 
    "date_unix" : NumberInt(1453355598),
    "current_count":NumberInt(5)
}

"request":{
        "type":"Follow"/"Unfollow",
        "owner_id":<int>,(id of the user liking)
        "target_user_id":<int> (id of user to be liked)
    }

*/

var start = function(dataJSON,dataStruct,req,res)
{
    //check if the person has already liked this
    var query = {
        'target_user_id':parseInt(dataJSON.request.target_user_id),
        'user_id':parseInt(dataJSON.request.owner_id),
        'type' : "follow profile history"
    };
    GLOBAL.mongodb.followRecords.findOne(query,function(err,doc){
        if(err==null)
        {
            dataJSON['reportForm']=[];
            dataJSON['reportForm']['type']=[];
            dataJSON['reportForm']['action']=[];
            dataJSON['reportForm']['to']=[];
            dataJSON.reportForm.type='profile';
            dataJSON.reportForm.action='follow profile';
            dataJSON.reportForm.to=[];
            dataJSON.reportForm.to.push(parseInt(dataJSON.request.target_user_id));
            dataJSON.reportForm.to.push(parseInt(dataJSON.request.owner_id));
            if(doc==null)
            {
                //meaning no records found
                //then insert like product history
                insertFollowHistory(dataJSON,dataStruct,req,res);
                //upsert like product summary
                upsertFollowSummary(dataJSON,dataStruct,req,res);
            }else{
                //record found and reply success
                var msg = {
                    "success":true, "status":"Success",
                    "desc":"[followProfile:start]This profile is already followed by the user"
                };
                respond.respondError(msg,res,req);
                reportMsg.getRobotApiKey(doc,dataJSON,dataStruct);
            }
            //dataStruct.new_unix_timestamp
            //dataStruct.new_simple_timestamp
        }else{
            var msg = {
                "success":false, "status":"Error",
                "desc":"[followProfile:start]Error in DB",
                "err":err
            };
            respond.respondError(msg,res,req);
        }
        
    });
};
/*
var msg = {
                    "success":true, "status":"Success",
                    "desc":"[followProfile:start]Like is successful"
                };
                respond.respondError(msg,res,req);
*/

function insertFollowHistory(dataJSON,dataStruct,req,res)
{
    //dataStruct.new_unix_timestamp
    //dataStruct.new_simple_timestamp
    var query = {
        "target_user_id" : parseInt(dataJSON.request.grouping_id), 
        "user_id" : parseInt(dataJSON.request.owner_id), 
        "type" :"follow profile history", 
        "date_simple" : dataStruct.new_unix_timestamp.toString(), 
        "date_unix" : dataStruct.new_simple_timestamp
    };
    /*
   "_id" : ObjectId("56a0724f1e30590545825b40"), 
    "target_user_id" : NumberInt(1), 
    "user_id" : NumberInt(1), 
    "type" : "follow profile history", 
    "date_simple" : "2016-01-21 13:53:18", 
    "date_unix" : NumberInt(1453355598)
    */
    GLOBAL.mongodb.followRecords.insert(query,function(err,retVal){
        if(err!=null)
        {
            var msg = {
                "success":false, "status":"Error",
                "desc":"[followProfile:insertFollowHistory]Error in DB",
                "err":err
            };
            respond.respondError(msg,res,req);
            reportMsg.getRobotApiKey(query,dataJSON,dataStruct);
        }
        
    });
}

function upsertFollowSummary(dataJSON,dataStruct,req,res)
{
    /*
    "_id" : ObjectId("56a0724f1e30590545825b41"), 
    "target_user_id":NumberInt(1),
    "type" : "follow profile summary", 
    "date_simple" : "2016-01-21 13:53:18", 
    "date_unix" : NumberInt(1453355598),
    "current_count":NumberInt(5)
    */
    var query = {
        "target_user_id":parseInt(dataJSON.request.target_user_id),
        "type" : "follow profile summary"
    };
    var options = {
        'upsert': true,
        'multi': false
    };
    var updatedSet = {
        "target_user_id":parseInt(dataJSON.request.target_user_id),
        "type" : "follow profile summary",
        "date_simple" : dataStruct.new_unix_timestamp.toString(), 
        "date_unix" : dataStruct.new_simple_timestamp
    };
    var updateQ = {
        '$set':updatedSet,
        '$inc':{'current_count':1}
    };
    GLOBAL.mongodb.followRecords.update(query,updateQ,options,function(err,retVal){
        if(err!=null)
        {
            var msg = {
                "success":false, "status":"Error",
                "desc":"[followProfile:insertFollowHistory]Error in DB",
                "err":err
            };
            respond.respondError(msg,res,req);
        }else{
            var msg = {
                "success":true, "status":"Success",
                "desc":"[followProfile:insertFollowHistory]followed"
            };
            respond.respondError(msg,res,req);
        }
        
    });
}

module.exports={
    start:start
};