
const respond = require(GLOBAL.server_location+'/EventListeners/errorListener');
const start = function(dataJSON,dataStruct,req,res)
{
	var query = {
        "target_user_id":parseInt(dataJSON.request.target_user_id),
        "type" : "follow profile summary"
    };
    GLOBAL.mongodb.followRecords.findOne(query,function(err,doc){
    	if(err==null)
    	{
    		var msg = {
                "success":true, "status":"Success",
                "desc":"[countFollowProfile:start]Found Record",
                "data":[]
            };
    		if(doc!=null)
    		 {
    		 	msg.data.push(doc);
    		 }else{
    		 	msg.desc="[countFollowProfile:start]NO Found Record"
    		 }
            
            respond.respondError(msg,res,req);
    	}else{
    		 var msg = {
                "success":false, "status":"Error",
                "desc":"[followGroup:start]Error in DB",
                "err":err
            };
            respond.respondError(msg,res,req);
    	}
        
    });
};

module.exports={
	start:start
};