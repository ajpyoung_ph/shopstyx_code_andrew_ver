const respond = require(GLOBAL.server_location+'/EventListeners/errorListener');
/*
{ 
    "_id" : ObjectId("56a0724f1e30590545825b40"), 
    "target_user_id" : NumberInt(1), 
    "user_id" : NumberInt(1), 
    "type" : "follow profile history", 
    "date_simple" : "2016-01-21 13:53:18", 
    "date_unix" : NumberInt(1453355598)
}
*/

var start = function(dataJSON,dataStruct,req,res)
{
    //check if the person has already liked this
    var query = {
        'target_user_id':parseInt(dataJSON.request.target_user_id),
        'user_id':parseInt(dataJSON.request.owner_id),
        'type' : "follow profile history"
    };
    GLOBAL.mongodb.followRecords.findOne(query,function(err,doc){
        if(err==null)
        {
            if(doc==null)
            {
                var msg = {
                    "success":true, "status":"Success",
                    "desc":"[isFollowing:start]This profile is NOT followed by the user",
                    "data":false
                };
               respond.respondError(msg,res,req);
            }else{
                //record found and reply success
                var msg = {
                    "success":true, "status":"Success",
                    "desc":"[isFollowing:start]This profile is already followed by the user",
                    "data":true
                };
                respond.respondError(msg,res,req);
            }
        }else{
            var msg = {
                "success":false, "status":"Error",
                "desc":"[isFollowing:start]Error in DB",
                "err":err
            };
            respond.respondError(msg,res,req);
        }
        
    });
};

module.exports={
    start:start
};