
const respond = require(GLOBAL.server_location+'/EventListeners/errorListener');
const parseDataJSON = require(GLOBAL.server_location+'/helpers/parseDataJSON');
const followGroup = require(__dirname+'/followGroup');
const unFollowGroup = require(__dirname+'/unFollowGroup');
const listFollowGroup = require(__dirname+'/listFollowGroup');
const isFollowing = require(__dirname+'/isFollowing');
const countFollowGroup = require(__dirname+'/countFollowGroup');
const util = require('util');
/*
{ 
    "_id" : ObjectId("56a0724f1e30590545825b40"), 
    "grouping_id" : NumberInt(1), 
    "user_id" : NumberInt(1), 
    "type" : "follow group history", 
    "date_simple" : "2016-01-21 13:53:18", 
    "date_unix" : NumberInt(1453355598)
}
{ 
    "_id" : ObjectId("56a0724f1e30590545825b41"), 
    "grouping_id":NumberInt(1),
    "type" : "follow group summary", 
    "date_simple" : "2016-01-21 13:53:18", 
    "date_unix" : NumberInt(1453355598),
    "current_count":NumberInt(5)
}

"request":{
        "type":"Follow"/"Unfollow",
        "owner_id":<int>,(id of the user liking)
        "grouping_id":<int> (id of user to be liked)
    }

*/

var processRequest = function(dataJSON,dataStruct,req,res)
{
    var error=false;
    var error_mgs = [];
    if(typeof(dataJSON.request)=='undefined')
    {
        error = true;
        error_mgs.push('no request found');
    }
    if(typeof(dataJSON.request)!='undefined')
    {
        if(typeof(dataJSON.request.type)=='undefined')
        {
            error=true;
            error_mgs.push('no type request found');
        }
        if(typeof(dataJSON.request.owner_id)=='undefined')
        {
            try{
                if(dataJSON.request.type.toLowerCase().replace(/\s/g, '')!='list' && dataJSON.request.type.toLowerCase().replace(/\s/g, '')!='count')
                {
                    error=true;
                    error_mgs.push('no id of target user found');
                }
            }catch(error_type){
                error=true;
                error_mgs.push('no type request found');
            }
        }else{
            if(isNaN(dataJSON.request.owner_id)==true)
            {
                error=true;
                error_msg.push('Owner ID must be a number');
            }else{
                if(parseInt(dataJSON.request.owner_id<1))
                {
                    error=true;
                    error_msg.push('Owner must be a registered user');
                }
            }
            if(dataJSON.request.type.toLowerCase().replace(/\s/g, '')=='isfollowing')
            {
                 if(typeof(dataJSON.request.grouping_id)=='undefined')
                {
                    error=true;
                    error_mgs.push('no group to query');
                }
            }
        }
        if(parseInt(dataJSON.security.user_id)<1)
        {
            error=true;
            error_msg.push('User must be logged in');
        }
        if(typeof(dataJSON.request.grouping_id)=='undefined')
        {
            error=true;
            error_mgs.push('no group request found');
        }
    }
    if(error==false)
    {
        switch(dataJSON.request.type.toLowerCase().replace(/\s/g, ''))
        {
            case 'follow':
                followGroup.start(dataJSON,dataStruct,req,res);
                break;
            case 'unfollow':
                unFollowGroup.start(dataJSON,dataStruct,req,res);
                break;
            case 'list':
                listFollowGroup.start(dataJSON,dataStruct,req,res);
                break;
            case 'count':
                countFollowGroup.start(dataJSON,dataStruct,req,res);
                break;
            case 'isfollowing':
                isFollowing.start(dataJSON,dataStruct,req,res);
                break;
            default:
                var msg = {
                    "success":false, "status":"Error",
                    "desc":"[followCollectionRoutine:processRequest]Unknown requirement type"
                };
                respond.respondError(msg,res,req);
                break;
        }
        
    }else{
        var msg = {
            "success":false, "status":"Error",
            "desc":"[followCollectionRoutine:processRequest]Missing requirements",
            "err":error_mgs
        };
        respond.respondError(msg,res,req);
    }
};

module.exports={
    processRequest:processRequest
};