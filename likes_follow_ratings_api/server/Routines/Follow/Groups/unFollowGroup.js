
const respond = require(GLOBAL.server_location+'/EventListeners/errorListener');
const mongojs = require('mongojs');
/*
{ 
    "_id" : ObjectId("56a0724f1e30590545825b40"), 
    "grouping_id" : NumberInt(1), 
    "user_id" : NumberInt(1), 
    "type" : "follow group history", 
    "date_simple" : "2016-01-21 13:53:18", 
    "date_unix" : NumberInt(1453355598)
}
{ 
    "_id" : ObjectId("56a0724f1e30590545825b41"), 
    "grouping_id":NumberInt(1),
    "type" : "follow group summary", 
    "date_simple" : "2016-01-21 13:53:18", 
    "date_unix" : NumberInt(1453355598),
    "current_count":NumberInt(5)
}

"request":{
        "type":"Follow"/"Unfollow",
        "owner_id":<int>,(id of the user liking)
        "grouping_id":<int> (id of user to be liked)
    }

*/

var start = function(dataJSON,dataStruct,req,res)
{
    //check if the person has already liked this
    var query = {
        'grouping_id':parseInt(dataJSON.request.grouping_id),
        'user_id':parseInt(dataJSON.request.owner_id),
        'type' : "follow group history"
    };
    GLOBAL.mongodb.followRecords.findOne(query,function(err,doc){
        if(err==null)
        {
            if(doc==null)
            {
                //meaning no records found
                 var msg = {
                    "success":true, "status":"Success",
                    "desc":"[unFollowGroup:start]This group is already NOT followed by the user"
                };
                respond.respondError(msg,res,req);
            }else{
                //record found and reply success
               //then insert like product history
                deleteFollowHistory(dataJSON,dataStruct,doc,req,res);
                //upsert like product summary
                upsertFollowSummary(dataJSON,dataStruct,req,res);
            }
            //dataStruct.new_unix_timestamp
            //dataStruct.new_simple_timestamp
        }else{
            var msg = {
                "success":false, "status":"Error",
                "desc":"[unFollowGroup:start]Error in DB",
                "err":err
            };
            respond.respondError(msg,res,req);
        }
        
    });
};
/*
var msg = {
                    "success":true, "status":"Success",
                    "desc":"[unFollowGroup:start]Like is successful"
                };
                respond.respondError(msg,res,req);
*/

function deleteFollowHistory(dataJSON,dataStruct,record,req,res)
{
    //dataStruct.new_unix_timestamp
    //dataStruct.new_simple_timestamp
    var query = {
        '_id':mongojs.ObjectId((record._id).toString())
    };
    /*
   "_id" : ObjectId("56a0724f1e30590545825b40"), 
    "grouping_id" : NumberInt(1), 
    "user_id" : NumberInt(1), 
    "type" : "follow group history", 
    "date_simple" : "2016-01-21 13:53:18", 
    "date_unix" : NumberInt(1453355598)
    */
    GLOBAL.mongodb.followRecords.remove(query,function(err,retVal){
        if(err!=null)
        {
            var msg = {
                "success":false, "status":"Error",
                "desc":"[unFollowGroup:deleteFollowHistory]Error in DB",
                "err":err
            };
            respond.respondError(msg,res,req);
        }
        
    });
}

function upsertFollowSummary(dataJSON,dataStruct,req,res)
{
    /*
    "_id" : ObjectId("56a0724f1e30590545825b41"), 
    "grouping_id":NumberInt(1),
    "type" : "follow group summary", 
    "date_simple" : "2016-01-21 13:53:18", 
    "date_unix" : NumberInt(1453355598),
    "current_count":NumberInt(5)
    */
    var query = {
        "grouping_id":parseInt(dataJSON.request.grouping_id),
        "type" : "follow group summary"
    };
    var options = {
        'upsert': true,
        'multi': false
    };
    var updatedSet = {
        "grouping_id":parseInt(dataJSON.request.grouping_id),
        "type" : "follow group summary",
        "date_simple" : dataStruct.new_unix_timestamp.toString(), 
        "date_unix" : dataStruct.new_simple_timestamp
    };
    var updateQ = {
        '$set':updatedSet,
        '$inc':{'current_count':-1}
    };
    GLOBAL.mongodb.followRecords.update(query,updateQ,options,function(err,retVal){
        if(err!=null)
        {
            var msg = {
                "success":false, "status":"Error",
                "desc":"[unFollowGroup:deleteFollowHistory]Error in DB",
                "err":err
            };
            respond.respondError(msg,res,req);
        }else{
            var msg = {
                "success":true, "status":"Success",
                "desc":"[unFollowGroup:deleteFollowHistory]unFollowed"
            };
            respond.respondError(msg,res,req);
        }
        
    });
}

module.exports={
    start:start
};