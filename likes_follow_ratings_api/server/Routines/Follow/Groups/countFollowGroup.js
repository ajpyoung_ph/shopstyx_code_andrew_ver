
const respond = require(GLOBAL.server_location+'/EventListeners/errorListener');
const start = function(dataJSON,dataStruct,req,res)
{
	var query = {
        "grouping_id":parseInt(dataJSON.request.grouping_id),
        "type" : "follow group summary"
    };
    GLOBAL.mongodb.followRecords.findOne(query,function(err,doc){
    	if(err==null)
    	{
    		var msg = {
                "success":true, "status":"Success",
                "desc":"[countFollowGroup:start]Found Record",
                "data":[]
            };
    		if(doc!=null)
    		 {
    		 	msg.data.push(doc);
    		 }else{
    		 	msg.desc="[countFollowGroup:start]NO Found Record"
    		 }
            
            respond.respondError(msg,res,req);
    	}else{
    		 var msg = {
                "success":false, "status":"Error",
                "desc":"[countFollowGroup:start]Error in DB",
                "err":err
            };
            respond.respondError(msg,res,req);
    	}
        
    });
};

module.exports={
	start:start
};