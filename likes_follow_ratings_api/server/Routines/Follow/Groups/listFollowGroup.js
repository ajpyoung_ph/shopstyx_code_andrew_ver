
const respond = require(GLOBAL.server_location+'/EventListeners/errorListener');
const start = function (dataJSON,dataStruct,req,res) 
{
	var query = {
        "grouping_id":parseInt(dataJSON.request.grouping_id),
        "type" : "follow group history"
    };
    
	var skip = parseInt(dataJSON.request.number_items) * parseInt(dataJSON.request.page_number);
	var limit =  parseInt(dataJSON.request.number_items);
	GLOBAL.mongodb.followRecords.find(query).sort({"date_unix":-1}).limit(limit).skip(skip,function(err,docs){
		if(err==null)
		{
			var msg = {
                "success":true, "status":"Success",
                "desc":"[listFollowGroup:start]Records Found",
                "data":[]
            };
            if(docs.length>0)
            {
            	msg.data=docs;
            }else{
            	msg.desc="[listFollowGroup:start]NO Records Found";
            }
            respond.respondError(msg,res,req);
		}else{
			var msg = {
                "success":false, "status":"Error",
                "desc":"[listFollowGroup:start]Error in DB",
                "err":err
            };
            respond.respondError(msg,res,req);
		}
        
	});
};

module.exports={
	start:start
};