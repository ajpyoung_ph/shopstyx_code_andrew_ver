
var initExitProtocols = function(dataStruct)
{
	var shutdown_counter = dataStruct.shutdown_counter;
	dataStruct.app.on('message', function(msg) {  
	  if (msg === 'shutdown') {
	  	shutdown_counter.inc();
	    close_all_connections();
	    //delete_logs();
	    GLOBAL.server._connections=0;
	    GLOBAL.server.close(function(){
	    	console.log("closing server");
	    });
	    process.exit(0);
	  }
	});
};

//graceful reload for PM2 manager
function close_all_connections(){
	GLOBAL.mongodb.close();
	GLOBAL.db_ss_common.end();
}

module.exports = {
	initExitProtocols:initExitProtocols
};