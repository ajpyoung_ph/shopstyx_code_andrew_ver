
const respond = require(GLOBAL.server_location+'/EventListeners/errorListener');
const Promise = require('bluebird');
const util = require('util');
var parseJSON = function(dataStruct,req,res)
{
	try{
		//console.log(req);
		var dataJSON = req.body.data;
		// console.log('data variable:');
		// console.log(dataJSON);
		// console.log('body of request');
		// console.log(req.body);
		if(typeof(dataJSON)=="string")
		{
			//dataJSON = eval("("+dataJSON+")");
			dataJSON = JSON.parse(dataJSON);
		}
		//check if 
		return dataJSON;
	}catch(err){
		var msg = {
			"success":false, "status":"Error",
			"desc":"[parseJSON]Data Not a proper JSON format",
			"err":err,
			'data':req.body.data
		}
		//respond.respondError(msg,res,req);
		return new Error(JSON.stringify(msg));
	}
	
};

module.exports={
	parseJSON:parseJSON
};
