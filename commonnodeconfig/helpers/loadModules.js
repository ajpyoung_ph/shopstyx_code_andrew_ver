const fs = require('fs');
const util = require('util');

var startLoad =function(directory)
{
	// GLOBAL.modules - our target
	console.log(directory);
	GLOBAL.wdubNames=[];
	GLOBAL.wodubNames=[];
	var files = fs.readdirSync(directory);
	var modules = 0;
	try{
		modules = GLOBAL.modules.length;
	}catch(error){
		//do nothing
		if(util.isNullOrUndefined(error.stack)==false)
		{
			console.log(error.stack);
		}
	}
	if(modules==undefined)
	{
		modules=0;
		GLOBAL.modules.length=0;
	}
	if(modules < files.length)
	{
		files.forEach(function(file){
			console.log(file);
			var filenameCatch = file.split(".js")
			var filename = filenameCatch[0];
			var stats = fs.lstatSync(directory+'/'+file);
			console.log("validating "+filename);
			if(filename!='template')
			{
				console.log("modularizing "+filename);
				try{
					if(stats.isFile())
					{
						processFilename(filename);
						GLOBAL.modules[filename]=require(directory+'/'+file);
						if((typeof GLOBAL.modules[filename]) != 'function')
						{
							
								GLOBAL.modules[filename] = GLOBAL.modules.exports;
						}
						delete GLOBAL.modules.exports;
						GLOBAL.modules.length++;
					}
				}catch(error){
					console.log(error);
					if(util.isNullOrUndefined(error.stack)==false)
					{
						console.log(error.stack);
					}
					//skip and do nothing
				}
			}
			
		});
		console.log("wdubNames");
		console.log(GLOBAL.wdubNames);
		console.log("wodubNames");
		console.log(GLOBAL.wodubNames);
	}
}

function processFilename(filename)
{
	if(filename!='generic_scraping')
	{
		console.log("saving "+filename);
		GLOBAL.wdubNames.push(filename);
		//remove www
		var newHolder = filename.split('www.');
		var newfilename='';
		if(newHolder.length==1)
		{
			newHolder = filename.split('wc.');
		}
		newfilename=newHolder[1];
		GLOBAL.wodubNames.push(newfilename);
	}
}

module.exports = {
	startLoad:startLoad
};