const mysql = require(__dirname +"/mysql_helpers");
const util = require('util');

module.exports = function(dataSend,dataSource)
{
	try{
		dataSend.product_name = dataSource[0].trim();
	}catch(err){
		dataSend.product_name = dataSource[0];
	}
	try{
		dataSend.price = dataSource[1].trim();
	}catch(err){
		dataSend.price = dataSource[1];
	}
	if(Array.isArray(dataSend.price)==true)
	{
		dataSend.price=dataSend.price[0];
	}
	try{
		dataSend.image = dataSource[2].trim();
	}catch(err){
		dataSend.image = dataSource[2];
	}
	dataSend.images = dataSource[3];
	if(util.isNullOrUndefined(dataSource[4])==false)
	{
		dataSend.mainDescription = dataSource[4];
	}
	if(util.isNullOrUndefined(dataSource[5])==false)
	{
		dataSend.otherPrices = dataSource[5];
	}
	if(util.isNullOrUndefined(dataSource[6])==false)
	{
		dataSend.seller_url=dataSource[6];
	}
	//mysql.saveNewDataSend(dataSend);
	if(util.isNullOrUndefined(dataSend.product_name)==true)
	{
		dataSend.product_name='';
	}
	if(util.isNullOrUndefined(dataSend.price)==true)
	{
		dataSend.price='';
	}
	if(util.isNullOrUndefined(dataSend.image)==true)
	{
		dataSend.image='';
	}
	return dataSend;
	//
}