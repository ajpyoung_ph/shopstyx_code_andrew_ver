const Promise = require('bluebird');
const util = require('util');

const mysql = require('mysql');
const mytime = require('../../helpers/mysql_convertions');
const fs = require('fs');
const mysqlForceTimeOut = 120000;

const start = function(dataStruct)
{
	var db_init=dataStruct.mysql_db_init;
	connectSsCommon(JSON.parse(JSON.stringify(db_init)));
	connectSsTransactionDumps(JSON.parse(JSON.stringify(db_init)));
	connectSsActivities(JSON.parse(JSON.stringify(db_init)));
	return Promise.resolve(dataStruct);
};
function tryForceRelease(connection)
{
	try{
		//console.log("force releasing connection");
		connection.release();
	}catch(err){
		//do nothing
		//console.log("connection already released");
	}
}
function connectSsCommon(db_init)
{
	var connection_count = 0;
	// db_init.database = 'cs_search_results';
	// GLOBAL.db_cs_search_results = mysql.createConnection(db_init);
	try{
		GLOBAL.db_ss_common.end();
		console.log("ending SS_COMMON MySQL connection");
	}catch(err){
		console.log("No MySQL SS_COMMON Connection to Close");
	}
	console.log("creating new SS_COMMON mysql connection");
	console.log(mytime.MySQLDateTimeNOW());
	db_init.database = 'ss_common';
	db_init=ssl_set(db_init);
	//GLOBAL.db_ss_common = mysql.createConnection(db_init);
	GLOBAL.db_ss_common = mysql.createPool(db_init);
	// GLOBAL.db_ss_common.on('connection', function (connection) {
	//   	//console.log("Connection given");
	//   	//connection.query('SET SESSION auto_increment_increment=1');
	//   	setTimeout(tryForceRelease, mysqlForceTimeOut,connection);
	// });
	// GLOBAL.db_ss_common.on('enqueue', function () {
	// 	console.log('Waiting for available connection slot');
	// });

	// GLOBAL.db_ss_common.connect(function(err) {              // The server is either down
 //    if(err) {                                     // or restarting (takes a while sometimes).
	//       console.log('error when connecting to db:', err);
	//       setTimeout(connectCsCommon, 2000, db_init); // We introduce a delay before attempting to reconnect,
	//     }                                     // to avoid a hot loop, and to allow our node script to
	//   });                                     // process asynchronous requests in the meantime.
	//                                           // If you're also serving http, display a 503 error.
	// GLOBAL.db_ss_common.on('error', function(err) {
	// 	console.log('db error', err);
	// 	if(err.code === 'PROTOCOL_CONNECTION_LOST') { // Connection to the MySQL server is usually
	// 		PROTOCOL_CONNECTION_LOST_counter.inc();
	// 		connectCsCommon(db_init);                         // lost due to either server restart, or a
	// 	} else {                                      // connnection idle timeout (the wait_timeout
	// 	  throw err;                                  // server variable configures this)
	// 	}
	// });
}

function connectSsTransactionDumps(db_init)
{
	//connect to SS_TRANSACTION_DUMPS
	try{
		GLOBAL.db_ss_transaction_dumps.end();
		console.log("ending SS_TRANSACTION_DUMPS MySQL connection");
	}catch(err){
		console.log("No MySQL SS_TRANSACTION_DUMPS Connection to Close");
	}
	console.log("creating new SS_TRANSACTION_DUMPS mysql connection");
	console.log(mytime.MySQLDateTimeNOW());
	db_init.database = 'ss_transaction_dumps';
	db_init=ssl_set(db_init);
	//GLOBAL.db_ss_transaction_dumps = mysql.createConnection(db_init);
	GLOBAL.db_ss_transaction_dumps = mysql.createPool(db_init);
	// GLOBAL.db_ss_transaction_dumps.on('connection', function (connection) {
	//   	// console.log("Connection given");
	//   	// connection.query('SET SESSION auto_increment_increment=1');
	//   	setTimeout(tryForceRelease, mysqlForceTimeOut,connection);
	// });
	// GLOBAL.db_ss_transaction_dumps.on('enqueue', function () {
	// 	console.log('Waiting for available connection slot');
	// });
}

function connectSsActivities(db_init)
{
	//connect to SS_TRANSACTION_DUMPS
	try{
		GLOBAL.db_ss_activities.end();
		console.log("ending SS_TRANSACTION_DUMPS MySQL connection");
	}catch(err){
		console.log("No MySQL SS_TRANSACTION_DUMPS Connection to Close");
	}
	console.log("creating new SS_TRANSACTION_DUMPS mysql connection");
	console.log(mytime.MySQLDateTimeNOW());
	db_init.database = 'ss_activities';
	db_init=ssl_set(db_init);
	//GLOBAL.db_ss_transaction_dumps = mysql.createConnection(db_init);
	GLOBAL.db_ss_activities = mysql.createPool(db_init);
	// GLOBAL.db_ss_activities.on('connection', function (connection) {
	//   	// console.log("Connection given");
	//   	// connection.query('SET SESSION auto_increment_increment=1');
	//   	setTimeout(tryForceRelease, mysqlForceTimeOut,connection);
	// });
	// GLOBAL.db_ss_activities.on('enqueue', function () {
	// 	console.log('Waiting for available connection slot');
	// });
}

function ssl_set(db_init)
{
	switch(process.env.NODE_ENV.toLowerCase())
	{
		case 'local':
		case 'development':
			break;

		case 'testing':
		case 'staging':
			break;
		// case 'cloud_staging':
		// 	db_init.ssl={
		// 		ca : fs.readFileSync(server_location + 'certificates/MySQL/server-ca.pem'),
		// 		key : fs.readFileSync(server_location + 'certificates/MySQL/client-key.pem'),
		// 		cert : fs.readFileSync(server_location + 'certificates/MySQL/client-cert.pem')
		// 	};
		// 	break;

		case 'production':
		case 'final':
			// db_init.ssl={
			// 	ca : fs.readFileSync(GLOBAL.server_location + '/certificates/MySQL/server-ca.pem'),
			// 	key : fs.readFileSync(GLOBAL.server_location + '/certificates/MySQL/client-key.pem'),
			// 	cert : fs.readFileSync(GLOBAL.server_location + '/certificates/MySQL/client-cert.pem')
			// };
			break;
	}
	return db_init;
}


module.exports={
	start:start
};