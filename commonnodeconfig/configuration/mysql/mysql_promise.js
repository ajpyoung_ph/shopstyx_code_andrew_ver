"use strict";
const fs = require('fs');
const util = require('util');
const uuid = require('uuid');

var mysql_connect = class mysql_connect{
	constructor(q,p)
	{
		console.log("constructed class");
		this.p = p;
		this.q = q;
	}

	ssl_set(db_init)
	{
		switch(process.env.NODE_ENV.toLowerCase())
		{
			case 'local':
			case 'development':
				break;

			case 'testing':
			case 'staging':
				break;
			// case 'cloud_staging':
			// 	db_init.ssl={
			// 		ca : fs.readFileSync(server_location + 'certificates/MySQL/server-ca.pem'),
			// 		key : fs.readFileSync(server_location + 'certificates/MySQL/client-key.pem'),
			// 		cert : fs.readFileSync(server_location + 'certificates/MySQL/client-cert.pem')
			// 	};
			// 	break;

			case 'production':
			case 'final':
				db_init.ssl={
					ca : fs.readFileSync(GLOBAL.server_location + '/certificates/MySQL/server-ca.pem'),
					key : fs.readFileSync(GLOBAL.server_location + '/certificates/MySQL/client-key.pem'),
					cert : fs.readFileSync(GLOBAL.server_location + '/certificates/MySQL/client-cert.pem')
				};
				break;
		}
		return db_init;
	}

	executeMySQL(mysql,db_init)
	{
		if(!mysql.isConfigured())
		{
			mysql.configure(db_init);
		}
		if(util.isNullOrUndefined(mysql)==false)
		{
			if(util.isNullOrUndefined(this.p)==true)
			{
				return mysql.query(this.q).spread(function(rows){
					// console.log('rows:');
					// console.log(rows);
					mysql.end();
					return rows;
				}).catch(function(err){
					if(err.message=='Handshake inactivity timeout')
					{
						return this.executeMySQL(mysql,db_init);
					}else{
						return err;
					}
				});
			}else{
				return mysql.query(this.q,this.p).spread(function(rows){
					// console.log('rows:');
					// console.log(rows);
					mysql.end();
					return rows;
				}).catch(function(err){
					if(err.message=='Handshake inactivity timeout')
					{
						return this.executeMySQL(mysql,db_init);
					}else{
						return err;
					}
				});
			}
			
		}else{
			var err = {
				'status':'Error',
				'message':'No Connection',
				'stack':new Error().stack,
				'code':'could not configure DB common'
			}
			return new Error(err);
		}
	}

	results_ss_common()
	{
		var myuuid = uuid.v1();
		var mysql = require('mysql-promise')(myuuid);
		console.log('called ss_common');
		var db_init = JSON.parse(JSON.stringify(GLOBAL.db_init));
		db_init.database = 'ss_common';
		db_init=this.ssl_set(db_init);
		
		return this.executeMySQL(mysql,db_init);
	}
	results_ss_activities()
	{
		var myuuid = uuid.v1();
		var mysql = require('mysql-promise')(myuuid);
		console.log('called ss_activities');
		var db_init = JSON.parse(JSON.stringify(GLOBAL.db_init));
		db_init.database = 'ss_activities';
		db_init=this.ssl_set(db_init);
		// if(!mysql.isConfigured())
		// {
		// 	mysql.configure(db_init);
		// }
		return this.executeMySQL(mysql,db_init);
	}
	results_ss_transaction_dumps()
	{
		var myuuid = uuid.v1();
		var mysql = require('mysql-promise')(myuuid);
		console.log('called ss_transaction_dumps');
		var db_init = JSON.parse(JSON.stringify(GLOBAL.db_init));
		db_init.database = 'ss_transaction_dumps';
		db_init=this.ssl_set(db_init);
		return this.executeMySQL(mysql,db_init);
	}
}

module.exports={
	mysql_connect:mysql_connect
};