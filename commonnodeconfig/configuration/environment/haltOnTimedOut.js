var util = require('util');

var haltOnTimedout = function(err,req, res, next){
  if (!req.timedout)
  {
  	next();
  }else{
  	console.log("=========== CHECKING ============");
  	console.log(err);
  	console.log("=================================");
  	console.error(err.stack); //the stack is actually not going to be helpful in a timeout
    if(!res.headersSent){ //just because of your current problem, no need to exacerbate it.
        errcode = err.status || 500; //err has status not statusCode
        msg = err.message || 'server error!';
        err.server_code = errcode;
        err.status="server error";
        console.log(err);
        console.log(err.stack);
        //res.status(errcode).send(msg); //the future of send(status,msg) is hotly debated
        res.status(errcode).json(err); //the future of send(status,msg) is hotly debated
  	}
  }
};

module.exports={
	haltOnTimedout:haltOnTimedout
};