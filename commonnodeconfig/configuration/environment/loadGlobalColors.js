const Promise = require('bluebird');
const util = require('util');

const start = function(dataStruct)
{
	var query = "SELECT * FROM `default_colors` ORDER BY `color_id`;";
	var default_colors=[];
	var alternative_colors=[];
	var mysql = new GLOBAL.mysql.mysql_connect(query);
	mysql.results_ss_common().then(function(results){
		if(util.isError(results)==true){
			var msg = {
				"success":false, "status":"Error",
				'desc':'[loadGlobalColors:start]DB Error Connection',
				'query':query,
				'message':results.message,
				'stack':results.stack||null
			};
			process.emit('Shopstyx:logError',msg);
		}else{
			var rows=results;
			default_colors=rows.slice();
			query = "SELECT * FROM `default_colors_alternative` ORDER BY `color_id`;";
			var mysql2 = new GLOBAL.mysql.mysql_connect(query);
			mysql2.results_ss_common().then(function(results2){
				if(util.isError(results2)==true){
					var msg = {
						"success":false, "status":"Error",
						'desc':'[loadGlobalColors:start]DB Error Connection2',
						'query':query,
						'message':results2.message,
						'stack':results2.stack||null
					};
					process.emit('Shopstyx:logError',msg);
				}else{
					var rows2=results2;
					alternative_colors=rows2.slice();
					GLOBAL.default_colors=default_colors.concat(alternative_colors);
					//console.log(GLOBAL.default_colors);
				}
			});
		}
	});
	
	//default length
	var query_LWH = 'SELECT * FROM `default_length_units` ORDER BY `length_unit_id`;';
	var mysql_LWH = new GLOBAL.mysql.mysql_connect(query_LWH);
	mysql_LWH.results_ss_common().then(function(results){
		if(util.isError(results)==true){
			var msg = {
				"success":false, "status":"Error",
				'desc':'[loadGlobalColors:start:LWH]DB Error Connection',
				'query':query,
				'message':results.message,
				'stack':results.stack||null
			};
			process.emit('Shopstyx:logError',msg);
		}else{
			var rows_LWH = results;
			GLOBAL.default_length_units=rows_LWH.slice();
			//console.log(GLOBAL.default_length_units);
		}
	});
	//default monetary
	var query_default_monetary_units = 'SELECT * FROM `default_monetary_units` ORDER BY `money_id`;';
	var mysql_default_monetary_units = new GLOBAL.mysql.mysql_connect(query_default_monetary_units);
	mysql_default_monetary_units.results_ss_common().then(function(results){
		if(util.isError(results)==true){
			var msg = {
				"success":false, "status":"Error",
				'desc':'[loadGlobalColors:start:money]DB Error Connection',
				'query':query,
				'message':results.message,
				'stack':results.stack||null
			};
			process.emit('Shopstyx:logError',msg);
		}else{
			var rows_money = results;
			GLOBAL.default_monetary_units=rows_money.slice();
			//console.log(GLOBAL.default_monetary_units);
		}
	});
	//default weight
	var query_default_weight_units = 'SELECT * FROM `default_weight_units` ORDER BY `weight_units_id`;';
	var mysql_default_weight_units = new GLOBAL.mysql.mysql_connect(query_default_weight_units);
	mysql_default_weight_units.results_ss_common().then(function(results){
		if(util.isError(results)==true){
			var msg = {
				"success":false, "status":"Error",
				'desc':'[loadGlobalColors:start:weight]DB Error Connection',
				'query':query,
				'message':results.message,
				'stack':results.stack||null
			};
			process.emit('Shopstyx:logError',msg);
		}else{
			var rows_weight=results;
			GLOBAL.default_weight_units=rows_weight.slice();
			//console.log(GLOBAL.default_weight_units);
		}
	});
};

module.exports={
	start:start
};