const Promise = require('bluebird');
const util = require('util');

const start = function(dataStruct)
{
	return new Promise(function(resolve,reject){
		console.log('writing CORs');
		dataStruct.app.all('*', function(req, res, next) {
			// res.header("Access-Control-Allow-Origin", "*");
			// res.header("Access-Control-Allow-Headers", "X-Requested-With");
			// next();
			//console.log("CORs activated");
			res.header('Access-Control-Allow-Origin', '*');
		    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
		    res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, X-Requested-With');

		    // intercept OPTIONS method
		    if ('OPTIONS' == req.method) {
		      res.sendStatus(200);
		    }
		    else {
		      next();
		    }
		});
		resolve(dataStruct);
	});
};

module.exports={
	start:start
};