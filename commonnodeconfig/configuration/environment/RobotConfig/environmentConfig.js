
const Promise = require('bluebird');
const util = require('util');

const bodyParser = require('body-parser');
const logger = require('morgan');
const gcloud = require('gcloud');


const start = function(dataStruct)
{
	var server_dir_array = (__dirname).toString().split("configuration");
	var server_location = server_dir_array[0];
	console.log("pulling certificates from "+server_location);
	GLOBAL.modules = {};
	GLOBAL.kind = {};
	switch(process.env.NODE_ENV.toLowerCase())
	{
		case 'local':
		case 'development':
			dataStruct.HTTP_location = 'http://localhost';
			dataStruct.mongo_db_location = "localhost";
			dataStruct.mysql_db_init = {
				host     : 'localhost',
				user     : 'root',
				password : '',
				database : ''
			};
			
			GLOBAL.gcloud = require('gcloud')({
				projectId: 'shopstyx',
				keyFilename: server_location + '/certificates/GDataStore/Shopstyx-5595286364f0.json'
			});
			GLOBAL.kind={
				styxName:"local_styxDB",
				defaultLogName:"local_logs"
			};
			
			GLOBAL.storage_names = {
				'advertisements':'shopstyx-advertisements-local',
				'products':'shopstyx-products-local',
				'profiles':'shopstyx-profiles-local'
			};
			
			break;

		case 'testing':
		case 'staging':
			dataStruct.HTTP_location = 'http://pasilshop.com';
			dataStruct.mongo_db_location = "104.199.138.98";
			dataStruct.mysql_db_init = {
				host     : '104.199.138.98',
				user     : 'mymugzy',
				password : 'mymugzy!2009',
				database : ''
			};
			GLOBAL.gcloud = require('gcloud')({
				projectId: 'shopstyx'
			});
			GLOBAL.kind={
				styxName:"staging_styxDB",
				defaultLogName:"staging_logs"
			};
			GLOBAL.storage_names = {
				'advertisements':'shopstyx-advertisements-staging',
				'products':'shopstyx-products-staging',
				'profiles':'shopstyx-profiles-staging'
			};
			break;
		// case 'cloud_staging':
		// 	dataStruct.HTTP_location = '173.194.250.237';
		// 	dataStruct.mongo_db_location = null;
		// 	dataStruct.mysql_db_init = {
		// 		host     : 'localhost',
		// 		user     : 'mymugzy',
		// 		password : 'mymugzy2009',
		// 		database : '',
		// 		ssl:{
		// 			ca : fs.readFileSync(server_location + 'certificates/MySQL/server-ca.pem'),
		// 			key : fs.readFileSync(server_location + 'certificates/MySQL/client-key.pem'),
		// 			cert : fs.readFileSync(server_location + 'certificates/MySQL/client-cert.pem')
		// 		}
		// 	};
		// 	dataStruct.gcloud_db_location = {
		// 		projectId: 'big-potential-110506',
		// 		keyFilename: server_location + 'certificates/GDataStore/TestProject-9356fb29fe3c.json'
		// 	};
			// dataStruct.gcloud_db_location = {
			// 	projectId: 'big-potential-110506'
			// }
			break;

		case 'production':
		case 'final':
			dataStruct.HTTP_location = 'https://shopstyx.com';
			//dataStruct.mongo_db_location = "104.130.206.240:27017,104.130.162.34:27017,104.239.135.106:27017";
			dataStruct.mongo_db_location = "104.130.206.240:27017";
			dataStruct.mysql_db_init = {
				host     : '173.194.248.67',
				user     : 'mymugzy',
				password : 'mymugzy!2009',
				database : ''
			};
			GLOBAL.gcloud = require('gcloud')({
				projectId: 'shopstyx'
			});
			GLOBAL.kind={
				styxName:"production_styxDB",
				defaultLogName:"production_logs"
			};
			GLOBAL.storage_names = {
				'advertisements':'shopstyx-advertisements',
				'products':'shopstyx-products',
				'profiles':'shopstyx-profiles'
			};
			break;
	};

	GLOBAL.storage_url = "https://storage.googleapis.com/";//https://storage.googleapis.com/+GLOBAL.storage_names+/directory+/filename
	//handle location variables;
	GLOBAL.HTTP_location=dataStruct.HTTP_location;
	return Promise.resolve(dataStruct);
};

module.exports={
	start:start
};