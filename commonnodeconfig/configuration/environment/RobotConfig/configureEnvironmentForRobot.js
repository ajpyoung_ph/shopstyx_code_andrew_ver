const Promise = require('bluebird');
const util = require('util');

const environmentConfig = require(__dirname+'/environmentConfig');
const configureGlobalActivityLogs = require(__dirname+'/configureGlobalActivityLogs');
const configureMySQL = require('../../mysql/db_conf');

const start = function(dataStruct)
{
	
	console.log("setting up running environment");
	environmentConfig.start(retVal);
	console.log("setting up global mysql access");
	configureMySQL.start(retVal);
	

	return Promise.resolve(dataStruct);
};

module.exports={
	start:start
};