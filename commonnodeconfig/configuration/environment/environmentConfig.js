
const Promise = require('bluebird');
const util = require('util');

const bodyParser = require('body-parser');
const logger = require('morgan');
const haltOnTimedout = require(__dirname+'/haltOnTimedOut');
const gcloud = require('gcloud');



const start = function(dataStruct)
{
	var server_dir_array = (__dirname).toString().split("configuration");
	var server_location = server_dir_array[0];
	console.log("pulling certificates from "+server_location);
	GLOBAL.modules = {};
	GLOBAL.kind = {};

	dataStruct.mysql_manager_db_init={};
	switch(process.env.NODE_ENV.toLowerCase())
	{
		case 'local':
		case 'development':
			dataStruct.HTTP_location = 'http://shopstyx.local';
			dataStruct.mongo_db_location = "localhost";
			dataStruct.mysql_db_init = {
				host     : 'localhost',
				user     : 'root',
				password : '',
				database : '',
				supportBigNumbers: true,
				connectionLimit:100
			};
			
			dataStruct.mysql_manager_db_init = {
				host: 'localhost',// Host name for database connection. 
				port: 3306,// Port number for database connection. 
				user: 'root',// Database user. 
				password: '',// Password for the above database user. 
				database: '',// Database name. 
				autoReconnect: true,// Whether or not to re-establish a database connection after a disconnect. 
				reconnectDelay: [
					500,// Time between each attempt in the first group of reconnection attempts; milliseconds. 
					1000,// Time between each attempt in the second group of reconnection attempts; milliseconds. 
					5000,// Time between each attempt in the third group of reconnection attempts; milliseconds. 
					30000,// Time between each attempt in the fourth group of reconnection attempts; milliseconds. 
					300000// Time between each attempt in the fifth group of reconnection attempts; milliseconds. 
				],
				useConnectionPooling: true,// Whether or not to use connection pooling. 
				reconnectDelayGroupSize: 5,// Number of reconnection attempts per reconnect delay value. 
				maxReconnectAttempts: 25,// Maximum number of reconnection attempts. Set to 0 for unlimited. 
				keepAlive: true,// Whether or not to send keep-alive pings on the database connection(s). 
				keepAliveInterval: 30000// How frequently keep-alive pings will be sent; milliseconds. 
			};

			GLOBAL.gcloud = require('gcloud')({
				projectId: 'shopstyx',
				keyFilename: server_location + '/certificates/GDataStore/Shopstyx-5595286364f0.json'
			});
			GLOBAL.kind={
				styxName:"local_styxDB",
				defaultLogName:"local_logs"
			};
			
			GLOBAL.storage_names = {
				'advertisements':'shopstyx-advertisements-local',
				'products':'shopstyx-products-local',
				'profiles':'shopstyx-profiles-local'
			};
			
			break;

		case 'testing':
		case 'staging':
			dataStruct.HTTP_location = 'http://pasilshop.com';
			dataStruct.mongo_db_location = "104.197.207.19";
			dataStruct.mysql_db_init = {
				host     : '173.194.231.107',
				user     : 'mymugzy',
				password : 'mymugzy!2009',
				database : '',
				supportBigNumbers: true,
				connectionLimit:100
			};
			dataStruct.mysql_manager_db_init = {
				host: '173.194.231.107',// Host name for database connection. 
				port: 3306,// Port number for database connection. 
				user: 'mymugzy',// Database user. 
				password: 'mymugzy!2009',// Password for the above database user. 
				database: '',// Database name. 
				autoReconnect: true,// Whether or not to re-establish a database connection after a disconnect. 
				reconnectDelay: [
					500,// Time between each attempt in the first group of reconnection attempts; milliseconds. 
					1000,// Time between each attempt in the second group of reconnection attempts; milliseconds. 
					5000,// Time between each attempt in the third group of reconnection attempts; milliseconds. 
					30000,// Time between each attempt in the fourth group of reconnection attempts; milliseconds. 
					300000// Time between each attempt in the fifth group of reconnection attempts; milliseconds. 
				],
				useConnectionPooling: true,// Whether or not to use connection pooling. 
				reconnectDelayGroupSize: 5,// Number of reconnection attempts per reconnect delay value. 
				maxReconnectAttempts: 25,// Maximum number of reconnection attempts. Set to 0 for unlimited. 
				keepAlive: true,// Whether or not to send keep-alive pings on the database connection(s). 
				keepAliveInterval: 30000// How frequently keep-alive pings will be sent; milliseconds. 
			};
			GLOBAL.gcloud = require('gcloud')({
				projectId: 'shopstyx'
			});
			GLOBAL.kind={
				styxName:"staging_styxDB",
				defaultLogName:"staging_logs"
			};
			GLOBAL.storage_names = {
				'advertisements':'shopstyx-advertisements-staging',
				'products':'shopstyx-products-staging',
				'profiles':'shopstyx-profiles-staging'
			};
			break;
		// case 'cloud_staging':
		// 	dataStruct.HTTP_location = '173.194.250.237';
		// 	dataStruct.mongo_db_location = null;
		// 	dataStruct.mysql_db_init = {
		// 		host     : 'localhost',
		// 		user     : 'mymugzy',
		// 		password : 'mymugzy2009',
		// 		database : '',
		// 		ssl:{
		// 			ca : fs.readFileSync(server_location + 'certificates/MySQL/server-ca.pem'),
		// 			key : fs.readFileSync(server_location + 'certificates/MySQL/client-key.pem'),
		// 			cert : fs.readFileSync(server_location + 'certificates/MySQL/client-cert.pem')
		// 		}
		// 	};
		// 	dataStruct.gcloud_db_location = {
		// 		projectId: 'big-potential-110506',
		// 		keyFilename: server_location + 'certificates/GDataStore/TestProject-9356fb29fe3c.json'
		// 	};
			// dataStruct.gcloud_db_location = {
			// 	projectId: 'big-potential-110506'
			// }
			break;

		case 'production':
		case 'final':
			dataStruct.HTTP_location = 'https://shopstyx.com';
			//dataStruct.mongo_db_location = "104.130.206.240:27017,104.130.162.34:27017,104.239.135.106:27017";
			dataStruct.mongo_db_location = "146.148.48.214:27017";
			dataStruct.mysql_db_init = {
				host     : '173.194.83.20',
				user     : 'mymugzy',
				password : 'mymugzy!2009',
				database : '',
				supportBigNumbers: true,
				connectionLimit:100
			};
			dataStruct.mysql_manager_db_init = {
				host: '173.194.83.20',// Host name for database connection. 
				port: 3306,// Port number for database connection. 
				user: 'mymugzy',// Database user. 
				password: 'mymugzy!2009',// Password for the above database user. 
				database: '',// Database name. 
				autoReconnect: true,// Whether or not to re-establish a database connection after a disconnect. 
				reconnectDelay: [
					500,// Time between each attempt in the first group of reconnection attempts; milliseconds. 
					1000,// Time between each attempt in the second group of reconnection attempts; milliseconds. 
					5000,// Time between each attempt in the third group of reconnection attempts; milliseconds. 
					30000,// Time between each attempt in the fourth group of reconnection attempts; milliseconds. 
					300000// Time between each attempt in the fifth group of reconnection attempts; milliseconds. 
				],
				useConnectionPooling: true,// Whether or not to use connection pooling. 
				reconnectDelayGroupSize: 5,// Number of reconnection attempts per reconnect delay value. 
				maxReconnectAttempts: 25,// Maximum number of reconnection attempts. Set to 0 for unlimited. 
				keepAlive: true,// Whether or not to send keep-alive pings on the database connection(s). 
				keepAliveInterval: 30000// How frequently keep-alive pings will be sent; milliseconds. 
			};
			GLOBAL.gcloud = require('gcloud')({
				projectId: 'shopstyx'
			});
			GLOBAL.kind={
				styxName:"production_styxDB",
				defaultLogName:"production_logs"
			};
			GLOBAL.storage_names = {
				'advertisements':'shopstyx-advertisements',
				'products':'shopstyx-products',
				'profiles':'shopstyx-profiles'
			};
			break;
	};

	GLOBAL.storage_url = "https://storage.googleapis.com/";//https://storage.googleapis.com/+GLOBAL.storage_names+/directory+/filename
	GLOBAL.number_list_items = 20;
	GLOBAL.start_list_page = 0;
	GLOBAL.advertisement_sizes = [960,650];
	GLOBAL.product_sizes = [960,650,300,100];
	GLOBAL.db_init=dataStruct.mysql_db_init;
	delete GLOBAL.db_init.connectionLimit;
	GLOBAL.mysql = require('../mysql/mysql_promise');
	GLOBAL.mongo_db_location = dataStruct.mongo_db_location;
	//handle post forms
	dataStruct.app.use(bodyParser.json()); // for parsing application/json
	dataStruct.app.use(haltOnTimedout.haltOnTimedout);
	dataStruct.app.use(bodyParser.urlencoded({ extended: true })); // for parsing application/x-www-form-urlencoded
	dataStruct.app.use(haltOnTimedout.haltOnTimedout);
	//dataStruct.app.use(logger('dev'));//log http requests
	dataStruct.app.use(haltOnTimedout.haltOnTimedout);

	//handle location variables;
	GLOBAL.HTTP_location=dataStruct.HTTP_location;
	return Promise.resolve(dataStruct);
};

module.exports={
	start:start
};