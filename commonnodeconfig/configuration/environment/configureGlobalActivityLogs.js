const Promise = require('bluebird');
const util = require('util');
const mongojs = require('mongojs');

const start = function(dataStruct)
{
	console.log('mongodb_Logs set');
	switch(process.env.NODE_ENV.toLowerCase())
	{
		case 'local':
		case 'development':
		case 'testing':
		case 'staging':
    		//console.log('@'+dataStruct.mongo_db_location+'/ugc');
			GLOBAL.mongodb_Logs = mongojs('activityUser:s#0pst!cks@'+dataStruct.mongo_db_location+'/activityLogs', ['requestLogs,loginLogs,failedLogs'], {authMechanism: 'ScramSHA1'});
			break;

		case 'production':
		case 'final':
    		//console.log('@'+dataStruct.mongo_db_location+'/ugc?replicaSet=styx01');
			//GLOBAL.mongodb_Logs = mongojs('activityUser:s#0pst!cks@'+dataStruct.mongo_db_location+'/activityLogs?replicaSet=styx01', ['requestLogs,loginLogs,failedLogs'], {authMechanism: 'ScramSHA1'});
			GLOBAL.mongodb_Logs = mongojs('activityUser:s#0pst!cks@'+dataStruct.mongo_db_location+'/activityLogs', ['requestLogs,loginLogs,failedLogs'], {authMechanism: 'ScramSHA1'});
			break;
	}
	return Promise.resolve(dataStruct);
};

module.exports={
	start:start
};