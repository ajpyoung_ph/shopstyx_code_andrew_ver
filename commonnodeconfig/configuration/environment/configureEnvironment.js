const Promise = require('bluebird');
const util = require('util');

const cors = require(__dirname+'/CORs');
const environmentConfig = require(__dirname+'/environmentConfig');
const configureGlobalActivityLogs = require(__dirname+'/configureGlobalActivityLogs');
const configureMySQL = require('../mysql/db_conf');
const loadGlobalColors = require(__dirname+'/loadGlobalColors');

const start = function(dataStruct)
{
	console.log("setting Up Cors");
	cors.start(dataStruct).then(function(retVal){
		console.log("finished Setup Cors");
		console.log("setting up running environment");
		environmentConfig.start(retVal);
		console.log("setting up GLOBAL Activity Logs");
		configureGlobalActivityLogs.start(retVal);
		console.log("setting up global mysql access");
		// configureMySQL.start(retVal).then(function(retVal){
		// 	loadGlobalColors.start(retVal);
		// });
		loadGlobalColors.start(retVal);
		
		return(retVal);
	}).catch(function(err){
		console.log("Error in CORs Setup");
		console.log(err);
	});

	return Promise.resolve(dataStruct);
};

module.exports={
	start:start
};