const util = require('util');
const mysqlStuff = require('../helpers/mysql_convertions');
const nodemailer = require('nodemailer');
const smtpTransport = require('nodemailer-smtp-transport');
const transporter = nodemailer.createTransport(smtpTransport({
    host: 'smtp.mailgun.org',
    port: 587,
    auth: {
        user: 'postmaster@shopstyx.com',
        pass: '0fkpo68eh3t4'
    }
}));

const start = function()
{
	//email folder
	process.on('Shopstyx:resError',respondError);
	process.on('Shopstyx:emailError',emailError);
	process.on('Shopstyx:logError',logError);
};

const respondError=function(msg,res,req)
{
	var holder = msg;
	if(util.isNullOrUndefined(msg.err)==false)
	{
		msg.err = util.inspect(msg.err,{showHidden: false, depth: null});
	}
	var now = Date.now();
	console.log('RESPONDING TO: '+req.protocol + '://' + req.get('host') + req.originalUrl + " -------- "+Date());
	console.log('---> ('+(now-req.start_time)+' ms)');
	try{
		res.json(msg);
		console.log('=============================================No Error Occurred - Response sent');
		if(util.isNullOrUndefined(msg.uuid_v1)==false)
		{
			try{
				delete holder.additional_info;
			}catch(dontcarehere){
				
			}
			
		}else if( util.isNullOrUndefined(msg.status)==false)
		{
			if( msg.status.toLowerCase()!='success')
			{
				console.log(msg);
			}
		}
		if(util.isNullOrUndefined(holder.data)==false)
		{
			delete holder.data;
		}
		console.log(holder);
		
	}catch(err){
		//do nothing
		//logError(msg);
		console.log('================================================Error occurred - in catch');
		console.log(err);
		if(util.isNullOrUndefined(err.stack)==false)
		{
			console.log(err.stack);
		}
		if(util.isNullOrUndefined(holder.data)==false)
		{
			delete holder.data;
		}
		console.log(holder);
		
	}
}

const logError=function(msg)
{
	// if(typeof(msg.err)!='undefined')
	// {
		if(util.isNullOrUndefined(msg.err)==false)
		{
			if(util.isNullOrUndefined(msg.err.stack)==false)
			{
				console.log(msg);
				console.log(msg.err);
				console.log(msg.err.stack);
			}
		}else{
			msg = util.inspect(msg,{showHidden: false, depth: null});
			try{
				console.log(msg);
			}catch(err){
				//do nothing
			}
		}
		
	//}
}

const emailError=function(msg)
{
	console.log("SENDING EMAIL");
	setImmediate(logError,msg);
	if(typeof(msg.err)!='undefined')
	{
		msg.err = util.inspect(msg.err,{showHidden: false, depth: null});
	}
	
	console.log(msg);
	var timeStuff = {
		"new_unix_timestamp":'',
		"new_simple_timestamp":''
	};
	timeStuff.new_simple_timestamp = mysqlStuff.populateMySQLTimeStamp(timeStuff);
	msg['date']=[];
	msg['date_simple']=[];
	msg.date=timeStuff.new_unix_timestamp;
	msg.date_simple=timeStuff.new_simple_timestamp;
	var message = JSON.stringify(msg);
	var mailOptions = {
	    from: 'nodeservices@shopstyx.com', // sender address
	    to: 'andrew@shopstyx.com', // list of receivers
	    subject: 'Transaction API Status Error', // Subject line
	    text: message // plaintext body
	    //html: '<b>Hello world ✔</b>' // html body
	};
	if(process.env.NODE_ENV.toLowerCase()!='production' || process.env.NODE_ENV.toLowerCase()!='final')
	{
		mailOptions.subject="["+process.env.NODE_ENV.toLowerCase()+"]"+mailOptions.subject;
	}
	transporter.sendMail(mailOptions, function(error, info){
	    if(error){
	        console.log(error);
	    }else{
	        console.log('Message sent: ' + info.response);
	    }
	});		
}


module.exports={
	start:start,
	respondError:respondError,
	logError:logError,
	emailError:emailError
};