const Promise = require('bluebird');
const util = require('util');
//find activity_logger
var activity_logger_array = (__dirname).toString().split("commonnodeconfig");
activity_logger_location = activity_logger_array[0]+"activity_logger/server/Routine";
delete server_dir_array;

const logRequests = require(activity_logger_location+"/LogActivities/logRequests");
const respond = require(__dirname+'/errorListener');

const securityChk = require('../security/checkUser');
const mysqlStuff = require('../helpers/mysql_convertions');
const parseDataJSON = require('../helpers/parseDataJSON');
const jwt = require('jwt-simple');

const start = function(dataStruct,req,res)
{

	//email folder
	//process.on('Shopstyx:checkUser',security.checkToken);
	dataStruct.new_simple_timestamp = mysqlStuff.populateMySQLTimeStamp(dataStruct);
	req.start_time = Date.now();
	//dataStruct.new_unix_timestamp
	console.log("executing security checks");
	var dataJSON = parseDataJSON.parseJSON(dataStruct,req,res);
	if(util.isError(dataJSON)==true)
	{
		console.log('/////////////ERROR');
		console.log(util.inspect(dataJSON,{showHidden: false, depth: null}));
		return Promise.reject(dataJSON);
	}else if(util.isNullOrUndefined(dataJSON)==false)
	{
		var dataJSON2=JSON.parse(JSON.stringify(dataJSON));
		var fullUrl = req.protocol + '://' + req.get('host') + req.originalUrl;
		dataJSON2.fullUrl = fullUrl;
		dataJSON2.new_simple_timestamp = dataStruct.new_simple_timestamp;
		dataJSON2.new_unix_timestamp = dataStruct.new_unix_timestamp;
		setImmediate(logRequests.start,dataJSON2);
		//data request found
		var error=false;
		var error_msg=[];
		if(typeof(dataJSON.security)=='undefined')
		{
			error=true;
			error_msg.push('No Security Parameters found');
		}
		if(typeof(dataJSON.security)!='undefined')
		{
			if(typeof(dataJSON.security.token)=='undefined')
			{
				error=true;
				error_msg.push('No Token Found');
			}
			if(typeof(dataJSON.security.user_id)=='undefined')
			{
				error=true;
				error_msg.push('No User ID Found');
			}
			if(typeof(dataJSON.security.uuid)=='undefined')
			{
				error=true;
				error_msg.push('No UUID Found');
			}
			if(typeof(dataJSON.security.type)=='undefined')
			{
				error=true;
				error_msg.push('No Security Type found');
			}
		}
		if(error==false)
		{
			if(verifyLogin(dataJSON)==true)
			{
				console.log('executing callback');
				return Promise.resolve([dataJSON,dataStruct]);
			}else{
				var msg = {
					"success":false, "status":"Error",
					"desc":"[securityListener:populateDataStruct]Token not valid"
				}
				return Promise.reject(new Error(JSON.stringify(msg)));
			}
		}else{
			var msg = {
				"success":false, "status":"Error",
				"desc":"[securityListener:populateDataStruct]No Security Data Found",
				"err":error_msg
			}
			return Promise.reject(new Error(JSON.stringify(msg)));
		}
	}else{
		var msg = {
			"success":false, "status":"Error",
			"desc":"[securityListener:populateDataStruct]No Valid Data Found"
		}
		return Promise.reject(new Error(JSON.stringify(msg)));
	}
}

const verifyLogin=function(dataJSON)
{
	// if(process.env.NODE_ENV.toLowerCase().trim()=='development' || process.env.NODE_ENV.toLowerCase().trim()=='staging')
	// {
		console.log("Development Environment Found");
		return true;
	// }else{
	// 	var keys = {
	// 		"firebase" : "YN4JTexzDRtT30HwBk3UHKS9jZIAJ6sKa9UB8cQX",
	// 		"shopstyx_nologin" : "DrqvAPOJSu2LYQtPYpfS",
	// 		"shopstyx_login" : "oCnwAdd2Kd4izMUQCcfH"
	// 	};
	// 	var secret = keys.shopstyx_login; // get key for shopstyx
	// 	var token = dataJSON.security.token; // get the token
	// 	try
	// 	{
	// 		var decoded = jwt.decode(token, secret);
	// 		return true;
	// 	} catch (e) {
	// 		var msg = {
	// 			"success":false, "status":"Error",
	// 			"desc":"[securityListener:verifyLogin]Invalid Key",
	// 			"err":e
	// 		}
	// 		process.emit('Shopstyx:logError',msg);
	// 		return false;
	// 	}
	// }
	// return false;
}
//verifyRobot
const verifyRobot = function(dataStruct,callback,req,res)
{
	var dataJSON = parseDataJSON.parseJSON(dataStruct,req,res);
	if(util.isNullOrUndefined(dataJSON)==false)
	{
		var query = 'SELECT * FROM `default_settings` WHERE `group`="api" AND `key`="webservice_signature";';

		var mysql = new GLOBAL.mysql.mysql_connect(query);
		mysql.results_ss_common().then(function(results){
			if(util.isError(results)==true){
				var msg = {
					"success":false, "status":"Error",
					'desc':'[securityListener:verifyRobot]DB Error Connection',
					'query':query,
					'message':results.message,
					'stack':results.stack||null
				};
				respond.respondError(msg,res,req);
			}else{
				var resultDoc = results;
				if(err==null)
				{
					if(typeof(dataJSON.security)=='undefined')
					{
						var msg = {
							"success":false, "status":"Error",
							"desc":"[securityListener:verifyRobot]Missing Security"
						};
						respond.respondError(msg,res,req);
					}else{
						if(typeof(dataJSON.security.token)=='undefined' || typeof(dataJSON.security.uuid)=='undefined' || typeof(dataJSON.security.user_id)=='undefined')
						{
							var msg = {
								"success":false, "status":"Error",
								"desc":"[securityListener:verifyRobot]Missing Security Information"
							};
							respond.respondError(msg,res,req);
						}else{
							if(dataJSON.security.token==resultDoc[0].value)
							{
								var msg = {
									"success":true, "status":"Success",
									"desc":"[securityListener:verifyRobot]Validation done, doing requested background process"
								};
								respond.respondError(msg,res,req);
								setImmediate(callback, dataStruct);
							}else{
								var msg = {
									"success":false, "status":"Error",
									"desc":"[securityListener:verifyRobot]You're validation is rejected"
								};
								respond.respondError(msg,res,req);
							}
						}
					}
				}else{
					var msg = {
						"success":false, "status":"Error",
						"desc":"[securityListener:verifyRobot]Error in DB",
						"err":err,
						"query":query
					};
					process.emit('Shopstyx:logError',msg);
				}

			}
		});
	}else{
		var msg = {
			"success":false, "status":"Error",
			"desc":"[securityListener:verifyRobot]No Valid Data Found",
			"req.body":req.body,
			"req.body.data":req.body.data
		}
		respond.respondError(msg,res,req);
	}
}
module.exports={
	start:start,
	verifyLogin:verifyLogin,
	verifyRobot:verifyRobot
};