//find activity_logger
var activity_logger_array = (__dirname).toString().split("commonnodeconfig");
GLOBAL.activity_logger_location = activity_logger_array[0]+"activity_logger/server/Routine";
delete server_dir_array;

const logRequests = require(GLOBAL.activity_logger_location+"/LogActivities/logRequests");
const respond = require(__dirname+'/errorListener');

const securityChk = require('../security/checkUser');
const mysqlStuff = require('../helpers/mysql_convertions');
const parseDataJSON = require('../helpers/parseDataJSON');
const jwt = require('jwt-simple');
const util = require('util');

const start = function(dataStruct,callback,req,res)
{

	//email folder
	//process.on('Shopstyx:checkUser',security.checkToken);
	dataStruct.new_simple_timestamp = mysqlStuff.populateMySQLTimeStamp(dataStruct);
	//dataStruct.new_unix_timestamp
	//dataStruct.new_simple_timestamp
	//check for jwt code

	setImmediate(populateDataStruct,dataStruct,callback,req,res);
};

const populateDataStruct=function(dataStruct,callback,req,res)
{
	console.log("executing security checks");
	var dataJSON = parseDataJSON.parseJSON(dataStruct,req,res);
	if(util.isNullOrUndefined(dataJSON)==false)
	{
		var dataJSON2=JSON.parse(JSON.stringify(dataJSON));
		var fullUrl = req.protocol + '://' + req.get('host') + req.originalUrl;
		dataJSON2.fullUrl = fullUrl;
		dataJSON2.new_simple_timestamp = dataStruct.new_simple_timestamp;
		dataJSON2.new_unix_timestamp = dataStruct.new_unix_timestamp;
		setImmediate(logRequests.start,dataJSON2);
		//data request found
		var error=false;
		var error_msg=[];
		if(typeof(dataJSON.security)=='undefined')
		{
			error=true;
			error_msg.push('No Security Parameters found');
		}
		if(typeof(dataJSON.security)!='undefined')
		{
			if(typeof(dataJSON.security.token)=='undefined')
			{
				error=true;
				error_msg.push('No Token Found');
			}
			if(typeof(dataJSON.security.user_id)=='undefined')
			{
				error=true;
				error_msg.push('No User ID Found');
			}
			if(typeof(dataJSON.security.uuid)=='undefined')
			{
				error=true;
				error_msg.push('No UUID Found');
			}
			if(typeof(dataJSON.security.type)=='undefined')
			{
				error=true;
				error_msg.push('No Security Type found');
			}
		}
		if(error==false)
		{
			if(verifyLogin(dataJSON)==true)
			{
				console.log('executing callback');
				setImmediate(callback,dataStruct,req,res);
			}else{
				var msg = {
					"success":false, "status":"Error",
					"desc":"[securityListener:populateDataStruct]Token not valid"
				}
				respond.respondError(msg,res,req);
			}
		}else{
			var msg = {
				"success":false, "status":"Error",
				"desc":"[securityListener:populateDataStruct]No Security Data Found",
				"err":error_msg
			}
			respond.respondError(msg,res,req);
		}
	}else{
		var msg = {
			"success":false, "status":"Error",
			"desc":"[securityListener:populateDataStruct]No Valid Data Found"
		}
		respond.respondError(msg,res,req);
	}
}

const verifyLogin=function(dataJSON)
{
	if(process.env.NODE_ENV.toLowerCase().trim()=='development' || process.env.NODE_ENV.toLowerCase().trim()=='staging')
	{
		console.log("Development Environment Found");
		return true;
	}else{
		var keys = {
			"firebase" : "YN4JTexzDRtT30HwBk3UHKS9jZIAJ6sKa9UB8cQX",
			"shopstyx_nologin" : "DrqvAPOJSu2LYQtPYpfS",
			"shopstyx_login" : "oCnwAdd2Kd4izMUQCcfH"
		};
		var secret = keys.shopstyx_login; // get key for shopstyx
		var token = dataJSON.security.token; // get the token
		try
		{
			var decoded = jwt.decode(token, secret);
			return true;
		} catch (e) {
			var msg = {
				"success":false, "status":"Error",
				"desc":"[securityListener:verifyLogin]Invalid Key",
				"err":e
			}
			process.emit('Shopstyx:logError',msg);
			return false;
		}
	}
	return false;
}

const verifyRobot = function(dataStruct,callback,req,res)
{
	var dataJSON = parseDataJSON.parseJSON(dataStruct,req,res);
	if(util.isNullOrUndefined(dataJSON)==false)
	{
		var query = 'SELECT * FROM `default_settings` WHERE `group`="api" AND `key`="webservice_signature";';

		GLOBAL.db_ss_common.getConnection(function(err, connection) {
			if(err!=null)
			{
				var msg = {
					"success":false, "status":"Error",
					"desc":"[securityListener:verifyRobot]Error saving in DB",
					"err":err,
					"query":query
				};
				respond.respondError(msg,res,req);
			}else{
				connection.query(query,function(err,resultDoc){
					if(err==null)
					{
						if(typeof(dataJSON.security)=='undefined')
						{
							var msg = {
								"success":false, "status":"Error",
								"desc":"[securityListener:verifyRobot]Missing Security"
							};
							process.emit('Shopstyx:logError',msg);
						}else{
							if(typeof(dataJSON.security.token)=='undefined' || typeof(dataJSON.security.uuid)=='undefined' || typeof(dataJSON.security.user_id)=='undefined')
							{
								var msg = {
									"success":false, "status":"Error",
									"desc":"[securityListener:verifyRobot]Missing Security Information"
								};
								process.emit('Shopstyx:logError',msg);
							}else{
								if(dataJSON.security.token==resultDoc[0].value)
								{
									var msg = {
										"success":true, "status":"Success",
										"desc":"[securityListener:verifyRobot]Validation done, doing requested background process"
									};
									process.emit('Shopstyx:logError',msg);
									setImmediate(callback, dataStruct);
								}else{
									var msg = {
										"success":false, "status":"Error",
										"desc":"[securityListener:verifyRobot]You're validation is rejected"
									};
									process.emit('Shopstyx:logError',msg);
								}
							}
						}
					}else{
						var msg = {
							"success":false, "status":"Error",
							"desc":"[securityListener:verifyRobot]Error in DB",
							"err":err,
							"query":query
						};
						process.emit('Shopstyx:logError',msg);
					}
					connection.release();
					
				});
			}
		});
	}else{
		var msg = {
			"success":false, "status":"Error",
			"desc":"[securityListener:verifyRobot]No Valid Data Found"
		}
		respond.respondError(msg,res,req);
	}
}
// var populateDataStructOLD=function(dataStruct,callback)
// {
// 	/*
// 	dataStruct = {
// 		"user_id":'',
// 		"token":'',
// 		"req": '',
// 		"res": '',
// 		"new_unix_timestamp":'',
// 		"new_simple_timestamp":'',
// 		"timezone":"America/Los_Angeles"
// 	}
// 	*/
// 	//check data posted
// 	//try{
// 		console.log("process.env.NODE_ENV:"+process.env.NODE_ENV.toLowerCase());
// 		//Log Activity here

// 		//end Log Activity
// 		if(process.env.NODE_ENV.toLowerCase().trim()=='development' || process.env.NODE_ENV.toLowerCase().trim()=='local' || process.env.NODE_ENV.toLowerCase().trim()=='staging')
// 		{
// 			console.log("executing immediate callback");
// 			setImmediate(callback,dataStruct);
// 		}else{
// 			console.log("executing security checks");
// 			var dataJSON = parseDataJSON.parseJSON(dataStruct);
// 			if(util.isNullOrUndefined(dataJSON)==false)
// 			{
// 				if(typeof(dataJSON.security.user_id)!='undefined' && parseInt(dataJSON.security.user_id)==0)
// 				{
// 					//non-logged in user
// 					dataStruct.loggedIn = false;
// 					setImmediate(callback,dataStruct);
// 				}else{
// 					var error = true;
// 					if(typeof(dataJSON.security.type)!='undefined')
// 					{
// 						switch(dataJSON.security.type.toLowerCase())
// 						{
// 							case 'web':
// 								if(typeof(dataJSON.security.user_id)!='undefined')
// 								{
// 									dataStruct.loggedIn = true;
// 									dataStruct.user_id = parseInt(dataJSON.security.user_id);
// 									dataStruct.token = dataJSON.security.token;
									
// 									setImmediate(securityChk.checkToken,dataStruct,callback);
// 									error=false;
// 								}
// 								if(typeof(dataJSON.security.sess_id)!='undefined')
// 								{
// 									dataStruct.loggedIn = true;
// 									setImmediate(securityChk.checkSession,dataJSON,dataStruct,callback);
// 									error=false;
// 								}
// 								break;

// 							case 'app':
// 								if(typeof(dataJSON.security.user_id)!='undefined')
// 								{
// 									dataStruct.loggedIn = true;
// 									dataStruct.user_id = parseInt(dataJSON.security.user_id);
// 									dataStruct.token = dataJSON.security.token;
									
// 									setImmediate(securityChk.checkToken,dataStruct,callback);
// 									error=false;
// 								}
// 								break;
// 						}
// 					}
// 					if(error==true)
// 					{
// 						var msg = {
// 							"success":false, "status":"Error",
// 							"desc":"[populateDataStruct]Security Information not valid",
// 							"err":"code 1"
// 						}
// 						respond.respondError(msg,res,req);
// 						return null;
// 					}
// 				}
// 			}
// 		}
// 	// }catch(err){
// 	// 	var msg = {
// 	// 		"success":false, "status":"Error",
// 	// 		"desc":"[populateDataStruct]Security Information not valid",
// 	// 		"err":err
// 	// 	}
// 	// 	respond.respondError(msg,res,req);
// 	// 	return null;
// 	// }

// };
module.exports={
	start:start,
	populateDataStruct:populateDataStruct
};