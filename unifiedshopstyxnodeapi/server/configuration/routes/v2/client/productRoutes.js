const respond = require(GLOBAL.server_location+'/EventListeners/errorListener');
const productListRoutines = require('../../../../Routines/ProductLists/v2/client/productListRoutines');
const productDetailsRoutines = require('../../../../Routines/ProductInfo/v2/client/ProductInfo');
const securityListener = require(GLOBAL.server_location+'/EventListeners/securityListener');
const multer  = require('multer');
const upload = multer({ dest: GLOBAL.upload_location });
const mkdirp = require('mkdirp');
const cpUpload = upload.fields([{ name: 'prevpic', maxCount: 10 }, { name: 'src', maxCount: 10 }]);
const parseDataJSON = require(GLOBAL.server_location+'/helpers/parseDataJSON');
const util = require('util');

var configRoutes = function(dataStruct)
{   
    var router = dataStruct.router;

    router.route('/v2/product/client/list/request')
        .post(cpUpload,function(req,res){
            var dataJSON = parseDataJSON.parseJSON(res,req);
            if(util.isNullOrUndefined(dataJSON)==false)
            {
                mkdirp(GLOBAL.upload_location, function (err) {
                     if (err){
                        var msg = {
                            'status':'Error',
                            'desc':'Error creating folder'
                        };
                        respond.respondError(msg,res,req);
                    }else{
                        //console.log('Starting: '+req.protocol + '://' + req.get('host') + req.originalUrl + " -------- "+Date());
                        securityListener.start(dataStruct,req).spread(function(dataJSON,dataStruct){
                            try{
                                //console.log('executing: '+req.protocol + '://' + req.get('host') + req.originalUrl + " -------- "+Date());
                                setImmediate(productListRoutines.processRequest,dataJSON,dataStruct,req,res);
                            }catch(err){
                                var msg={
                                    "success":false, "status":"Error",
                                    "err":"Error Code Root 1",
                                    "url":"",
                                    "error_report":err,
                                    "stack":err.stack
                                }
                                respond.respondError(err,res,req);
                            }
                            
                        }).catch(function(err){
                            if(util.isError(err))
                            {
                                try{
                                    try{
                                        var msg = JSON.parse(err.message);
                                    }catch(dont_care){
                                        var msg = {
                                            message:err.message
                                        };
                                    }
                                    if(util.isNullOrUndefined(err.stack)==false)
                                    {
                                        msg.stack = err.stack;
                                        console.error(err.stack);
                                    }
                                    respond.respondError(msg,res,req);
                                }catch(somethingHappened){
                                    ////console.log(err);
                                    respond.respondError(err,res,req);
                                }
                            }else{
                                if(util.isNullOrUndefined(err.stack)==false)
                                {
                                    console.error(err.stack);
                                }
                                throw err;
                            }
                        });
                       //securityListener.start(dataJSON,dataStruct,productListRoutines.processRequest,req,res);
                    }
                });
            }else{
                var msg = {
                    "success":false, "status":"Error",
                    "desc":"[ProductListing:processRequest]No data to process"
                };
                respond.respondError(msg,res,req);
            }
            //////console.log("*************************** /v2/product/client/list/request");
        });
    router.route('/v2/product/client/details/request')
        .post(cpUpload,function(req,res){
            //////console.log("*************************** /v2/product/client/details/request");
            var dataJSON = parseDataJSON.parseJSON(res,req);
            if(util.isNullOrUndefined(dataJSON)==false)
            {
                mkdirp(GLOBAL.upload_location, function (err) {
                    if(err)
                    {
                        var msg = {
                            'status':'Error',
                            'desc':'Error creating folder'
                        };
                        respond.respondError(msg,res,req);
                    }else{
                        //console.log("Start request /v2/default/value/request --- "+Date());
                        securityListener.start(dataStruct,req).spread(function(dataJSON,dataStruct){
                            try{
                                setImmediate(productDetailsRoutines.processRequest,dataJSON,dataStruct,req,res);
                            }catch(err){
                                var msg={
                                    "success":false, "status":"Error",
                                    "err":"Error Code Root 1",
                                    "url":"",
                                    "error_report":err,
                                    "stack":err.stack
                                }
                                respond.respondError(err,res,req);
                            }
                        }).catch(function(err){
                            if(util.isError(err))
                            {
                                try{
                                    try{
                                        var msg = JSON.parse(err.message);
                                    }catch(dont_care){
                                        var msg = {
                                            message:err.message
                                        };
                                    }
                                    if(util.isNullOrUndefined(err.stack)==false)
                                    {
                                        msg.stack = err.stack;
                                        console.error(err.stack);
                                    }
                                    respond.respondError(msg,res,req);
                                }catch(somethingHappened){
                                    ////console.log(err);
                                    respond.respondError(err,res,req);
                                }
                            }else{
                                if(util.isNullOrUndefined(err.stack)==false)
                                {
                                    console.error(err.stack);
                                }
                                throw err;
                            }
                        });
                        //securityListener.start(dataJSON,dataStruct,productDetailsRoutines.processRequest,req,res);
                    }
                    
                });
            }else{
                var msg = {
                    "success":false, "status":"Error",
                    "desc":"[ProductListing:processRequest]No data to process"
                };
                respond.respondError(msg,res,req);
            }
        });
   
    return dataStruct;
}


module.exports = {
    configRoutes:configRoutes
};