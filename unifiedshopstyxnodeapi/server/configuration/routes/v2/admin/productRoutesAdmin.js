
const securityListener = require(GLOBAL.server_location+'/EventListeners/securityListener');
const productListRoutines = require('../../../../Routines/ProductLists/v2/admin/ProductListingAdmin');
const productSaveDetails = require('../../../../Routines/ProductSave/v2/admin/productSaveDetails')
const multer  = require('multer');
const upload = multer({ dest: GLOBAL.upload_location });
const mkdirp = require('mkdirp');
const cpUpload = upload.fields([{ name: 'prevpic', maxCount: 10 }, { name: 'src', maxCount: 10 }]);
const respond = require(GLOBAL.server_location+'/EventListeners/errorListener');
const util = require('util');

var configRoutes = function(dataStruct)
{   
    var router = dataStruct.router;

    router.route('/v2/product/admin/request')
        .post(cpUpload,function(req,res){
            mkdirp(GLOBAL.upload_location, function (err) {
                if (err){
                    var msg = {
                        'status':'Error',
                        'desc':'Error creating folder'
                    };
                    respond.respondError(msg,res,req);
                }else{
                    securityListener.start(dataStruct,req).spread(function(dataJSON,dataStruct){
                        try{
                            productListRoutines.processRequest(dataJSON,dataStruct,req,res);
                        }catch(err){
                            var msg={
                                "success":false, "status":"Error",
                                "err":"Error Code Root 1",
                                "url":"",
                                "error_report":err,
                                "stack":err.stack
                            }
                            respond.respondError(err,res,req);
                        }
                        
                    }).catch(function(err){
                        if(util.isError(err))
                        {
                            try{
                                try{
                                    var msg = JSON.parse(err.message);
                                }catch(dont_care){
                                    var msg = {
                                        message:err.message
                                    };
                                }
                                if(util.isNullOrUndefined(err.stack)==false)
                                {
                                    msg.stack = err.stack;
                                    console.error(err.stack);
                                }
                                respond.respondError(msg,res,req);
                            }catch(somethingHappened){
                                ////console.log(err);
                                respond.respondError(err,res,req);
                            }
                        }else{
                            if(util.isNullOrUndefined(err.stack)==false)
                            {
                                console.error(err.stack);
                            }
                            throw err;
                        }
                    });
                    //securityListener.start(dataStruct.routeData,productListRoutines.processRequest,req,res);
                }
                
            });
        });
	router.route('/v2/product/admin/save/details')
		.post(cpUpload,function(req,res){
            mkdirp(GLOBAL.upload_location, function (err) {
                if (err){
                    var msg = {
                        'status':'Error',
                        'desc':'Error creating folder'
                    };
                    respond.respondError(msg,res,req);
                }else{
        		    securityListener.start(dataStruct,req).spread(function(dataJSON,dataStruct){
                        try{
                            productSaveDetails.processRequest(dataJSON,dataStruct,req,res);
                        }catch(err){
                            var msg={
                                "success":false, "status":"Error",
                                "err":"Error Code Root 1",
                                "url":"",
                                "error_report":err,
                                "stack":err.stack
                            }
                            respond.respondError(err,res,req);
                        }
                        
                    }).catch(function(err){
                        if(util.isError(err))
                        {
                            try{
                                try{
                                    var msg = JSON.parse(err.message);
                                }catch(dont_care){
                                    var msg = {
                                        message:err.message
                                    };
                                }
                                if(util.isNullOrUndefined(err.stack)==false)
                                {
                                    msg.stack = err.stack;
                                    console.error(err.stack);
                                }
                                respond.respondError(msg,res,req);
                            }catch(somethingHappened){
                                ////console.log(err);
                                respond.respondError(err,res,req);
                            }
                        }else{
                            if(util.isNullOrUndefined(err.stack)==false)
                            {
                                console.error(err.stack);
                            }
                            throw err;
                        }
                    });
        		    //securityListener.start(dataStruct.routeData,productSaveDetails.processRequest,req,res);
                }
                
            });
		});
    return dataStruct;
}


module.exports = {
    configRoutes:configRoutes
};