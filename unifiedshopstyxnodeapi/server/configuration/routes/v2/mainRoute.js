const test = require(__dirname+'/testRoute');
const healthcheck = require(__dirname+'/healthcheck');
const products = require(__dirname+'/client/productRoutes');
const adminProducts = require(__dirname+'/admin/productRoutesAdmin');
const defaultValues = require(__dirname+'/client/defaultValueRoutes');
const collections = require(__dirname+'/client/collectionRoutes');

var mainRoute = function(dataStruct)
{
	dataStruct = defaultValues.configRoutes(dataStruct);
	dataStruct = collections.configRoutes(dataStruct);
	dataStruct = products.configRoutes(dataStruct);
	dataStruct = healthcheck.configRoutes(dataStruct);
	dataStruct = test.configRoutes(dataStruct);
	dataStruct = adminProducts.configRoutes(dataStruct);
	// REGISTER OUR ROUTES -------------------------------
	// all of our routes will be from root /
	if(process.env.NODE_ENV.toLowerCase()==='development' || process.env.NODE_ENV.toLowerCase()==='local')
	{
		////console.log('registering route for '+process.env.NODE_ENV.toLowerCase())
		dataStruct.app.use('/', dataStruct.router);
	}else{
		////console.log('registering route for '+process.env.NODE_ENV.toLowerCase())
		dataStruct.app.use('/api/', dataStruct.router);
	}
	return dataStruct;
}

module.exports = {
	mainRoute:mainRoute
};