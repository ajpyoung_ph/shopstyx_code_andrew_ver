const respond = require(GLOBAL.server_location+'/EventListeners/errorListener');
const parseDataJSON = require(GLOBAL.server_location+'/helpers/parseDataJSON');
var configRoutes = function(dataStruct)
{	
	var router = dataStruct.router;

	router.route('/v2/testEcho/:echo')
		.get(function(req,res){
			// dataStruct.routeData.req = req;
			// res = res;
			// if(req.timedout)
			// {
			// 	var mymsg = {
			// 		"status":"My Server Timed Out",
			// 		"message":"Please retry"
			// 	};
			// 	return next(createError(490,mymsg));
			// }
			req.start_time=Date.now();
			try{
				////console.log("**************************");
				////console.log(GLOBAL.default_colors);
				////console.log("***************************");
				var msg = {
					"status":"TestRoute",
					"echo":req.params.echo
				};
				//setTimeout(respond.respondError,5000,msg,res,req);
			}catch(err){
				var msg = {
					"status":"Error in Route",
					"desc":"/v2/testEcho/",
					"err":err,
					"stack":err.stack
				};
			}
			setTimeout(respond.respondError,2000,msg,res,req);
			// if(req.timedout)
			// {
			// 	return next(createError(503,'Response timeout'));
			// }
   //  		req.send("I processed that for you!");
		});

	return dataStruct;
}


module.exports = {
	configRoutes:configRoutes
};


/*
profile stories:
- registration and validation
- created advertisement/styx
- edited profile/updated profile
- follow collections/advertisement/profile
- share product/advertisement
- added to collection product
- like product/advertisement
- click on buy
- click on go to buy

stykable stories:
- share stykable
- like stykable
- comments on stykable
- edited/updated stykable
- mood placed on stykable


*/