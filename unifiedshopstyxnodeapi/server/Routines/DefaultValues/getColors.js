const util = require('util');
const respond = require(GLOBAL.server_location+'/EventListeners/errorListener');

var start = function(dataJSON,dataStruct,req,res)
{
	var now = Date.now();
	//console.log("Getting Data for Colors ============ "+Date());
	var query2="SELECT * FROM `default_colors`;";
	var mysql = new GLOBAL.mysql.mysql_connect(query2);
	mysql.results_ss_common().then(function(results){
		if(util.isError(results)==true){
			var msg = {
				"success":false, "status":"Error",
				'desc':'[getColors:start]DB Error Connection',
				'query':query2,
				'message':results.message,
				'stack':results.stack||null
			};
			respond.respondError(msg,res,req);
		}else{
			var msg = {
				"success":true, "status":"Success",
				"details":"[getColors:start]Data Found",
				"data":results
			}
			respond.respondError(msg,res,req);
		}
	});
};

module.exports={
	start:start
};