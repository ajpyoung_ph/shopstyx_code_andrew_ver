const util = require('util');
const respond = require(GLOBAL.server_location+'/EventListeners/errorListener');

const start = function(dataJSON,dataStruct,req,res)
{
	var now = Date.now();
	//console.log("Getting Data for Departments ============ "+Date());
	var query2="SELECT * FROM `default_departments`;";
	var mysql = new GLOBAL.mysql.mysql_connect(query2);
	mysql.results_ss_common().then(function(results){
		if(util.isError(results)==true){
			var msg = {
				"success":false, "status":"Error",
				'desc':'[getDepartments:start]DB Error Connection',
				'query':query2,
				'message':results.message,
				'stack':results.stack||null
			};
			process.emit('Shopstyx:logError',msg);
		}else{
			var msg = {
				"success":true, "status":"Success",
				"details":"[getDepartments:start]Data Found",
				"data":results
			};
			respond.respondError(msg,res,req);
		}
	});
};

module.exports={
	start:start
};