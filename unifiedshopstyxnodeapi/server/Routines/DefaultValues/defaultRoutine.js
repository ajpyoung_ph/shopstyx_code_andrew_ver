const respond = require(GLOBAL.server_location+'/EventListeners/errorListener');
const getColors = require(__dirname+'/getColors');
const getDepartments = require(__dirname+'/getDepartments');
const parseDataJSON = require(GLOBAL.server_location+'/helpers/parseDataJSON');
const util = require('util');
const uuid = require('uuid');
//GLOBAL.readCollection_res=[];

var processRequest = function(dataJSON,dataStruct,req,res)
{
	// dataJSON['readCollectionuuid']=uuid.v1();
	// GLOBAL.readCollection_res[dataJSON['readCollectionuuid']]=res;
	var error = false;
	var error_msg = [];
	if(typeof(dataJSON.request)=='undefined')
	{
		error=true;
		error_msg.push('No Request Found');
	}
	if(typeof(dataJSON.request)!='undefined')
	{
		if(typeof(dataJSON.request.type)=='undefined')
		{
			error=true;
			error_msg.push('No Request Type Found');
		}
	}
	if(typeof(dataJSON.request.type)!='string')
	{
		error=true;
		error_msg.push('Request Type is not of a valid format');
	}
	if(error==false)
	{
		var type_request = (dataJSON.request.type).toString();
		switch(type_request.toLowerCase().replace(/\s/g, ''))
		{
			case 'colors':
				getColors.start(dataJSON,dataStruct,req,res);
				break;
			case 'departments':
				getDepartments.start(dataJSON,dataStruct,req,res);
				break;
			case 'moods':
				getMoods.start(dataJSON,dataStruct,req,res);
				break;
			default:
				var msg = {
					"success":false, "status":"Error",
					"desc":"[defaultRoutine:processRequest]Unknown Request Type",
					"request_type":type_request
				};
				respond.respondError(msg,res,req);

						
						
				break;
		}
	}else{
		var msg = {
			"success":false, "status":"Error",
			"desc":"[defaultRoutine:processRequest]Missing Requirements",
			"err":error_msg,
			"request_type":dataJSON.request.type
		};
		respond.respondError(msg,res,req);
		
						
						
	}
};

module.exports={
	processRequest:processRequest
};