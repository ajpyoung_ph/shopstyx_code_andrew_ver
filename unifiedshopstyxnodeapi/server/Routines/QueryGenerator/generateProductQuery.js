const cleanString = require(GLOBAL.server_location+'/helpers/encodeDecode');

var start = function(searchPattern,dataJSON, dataStruct,cb,req,res)
{
	/*
	SELECT *,
	    IF(
	            `name` LIKE "searchterm%",  20, 
	         IF(`name` LIKE "%searchterm%", 10, 0)
	      )
	      + IF(`description` LIKE "%searchterm%", 5,  0)
	      + IF(`url`         LIKE "%searchterm%", 1,  0)
	    AS `weight`
	FROM `myTable`
	WHERE (
	    `name` LIKE "%searchterm%" 
	    OR `description` LIKE "%searchterm%"
	    OR `url`         LIKE "%searchterm%"
	)
	ORDER BY `weight` DESC
	LIMIT 20
	*/

	// searchPattern Scoring
	// searchPattern['search_history']
	// searchPattern['product_visited_history'] ok
	// searchPattern['color_search_history'] ok
	// searchPattern['search_string'] ok
	var select_clause = 'SELECT *,';
	var if_query = 'IF(';
	var end_if_query = ')';
	var tempString = '';
	var minor_if_query = 'IF(';
	var end_minor_if_query = ')';
	var color_if_query = 'IF(';
	var end_color_if_query = ')';
	var where_clause = '';

	if(typeof(dataJSON['where_clause'])=='undefined')
	{
		dataJSON['where_clause']='';
	}
	//score 20
	if(typeof(searchPattern['search_string'])!='undefined')
	{
		select_clause = select_clause + 'MATCH (`product_name`,`product_description`,`variant_titles_csv`,`product_color`,`product_sku`) AGAINST ("'+searchPattern['search_string']+'" IN NATURAL LANGUAGE MODE) as `text_score` ';
		if(dataJSON['where_clause']!='')
		{
			dataJSON['where_clause']=dataJSON['where_clause']+' AND ';
		}
		dataJSON['where_clause']= dataJSON['where_clause']+'(MATCH (`product_name`,`product_description`,`variant_titles_csv`,`product_color`,`product_sku`) AGAINST ("'+searchPattern['search_string']+'" IN NATURAL LANGUAGE MODE))';
		// if(dataJSON['where_clause']=='')
		// {
		// 	dataJSON['where_clause']=dataJSON['where_clause']+'WHERE (MATCH (`product_name`,`product_description`,`variant_titles_csv`,`product_color`,`product_sku`) AGAINST ("'+searchPattern['search_string']+'" IN NATURAL LANGUAGE MODE)) ';
		// }else{
		// 	dataJSON['where_clause']=dataJSON['where_clause']+'AND (MATCH (`product_name`,`product_description`,`variant_titles_csv`,`product_color`,`product_sku`) AGAINST ("'+searchPattern['search_string']+'" IN NATURAL LANGUAGE MODE)) ';
		// }
		//major IF query
		// if_query = if_query+'`product_name` LIKE "'+searchPattern['search_string']+'",20';

		// if_query = if_query+',IF(`product_name` LIKE "'+searchPattern['search_string']+'%",19';
		// end_if_query=end_if_query+')';
		// tempString = cleanString.removeWordsLessThanThreeCharacters(searchPattern['search_string']);
		// tempString = tempString.replace(' ','%');
		// if_query = if_query+',IF(`product_name` LIKE "%'+tempString+'%",9';
		// end_if_query=end_if_query+')';

		// //populate minor if
		// if(typeof(GLOBAL.collectWeight)!='undefined')
		// {
		// 	if(typeof(GLOBAL.collectWeight[dataJSON.uuid])!='undefined')
		// 	{
		// 		minor_if_query = minor_if_query+'`product_description` LIKE "'+GLOBAL.collectWeight[dataJSON.uuid]['search_key']+'",5';
		// 		minor_if_query = minor_if_query+',IF(`product_description` LIKE "'+GLOBAL.collectWeight[dataJSON.uuid]['search_key']+'%",4';
		// 		end_minor_if_query=end_minor_if_query+')';
		// 		minor_if_query = minor_if_query+',IF(`product_description` LIKE "%'+tempString+'%",3';
		// 		end_if_query=end_if_query+')';
		// 	}
		// }
	}
	if(typeof(searchPattern['product_visited_history'])!='undefined')
	{
		for(var x = 0;x<searchPattern['product_visited_history'].length;x++)
		{
			if(if_query.length!=3)
			{
				if_query=if_query+',IF(';
				end_if_query=end_if_query+')';
			}
			// if(minor_if_query.length!=3)
			// {
			// 	minor_if_query=minor_if_query+',IF(';
			// 	end_minor_if_query=end_minor_if_query+')';
			// }
			if_query = if_query+'`product_id`='+searchPattern['product_visited_history'][x]+',9';
		}
	}
	if(typeof(searchPattern['color_search_history'])!='undefined')
	{
		for(var x = 0;x<searchPattern['color_search_history'].length;x++)
		{
			if(if_query.length!=3)
			{
				if_query=if_query+',IF(';
				end_if_query=end_if_query+')';
			}
			if(color_if_query.length!=3)
			{
				color_if_query=color_if_query+',IF(';
				end_color_if_query=end_color_if_query+')';
			}
			//populate major if
			if_query = if_query+'`product_name` LIKE "%'+searchPattern['color_search_history'][x]+'%",6';
			
			//populate minor if
			end_color_if_query = end_color_if_query+'`product_desc` LIKE "%'+searchPattern['color_search_history'][x]+'%",1';
			end_color_if_query = end_color_if_query+',IF(`variant_titles_csv` LIKE "'+searchPattern['color_search_history'][x]+'",5';
			end_color_if_query=end_color_if_query+')';
		}
	}
	if(typeof(searchPattern['search_history'])!='undefined')
	{
		for(var x = 0;x<searchPattern['search_history'].length;x++)
		{
			if(if_query.length!=3)
			{
				if_query=if_query+',IF(';
				end_if_query=end_if_query+')';
			}
			if(minor_if_query.length!=3)
			{
				minor_if_query=minor_if_query+',IF(';
				end_minor_if_query=end_minor_if_query+')';
			}
			//populate major if
			if_query = if_query+'`product_name` LIKE "'+searchPattern['search_history'][x]+'",8';
			if_query = if_query+',IF(`product_name` LIKE "'+searchPattern['search_history'][x]+'%",7';
			end_if_query=end_if_query+')';
			tempString = cleanString.removeWordsLessThanThreeCharacters(searchPattern['search_history'][x]);
			tempString = tempString.replace(/ /g, "%");
			if_query = if_query+',IF(`product_name` LIKE "%'+tempString+'%",5';
			end_if_query=end_if_query+')';
			//populate minor if
			minor_if_query = minor_if_query+'`product_desc` LIKE "'+searchPattern['search_history'][x]+'",3';
			minor_if_query = minor_if_query+',IF(`product_desc` LIKE "'+searchPattern['search_history'][x]+'%",2';
			end_minor_if_query=end_minor_if_query+')';
			minor_if_query = minor_if_query+',IF(`product_desc` LIKE "%'+tempString+'%",1';
			end_minor_if_query=end_minor_if_query+')';
		}
	}
	
	//identify WHERE CLAUSE
	if(typeof(dataJSON.request.filter)!='undefined')
	{
		if(typeof(dataJSON.request.filter.color)!='undefined')
		{
			if(dataJSON['where_clause']!='')
			{
				dataJSON['where_clause']=dataJSON['where_clause']+' AND ';
			}
			dataJSON['where_clause']= dataJSON['where_clause']+'(`product_color` LIKE "%'+dataJSON.request.filter.color+'%" OR `variant_titles_csv` LIKE "'+dataJSON.request.filter.color+'")';
		}
		if(typeof(dataJSON.request.filter.department_id)!='undefined')
		{
			if(dataJSON['where_clause']!='')
			{
				dataJSON['where_clause']=dataJSON['where_clause']+' AND ';
			}
			dataJSON['where_clause']= dataJSON['where_clause']+'(`department_id`='+parseInt(dataJSON.request.filter.department_id)+')';
		}
		if(typeof(dataJSON.request.filter.category_id)!='undefined')
		{
			if(dataJSON['where_clause']!='')
			{
				dataJSON['where_clause']=dataJSON['where_clause']+' AND ';
			}
			dataJSON['where_clause']= dataJSON['where_clause']+'(`category_id`='+parseInt(dataJSON.request.filter.category_id)+')';
		}
		if(typeof(dataJSON.request.filter.price_range)!='undefined')
		{
			if(dataJSON['where_clause']!='')
			{
				dataJSON['where_clause']=dataJSON['where_clause']+' AND ';
			}
			dataJSON['where_clause']= dataJSON['where_clause']+'(`product_price` BETWEEN '+parseInt(dataJSON.request.filter.price_range.min)+' AND '+parseInt(dataJSON.request.filter.price_range.max)+')';
		}
	}
	//identify ORDER BY
	if(typeof(dataJSON.request.sort_algorithm)!='undefined')//dataJSON.request.sort_algorithm is an array
	{
		if(dataJSON.request.sort_algorithm.join("`").toLowerCase().replace(/\s/g, '').split("`").indexOf('latest')>-1)
		{
			if(dataJSON['order_by_clause']!='')
			{
				dataJSON['order_by_clause']=dataJSON['order_by_clause']+',';
			}
			dataJSON['order_by_clause']=dataJSON['order_by_clause']+"`date_added` ASC";
		}
		if(dataJSON.request.sort_algorithm.join("`").toLowerCase().replace(/\s/g, '').split("`").indexOf('prodnameasc')>-1)
		{
			if(dataJSON['order_by_clause']!='')
			{
				dataJSON['order_by_clause']=dataJSON['order_by_clause']+',';
			}
			dataJSON['order_by_clause']=dataJSON['order_by_clause']+"`product_name` ASC";
		}
		if(dataJSON.request.sort_algorithm.join("`").toLowerCase().replace(/\s/g, '').split("`").indexOf('prodnamedesc')>-1)
		{
			if(dataJSON['order_by_clause']!='')
			{
				dataJSON['order_by_clause']=dataJSON['order_by_clause']+',';
			}
			dataJSON['order_by_clause']=dataJSON['order_by_clause']+"`product_name` DESC";
		}
		if(dataJSON.request.sort_algorithm.join("`").toLowerCase().replace(/\s/g, '').split("`").indexOf('prodidasc')>-1)
		{
			if(dataJSON['order_by_clause']!='')
			{
				dataJSON['order_by_clause']=dataJSON['order_by_clause']+',';
			}
			dataJSON['order_by_clause']=dataJSON['order_by_clause']+"`product_id` ASC";
		}
		if(dataJSON.request.sort_algorithm.join("`").toLowerCase().replace(/\s/g, '').split("`").indexOf('prodiddesc')>-1)
		{
			if(dataJSON['order_by_clause']!='')
			{
				dataJSON['order_by_clause']=dataJSON['order_by_clause']+',';
			}
			dataJSON['order_by_clause']=dataJSON['order_by_clause']+"`product_id` DESC";
		}
		if(dataJSON.request.sort_algorithm.join("`").toLowerCase().replace(/\s/g, '').split("`").indexOf('ownedbyuserid')>-1)
		{
			if(dataJSON['where_clause']!='')
			{
				dataJSON['where_clause']=dataJSON['where_clause']+' AND ';
			}
			dataJSON['where_clause']= dataJSON['where_clause']+'(`owner_user_id`='+parseInt(dataJSON.request.additional_info.user_id)+')';
		}
		// for(var x=0;x<dataJSON.request.sort_algorithm.length;x++)
		// {
		// 	if(dataJSON['order_by_clause']!='' && dataJSON.request.sort_algorithm.toLowerCase()!='ownedbyuserid')
		// 	{
		// 		dataJSON['order_by_clause']=dataJSON['order_by_clause']+',';
		// 	}
			
		// 	switch(dataJSON.request.sort_algorithm.toLowerCase())
		// 	{
		// 		case 'latest':
		// 			dataJSON['order_by_clause']=dataJSON['order_by_clause']+"`date_added` ASC";
		// 			break;
		// 		case 'prodnameasc':
		// 			dataJSON['order_by_clause']=dataJSON['order_by_clause']+"`product_name` ASC";
		// 			break;
		// 		case 'prodnamedesc':
		// 			dataJSON['order_by_clause']=dataJSON['order_by_clause']+"`product_name` DESC";
		// 			break;
		// 		case 'prodidasc':
		// 			dataJSON['order_by_clause']=dataJSON['order_by_clause']+"`product_id` ASC";
		// 			break;
		// 		case 'prodiddesc':
		// 			dataJSON['order_by_clause']=dataJSON['order_by_clause']+"`product_id` DESC";
		// 			break;
		// 		case 'ownedbyuserid':
		// 			if(dataJSON['where_clause']!='')
		// 			{
		// 				dataJSON['where_clause']=dataJSON['where_clause']+' AND ';
		// 			}
		// 			dataJSON['where_clause']= dataJSON['where_clause']+'(`owner_user_id`='+parseInt(dataJSON.request.additional_info.user_id)+')';
		// 			break;
		// 	}
			
		// }
	}

	//end main IF
	if_query = if_query+',0';
	//end minor IF
	minor_if_query = minor_if_query+',0';
	//end color IF
	color_if_query = color_if_query+',0';
	//complete SELECT
	
	if((if_query+end_if_query!='IF(,0)'))
	{
		select_clause = select_clause+if_query+end_if_query;
	}
	if((minor_if_query+end_minor_if_query!='IF(,0)') && select_clause!='SELECT *,')
	{
		select_clause = select_clause+'+'+minor_if_query+end_minor_if_query;
	}else if((minor_if_query+end_minor_if_query!='IF(,0)') && select_clause=='SELECT *,')
	{
		select_clause = select_clause+minor_if_query+end_minor_if_query;
	}
	if((color_if_query+end_color_if_query!='IF(,0)') && select_clause!='SELECT *,')
	{
		select_clause = select_clause+'+'+color_if_query+end_color_if_query;
	}else if((color_if_query+end_color_if_query!='IF(,0)') && select_clause=='SELECT *,')
	{
		select_clause = select_clause+color_if_query+end_color_if_query;
	}

	if(select_clause=='SELECT *,')
	{
		select_clause = 'SELECT *'
	}else if((if_query+end_if_query!='IF(,0)') || (minor_if_query+end_minor_if_query!='IF(,0)') || (color_if_query+end_color_if_query!='IF(,0)')){
		select_clause = select_clause+' as `weight_score`';
	}
	//select_clause = 'SELECT *,'+if_query+end_if_query+'+'+minor_if_query+end_minor_if_query+'+'color_if_query+end_color_if_query+' as `weight_score`';

	//complete FROM 
	var from_clause = ' FROM `products`';
	//complete the WHERE
	if(typeof(dataJSON['where_clause'])!='undefined' && dataJSON['where_clause']!='')
	{
		where_clause = ' WHERE '+dataJSON['where_clause'];
	}else{
		where_clause = '';
 	}
 	var in_nat_lang_patt = new RegExp("IN NATURAL LANGUAGE MODE");

	//complete the ORDER BY
	//complete the ORDER BY
	if(((if_query+end_if_query!='IF(,0)') || (minor_if_query+end_minor_if_query!='IF(,0)') || (color_if_query+end_color_if_query!='IF(,0)') && (in_nat_lang_patt.test(select_clause)==true) && (typeof(dataJSON['order_by_clause'])!='undefined' && dataJSON['order_by_clause']!='')))
	{
		var order_by_clause = ' ORDER BY `text_score` DESC, `weight_score` DESC,'+dataJSON['order_by_clause'];
	}else if(((if_query+end_if_query!='IF(,0)') || (minor_if_query+end_minor_if_query!='IF(,0)') || (color_if_query+end_color_if_query!='IF(,0)') && (in_nat_lang_patt.test(select_clause)!=true) && (typeof(dataJSON['order_by_clause'])!='undefined' && dataJSON['order_by_clause']!='')))
	{
		var order_by_clause = ' ORDER BY `weight_score` DESC,'+dataJSON['order_by_clause'];
	}else if(((if_query+end_if_query=='IF(,0)') && (minor_if_query+end_minor_if_query=='IF(,0)') && (color_if_query+end_color_if_query=='IF(,0)') && (in_nat_lang_patt.test(select_clause)==true) && (typeof(dataJSON['order_by_clause'])!='undefined' && dataJSON['order_by_clause']!='')))
	{
		var order_by_clause = ' ORDER BY `text_score` DESC,'+dataJSON['order_by_clause'];
	}else if(((if_query+end_if_query=='IF(,0)') && (minor_if_query+end_minor_if_query=='IF(,0)') && (color_if_query+end_color_if_query=='IF(,0)') && (in_nat_lang_patt.test(select_clause)!=true) && (typeof(dataJSON['order_by_clause'])!='undefined' && dataJSON['order_by_clause']!='')))
	{
		var order_by_clause = ' ORDER BY '+dataJSON['order_by_clause'];
	}else if(((if_query+end_if_query=='IF(,0)') && (minor_if_query+end_minor_if_query=='IF(,0)') && (color_if_query+end_color_if_query=='IF(,0)') && (in_nat_lang_patt.test(select_clause)==true) && (typeof(dataJSON['order_by_clause'])=='undefined' || dataJSON['order_by_clause']=='')))
	{
		var order_by_clause = ' ORDER BY `text_score` DESC';
	}else if(((if_query+end_if_query!='IF(,0)') || (minor_if_query+end_minor_if_query!='IF(,0)') || (color_if_query+end_color_if_query!='IF(,0)') && (in_nat_lang_patt.test(select_clause)!=true) && (typeof(dataJSON['order_by_clause'])=='undefined' || dataJSON['order_by_clause']=='')))
	{
		var order_by_clause = ' ORDER BY `weight_score` DESC';
	}else{
		if(select_clause != 'SELECT *')
		{
			var order_by_clause = ' ORDER BY `weight_score` DESC';
		}else{
			var order_by_clause = ' ';
		}
		
	}
	

	//complete limit_clause
	if(typeof(dataJSON.request.number_items)=='undefined')
	{
		dataJSON.request['number_items']=GLOBAL.number_list_items;
	}
	if(typeof(dataJSON.request.page_number)=='undefined')
	{
		dataJSON.request['page_number']=GLOBAL.start_list_page;
	}

	var start_row = parseInt(dataJSON.request.number_items) * parseInt(dataJSON.request.page_number);
	var limit_clause = ' LIMIT '+start_row+','+parseInt(dataJSON.request.number_items);

	dataStruct['formed_query']=select_clause+from_clause+where_clause+order_by_clause+limit_clause+';';
	////console.log(dataStruct['formed_query']);
	// var msg = {
	 //        'status':'CHECKING LANG',
	 //        'desc':'Ka basa ka?'
	 //    };
	 //    res.json(msg);
		//scroring method
	cb(dataJSON,dataStruct,req,res);
};


module.exports={
	start:start
};