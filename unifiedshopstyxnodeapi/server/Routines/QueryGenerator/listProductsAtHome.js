//var uuid = require('node-uuid');
const collect_activity_search_history = require('../ActivitiesQueries/collectSearchHistory');
const collect_activity_products_visited = require('../ActivitiesQueries/collectProductsVisited');
const collect_activity_color_search = require('../ActivitiesQueries/collectColorSearchHistory');
const generateProductQuery = require(__dirname+'/generateProductQuery');
const util = require('util');

// GLOBAL.listProductsAtHome=[];
var start = function(dataJSON, dataStruct,cb,req,res)
{
	var myuuid = dataJSON['uuid'];//uuid.v1();
	
	setImmediate(collect_activity_search_history.start,myuuid,dataJSON, dataStruct,cb,collateData,req,res);
};
//collect the activity data and send to the query generator to be executed by the callback function
function collateData(myuuid,dataJSON, dataStruct,cb,req,res)
{
	//////console.log("^^^^^^^^^^^^^^^^^^ collateData Loop");
	if(util.isNullOrUndefined(dataJSON['product_visited_history'])==true)
	{
		//////console.log("&&&&&&&&&&&&&&&&& collecting products visited");
		setImmediate(collect_activity_products_visited.start,myuuid,dataJSON, dataStruct,cb,collateData,req,res);
	}else if(util.isNullOrUndefined(dataJSON['color_search_history'])==true)
	{
		//////console.log("&&&&&&&&&&&&&&&&& collecting color search");
		setImmediate(collect_activity_color_search.start,myuuid,dataJSON, dataStruct,cb,collateData,req,res);
	}
	if(util.isNullOrUndefined(dataJSON['search_history'])==false && util.isNullOrUndefined(dataJSON['product_visited_history'])==false && util.isNullOrUndefined(dataJSON['color_search_history'])==false)
	{
		var searchPattern=[];
		searchPattern['search_history']=dataJSON['search_history'].slice();
		searchPattern['product_visited_history']=dataJSON['product_visited_history'].slice();
		searchPattern['color_search_history']=dataJSON['color_search_history'].slice();
		if(util.isNullOrUndefined(dataJSON.request.additional_info)==false)
		{
			if(util.isNullOrUndefined(dataJSON.request.additional_info.search_key)==false)
			{
				searchPattern['search_string']=dataJSON.request.additional_info.search_key;
			}
		}
		dataJSON['where_query'] = '';
		//after forming query
		delete dataJSON['search_history'];
		delete dataJSON['product_visited_history'];
		delete dataJSON['color_search_history'];
		setImmediate(generateProductQuery.start,searchPattern,dataJSON, dataStruct,cb,req,res);
	}
}


module.exports={
	start:start
};