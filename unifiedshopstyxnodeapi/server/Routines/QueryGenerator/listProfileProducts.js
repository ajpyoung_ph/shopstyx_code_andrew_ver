//var uuid = require('uuid');
const collect_activity_search_history = require('../ActivitiesQueries/collectSearchHistory');
const collect_activity_products_visited = require('../ActivitiesQueries/collectProductsVisited');
const collect_activity_color_search = require('../ActivitiesQueries/collectColorSearchHistory');
const generateProductQuery = require(__dirname+'/generateProductQuery');
const util = require('util');

var start = function(dataJSON, dataStruct,cb,req,res)
{
	
	//list all products based on activities
	//collect all activity_search_history values
	var myuuid = dataJSON.security.uuid;
	collect_activity_search_history.start(myuuid,dataJSON, dataStruct,cb,collateData,req,res);
	//collect all activity_products_visited top 20 ORDER by visits
	collect_activity_products_visited.start(myuuid,dataJSON, dataStruct,cb,collateData,req,res);
	//collect all activity_color_search top 5 colors searchd
	collect_activity_color_search.start(myuuid,dataJSON, dataStruct,cb,collateData,req,res);

};

//collect the activity data and send to the query generator to be executed by the callback function
function collateData(myuuid,dataJSON, dataStruct,cb,req,res)
{
	//,req,res
	if(util.isNullOrUndefined(dataJSON['search_history'])==false && util.isNullOrUndefined(dataJSON['product_visited_history'])==false && util.isNullOrUndefined(dataJSON['color_search_history'])==false)
	{
		//scroring method
		var searchPattern=[];
		searchPattern['search_history']=dataJSON['search_history'].slice();
		searchPattern['product_visited_history']=dataJSON['product_visited_history'].slice();
		searchPattern['color_search_history']=dataJSON['color_search_history'].slice();
		if(typeof(dataJSON.request.additional_info)!='undefined')
		{
			if(typeof(dataJSON.request.additional_info.search_key)!='undefined')
			{
				searchPattern['search_string']=dataJSON.request.additional_info.search_key;
			}
		}
		dataJSON['where_query'] = '(`owner_user_id`='+parseInt(dataJSON.request.additional_info.user_id)+' AND `is_added_from_web`=1)';
		//after forming query
		setImmediate(generateProductQuery.start,searchPattern,dataJSON, dataStruct,cb,req,res);
	}
}


module.exports={
	start:start
};