const util = require('util');
const respond = require(GLOBAL.server_location+'/EventListeners/errorListener');
/*
"request":{
    "type":"Add"/"Delete"/"Update"/"Read"/"Read Members"/"Add Members"/"Remove Members"/"Find Collections Under Shipping Class",
    "group_owner_user_id":<int>,
    "additional_information":{
         "grouping_name":<string>,(for Add,Update)
         "grouping_id":<int>,(for Update,Delete,Read,Read Members,Add Members,Remove Members)
         "group_description":<string>,(for Add,Update)
         "product_ids":[<int>,<int>,<int>],(for Add Members,Remove Members)
         (optional)
         "shipping_class_id_implement":<int>,(for Add, Update, Find Collections Under Shipping Class)
         "group_owner_shop_id":<int>(for Add, Update)
    }
    
}
*/
const mysqlConv = require(GLOBAL.server_location+'/helpers/mysql_convertions');
var start = function(dataJSON,dataStruct,req,res)
{
	//grouping_type_id=1 is for Collections
	var error = false;
	var err_msg=[];
	if(typeof(dataJSON.request.additional_information.grouping_name)=='undefined')
	{
		error=true;
		err_msg.push('Collection Name not Found');
	}
	if(typeof(dataJSON.request.additional_information.group_description)=='undefined')
	{
		error=true;
		err_msg.push('Collection Description not Found');
	}
	if(typeof(dataJSON.request.additional_information.grouping_id)=='undefined')
	{
		error=true;
		err_msg.push('Collection ID not Found');
	}
	if(typeof(dataJSON.request.additional_information.group_owner_shop_id)=='undefined')
	{
		error=true;
		err_msg.push('Shop ID not Found');
	}
	if(error==false)
	{
		var query = 'UPDATE `product_groupings` SET `grouping_type_id`=1, `group_owner_user_id`='+parseInt(dataJSON.request.group_owner_user_id)+' , `grouping_name`="'+mysqlConv.mysql_real_escape_string(dataJSON.request.additional_information.grouping_name)+'", `group_description`="'+mysqlConv.mysql_real_escape_string(dataJSON.request.additional_information.group_description)+'" ';
		if(typeof(dataJSON.request.additional_information.group_owner_shop_id)!='undefined')
		{
			 query=query+', `group_owner_shop_id`='+parseInt(dataJSON.request.additional_information.group_owner_shop_id);
		}
		if(typeof(dataJSON.request.additional_information.shipping_class_id_implement)!='undefined')
		{
			query=query+', `shipping_class_id_implement`='+parseInt(dataJSON.request.additional_information.shipping_class_id_implement);
		}
		query=query+' WHERE `grouping_id`='+parseInt(dataJSON.request.additional_information.grouping_id)+';';

		var mysql = new GLOBAL.mysql.mysql_connect(query);
		mysql.results_ss_common().then(function(results){
			if(util.isError(results)==true){
				var msg = {
					"success":false, "status":"Error",
					'desc':'[updateCollection:start]DB Error Connection',
					'query':query,
					'message':results.message,
					'stack':results.stack||null
				};
				respond.respondError(msg,res,req);
			}else{
				var msg = {
					"success":true, "status":"Success",
					"desc":"[updateCollection:start]Updated Collection"
				};
				respond.respondError(msg,res,req);
			}
		});
	}else{
		var msg = {
			"success":false, "status":"Error",
			"desc":"[updateCollection:start]Missing Requirements",
			"err":err_msg
		};
		respond.respondError(msg,res,req);
		
						
						
	}
	
};

module.exports={
	start:start
};