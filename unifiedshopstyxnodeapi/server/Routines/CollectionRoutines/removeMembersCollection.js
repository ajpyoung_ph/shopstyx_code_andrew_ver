const util = require('util');
const respond = require(GLOBAL.server_location+'/EventListeners/errorListener');
const reportMsg = require('../ReportToAPI/Stories/reportMsg');
/*
"request":{
    "type":"Add"/"Delete"/"Update"/"Read"/"Read Members"/"Add Members"/"Remove Members"/"Find Collections Under Shipping Class",
    "group_owner_user_id":<int>,
    "additional_information":{
         "grouping_name":<string>,(for Add,Update)
         "grouping_id":<int>,(for Update,Delete,Read,Read Members,Add Members,Remove Members)
         "group_description":<string>,(for Add,Update)
         "product_ids":[<int>,<int>,<int>],(for Add Members,Remove Members)
         (optional)
         "shipping_class_id_implement":<int>,(for Add, Update, Find Collections Under Shipping Class)
         "group_owner_shop_id":<int>(for Add, Update)
    }
    
}
*/
const mysqlConv = require(GLOBAL.server_location+'/helpers/mysql_convertions');
const removeProductsFromGroup = require(__dirname+'/removeProductsFromGroup');
var start = function(dataJSON,dataStruct,req,res)
{
	//grouping_type_id=1 is for Collections
	var error = false;
	var err_msg=[];
	if(typeof(dataJSON.request.additional_information.grouping_id)=='undefined')
	{
		error=true;
		err_msg.push('Collection ID not Found');
	}
	if(typeof(dataJSON.request.additional_information.product_ids)=='undefined')
	{
		error=true;
		err_msg.push('Product List not Found');
	}
	if(typeof(dataJSON.request.additional_information.product_ids)!='undefined')
	{
		if(typeof(dataJSON.request.additional_information.product_ids)!='object')
		{
			error=true;
			err_msg.push('Product is not in a valid form');
		}
	}
	if(error==false)
	{
		setImmediate(removeProduct,dataJSON.request.additional_information.product_ids,dataJSON.request.group_owner_user_id,0);
		var msg = {
			"success":true, "status":"Success",
			"desc":"[deleteCollection:start]Data Deleted"
		};
		respond.respondError(msg,res,req);
						
						
	}else{
		var msg = {
			"success":false, "status":"Error",
			"desc":"[deleteCollection:start]Missing Requirements",
			"err":err_msg
		};
		respond.respondError(msg,res,req);
						
						
	}
	
};

function removeProduct(product_ids,group_owner_user_id,x)
{
	if(x<product_ids.length)
	{
		var product_id=product_ids[x];
		var query = 'DELETE FROM `product_groupings_product_members` WHERE `product_id`='+parseInt(product_id)+' AND `group_owner_user_id`='+parseInt(group_owner_user_id)+' AND `grouping_id`='+parseInt(dataJSON.request.additional_information.grouping_id);
		query=query+';';

		var mysql = new GLOBAL.mysql.mysql_connect(query);
		mysql.results_ss_common().then(function(results){
			if(util.isError(results)==true){
				var msg = {
					"success":false, "status":"Error",
					'desc':'[removeMembersCollection:removeProduct]DB Error Connection',
					'query':query,
					'message':results.message,
					'stack':results.stack||null
				};
				process.emit('Shopstyx:logError',msg);
			}else{
				dataJSON['reportForm']=[];
	            dataJSON['reportForm']['type']=[];
	            dataJSON['reportForm']['action']=[];
	            dataJSON['reportForm']['to']=[];
	            dataJSON.reportForm.type='profile';
	            dataJSON.reportForm.action='remove product to group';
	            dataJSON.reportForm.to.push(parseInt(dataJSON.request.group_owner_user_id));
				reportMsg.findGroupAndProduct(dataJSON,dataStruct,x);
				x=x+1;
				setImmediate(removeProduct,product_ids,group_owner_user_id,x);
				decrementCollection(dataJSON,dataStruct);
			}
		});
	}else{

	}
	
}

function decrementCollection(dataJSON,dataStruct)
{
	//grouping_type_id is fixed to 1
	var grouping_type_id = 1;
	try{
		var group_owner_shop_id = parseInt(dataJSON.request.additional_information.group_owner_shop_id);
		if(isNaN(group_owner_shop_id)==true)
		{
			group_owner_shop_id = 0;
		}
	}catch(error_owner_shop_id){
		var group_owner_shop_id = 0;
	}
	
	var unique_id = parseInt(dataJSON.request.additional_information.grouping_id) + '-' + grouping_type_id + '-' + parseInt(dataJSON.request.group_owner_user_id) + '-' + group_owner_shop_id;
	//need to finish this
	var query = 'INSERT INTO `product_groupings_summary` SET `unique_key`="'+unique_id+'", `grouping_id`='+parseInt(dataJSON.request.additional_information.grouping_id)+', `grouping_type_id`='+grouping_type_id+', `group_owner_user_id`='+parseInt(dataJSON.request.group_owner_user_id)+', `group_owner_shop_id`='+group_owner_shop_id+', `total_product`=`total_product`-1 ON DUPLICATE KEY UPDATE `total_product`=`total_product`-1;';
	var mysql = new GLOBAL.mysql.mysql_connect(query);
	mysql.results_ss_common().then(function(results){
		if(util.isError(results)==true){
			var msg = {
				"success":false, "status":"Error",
				'desc':'[addMembersCollection:incrementCollection]DB Error Connection',
				'query':query,
				'message':results.message,
				'stack':results.stack||null
			};
			process.emit('Shopstyx:logError',msg);
		}
	});
}

module.exports={
	start:start
};