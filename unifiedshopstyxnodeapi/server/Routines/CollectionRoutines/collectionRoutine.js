// var server_dir_array = (__dirname).toString().split("unifiedshopstyxnodeapi");
// GLOBAL.server_location = server_dir_array[0]+"commonnodeconfig";
const respond = require(GLOBAL.server_location+'/EventListeners/errorListener');
const addCollection = require(__dirname+'/addCollection');
const deleteCollection = require(__dirname+'/deleteCollection');
const updateCollection = require(__dirname+'/updateCollection');
const readCollection = require(__dirname+'/readCollection');
const readMembersCollection = require(__dirname+'/readMembersCollection');
const addMembersCollection = require(__dirname+'/addMembersCollection');
const removeMembersCollection = require(__dirname+'/removeMembersCollection');
const parseDataJSON = require(GLOBAL.server_location+'/helpers/parseDataJSON');
const util = require('util');
//var findCollectionViaShippingClass = require(__dirname+'/findCollectionViaShippingClass');

var processRequest = function(dataJSON,dataStruct,req,res)
{
	var error = false;
	var error_msg = [];
	if(typeof(dataJSON.request)=='undefined')
	{
		error=true;
		error_msg.push('Request Structure not found');
	}
	if(typeof(dataJSON.request)!='undefined')
	{
		if(typeof(dataJSON.request.type)=='undefined')
		{
			error=true;
			error_msg.push('Request Type not Found');
		}
		if(typeof(dataJSON.request.group_owner_user_id)=='undefined')
		{
			error=true;
			error_msg.push('Target Owner not Found');
		}
		if(typeof(dataJSON.request.type)!='undefined')
		{
			if(typeof(dataJSON.request.additional_information)=='undefined' && (dataJSON.request.type.toLowerCase().replace(/\s/g, '')!='list' && dataJSON.request.type.toLowerCase().replace(/\s/g, '')!='read'))
			{
				error=true;
				error_msg.push('Additional Information not Found');
			}
		}
		// if(typeof(dataJSON.request.additional_information)!='undefined')
		// {
		// 	if(typeof(dataJSON.request.additional_information)=='undefined')
		// 	{
		// 		error=true;
		// 		error_msg.push('Additional Information not Found');
		// 	}
		// }
	}
	if(error==false)
	{
		switch(dataJSON.request.type.toLowerCase().replace(/\s/g, ''))
		{
			//"type":"Add"/"Delete"/"Update"/"Read"/"Read Members"/"Add Members"/"Remove Members"/"Find Collections Under Shipping Class"
			case 'list':
				setImmediate(readCollection.start,dataJSON,dataStruct,req,res);
				break;
			case 'add':
				setImmediate(addCollection.start,dataJSON,dataStruct,req,res);
				break;
			case 'delete':
				setImmediate(deleteCollection.start,dataJSON,dataStruct,req,res);
				break;
			case 'update':
				setImmediate(updateCollection.start,dataJSON,dataStruct,req,res);
				break;
			case 'read':
				setImmediate(readCollection.start,dataJSON,dataStruct,req,res);
				break;
			case 'readmembers':
				setImmediate(readMembersCollection.start,dataJSON,dataStruct,req,res);
				break;
			case 'addmembers':
				setImmediate(addMembersCollection.start,dataJSON,dataStruct,req,res);
				break;
			case 'removemembers':
				setImmediate(removeMembersCollection.start,dataJSON,dataStruct,req,res);
				break;
			case 'findcollectionsundershippingclass':
				//findCollectionViaShippingClass.start(dataJSON,dataStruct);
				break;
			default:
				var msg = {
					"success":false, "status":"Error",
					"desc":"[collectionRoutine:processRequest]Unknown Type Request Found",
					"err":dataJSON.request.type
				};
				respond.respondError(msg,res,req);
				
				
				break;
		}
	}else{
		var msg = {
			"success":false, "status":"Error",
			"desc":"[collectionRoutine:processRequest]Incomplete Request Structure Detected",
			"err":error_msg
		};
		respond.respondError(msg,res,req);
		
		
	}
};

module.exports={
	processRequest:processRequest
};