const util = require('util');
var start = function(group_id,group_owner_user_id)
{
	var query = 'DELETE FROM `product_groupings_product_members` WHERE `grouping_id`='+parseInt(group_id)+' AND `group_owner_user_id`='+parseInt(group_owner_user_id)+'';
	query=query+';';

	var mysql = new GLOBAL.mysql.mysql_connect(query);
	mysql.results_ss_common().then(function(results){
		if(util.isError(results)==true){
			var msg = {
				"success":false, "status":"Error",
				'desc':'[removeProductsFromGroup:start]DB Error Connection',
				'query':query,
				'message':results.message,
				'stack':results.stack||null
			};
			process.emit('Shopstyx:logError',msg);
		}else{
			setImmediate(removeProductsFromGroup.start,parseInt(group_id),parseInt(group_owner_user_id));
		}
	});
};
module.exports={
	start:start
};