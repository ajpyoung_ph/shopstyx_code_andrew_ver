const respond = require(GLOBAL.server_location+'/EventListeners/errorListener');
const findImage = require('../ProductInfo/v2/client/findImage');
const findSEO = require("../ProductInfo/v2/client/findSEO");
const findAddFromWebInfo = require('../ProductInfo/v2/client/findAddFromWebInfo');
const findMonetaryUnit = require('../ProductInfo/v2/client/findMonetaryUnit');
const findLWHunit = require('../ProductInfo/v2/client/findLWHunit');
const findWeightUnit = require("../ProductInfo/v2/client/findWeightUnit");
const findShopInformation = require("../ProductInfo/v2/client/findShopInformation");

/*
"request":{
    "type":"Add"/"Delete"/"Update"/"Read"/"Read Members"/"Add Members"/"Remove Members"/"Find Collections Under Shipping Class",
    "group_owner_user_id":<int>,
    "additional_information":{
         "grouping_name":<string>,(for Add,Update)
         "grouping_id":<int>,(for Update,Delete,Read,Read Members,Add Members,Remove Members)
         "group_description":<string>,(for Add,Update)
         "product_ids":[<int>,<int>,<int>],(for Add Members,Remove Members)
         (optional)
         "shipping_class_id_implement":<int>,(for Add, Update, Find Collections Under Shipping Class)
         "group_owner_shop_id":<int>(for Add, Update)
    }
    
}
*/
const encodeDecode = require (GLOBAL.server_location+'/helpers/encodeDecode');
const mysqlConv = require(GLOBAL.server_location+'/helpers/mysql_convertions');
const util = require('util');



var start = function(dataJSON,dataStruct,req,res)
{
	//grouping_type_id=1 is for Collections
	var error = false;
	// if(typeof(dataJSON.request.additional_information.grouping_id)=='undefined')
	// {
	// 	error=true;
	// 	err_msg.push('Collection ID not Found');
	// }
	if(error==false)
	{
		var query = 'SELECT * FROM `product_groupings` WHERE `group_owner_user_id`='+parseInt(dataJSON.request.group_owner_user_id)+' OR `group_owner_user_id`=0 ORDER BY `grouping_name`;';

		var mysql = new GLOBAL.mysql.mysql_connect(query);
		mysql.results_ss_common().then(function(results){
			if(util.isError(results)==true){
				var msg = {
					"success":false, "status":"Error",
					'desc':'[readCollection:start]DB Error Connection',
					'query':query,
					'message':results.message,
					'stack':results.stack||null
				};
				respond.respondError(msg,res,req);
			}else{
				var rows=results;
				dataJSON['resVal']={
					"success":true, "status":"Success",
					"desc":"[readCollection:start]Collections Found",
					"data":[]
				};
				dataJSON.rows=rows;
				var product_grouping_array=[];
				for(var x=0;x<rows.length;x++)
				{
					product_grouping_array.push(rows[x].grouping_id);
				}
				// //console.log("product_grouping_array");
				// //console.log(product_grouping_array);
				setImmediate(readTopMembers,product_grouping_array,dataJSON,dataStruct,req,res);
			}
		});
	}else{
		var msg = {
			"success":false, "status":"Error",
			"desc":"[readCollection:start]Missing Requirements",
			"err":err_msg
		};
		respond.respondError(msg,res,req);
						
						
	}
	
};

const readTopMembers = function(product_grouping_array,dataJSON,dataStruct,req,res)
{
	dataJSON['product_rows']=[];
	iterateTopMembers(0,product_grouping_array,dataJSON,dataStruct,req,res)
}
function iterateTopMembers(x,product_grouping_array,dataJSON,dataStruct,req,res)
{
	var now = Date.now();
	var query = 'SELECT pgpm.grouping_id, pgpm.group_owner_user_id, p.* FROM `product_groupings_product_members` as pgpm LEFT JOIN `products` as p ON (pgpm.product_id = p.product_id) WHERE pgpm.grouping_id = '+product_grouping_array[x]+' AND pgpm.group_owner_user_id='+parseInt(dataJSON.request.group_owner_user_id)+' ORDER BY p.date_added';

	if(util.isNullOrUndefined(dataStruct['readMembers'])==false)
	{
		query = query +';';
	}else{
		query = query +' LIMIT 3;';
	}
	console.log("Getting Data for readTopMembers ============ "+Date());
	console.log(query);
	var mysql = new GLOBAL.mysql.mysql_connect(query);
	mysql.results_ss_common().then(function(results){
		console.log("response from MySQL readTopMembers ============= "+Date());
		console.log("MySQL response readTopMembers =========> "+(Date.now()-now)+" ms");
		if(util.isError(results)==true){
			var msg = {
				"success":false, "status":"Error",
				'desc':'[readCollection:iterateTopMembers]DB Error Connection',
				'query':query,
				'message':results.message,
				'stack':results.stack||null
			};
			process.emit('Shopstyx:logError',msg);
			dataJSON.rows[x]['row_count']=0;
			data = [];
			dataJSON['product_rows'] = dataJSON['product_rows'].concat(data);
			if(x<product_grouping_array.length-1)
			{
				x=x+1;
				setImmediate(iterateTopMembers,x,product_grouping_array,dataJSON,dataStruct,req,res);
			}else{
				setImmediate(iterateRows,0,dataJSON,dataStruct,req,res);
			}
		}else{
			var rows=results;
			
			////console.log(rows);
				////console.log("//////////// iterating "+x+" : "+dataJSON.rows.length-1);
			dataStruct['returnMax_totalRows']=dataJSON.rows.length;
			dataJSON['product_rows'] = dataJSON['product_rows'].concat(rows);
			// //console.log('product_rows');
			// //console.log(dataJSON['product_rows']);
			if(x<product_grouping_array.length-1)
			{
				x=x+1;
				setImmediate(iterateTopMembers,x,product_grouping_array,dataJSON,dataStruct,req,res);
			}else{
				setImmediate(iterateRows,0,dataJSON,dataStruct,req,res);
			}
		}
	});
};

function iterateRows(x,dataJSON,dataStruct,req,res)
{
	////console.log('calling getTotalRows '+x+" : "+dataJSON.rows.length);
	setImmediate(getTotalRows,dataJSON.rows[x],x,dataJSON,dataStruct,req,res);
}
function getTotalRows(targetData,x,dataJSON,dataStruct,req,res)
{
	if(util.isNullOrUndefined(targetData)==false)
	{
		var now = Date.now();
		var query2 = 'SELECT * FROM `product_groupings_summary` WHERE grouping_id='+parseInt(targetData.grouping_id)+' AND group_owner_user_id='+parseInt(dataJSON.request.group_owner_user_id)+' AND `grouping_type_id`=1 AND `group_owner_shop_id`=0;';
		console.log(query2);
		console.log("Getting Data for getTotalRows ============ "+Date());
		var mysql = new GLOBAL.mysql.mysql_connect(query2);
		mysql.results_ss_common().then(function(results){
			console.log("response from MySQL getTotalRows ============= "+Date());
			console.log("MySQL response getTotalRows =========> "+(Date.now()-now)+" ms");
			var count = 0;
			if(util.isError(results)==true){
				var msg = {
					"success":false, "status":"Error",
					'desc':'[readCollection:getTotalRows]DB Error Connection',
					'query':query2,
					'message':results.message,
					'stack':results.stack||null
				};
				process.emit('Shopstyx:logError',msg);
				dataJSON['rows'][x]['row_count']=[];
				dataJSON['rows'][x]['row_count']=0;
				//console.log('calling collectResults from error '+x);
				setImmediate(collectResults,x,dataJSON,dataStruct,req,res);
			}else{
				//total_product
				dataJSON['rows'][x]['row_count']=[];
				if(results.length>0)
				{
					dataJSON['rows'][x]['row_count']=results[0].total_product;
				}else{
					dataJSON['rows'][x]['row_count']=0;
				}
				//console.log('calling collectResults '+x);
				setImmediate(collectResults,x,dataJSON,dataStruct,req,res);
			}
		});
	}else{
		var msg = {
			"success":false, "status":"Error",
			"desc":"[readCollection:getTotalRows]Error reading in DB",
			"err":err2,
			"query":query2
		};
		process.emit('Shopstyx:logError',msg);
		dataJSON['rows'][x]['row_count']=[];
		dataJSON['rows'][x]['row_count']=0;
		//console.log('calling collectResults from error '+x);
		setImmediate(collectResults,x,dataJSON,dataStruct,req,res);
	}
}

function collectResults(x,dataJSON,dataStruct,req,res)
{
	////console.log('comparing '+x+" : "+dataJSON.rows.length-1);
	if(x==dataJSON.rows.length-1)
	{
		// dataJSON['resVal']['data']=dataJSON.rows;
		// respond.respondError(dataJSON['resVal'],res,req);

		//at this point we have all the 
		setImmediate(breakdownProdIds,dataJSON,dataStruct,req,res);
	}else{
		x=x+1;
		setImmediate(iterateRows,x,dataJSON,dataStruct,req,res);
	}
}

const breakdownProdIds=function(dataJSON,dataStruct,req,res)
{
	var rows = dataJSON.product_rows;
	var prod_ids = [];
	var shop_ids = [];
	var variant_product_ids = [];
	var variant_color_names = [];
	for(var x = 0 ;x<rows.length;x++)
	{
		if(isNaN(parseInt(rows[x].product_id))==false)
		{
			prod_ids.push(parseInt(rows[x].product_id));
		}else{
			//console.log("product id NaN "+rows[x].product_id);
			//console.log(parseInt(rows[x].product_id));
			//console.log(isNaN(parseInt(rows[x].product_id)));
		}
		if(isNaN(parseInt(rows[x].owner_shop_id))==false)
		{
			shop_ids.push(parseInt(rows[x].owner_shop_id));
		}else{
			//console.log("owner_shop_id id NaN "+rows[x].owner_shop_id);
			//console.log(parseInt(rows[x].owner_shop_id));
			//console.log(isNaN(parseInt(rows[x].owner_shop_id)));
		}
		
		if(typeof(dataJSON.request.filter)!='undefined')
		{
			if(typeof(dataJSON.request.filter.color)!='undefined')
			{
				if(row[x].product_color.toLowerCase()!=dataJSON.request.filter.color.toLowerCase().trim())
				{
					variant_product_ids.push(rows[x].product_id);
					if(variant_color_names.length==0)
					{
						variant_color_names.push(dataJSON.request.filter.color.toLowerCase().trim());
					}
				}
			}
		}
	}
	// //console.log("**************************** prod_ids ***********************");
	// //console.log(prod_ids);
	// //console.log("*************************** shop_ids ************************");
	// //console.log(shop_ids);
	dataStruct['returnMax']=4;
	dataStruct['currentCounter']=0;
	// if(variant_color_names.length>0)
	// {
	// 	dataStruct['returnMax']=5;
	// }
	//using prod_ids
	setImmediate(findImage.findPrimaryImage,prod_ids,dataJSON,dataStruct,formResponse,req,res);
	setImmediate(findAddFromWebInfo.findAddFromWebInfo,prod_ids,dataJSON,dataStruct,formResponse,req,res);
	setImmediate(findSEO.start,prod_ids,dataJSON,dataStruct,formResponse,req,res);
	//using shop_ids
	setImmediate(findShopInformation.start,shop_ids,dataJSON,dataStruct,formResponse,req,res);
	//using variant_color_names
	// if(variant_color_names.length>0)
	// {
	// 	setImmediate(findImage.findVariantImage,prod_ids,variant_color_names,dataJSON,dataStruct,formResponse,req,res);
	// }
}

function formResponse(additional_rows,dataJSON,dataStruct,req,res)
{
	//var rows = dataJSON.rows;
	var error =false;
	////console.log('Processing '+dataStruct['currentCounter']+":"+dataStruct['returnMax']);
	// if(additional_rows.length>0 && util.isNullOrUndefined(dataJSON.rows)==false && dataJSON.rows.length>0)
	// {
		//identify if AddFromWeb
		try{
			if(util.isNullOrUndefined(additional_rows[0].url_source)==false)
			{
				//dataJSON.rows[x]['additional_info']=[];
				//process AddFromWeb
				for(var x=0;x<dataJSON.product_rows.length;x++)
				{
					for(var y=0;y<additional_rows.length;y++)
					{
						if(dataJSON.product_rows[x].product_id==additional_rows[y].product_id)
						{
							dataJSON.product_rows[x]['additional_info']=additional_rows[y];
							break;
						}
					}
				}
			}
		}catch(err){
			// //console.log(err.message);
			// //console.log(err.stack);
			//console.log('Processing '+dataStruct['currentCounter']+":"+dataStruct['returnMax']);
		}
		
		//identify if SEO
		try{
			if(util.isNullOrUndefined(additional_rows[0].seo_title)==false)
			{
				//dataJSON.rows[x]['SEO']=[];
				//process SEO
				for(var x=0;x<dataJSON.product_rows.length;x++)
				{
					for(var y=0;y<additional_rows.length;y++)
					{
						if(dataJSON.product_rows[x].product_id==additional_rows[y].product_id)
						{
							dataJSON.product_rows[x]['SEO']=additional_rows[y];
							break;
						}
					}
				}
			}
		}catch(err){
			// //console.log(err.message);
			// //console.log(err.stack);
			//console.log('Processing '+dataStruct['currentCounter']+":"+dataStruct['returnMax']);
		}
		
		//identify if shopInformation
		try{
			if(util.isNullOrUndefined(additional_rows[0].store_id)==false)
			{
				//dataJSON.rows[x]['shop_information']=[];
				//process SEO
				for(var x=0;x<dataJSON.product_rows.length;x++)
				{
					for(var y=0;y<additional_rows.length;y++)
					{
						if(dataJSON.product_rows[x].store_id==additional_rows[y].store_id)
						{
							dataJSON.product_rows[x]['shop_information']=additional_rows[y];
							break;
						}
					}
				}
			}
		}catch(err){
			// //console.log(err.message);
			// //console.log(err.stack);
			//console.log('Processing '+dataStruct['currentCounter']+":"+dataStruct['returnMax']);
		}
		
		//identify if Primary Images
		try{
			if(util.isNullOrUndefined(additional_rows[0].product_image_id)==false)
			{
				//process Primary Images
				////console.log("found images");
				for(var x=0;x<dataJSON.product_rows.length;x++)
				{
					dataJSON.product_rows[x]['product_images']=[];
					for(var y=0;y<additional_rows.length;y++)
					{
						if(dataJSON.product_rows[x].product_id==additional_rows[y].product_id)
						{
							////console.log("pushing primary images");
							try{
								dataJSON.product_rows[x]['product_images'].push(additional_rows[y]);
							}catch(err){
								
								if(util.isNullOrUndefined(dataJSON.product_rows[x])==true)
								{
									//console.log("dataJSON.product_rows[x] is undefined");
									//console.log(x);
								}
								// //console.log(err.message);
								// //console.log(err.stack);
								break;
							}
							
						}
					}
				}
			}
		}catch(err){
			// //console.log(err.message);
			// //console.log(err.stack);
			//console.log('Processing '+dataStruct['currentCounter']+":"+dataStruct['returnMax']);
		}
		
		//identify if Variant Images
		// if(util.isNullOrUndefined(additional_rows[0].variant_id)==false)
		// {
		// 	//process SEO
		// 	for(var x=0;x<dataJSON.product_rows.length;x++)
		// 	{
		// 		var firstStepIn = true;
		// 		for(var y=0;y<additional_rows.length;y++)
		// 		{
		// 			if(dataJSON.product_rows[x].product_id==additional_rows[y].product_id)
		// 			{
		// 				if( (firstStepIn==true) && (dataJSON.product_rows[x]['product_images'].length>0) )
		// 				{
		// 					dataJSON.product_rows[x]['product_images']=[];
		// 					firstStepIn=false;
		// 				}
		// 				dataJSON.product_rows[x]['product_images'].push(additional_rows[y]);
		// 			}
		// 		}
		// 	}
		// }
	//}
	dataStruct['currentCounter']=dataStruct['currentCounter']+1;
	//////console.log("inside attachImage "+dataStruct['returnMax']+" : "+dataStruct['currentCounter']);

	if(dataStruct['currentCounter']==dataStruct['returnMax'])
	{
		//populate defaults
		for(var x=0;x<dataJSON.product_rows.length;x++)
		{
			for(var y=0;y<GLOBAL.default_length_units.length;y++)
			{
				if(dataJSON.product_rows[x].length_unit_id==GLOBAL.default_length_units[y].length_unit_id)
				{
					dataJSON.product_rows[x]['lwh_unit']=GLOBAL.default_length_units[y].length_unit_shortcut;
					break;
				}
			}
			for(var y=0;y<GLOBAL.default_monetary_units.length;y++)
			{
				if(dataJSON.product_rows[x].monetary_unit_id==GLOBAL.default_monetary_units[y].money_id)
				{
					dataJSON.product_rows[x]['monetary_unit']=GLOBAL.default_monetary_units[y].money_sign;
					break;
				}
			}
			for(var y=0;y<GLOBAL.default_weight_units.length;y++)
			{
				if(dataJSON.product_rows[x].weight_unit_id==GLOBAL.default_weight_units[y].weight_units_id)
				{
					dataJSON.product_rows[x]['weight_unit']=GLOBAL.default_weight_units[y].weight_units_shortcut;
					break;
				}
			}
		}
		//match dataJSON.row[x].grouping_id with dataJSON.product_rows[y].grouping_id
		for(var x=0;x<dataJSON.rows.length;x++)
		{
			dataJSON.rows[x]['products']=[];
			for(var y=0;y<dataJSON.product_rows.length;y++)
			{
				////console.log('comparing '+parseInt(dataJSON.rows[x].grouping_id)+" : "+parseInt(dataJSON.product_rows[y].grouping_id));
				if(parseInt(dataJSON.rows[x].grouping_id)==parseInt(dataJSON.product_rows[y].grouping_id))
				{
					dataJSON.rows[x]['products'].push(dataJSON.product_rows[y]);
				}
			}
		}
		dataJSON['resVal'].data=dataJSON.rows;
		respond.respondError(dataJSON['resVal'],res,req);
		delete dataJSON;
		delete dataStruct;

	}
}

module.exports={
	start:start,
	readTopMembers:readTopMembers
};