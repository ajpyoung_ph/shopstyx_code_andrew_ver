const respond = require(GLOBAL.server_location+'/EventListeners/errorListener');
const util=require('util');
/*
"request":{
    "type":"Add"/"Delete"/"Update"/"Read"/"Read Members"/"Add Members"/"Remove Members"/"Find Collections Under Shipping Class",
    "group_owner_user_id":<int>,
    "additional_information":{
         "grouping_name":<string>,(for Add,Update)
         "grouping_id":<int>,(for Update,Delete,Read,Read Members,Add Members,Remove Members)
         "group_description":<string>,(for Add,Update)
         "product_ids":[<int>,<int>,<int>],(for Add Members,Remove Members)
         (optional)
         "shipping_class_id_implement":<int>,(for Add, Update, Find Collections Under Shipping Class)
         "group_owner_shop_id":<int>(for Add, Update)
    }
    
}
*/
const mysqlConv = require(GLOBAL.server_location+'/helpers/mysql_convertions');
var start = function(dataJSON,dataStruct,req,res)
{
	//grouping_type_id=1 is for Collections
	var error = false;
	var err_msg=[];
	if(typeof(dataJSON.request.additional_information.grouping_name)=='undefined')
	{
		error=true;
		err_msg.push('Collection Name not Found');
	}
	if(typeof(dataJSON.request.additional_information.group_description)=='undefined')
	{
		error=true;
		err_msg.push('Collection Description not Found');
	}
	if(typeof(dataJSON.request.additional_information.group_owner_shop_id)=='undefined')
	{
		error=true;
		err_msg.push('Shop ID not Found');
	}
	if(error==false)
	{
		var query = 'INSERT INTO `product_groupings` SET `grouping_type_id`=1, `group_owner_user_id`='+parseInt(dataJSON.request.group_owner_user_id)+' , `grouping_name`="'+mysqlConv.mysql_real_escape_string(dataJSON.request.additional_information.grouping_name)+'", `group_description`="'+mysqlConv.mysql_real_escape_string(dataJSON.request.additional_information.group_description)+'" ';
		if(typeof(dataJSON.request.additional_information.group_owner_shop_id)!='undefined')
		{
			 query=query+', `group_owner_shop_id`='+parseInt(dataJSON.request.additional_information.group_owner_shop_id);
		}
		if(typeof(dataJSON.request.additional_information.shipping_class_id_implement)!='undefined')
		{
			query=query+', `shipping_class_id_implement`='+parseInt(dataJSON.request.additional_information.shipping_class_id_implement);
		}
		query=query+';';

		var mysql = new GLOBAL.mysql.mysql_connect(query);
		mysql.results_ss_common().then(function(results){
			if(util.isError(results)==true){
				var msg = {
					"success":false, "status":"Error",
					'desc':'[addCollection:start]DB Error Connection',
					'query':query,
					'message':results.message,
					'stack':results.stack||null
				};
				respond.respondError(msg,res,req);
			}else{
				var resultDoc=results;
				var msg = {
					"success":true, "status":"Success",
					"desc":"[addCollection:start]Data Saved",
					"grouping_id":resultDoc.insertId
				};
				respond.respondError(msg,res,req);
				dataJSON.request.additional_information.grouping_id = resultDoc.insertId;
				incrementCollection(dataJSON,dataStruct)
			}
		});
	}else{
		var msg = {
			"success":false, "status":"Error",
			"desc":"[addCollection:start]Missing Requirements",
			"err":err_msg
		};
		respond.respondError(msg,res,req);
		
		
	}
	
};

var incrementCollection=function(dataJSON,dataStruct)
{
	//grouping_type_id is fixed to 1
	var grouping_type_id = 1;
	try{
		var group_owner_shop_id = parseInt(dataJSON.request.additional_information.group_owner_shop_id);
		if(isNaN(group_owner_shop_id)==true)
		{
			group_owner_shop_id = 0;
		}
	}catch(error_owner_shop_id){
		var group_owner_shop_id = 0;
	}
	
	var unique_id = parseInt(dataJSON.request.additional_information.grouping_id) + '-' + grouping_type_id + '-' + parseInt(dataJSON.request.group_owner_user_id) + '-' + group_owner_shop_id;
	//need to finish this
	var query = 'INSERT INTO `product_groupings_summary` SET `unique_key`="'+unique_id+'", `grouping_id`='+parseInt(dataJSON.request.additional_information.grouping_id)+', `grouping_type_id`='+grouping_type_id+', `group_owner_user_id`='+parseInt(dataJSON.request.group_owner_user_id)+', `group_owner_shop_id`='+group_owner_shop_id+', `total_product`=0;';
	var mysql = new GLOBAL.mysql.mysql_connect(query);
	mysql.results_ss_common().then(function(results){
		if(util.isError(results)==true){
			var msg = {
				"success":false, "status":"Error",
				'desc':'[addCollection:incrementCollection]DB Error Connection',
				'query':query,
				'message':results.message,
				'stack':results.stack||null
			};
			process.emit('Shopstyx:logError',msg);
		}
	});
}
module.exports={
	start:start
};