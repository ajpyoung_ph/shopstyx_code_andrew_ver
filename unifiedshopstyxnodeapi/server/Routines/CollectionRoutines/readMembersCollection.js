const util = require('util');
const respond = require(GLOBAL.server_location+'/EventListeners/errorListener');
const readCollection = require(__dirname+'/readCollection');
/*
"request":{
    "type":"Add"/"Delete"/"Update"/"Read"/"Read Members"/"Add Members"/"Remove Members"/"Find Collections Under Shipping Class",
    "group_owner_user_id":<int>,
    "additional_information":{
         "grouping_name":<string>,(for Add,Update)
         "grouping_id":<int>,(for Update,Delete,Read,Read Members,Add Members,Remove Members)
         "group_description":<string>,(for Add,Update)
         "product_ids":[<int>,<int>,<int>],(for Add Members,Remove Members)
         (optional)
         "shipping_class_id_implement":<int>,(for Add, Update, Find Collections Under Shipping Class)
         "group_owner_shop_id":<int>(for Add, Update)
    }
    
}
*/
const mysqlConv = require(GLOBAL.server_location+'/helpers/mysql_convertions');
var start = function(dataJSON,dataStruct,req,res)
{
	//grouping_type_id=1 is for Collections
	var error = false;
	if(typeof(dataJSON.request.additional_information.grouping_id)=='undefined')
	{
		error=true;
		err_msg.push('Collection ID not Found');
	}
	if(error==false)
	{
		var query = 'SELECT * FROM `product_groupings` WHERE `grouping_id`='+parseInt(dataJSON.request.additional_information.grouping_id)+' AND `group_owner_user_id`='+parseInt(dataJSON.request.group_owner_user_id)+';';
		////console.log(query)
		var mysql = new GLOBAL.mysql.mysql_connect(query);
		mysql.results_ss_common().then(function(results){
			if(util.isError(results)==true){
				var msg = {
					"success":false, "status":"Error",
					'desc':'[readCollection:start]DB Error Connection',
					'query':query,
					'message':results.message,
					'stack':results.stack||null
				};
				respond.respondError(msg,res,req);
			}else{
				var rows=results;
				dataStruct['readMembers']=true;
				dataJSON['resVal']={
					"success":true, "status":"Success",
					"desc":"[readCollection:start]Collections Found",
					"data":[]
				};
				dataJSON.rows=rows;
				var product_grouping_array=[];
				for(var x=0;x<rows.length;x++)
				{
					product_grouping_array.push(rows[x].grouping_id);
				}
				//readTopMembers = function(product_grouping_array,dataJSON,dataStruct,req,res)
				setImmediate(readCollection.readTopMembers,product_grouping_array,dataJSON,dataStruct,req,res);
				//setTimeout(respond.respondError,2000,dataJSON['resVal'],res,req);
				//setImmediate(readCollection.readTopMembers,rows,dataJSON,dataStruct,0,req,res);
			}
		});
	}else{
		var msg = {
			"success":false, "status":"Error",
			"desc":"[readCollection:start]Missing Requirements",
			"err":err_msg
		};
		respond.respondError(msg,res,req);
						
						
	}
	
};

function getProducts(resVal,req,res)
{
	var query = 'SELECT pgpm.*, p.* FROM `product_groupings_product_members` as pgpm LEFT JOIN `products` as p ON (pgpm.product_id = p.product_id) WHERE pgpm.grouping_id='+parseInt(resVal.data[0].additional_information.grouping_id)+' AND pgpm.group_owner_user_id='+parseInt(resVal.data[0].group_owner_user_id)+';';
	var mysql = new GLOBAL.mysql.mysql_connect(query);
	mysql.results_ss_common().then(function(results){
		if(util.isError(results)==true){
			var msg = {
				"success":false, "status":"Error",
				'desc':'[readMembersCollection:getProducts]DB Error Connection',
				'query':query,
				'message':results.message,
				'stack':results.stack||null
			};
			respond.respondError(msg,res,req);
		}else{
			var rows=results;
			resVal.data['products']=rows;
			setImmediate(getProducts,resVal,req,res);
		}
	});
}
module.exports={
	start:start
};