const util = require('util');
const request = require('request');
const respond = require(GLOBAL.server_location+'/EventListeners/errorListener');

var findGroupAndProduct = function(dataJSON,dataStruct,x)
{
    var query = "SELECT `product_name` FROM `products` WHERE `product_id`="+parseInt(dataJSON.request.additional_information.product_ids[x])+" LIMIT 1;";
    var mysql = new GLOBAL.mysql.mysql_connect(query);
    mysql.results_ss_common().then(function(results){
        if(util.isError(results)==true){
            var msg = {
                "success":false, "status":"Error",
                'desc':'[reportMsg:findGroupAndProduct]DB Error Connection',
                'query':query,
                'message':results.message,
                'stack':results.stack||null
            };
            process.emit('Shopstyx:logError',msg);
        }else{
            var dataToBeSaved = {
                "product_name":results[0].product_name,
                "product_id":parseInt(dataJSON.request.additional_information.product_ids[x]),
                "group_name":'',
                "grouping_id":parseInt(dataJSON.request.additional_information.grouping_id),
                "action":"add to collection"
            };
            var q2 = "SELECT `grouping_name` FROM `product_groupings` WHERE `grouping_id`="+parseInt(dataJSON.request.additional_information.grouping_id)+""
            var mysql2 = new GLOBAL.mysql.mysql_connect(q2);
            mysql2.results_ss_common().then(function(results2){
                if(util.isError(results)==true){
                    var msg = {
                        "success":false, "status":"Error",
                        'desc':'[reportMsg:findGroupAndProduct:q2]DB Error Connection',
                        'query':q2,
                        'message':results2.message,
                        'stack':results2.stack||null
                    };
                    process.emit('Shopstyx:logError',msg);
                }else{
                    dataToBeSaved.group_name = results2[0].grouping_name;
                    getRobotApiKey(dataToBeSaved,dataJSON,dataStruct);
                }
            });
        }
    });
}
var getRobotApiKey = function(dataSavedToMongoDB,dataJSON,dataStruct)
{
    var query = 'SELECT * FROM `default_settings` WHERE `group`="api" AND `key`="webservice_signature";';

    var mysql = new GLOBAL.mysql.mysql_connect(query);
    mysql.results_ss_common().then(function(results){
        if(util.isError(results)==true){
            var msg = {
                "success":false, "status":"Error",
                'desc':'[callCreateStyx:start]DB Error Connection',
                'query':query,
                'message':results.message,
                'stack':results.stack||null
            };
            respond.logError(msg);
        }else{
            var resultDoc = results;
            var key = resultDoc[0].value;
            reportMsgProfile(key,dataSavedToMongoDB,dataJSON,dataStruct,0);
        }
    });
}

function reportMsgProfile(key,dataSavedToMongoDB,dataJSON,dataStruct,x)
{
    if( util.isNullOrUndefined(dataJSON.reportForm.to)==false)
    {
        var myForm = {
            "form":{
                "key":key,
                "type":dataJSON.reportForm.type,
                "action":dataJSON.reportForm.action,
                "message":dataSavedToMongoDB
            }
        };
        myForm['form']['to']=[];
        myForm.form.to=JSON.stringify(dataJSON.reportForm.to);
        request.post(GLOBAL.HTTP_location+'/api/v2/stories/add_stories', myForm, function(err,httpResponse,body){
            try{
                var sentData = '';
                ////console.log("{{{{{{{{{{{{{{{{{{{{{{{ Return FROM /api/v2/notifications/createstyx }}}}}}}}}}}}}}");
                ////console.log(body);
                if(typeof(body)=='string')
                {
                    sentData = JSON.parse(body);
                }else{
                    if(typeof(body)=='object')
                    {
                        sentData = JSON.parse(JSON.stringify(body));
                    }
                }
                if(sentData.success==false)
                {
                    //don't do anything yet
                    x=x+1;
                    if(x>10)
                    {
                        var msg = {
                            "success":false, "status":"Error",
                            "desc":"[reportMsg:reportMsgProfile] Clint's API keep saying error"
                        };
                        respond.logError(msg);
                    }else{
                        setTimeout(reportMsgProfile,2000,key,dataSavedToMongoDB,dataJSON,dataStruct,x);
                    }
                }
             }catch(error_here){
                var msg = {
                    "success":false, "status":"Error",
                    "desc":"[reportMsg:reportMsgProfile] Error occurred code 1",
                    "err":error_here,
                    "stack":error_here.stack,
                    "body":body,
                    "myForm":myForm||"No Form sent"
                };
                respond.logError(msg);
             }
        });
    }
    if(util.isNullOrUndefined(dataJSON.reportForm.styx_id)==false)
    {
        reportMsgStyx(key,dataSavedToMongoDB,dataJSON,dataStruct,0);
    }
}

function reportMsgStyx(key,dataSavedToMongoDB,dataJSON,dataStruct,x)
{
    var myForm = {
        "form":{
            "key":key,
            "type":dataJSON.reportForm.type,
            "action":dataJSON.reportForm.action,
            "message":dataSavedToMongoDB,
            "styx_id":dataJSON.reportForm.styx_id
        }
    };
    request.post(GLOBAL.HTTP_location+'/api/v2/stories/add_stories', myform, function(err,httpResponse,body){
        try{
            var sentData = '';
            ////console.log("{{{{{{{{{{{{{{{{{{{{{{{ Return FROM /api/v2/notifications/createstyx }}}}}}}}}}}}}}");
            ////console.log(body);
            if(typeof(body)=='string')
            {
                sentData = JSON.parse(body);
            }else{
                if(typeof(body)=='object')
                {
                    sentData = JSON.parse(JSON.stringify(body));
                }
            }
            if(sentData.success==false)
            {
                //don't do anything yet
                x=x+1;
                if(x>10)
                {
                    var msg = {
                        "success":false, "status":"Error",
                        "desc":"[reportMsg:reportMsgProfile] Clint's API keep saying error"
                    };
                    respond.logError(msg);
                }else{
                    setTimeout(reportMsgStyx,2000,key,dataSavedToMongoDB,dataJSON,dataStruct,x);
                }
            }
         }catch(error_here){
            var msg = {
                "success":false, "status":"Error",
                "desc":"[callCreateStyx:start] Error occurred code 1",
                "err":error_here,
                "stack":error_here.stack
            };
            respond.logError(msg);
         }
    });
}

module.exports={
    getRobotApiKey:getRobotApiKey,
    findGroupAndProduct:findGroupAndProduct
};