const util = require('util');
const mysqlConv = require(GLOBAL.server_location+'/helpers/mysql_convertions');
/*
{
        "type":"Product"/"Advertisement"/"Profile",
        "owner_id":Int,
        "uuid_hash_tracking":<string>,
        "product_id":<int>, (if type Product)
        "styx_id":<string>,(if type Advertisement)
        "user_id":<string>,(if type Profile)
}
*/
var start = function(myuuid,dataJSON, dataStruct,parent_cb,cb,req,res)
{
	dataJSON['product_visited_history']=[];
	var query = {
		"type":"Product",
        "owner_id":parseInt(dataJSON.security.user_id),
        "uuid_hash_tracking":""
	};
	// if(parseInt(dataJSON.security.user_id)==0)
	// {
	// 	query.uuid_hash_tracking=dataJSON.security.uuid;
	// }
	// GLOBAL.mongodb_activity_logs.viewLogs.find(query,function(err,docs){
	// 	if(err==null)
	// 	{
	// 		dataJSON['product_visited_history']=docs.slice();
	// 	}else{
	// 		var msg = {
	// 			"success":false, "status":"Error",
	// 			"desc":"[collectProductsVisited:start:executingQuery]Error in DB",
	// 			"err":err,
	// 			"query":query
	// 		};
	// 		process.emit('Shopstyx:emailError',msg);
	// 	}
	// });
	if(parseInt(dataJSON.security.user_id)>0)
	{
		var query = "SELECT product_id FROM `activity_product_views_per_user` WHERE `user_id`="+parseInt(dataJSON.security.user_id)+" ORDER BY average_view_duration DESC, view_frequency ASC, date_updated DESC LIMIT 20";
	}else{
		var query = "SELECT product_id FROM `activity_product_views_per_user` WHERE `user_id`=0 AND `uuid_hash_tracking_key`=\""+mysqlConv.mysql_real_escape_string(dataJSON.security.uuid)+"\" ORDER BY average_view_duration DESC, view_frequency ASC, date_updated DESC LIMIT 20";
	}
	
	var mysql = new GLOBAL.mysql.mysql_connect(query);
	mysql.results_ss_activities().then(function(results){
		if(util.isError(results)==true){
			var msg = {
				"success":false, "status":"Error",
				'desc':'[collectProductsVisited:start]DB Error Connection',
				'query':query,
				'message':results.message,
				'stack':results.stack||null
			};
			process.emit('Shopstyx:emailError',msg);
		}else{
			var rows=results;
			if(rows.length>0)
			{
				dataJSON['product_visited_history']=rows.slice();
			}else{
				collectMostViewedProducts(myuuid,dataJSON, dataStruct,parent_cb,cb,req,res);
			}
		}
		//cb(myuuid,dataJSON, dataStruct,parent_cb,req,res);
	});
}

function collectMostViewedProducts(myuuid,dataJSON, dataStruct,parent_cb,cb,req,res)
{
	var query = "SELECT product_id FROM `activity_product_views_per_user` ORDER BY average_view_duration DESC, view_frequency ASC, date_updated DESC LIMIT 20";

	var mysql = new GLOBAL.mysql.mysql_connect(query);
	mysql.results_ss_activities().then(function(results){
		if(util.isError(results)==true){
			var msg = {
				"success":false, "status":"Error",
				'desc':'[collectProductsVisited:start]DB Error Connection',
				'query':query,
				'message':results.message,
				'stack':results.stack||null
			};
			process.emit('Shopstyx:emailError',msg);
		}else{
			var rows=results;
			if(rows.length>0)
			{
				dataJSON['product_visited_history']=rows.slice();
			}
		}
		cb(myuuid,dataJSON, dataStruct,parent_cb,req,res);
	});
}

module.exports={
	start:start
};