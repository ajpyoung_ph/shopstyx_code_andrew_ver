// var server_dir_array = (__dirname).toString().split("unifiedshopstyxnodeapi");
// GLOBAL.server_location = server_dir_array[0]+"commonnodeconfig";
// delete server_dir_array;

const mysqlConv = require(GLOBAL.server_location+'/helpers/mysql_convertions');
const util = require('util');

var start = function(myuuid,dataJSON, dataStruct,parent_cb,cb,req,res)
{
	dataJSON['color_search_history']=[];
	if(parseInt(dataJSON.security.user_id)>0)
	{
		var query = "SELECT color_id FROM `activity_color_search` WHERE `user_id`="+parseInt(dataJSON.security.user_id)+" ORDER BY search_frequency, date_updated LIMIT 20";
	}else{
		var query = "SELECT color_id FROM `activity_color_search` WHERE `user_id`=0 AND `uuid_hash_tracking_key`=\""+mysqlConv.mysql_real_escape_string(dataJSON.security.uuid)+"\" ORDER BY search_frequency, date_updated LIMIT 20";
	}
	
	var mysql = new GLOBAL.mysql.mysql_connect(query);
	mysql.results_ss_activities().then(function(results){
		if(util.isError(results)==true){
			var msg = {
				"success":false, "status":"Error",
				'desc':'[collectColorSearchHistory:start]DB Error Connection',
				'query':query,
				'message':results.message,
				'stack':results.stack||null
			};
			process.emit('Shopstyx:logError',msg);
			setTimeout(start,1000,myuuid,dataJSON, dataStruct,parent_cb,cb,req,res);
		}else{
			if(results.length>0)
			{
				getColors(results,0,myuuid,dataJSON, dataStruct,parent_cb,cb,req,res);
			}else{
				collectMostUsedColors(myuuid,dataJSON, dataStruct,parent_cb,cb,req,res);
				//cb(myuuid,dataJSON, dataStruct,parent_cb,req,res);
			}
			
		}
	});
}

function collectMostUsedColors(myuuid,dataJSON, dataStruct,parent_cb,cb,req,res)
{
	var query = "SELECT color_id FROM `activity_color_search` ORDER BY search_frequency, date_updated LIMIT 20";
	var mysql = new GLOBAL.mysql.mysql_connect(query);
	mysql.results_ss_activities().then(function(results){
		if(util.isError(results)==true){
			var msg = {
				"success":false, "status":"Error",
				'desc':'[collectColorSearchHistory:start]DB Error Connection',
				'query':query,
				'message':results.message,
				'stack':results.stack||null
			};
			process.emit('Shopstyx:logError',msg);
			setTimeout(start,1000,myuuid,dataJSON, dataStruct,parent_cb,cb,req,res);
		}else{
			if(results.length>0)
			{
				getColors(results,0,myuuid,dataJSON, dataStruct,parent_cb,cb,req,res);
			}else{
				cb(myuuid,dataJSON, dataStruct,parent_cb,req,res);
			}
			
		}
	});
}

function getColors(results,y,myuuid,dataJSON, dataStruct,parent_cb,cb,req,res)
{
	if(y<results.length)
	{
		if(util.isNullOrUndefined(results[y])==false)
		{
			var row = results[y];
			var color_query = "SELECT dc.color_name,dca.color_name as alternative_color_name FROM `default_colors` as dc LEFT JOIN `default_colors_alternative` as dca ON dc.color_id=dca.color_id WHERE dc.`color_id`="+parseInt(row.color_id)+";";
			var mysql = new GLOBAL.mysql.mysql_connect(color_query);
			mysql.results_ss_activities().then(function(results){
				if(util.isError(results)==true){
					var msg = {
						"success":false, "status":"Error",
						'desc':'[collectColorSearchHistory:getColors]DB Error Connection',
						'query':color_query,
						'message':results.message,
						'stack':results.stack||null
					};
					process.emit('Shopstyx:logError',msg);
				}else{
					var rows=results;
					for(var x=0;x<rows.length;x++)
					{
						if(dataJSON['color_search_history'].join("`").toLowerCase().split("`").indexOf(rows[x].color_name.toLowerCase())==-1)
						{
							dataJSON['color_search_history'].push(rows[x].color_name.toLowerCase());
						}
						if(dataJSON['color_search_history'].join("`").toLowerCase().split("`").indexOf(rows[x].alternative_color_name.toLowerCase())==-1)
						{
							dataJSON['color_search_history'].push(rows[x].alternative_color_name.toLowerCase());
						}
					}
					y=y+1;
					getColors(results,y,myuuid,dataJSON, dataStruct,parent_cb,cb,req,res);
				}
			});
		}else{
			y=y+1;
			getColors(results,y,myuuid,dataJSON, dataStruct,parent_cb,cb,req,res);
		}
	}else{
		if(y==results.length)
		{
			cb(myuuid,dataJSON, dataStruct,parent_cb,req,res);
		}
	}
}
module.exports={
	start:start
};