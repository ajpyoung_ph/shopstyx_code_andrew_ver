const util = require('util');
const mysqlConv = require(GLOBAL.server_location+'/helpers/mysql_convertions');

var start = function(myuuid,dataJSON, dataStruct,parent_cb,cb,req,res)
{
	dataJSON['search_history']=[];
	if(parseInt(dataJSON.security.user_id)>0)
	{
		var query = "SELECT search_phrase FROM `activity_search_history` WHERE `user_id`="+parseInt(dataJSON.security.user_id)+" ORDER BY search_frequency, date_updated LIMIT 20";
	}else{
		var query = "SELECT search_phrase FROM `activity_search_history` WHERE `user_id`=0 AND `uuid_hash_tracking_key`=\""+mysqlConv.mysql_real_escape_string(dataJSON.security.uuid)+"\" ORDER BY search_frequency, date_updated LIMIT 20";
	}
	var mysql = new GLOBAL.mysql.mysql_connect(query);
	mysql.results_ss_activities().then(function(results){
		if(util.isError(results)==true){
			var msg = {
				"success":false, "status":"Error",
				'desc':'[collectSearchHistory:start]DB Error Connection',
				'query':query,
				'message':results.message,
				'stack':results.stack||null
			};
			process.emit('Shopstyx:emailError',msg);
		}else{
			var rows=results;
			if(rows.length>0)
			{
				dataJSON['search_history']=rows.slice();
			}else{
				collectMostSearchedHistory(myuuid,dataJSON, dataStruct,parent_cb,cb,req,res);
			}
		}
		//cb(myuuid,dataJSON, dataStruct,parent_cb,req,res);
	});
}

function collectMostSearchedHistory(myuuid,dataJSON, dataStruct,parent_cb,cb,req,res)
{
	var query = "SELECT search_phrase FROM `activity_search_history` ORDER BY search_frequency, date_updated LIMIT 20";
	var mysql = new GLOBAL.mysql.mysql_connect(query);
	mysql.results_ss_activities().then(function(results){
		if(util.isError(results)==true){
			var msg = {
				"success":false, "status":"Error",
				'desc':'[collectSearchHistory:start]DB Error Connection',
				'query':query,
				'message':results.message,
				'stack':results.stack||null
			};
			process.emit('Shopstyx:emailError',msg);
		}else{
			var rows=results;
			if(rows.length>0)
			{
				dataJSON['search_history']=rows.slice();
			}
		}
		cb(myuuid,dataJSON, dataStruct,parent_cb,req,res);
	});
}
module.exports={
	start:start
};