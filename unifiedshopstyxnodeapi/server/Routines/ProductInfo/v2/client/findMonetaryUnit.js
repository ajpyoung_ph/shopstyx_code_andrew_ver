const util = require('util');
var findMonetaryUnit = function(row,dataJSON,dataStruct,connection,cb,req,res)
{
	if(isNaN(parseInt(row.monetary_unit_id))==false)
	{
		var now = Date.now();
		//console.log("Getting Data for findMonetaryUnit ============ "+Date());
		var query = "SELECT * FROM `default_monetary_units` WHERE `money_id`="+parseInt(row.monetary_unit_id)+" LIMIT 1;";
		var mysql = new GLOBAL.mysql.mysql_connect(query);
		mysql.results_ss_common().then(function(results){
			//console.log("response from MySQL findMonetaryUnit ============= "+Date());
			//console.log("MySQL response findMonetaryUnit =========> "+(Date.now()-now)+" ms");
			if(util.isError(results)==true){
				var msg = {
					"success":false, "status":"Error",
					'desc':'[findMonetaryUnit:findMonetaryUnit]DB Error Connection',
					'query':query,
					'message':results.message,
					'stack':results.stack||null
				};
				process.emit('Shopstyx:logError',msg);
				var data=[];
				setImmediate(cb,data,row,dataJSON,dataStruct,connection,req,res);
			}else{
				var rows=results;
				setImmediate(cb,rows,row,dataJSON,dataStruct,connection,req,res);
			}
		});
	}else{
		var data=[];
		setImmediate(cb,data,row,dataJSON,dataStruct,connection,req,res);
	}
};

module.exports={
	findMonetaryUnit:findMonetaryUnit
};