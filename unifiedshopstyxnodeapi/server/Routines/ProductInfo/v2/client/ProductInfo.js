const respond = require(GLOBAL.server_location+'/EventListeners/errorListener');

const findSEO = require(__dirname+"/findSEO");
const findAddFromWebInfo = require(__dirname+"/findAddFromWebInfo");
const findImage = require(__dirname+"/findImage");
const findMonetaryUnit = require(__dirname+"/findMonetaryUnit");
const findLWHunit = require(__dirname+"/findLWHunit");
const findWeightUnit = require(__dirname+"/findWeightUnit");
const findShopInformation = require(__dirname+"/findShopInformation");

const listHome = require('../../../ProductLists/v2/client/listHome');

const parseDataJSON = require(GLOBAL.server_location+'/helpers/parseDataJSON');
const util = require('util');
const encodeDecode = require (GLOBAL.server_location+'/helpers/encodeDecode');

//http://mybugserver.shopstyx.com/wiki/index.php/Client_Product_Information
var processRequest = function(dataJSON,dataStruct,req,res)
{
	//check if required request variables are present
	var error = false;
	var error_msg = [];
	/*
	"request":{
        "product_id":<int>,
        "product_owner_user_id":<int>,
        "store_id":<int>
    }
	*/
	//check if all required variables are set
	if(typeof(dataJSON.request)!='undefined')
	{
		//check minimum request requirements
		if(typeof(dataJSON.request.product_id)=='undefined')
		{
			error=true;
			error_msg.push("No Product Identified for request");
		}
		if(typeof(dataJSON.request.product_id)!='object')
		{
			error=true;
			error_msg.push("Product Identification request not in the required format");
		}
		// if(typeof(dataJSON.request.variant_id)!='undefined')
		// {
		// 	if(isNaN(dataJSON.request.variant_id)==true)
		// 	{
		// 		error=true;
		// 		error_msg.push("Variant request is invalid");
		// 	}
		// }
		if(typeof(dataJSON.request.variant_id)!='undefined')
		{
			if(typeof(dataJSON.request.variant_id)!='object')
			{
				error=true;
				error_msg.push("Product Variant Identification request not in the required format");
			}else if(error==false){
				if(dataJSON.request.variant_id.length!=dataJSON.request.product_id.length)
				{
					error=true;
					error_msg.push("Product Variant Identification length is not equivalent to the Product Identification length");
				}
			}
		}
	}else{
		error=true;
		error_msg.push("No Request Made");
	}
	if(error==true)
	{
		var msg = {
			"success":false, "status":"Error",
			"desc":"[ProductInfo:processRequest]Error Processing Data",
			"additional_info":error_msg.join(','),
			"err":dataJSON
		};
		respond.respondError(msg,res,req);
		
						
						
	}
	if(error==false)
	{
		dataJSON['dataReturn'] = {
			"success":true, "status":"Success",
			"data":[]
		};
		dataJSON['finding_counter']=0;
		setImmediate(read_data,dataJSON,dataStruct,req,res);
	}
};


function read_data(dataJSON,dataStruct,req,res)
{
	var product_ids = dataJSON.request.product_id.join();
	var query = "SELECT * FROM `products` WHERE `product_id` IN ("+product_ids+");";
	var mysql = new GLOBAL.mysql.mysql_connect(query);
	mysql.results_ss_common().then(function(results){
		if(util.isError(results)==true){
			var msg = {
				"success":false, "status":"Error",
				'desc':'[ProductInfo:read_data]DB Error Connection',
				'query':query,
				'message':results.message,
				'stack':results.stack||null
			};
			process.emit('Shopstyx:emailError',msg);
		}else{
			var rows=results;
			dataJSON['resVal']={
				"success":true, "status":"Success",
				"desc":"[ProductInfo:read_data]Collections Found",
				"data":[]
			};
			dataJSON.rows=rows;
			dataStruct['from_ProductInfo']=true;
			setImmediate(listHome.breakdownProdIds,dataJSON,dataStruct,req,res);
		}
	});
}

module.exports={
	processRequest:processRequest
};