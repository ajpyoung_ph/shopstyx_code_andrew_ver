const util = require('util');
const findPrimaryImage = function(array_product_ids,dataJSON,dataStruct,cb,req,res)
{
	var now = Date.now();
	var product_ids = array_product_ids.join();
	if(product_ids=='')
	{
		var data=[];
		setImmediate(cb,data,dataJSON,dataStruct,req,res);
	}else{
		//console.log("Getting Data for findPrimaryImage ============ "+Date());
		var query = "SELECT * FROM `product_images` WHERE `product_id` IN ("+product_ids+") ORDER BY product_id ASC, product_default_image DESC, product_sort_order ASC;";
		var mysql = new GLOBAL.mysql.mysql_connect(query);
		mysql.results_ss_common().then(function(results){
			//console.log("response from MySQL findPrimaryImage ============= "+Date());
			//console.log("MySQL response findPrimaryImage =========> "+(Date.now()-now)+" ms");
			if(util.isError(results)==true){
				var msg = {
					"success":false, "status":"Error",
					'desc':'[findImage:findPrimaryImage]DB Error Connection',
					'query':query,
					'message':results.message,
					'stack':results.stack||null
				};
				process.emit('Shopstyx:logError',msg);
				var data=[];
				setImmediate(cb,data,dataJSON,dataStruct,req,res);
			}else{
				setImmediate(cb,results,dataJSON,dataStruct,req,res);
			}
		});
	}
}

const findVariantImage = function(array_product_ids,array_variant_names,dataJSON,dataStruct,cb,req,res)
{
	var error=false;
	var product_ids = array_product_ids.join();
	if(product_ids=='')
	{
		var data=[];
		setImmediate(cb,data,dataJSON,dataStruct,req,res);
	}else{
		var variant_names = array_variant_names.join();
		if(typeof(dataJSON.request.filter)!='undefined')
		{
			if(typeof(dataJSON.request.filter.color)!='undefined')
			{
				var query = "SELECT variant_id, product_id, variant_sku as product_sku FROM `product_variant` WHERE `product_id` IN ("+product_ids+" AND `variant_name` IN ("+variant_names+");";
			}else{
				error=true;
			}
		}else{
			error=true;
		}
		if(error==true)
		{
			var data = [];
			setImmediate(cb,data,row,dataJSON,dataStruct,connection,req,res);
		}else{
			var mysql = new GLOBAL.mysql.mysql_connect(query);
			mysql.results_ss_common().then(function(results){
				if(util.isError(results)==true){
					var msg = {
						"success":false, "status":"Error",
						'desc':'[findImage:findVariantImage]DB Error Connection',
						'query':query,
						'message':results.message,
						'stack':results.stack||null
					};
					process.emit('Shopstyx:logError',msg);
					var data=[];
					setImmediate(cb,data,dataJSON,dataStruct,req,res);
				}else{
					var rows=results;
					var array_variant_ids=[];
					for(var x=0;x<rows.length;x++)
					{
						if(array_variant_ids.indexOf(rows[x].variant_id)==-1)
						{
							array_variant_ids.push(rows[x].variant_id);
						}
					}
					var variant_ids = array_variant_ids.join();
					var query2="SELECT variant_id, variant_default_image as product_default_image, variant_image_filename as product_image_filename, variant_image_url as product_image_url, variant_image_sort_order as product_image_sort_order FROM `product_variant_images` WHERE `variant_id` IN ("+variant_ids+") ORDER BY variant_id ASC, variant_default_image DESC, product_sort_order ASC;";
					var mysql2 = new GLOBAL.mysql.mysql_connect(query2);
					mysql2.results_ss_common().then(function(results2){
						if(util.isError(results2)==true){
							var msg = {
								"success":false, "status":"Error",
								'desc':'[findImage:findVariantImage]DB Error Connection2',
								'query':query2,
								'message':results2.message,
								'stack':results2.stack||null
							};
							process.emit('Shopstyx:logError',msg);
							var data=[];
							setImmediate(cb,data,dataJSON,dataStruct,req,res);
						}else{
							var variant_rows = results2;
							var newRows = [];
							for(var x=0;x<rows.length;x++)
							{
								for(var y=0;y<variant_rows.length;y++)
								{
									if(rows[x].variant_id==variant_rows[y].variant_id)
									{
										var clone_variant_rows = variant_rows[y].slice();
										delete clone_variant_rows.variant_id;
										var joinedValue = rows[x].concat(clone_variant_rows);
										newRows.push(joinedValue);
									}
								}
							}

							setImmediate(cb,newRows,dataJSON,dataStruct,req,res);
						}
					});
				}
			});
		}
	}
}

const findVariantImageSingle = function(variant_id,row,dataJSON,dataStruct,cb,req,res)
{
	/*
	SELECT pvi.variant_default_image as product_default_image, pvi.variant_image_filename as product_image_filename, pvi.variant_image_url as product_image_url, pvi.variant_image_sort_order as product_image_sort_order,pvi.variant_id,pv.variant_sku as product_sku,pv.variant_name as product_name FROM `product_variant` pv LEFT JOIN `product_variant_images` pvi ON (pv.variant_id=pvi.variant_id) WHERE pv.variant_id="+parseInt(variant_id)+" ORDER BY pvi.variant_default_image DESC, pvi.variant_image_sort_order ASC
	*/
	var query = "SELECT variant_default_image as product_default_image, variant_image_filename as product_image_filename, variant_image_url as product_image_url, variant_image_sort_order as product_image_sort_order FROM `product_variant` WHERE `variant_id`="+parseInt(variant_id)+" ORDER BY variant_default_image DESC, product_sort_order ASC;";
	var mysql = new GLOBAL.mysql.mysql_connect(query);
	mysql.results_ss_common().then(function(results){
		if(util.isError(results)==true){
			var msg = {
				"success":false, "status":"Error",
				'desc':'[findImage:findVariantImageSingle]DB Error Connection',
				'query':query,
				'message':results.message,
				'stack':results.stack||null
			};
			process.emit('Shopstyx:logError',msg);
			var data=[];
			setImmediate(cb,data,row,dataJSON,dataStruct,null,req,res);
		}else{
			var rows=results;
			setImmediate(cb,rows,row,dataJSON,dataStruct,null,req,res);
		}
	});
}
module.exports={
	findPrimaryImage:findPrimaryImage,
	findVariantImage:findVariantImage,
	findVariantImageSingle:findVariantImageSingle
};