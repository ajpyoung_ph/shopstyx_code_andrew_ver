const util = require('util');
const findAddFromWebInfo = function(array_product_ids,dataJSON,dataStruct,cb,req,res)
{
	var now = Date.now();
	var product_ids = array_product_ids.join();
	if(product_ids=='')
	{
		var data=[];
		setImmediate(cb,data,dataJSON,dataStruct,req,res);
	}else{
		//console.log("Getting Data for findAddFromWebInfo ============ "+Date());
		var query2="SELECT * FROM `product_added_from_web_info` WHERE `product_id` IN ("+product_ids+");";
		var mysql = new GLOBAL.mysql.mysql_connect(query2);
		mysql.results_ss_common().then(function(results){
			if(util.isError(results)==true){
				var msg = {
					"success":false, "status":"Error",
					'desc':'[findAddFromWebInfo:findAddFromWebInfo]DB Error Connection',
					'query':query2,
					'message':results.message,
					'stack':results.stack||null
				};
				process.emit('Shopstyx:logError',msg);
				var data=[];
				setImmediate(cb,data,dataJSON,dataStruct,req,res);
			}else{
				setImmediate(cb,results,dataJSON,dataStruct,req,res);
			}
		});	
	}
};

module.exports={
	findAddFromWebInfo:findAddFromWebInfo
};