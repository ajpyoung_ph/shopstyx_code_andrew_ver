const util = require('util');

const start = function(array_shop_id,dataJSON,dataStruct,cb,req,res)
{
	var now = Date.now();
	var shop_ids = array_shop_id.join();
	if(shop_ids=='')
	{
		var data=[];
		setImmediate(cb,data,dataJSON,dataStruct,req,res);
	}else{
		//console.log("Getting Data for findShopInformation ============ "+Date());
		var query2="SELECT * FROM `stores` WHERE `store_id` IN ("+shop_ids+");";
		var mysql = new GLOBAL.mysql.mysql_connect(query2);
		mysql.results_ss_common().then(function(results){
			//console.log("response from MySQL findShopInformation ============= "+Date());
			//console.log("MySQL response findShopInformation =========> "+(Date.now()-now)+" ms");
			if(util.isError(results)==true){
				var msg = {
					"success":false, "status":"Error",
					'desc':'[findShopInformation:start]DB Error Connection',
					'query':query2,
					'message':results.message,
					'stack':results.stack||null
				};
				process.emit('Shopstyx:logError',msg);
				var data=[];
				setImmediate(cb,data,dataJSON,dataStruct,req,res);
			}else{
				var rows=results;
				setImmediate(cb,rows,dataJSON,dataStruct,req,res);
			}
		});
	}
};

module.exports={
	start:start
};