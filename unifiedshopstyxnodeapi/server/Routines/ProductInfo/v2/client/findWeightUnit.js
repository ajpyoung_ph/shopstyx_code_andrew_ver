const util = require('util');
var findWeightUnit = function(row,dataJSON,dataStruct,connection,cb,req,res)
{
	if(isNaN(parseInt(row.weight_unit_id))==false)
	{
		var now = Date.now();
		//console.log("Getting Data for findWeightUnit ============ "+Date());
		var query = "SELECT * FROM `default_weight_units` WHERE `weight_units_id`="+parseInt(row.weight_unit_id)+" LIMIT 1;";
		var mysql = new GLOBAL.mysql.mysql_connect(query);
		mysql.results_ss_common().then(function(results){
			//console.log("response from MySQL findWeightUnit ============= "+Date());
			//console.log("MySQL response findWeightUnit =========> "+(Date.now()-now)+" ms");
			if(util.isError(results)==true){
				var msg = {
					"success":false, "status":"Error",
					'desc':'[findShopInformation:start]DB Error Connection',
					'query':query,
					'message':results.message,
					'stack':results.stack||null
				};
				process.emit('Shopstyx:logError',msg);
				var data=[];
				setImmediate(cb,data,row,dataJSON,dataStruct,connection,req,res);
			}else{
				var rows=results;
				setImmediate(cb,rows,row,dataJSON,dataStruct,connection,req,res);
			}
		});
	}else{
		var data=[];
		setImmediate(cb,data,row,dataJSON,dataStruct,connection,req,res);
	}
};

module.exports={
	findWeightUnit:findWeightUnit
};