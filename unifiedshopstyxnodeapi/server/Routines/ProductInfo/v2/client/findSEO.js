const util = require('util');
const start = function(array_product_ids,dataJSON,dataStruct,cb,req,res)
{
	var now = Date.now();
	var product_ids = array_product_ids.join();
	if(product_ids=='')
	{
		var data=[];
		setImmediate(cb,data,dataJSON,dataStruct,req,res);
	}else{
		//console.log("Getting Data for findSEO ============ "+Date());
		var query2="SELECT * FROM `product_seo` WHERE `product_id` IN ("+product_ids+");";
		var mysql = new GLOBAL.mysql.mysql_connect(query2);
		mysql.results_ss_common().then(function(results){
			//console.log("response from MySQL findSEO ============= "+Date());
			//console.log("MySQL response findSEO =========> "+(Date.now()-now)+" ms");
			if(util.isError(results)==true){
				var msg = {
					"success":false, "status":"Error",
					'desc':'[findSEO:start]DB Error Connection',
					'query':query2,
					'message':results.message,
					'stack':results.stack||null
				};
				process.emit('Shopstyx:logError',msg);
				var data=[];
				setImmediate(cb,data,dataJSON,dataStruct,req,res);
			}else{
				setImmediate(cb,results,dataJSON,dataStruct,req,res);
			}
		});
	}
};

module.exports={
	start:start
};