
// var initializeTransactionDumpTables = function(my_id)
// {
// 	var user_id = (parse_int(my_id)).toString();
// 	var names = ['buyer_'+user_id+'_rewards_recieved','buyer_'+user_id+'_disputed_items','buyer_'+user_id+'_transactions','seller_'+user_id+'_disputed_items','seller_'+user_id+'_rewards_spent','seller_'+user_id+'_transactions'];
// 	for(var x =0;x<names.length;x++)
// 	{
// 		initDB(names[x],user_id);
// 	}
// };

// function initDB(db_name,user_id)
// {
// 	var query = '';
// 	switch(db_name)
// 	{
// 		case 'buyer_'+user_id+'_rewards_recieved':
// 			query = "CREATE TABLE IF NOT EXISTS `buyer_ID_rewards_recieved` (`item_bought_datastore_id` varchar(255) NOT NULL, `sold_from_user_id` bigint(20) unsigned NOT NULL,`sold_to_user_id` bigint(20) unsigned NOT NULL,`seller_mysql_transaction_id` bigint(20) unsigned NOT NULL,`buyer_mysql_transaction_id` bigint(20) unsigned NOT NULL,`total_rewards_recieved` bigint(20) unsigned NOT NULL,`date_rewarded` datetime NOT NULL,KEY `sold_from_user_id` (`sold_from_user_id`),KEY `sold_to_user_id` (`sold_to_user_id`),KEY `seller_mysql_transaction_id` (`seller_mysql_transaction_id`),KEY `buyer_mysql_transaction_id` (`buyer_mysql_transaction_id`),KEY `date_rewarded` (`date_rewarded`)) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;";
// 			break;
// 		case 'buyer_'+user_id+'_disputed_items':
// 			query = "CREATE TABLE IF NOT EXISTS `buyer_ID_disputed_items` (`dispute_record` bigint(20) unsigned NOT NULL AUTO_INCREMENT,`buyer_transaction_id_disputed` bigint(20) unsigned NOT NULL,`seller_transaction_id_disputed` bigint(20) unsigned NOT NULL,`item_bought_datastore_id` varchar(255) NOT NULL,`dispute_history_datastore_id` varchar(255) NOT NULL,`status` varchar(3) NOT NULL DEFAULT 'SP' COMMENT 'SP - still being processed/negotiated; R - rewarded to buyer ; N - negotiated without payout',`date_disputed` datetime NOT NULL,`date_resolved` datetime DEFAULT NULL,PRIMARY KEY (`dispute_record`),KEY `buyer_transaction_id_disputed` (`buyer_transaction_id_disputed`),KEY `seller_transaction_id_disputed` (`seller_transaction_id_disputed`),KEY `status` (`status`),KEY `date_disputed` (`date_disputed`),KEY `date_resolved` (`date_resolved`)) ENGINE=InnoDB DEFAULT CHARSET=utf8;";
// 			break;
// 		case 'buyer_'+user_id+'_transactions':
// 			query = "CREATE TABLE IF NOT EXISTS `buyer_ID_transactions` (`buyer_transaction_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,`bought_from_user_id` bigint(20) unsigned NOT NULL,`buyer_datastore_transaction_id` varchar(255) NOT NULL,`geo_spatial_point_data` point NOT NULL,`total_bought` bigint(20) unsigned NOT NULL,PRIMARY KEY (`buyer_transaction_id`),KEY `bought_from_user_id` (`bought_from_user_id`),KEY `total_bought` (`total_bought`),SPATIAL KEY `geo_spatial_point_data` (`geo_spatial_point_data`)) ENGINE=MyISAM DEFAULT CHARSET=utf8;";
// 			break;
// 		case 'seller_'+user_id+'_disputed_items':
// 			query = "CREATE TABLE IF NOT EXISTS `seller_ID_disputed_items` (`dispute_record` bigint(20) unsigned NOT NULL AUTO_INCREMENT,`buyer_transaction_id_disputed` bigint(20) unsigned NOT NULL,`seller_transaction_id_disputed` bigint(20) unsigned NOT NULL,`item_bought_datastore_id` varchar(255) NOT NULL,`dispute_history_datastore_id` varchar(255) NOT NULL,`status` varchar(3) NOT NULL DEFAULT 'SP' COMMENT 'SP - still being processed/negotiated; R - rewarded to buyer ; N - negotiated without payout',`date_disputed` datetime NOT NULL,`date_resolved` datetime DEFAULT NULL,PRIMARY KEY (`dispute_record`),KEY `buyer_transaction_id_disputed` (`buyer_transaction_id_disputed`),KEY `seller_transaction_id_disputed` (`seller_transaction_id_disputed`),KEY `status` (`status`),KEY `date_disputed` (`date_disputed`),KEY `date_resolved` (`date_resolved`)) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;";
// 			break;
// 		case 'seller_'+user_id+'_rewards_spent':
// 			query = "CREATE TABLE IF NOT EXISTS `seller_ID_rewards_spent` (`item_bought_datastore_id` varchar(255) NOT NULL,`rewards_to_user_id` bigint(20) unsigned NOT NULL,`sold_to_user_id` bigint(20) unsigned NOT NULL COMMENT 'needed to track the originating transaction record by the customer',`seller_mysql_transaction_id` bigint(20) unsigned NOT NULL,`buyer_mysql_transaction_id` bigint(20) unsigned NOT NULL,`total_rewards_recieved` bigint(20) unsigned NOT NULL,`date_rewarded` datetime NOT NULL,KEY `rewards_to_user_id` (`rewards_to_user_id`),KEY `sold_to_user_id` (`sold_to_user_id`),KEY `seller_mysql_transaction_id` (`seller_mysql_transaction_id`),KEY `buyer_mysql_transaction_id` (`buyer_mysql_transaction_id`),KEY `total_rewards_recieved` (`total_rewards_recieved`),KEY `date_rewarded` (`date_rewarded`)) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;";
// 			break;
// 		case 'seller_'+user_id+'_transactions':
// 			query = "CREATE TABLE IF NOT EXISTS `seller_ID_transactions` (`seller_transaction_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,`sold_to_user_id` bigint(20) unsigned NOT NULL,`seller_datastore_transaction_id` varchar(255) NOT NULL,`geo_spatial_point_data` point NOT NULL COMMENT 'WHERE THE BUYER IS',`total_sold` bigint(20) unsigned NOT NULL COMMENT 'TOTAL AMOUNT SOLD',PRIMARY KEY (`seller_transaction_id`),KEY `sold_to_user_id` (`sold_to_user_id`),KEY `total_sold` (`total_sold`),SPATIAL KEY `geo_spatial_point_data` (`geo_spatial_point_data`)) ENGINE=MyISAM DEFAULT CHARSET=utf8;";
// 			break;
// 		default:
// 			break;
// 	}
// 	if(query!='')
// 	{
// 		GLOBAL.db_ss_transaction_dumps.getConnection(function(err1, connection) {
// 			if(err1==null)
// 			{
// 				connection.query(query,function(err,docs){
// 					if(err!=null)
// 					{
// 						var msg = {
// 							"success":false, "status":"Error",
// 							"desc":"[mysqlDetectCreate:initDB]Error Processing Data",
// 							"err":err,
// 							"query":query,
// 							"db_name":db_name
// 						};
// 						process.emit('Shopstyx:emailError',msg);
// 					}
// 					connection.release();
// 				});
// 			}else{
// 				var msg = {
// 					"success":false, "status":"Error",
// 					"desc":"[mysqlDetectCreate:initDB]Error Getting Connection",
// 					"err":err1,
// 					"db_name":db_name
// 				};
// 				process.emit('Shopstyx:emailError',msg);
// 			}
// 		});
// 	}
// }
	

// module.exports={
// 	initializeTransactionDumpTables:initializeTransactionDumpTables
// };