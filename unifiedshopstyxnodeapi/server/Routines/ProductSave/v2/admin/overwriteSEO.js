const util = require('util');
const respond = require(GLOBAL.server_location+'/EventListeners/errorListener');
const mysqlConv = require(GLOBAL.server_location+'/helpers/mysql_convertions');

const start = function(dataJSON,dataStruct,cb,req,res)
{
	/*
	INSERT [LOW_PRIORITY | DELAYED | HIGH_PRIORITY] [IGNORE]
    [INTO] tbl_name
    [PARTITION (partition_name,...)]
    SET col_name={expr | DEFAULT}, ...
    [ ON DUPLICATE KEY UPDATE
      col_name=expr
        [, col_name=expr] ... ]
	*/
	var query = "UPDATE `product_seo` SET ";

	//check validity of JSON variables
	if(typeof(dataJSON.request.seo_info)!='undefined' && typeof(dataJSON.request.product_info)!='undefined')
	{
		var product_info_error = false;
		var product_info_NaN = false;
		if(typeof(dataJSON.request.product_info.product_id)=='undefined' || typeof(dataJSON.request.seo_info.seo_title)=='undefined' || typeof(dataJSON.request.seo_info.seo_description)=='undefined' || typeof(dataJSON.request.seo_info.seo_url)=='undefined' || typeof(dataJSON.request.seo_info.meta_keywords)=='undefined')
		{
			product_info_error=true;
		}
		//check if any id is NaN
		if(isNaN(dataJSON.request.product_info.product_id)==true)
		{
			product_info_NaN=true;
		}
		if(product_info_error==false && product_info_NaN==false)
		{
			query = query + "`product_id`="+parseInt(dataJSON.request.product_info.product_id)+", ";
			query = query + "`seo_title`='"+mysqlConv.mysql_real_escape_string(dataJSON.request.seo_info.seo_title)+"', ";
			query = query + "`seo_description`='"+mysqlConv.mysql_real_escape_string(dataJSON.request.seo_info.seo_description)+"', ";
			query = query + "`seo_url`='"+mysqlConv.mysql_real_escape_string(dataJSON.request.seo_info.seo_url)+"', ";
			query = query + "`meta_keywords`='"+mysqlConv.mysql_real_escape_string(dataJSON.request.seo_info.meta_keywords)+"' ";
			query = query + "WHERE `product_id`="+parseInt(dataJSON.request.product_info.product_id)+";";
			
			var mysql = new GLOBAL.mysql.mysql_connect(query);
			mysql.results_ss_common().then(function(results){
				if(util.isError(results)==true){
					var msg = {
						"success":false, "status":"Error",
						'desc':'[overwriteSEO:start]DB Error Connection',
						'query':query,
						'message':results.message,
						'stack':results.stack||null
					};
					respond.respondError(msg,res,req);
				}else{
					cb(dataJSON,dataStruct,req,res);
				}
			});
		}else{
			var msg = {
				"success":false, "status":"Error",
				"desc":"[overwriteSEO:start:LackProdInfo]Data Structure not valid",
				"product_info_error":product_info_error,
				"product_info_NaN":product_info_NaN
			};
			respond.respondError(msg,res,req);
						
						
		}
	}else{
		var msg = {
			"success":false, "status":"Error",
			"desc":"[overwriteSEO:start:NoProdInfo]Data Structure not valid"
		};
		respond.respondError(msg,res,req);
						
						
	}
};

module.exports={
	start:start
};