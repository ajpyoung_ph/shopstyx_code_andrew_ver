const util = require('util');
const respond = require(GLOBAL.server_location+'/EventListeners/errorListener');
const mysqlConv = require(GLOBAL.server_location+'/helpers/mysql_convertions');

const start = function(dataJSON,dataStruct,cb,req,res)
{
	/*
	INSERT [LOW_PRIORITY | DELAYED | HIGH_PRIORITY] [IGNORE]
    [INTO] tbl_name
    [PARTITION (partition_name,...)]
    SET col_name={expr | DEFAULT}, ...
    [ ON DUPLICATE KEY UPDATE
      col_name=expr
        [, col_name=expr] ... ]
	*/
	

	//check validity of JSON variables
	if(typeof(dataJSON.request.variant_info)!='undefined' && typeof(dataJSON.request.product_info)!='undefined')
	{
		var product_info_error = false;
		var product_info_NaN = false;
		if(typeof(dataJSON.request.product_info.product_id)=='undefined' || typeof(dataJSON.request.variant_info)=='object')
		{
			product_info_error=true;
		}
		//check if any id is NaN
		if(isNaN(dataJSON.request.product_info.product_id)==true || isNaN(dataJSON.request.variant_info.added_by_user_id))
		{
			product_info_NaN=true;
		}
		if(product_info_error==false && product_info_NaN==false)
		{
			var x =0;
			updateVariant(x,dataJSON,dataStruct,cb,req,res);
		}else{
			var msg = {
				"success":false, "status":"Error",
				"desc":"[overwriteVariant:start:LackProdInfo]Data Structure not valid",
				"product_info_error":product_info_error,
				"product_info_NaN":product_info_NaN
			};
			respond.respondError(msg,res,req);
						
						
		}
	}else{
		var msg = {
			"success":false, "status":"Error",
			"desc":"[overwriteVariant:start:NoProdInfo]Data Structure not valid"
		};
		respond.respondError(msg,res,req);
						
						
	}
};

function updateVariant(x,dataJSON,dataStruct,cb,req,res)
{
	//get current variant Name
	var error=false;
	try{
		var query = "SELECT * FROM `product_variant` WHERE `variant_id`="+parseInt(dataJSON.request.variant_info[x]["variant_id"])+" AND `product_id`="+parseInt(dataJSON.request.product_info.product_id)+";";
	}catch(err){
		var msg = {
			"success":false, "status":"Error",
			"desc":"[overwriteVariant:updateVariant:ErrorAccessVariable]Data Structure not valid",
			"err":dataJSON
		};
		respond.respondError(msg,res,req);
						
						
		error=true;
	}
	if(error==false)
	{
		var mysql = new GLOBAL.mysql.mysql_connect(query);
		mysql.results_ss_common().then(function(results){
			if(util.isError(results)==true){
				var msg = {
					"success":false, "status":"Error",
					'desc':'[overwriteVariant:updateVariant]DB Error Connection',
					'query':query,
					'message':results.message,
					'stack':results.stack||null
				};
				process.emit('Shopstyx:logError',msg);
			}else{
				var rows=results;
				var data = rows[0]['variant_name'].toLowerCase().trim();
				var q2 = "SELECT * FROM `products` WHERE `product_id`="+parseInt(dataJSON.request.product_info.product_id)+";";
				var mysql2 = new GLOBAL.mysql.mysql_connect(q2);
				mysql2.results_ss_common().then(function(results2){
					if(util.isError(results2)==true){
						var msg = {
							"success":false, "status":"Error",
							'desc':'[overwriteVariant:updateVariant]DB Error Connection2',
							'query':q2,
							'message':results2.message,
							'stack':results2.stack||null
						};
						process.emit('Shopstyx:logError',msg);
					}else{
						var rows2=results2;
						var data2 = rows2[0]['variant_titles_csv'];
						var arr_data = data2.split(",");
						arr_data[arr_data.indexOf(data)]=dataJSON.request.variant_info[x]["variant_name"].toLowerCase().trim();
						data2 = arr_data.join(',');
						var q3 = "UPDATE `products` SET `variant_titles_csv`='"+mysqlConv.mysql_real_escape_string(data2)+"' WHERE `product_id`="+parseInt(dataJSON.request.product_info.product_id)+";";
						var mysql3 = new GLOBAL.mysql.mysql_connect(q3);
						mysql3.results_ss_common().then(function(results3){
							if(util.isError(results3)==true){
								var msg = {
									"success":false, "status":"Error",
									'desc':'[overwriteVariant:updateVariant]DB Error Connection3',
									'query':q3,
									'message':results3.message,
									'stack':results3.stack||null
								};
								process.emit('Shopstyx:logError',msg);
							}else{
								saveData(x,dataJSON,dataStruct,cb,req,res);
							}
						});
					}
				});
			}
		});
	}
}
function saveData(x,dataJSON,dataStruct,cb,req,res)
{
	var error=false;
	try{
		var query = "UPDATE `product_variant` SET ";
		query = query + "`product_id`="+parseInt(dataJSON.request.product_info.product_id)+", ";
		query = query + "`variant_qty`="+parseInt(dataJSON.request.variant_info[x]["variant_qty"])+", ";
		query = query + "`variant_name`='"+mysqlConv.mysql_real_escape_string(dataJSON.request.variant_info[x]["variant_name"])+"' ";
		query = query + "WHERE `variant_id`="+parseInt(dataJSON.request.variant_info[x]["variant_id"])+";";
	}catch(err){

		var msg = {
			"success":false, "status":"Error",
			"desc":"[overwriteVariant:saveData:ErrorAccessVariable]Data Structure not valid",
			"err":dataJSON
		};
		respond.respondError(msg,res,req);
						
						
		error=true;
	}
	if(error==false)
	{
		var mysql = new GLOBAL.mysql.mysql_connect(query);
		mysql.results_ss_common().then(function(results){
			if(util.isError(results)==true){
				var msg = {
					"success":false, "status":"Error",
					'desc':'[overwriteVariant:saveData]DB Error Connection',
					'query':query,
					'message':results.message,
					'stack':results.stack||null
				};
				process.emit('Shopstyx:logError',msg);
				x=x+1;
				updateVariant(x,dataJSON,dataStruct,cb,req,res)
			}else{
				//check if length information is existing
				if(typeof(dataJSON.request.variant_info[x]["variant_length"])!='undefined' && typeof(dataJSON.request.variant_info[x]["variant_length"])!='undefined' && typeof(dataJSON.request.variant_info[x]["variant_length"])!='undefined' && typeof(dataJSON.request.variant_info[x]["variant_length"])!='undefined' && typeof(dataJSON.request.variant_info[x]["variant_length"])!='undefined' && typeof(dataJSON.request.variant_info[x]["variant_length"])!='undefined' )
				{
					saveVariantLength(x,dataJSON,dataStruct,cb,parseInt(dataJSON.request.variant_info[x]["variant_id"]),req,res);
				}else{
					if(x==dataJSON.request.variant_info.length-1)
					{
						if(typeof(cb)=='function')
						{
							cb(dataJSON,dataStruct,req,res);
						}else{
							var msg = {
								"success":true, "status":"Success",
								"desc":"[overwriteVariant:saveData:SuccessSaveSEO]Saved Add from Web Info"
							};
							process.emit('Shopstyx:logError',msg);
						}
					}else{
						x=x+1;
						updateVariant(x,dataJSON,dataStruct,cb,req,res)
					}
				}
			}
		});
	}
}

function saveVariantLength(x,dataJSON,dataStruct,cb,variant_id,req,res)
{
	var error=false;
	try{
		var query = "UPDATE `product_variant_sizes` SET ";
		query = query + "`variant_id`="+parseInt(variant_id)+", ";
		query = query + "`variant_length`="+parseInt(dataJSON.request.variant_info[x]["variant_length"])+", ";
		query = query + "`variant_width`="+parseInt(dataJSON.request.variant_info[x]["variant_width"])+", ";
		query = query + "`variant_height`="+parseInt(dataJSON.request.variant_info[x]["variant_height"])+", ";
		query = query + "`variant_lwh_unit_id`="+parseInt(dataJSON.request.variant_info[x]["variant_lwh_unit_id"])+", ";
		query = query + "`variant_weight`="+parseInt(dataJSON.request.variant_info[x]["variant_weight"])+", ";
		query = query + "`variant_weight_unit_id`="+parseInt(dataJSON.request.variant_info[x]["variant_weight_unit_id"])+" ";
		query = query + "WHERE `variant_id`="+parseInt(variant_id)+"; ";
	}catch(err){

		var msg = {
			"success":false, "status":"Error",
			"desc":"[overwriteVariant:saveVariantLength:ErrorAccessVariable]Data Structure not valid",
			"product_info_error":product_info_error,
			"product_info_NaN":product_info_NaN
		};
		respond.respondError(msg,res,req);
						
						
		error=true;
	}
	if(error==false)
	{
		var mysql = new GLOBAL.mysql.mysql_connect(query);
		mysql.results_ss_common().then(function(results){
			if(util.isError(results)==true){
				var msg = {
					"success":false, "status":"Error",
					'desc':'[overwriteVariant:saveVariantLength]DB Error Connection',
					'query':query,
					'message':results.message,
					'stack':results.stack||null
				};
				process.emit('Shopstyx:logError',msg);
				x=x+1;
				updateVariant(x,dataJSON,dataStruct,cb,req,res)
			}else{
				if(x==dataJSON.request.variant_info.length-1)
				{
					if(typeof(cb)=='function')
					{
						cb(dataJSON,dataStruct,req,res);
					}else{
						var msg = {
							"success":true, "status":"Success",
							"desc":"[overwriteVariant:saveVariantLength:SuccessSaveSEO]Saved Add from Web Info"
						};
						process.emit('Shopstyx:logError',msg);
					}
				}else{
					x=x+1;
					updateVariant(x,dataJSON,dataStruct,cb,req,res)
				}
			}
		});
	}
}
module.exports={
	start:start
};