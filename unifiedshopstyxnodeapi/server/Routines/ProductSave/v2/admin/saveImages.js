// GLOBAL.product_sizes = [960,650,300,100];
// GLOBAL.storage_names = {
// 				'advertisements':'shopstyx-advertisements',
// 				'products':'shopstyx-products',
// 				'profiles':'shopstyx-profiles'
// 			};
// GLOBAL.storage_url = "https://storage.googleapis.com/";//https://storage.googleapis.com/+GLOBAL.storage_names+/directory+/filename
// if(process.env.NODE_ENV.toLowerCase()!='production')
// {
// 	var upload_dir_array = (__dirname).toString().split("server");
// 	GLOBAL.upload_location = upload_dir_array[0]+"/uploads";
// 	delete upload_dir_array;
// }else{
// 	GLOBAL.upload_location = '/opt/temp/unifiedshopstyxnodeapi';
// }

const util = require('util');
const fs = require('fs');
const uuid = require('uuid');

if(util.isNullOrUndefined(process.env.NODE_USER)==false)
{
	const gm = require('gm').subClass({imageMagick: true});
}else{
	const sharp = require('sharp');
}

const start = function(dataJSON,dataStruct,product_id,origin_string,req,res)
{
	switch(origin_string)
	{
		case 'update':
			//compare_files
			//pero in reality do nothing since update should not have any images sent with the post
			break;
		case 'insert':
			//a bit more straight forward, check how many images, resample them, upload them to the system and insert the images to the table.
			collectInsertImages(product_id,req,res);
			break;
		default:
			break;
	}
}

//insert process
function collectInsertImages(product_id,req,res)
{
	//how many were uploaded
	var files = req.files;
	if(util.isNullOrUndefined(files.src)==false)
	{
		// console.log("filepath : "+single_file.path)
		var i=0;
		readSrc(i,product_id,req,res);
	}else{
		console.log("No Files Uploaded");
	}
}
function readSrc(i,product_id,req,res)
{
	var files = req.files;
	var single_file = req.files.src[i];
	console.log("filepath : "+single_file.path);
	var ext_array = single_file.mimetype.split("/");
	var extension = ext_array[ext_array.length-1];
	var file_info={
		new_filename:uuid.v1().toString()+"."+extension,
		single_file:single_file,
		default_image:(i==0)?true:false,
		sort_order:i+1,
		max:files.src.length
	};
	setImmediate(resampleImages,0,file_info,product_id,req,res);
}
function resampleImages(sample_counter,file_info,product_id,req,res)
{
	//image sample sizes should be GLOBAL.product_sizes
	var targetValue=GLOBAL.product_sizes[sample_counter];
	setImmediate(executeResample,file_info,product_id,targetValue,req,res);
}

function executeResample(file_info,product_id,targetValue,req,res)
{
	if(util.isNullOrUndefined(process.env.NODE_USER)==false)
	{
		gm(file_info.single_file.path)
		.resize(targetValue)
		.write(GLOBAL.upload_location+'/'+targetValue+'_'+file_info.new_filename,function(err){
			if(err==null)
			{
				fs.access(GLOBAL.upload_location+'/'+targetValue+'_'+file_info.new_filename, fs.R_OK | fs.W_OK, function (err) {
					if(err==null)
					{
						setImmediate(uploadFile,targetValue,file_info,product_id,req,res);
					}else{
						//console.log('no access to '+GLOBAL.upload_location+'/'+targetValue+'_'+filename);
						var msg = {
							"success":false, "status":"Error",
							"desc":"[saveImages:executeResample]Error Reading Target File "+GLOBAL.upload_location+'/'+targetValue+'_'+file_info.new_filename,
							"err":err
						};
						process.emit('Shopstyx:logError',msg);
						setImmediate(checkDelete,targetValue,file_info,product_id,req,res);
					}
				});
				
			}else{
				var msg = {
					"success":false, "status":"Error",
					"desc":"[saveImages:executeResample]Error Writing "+GLOBAL.upload_location+'/'+targetValue+'_'+file_info.new_filename,
					"err":err
				};
				process.emit('Shopstyx:logError',msg);
				setImmediate(checkDelete,targetValue,file_info,product_id,req,res);
				//deleteSourceFile(GLOBAL.upload_location+'/'+targetValue+'_'+filename);
			}
		});
	}else{
		sharp(file_info.single_file.path)
		.resize(targetValue)
		.toFile(GLOBAL.upload_location+'/'+targetValue+'_'+file_info.new_filename, function(err) {
			// output.jpg is a 300 pixels wide and 200 pixels high image
			// containing a scaled and cropped version of input.jpg
			fs.access(GLOBAL.upload_location+'/'+targetValue+'_'+file_info.new_filename, fs.R_OK | fs.W_OK, function (err) {
				if(err==null)
				{
					setImmediate(uploadFile,targetValue,file_info,product_id,req,res);
				}else{
					//console.log('no access to '+GLOBAL.upload_location+'/'+targetValue+'_'+filename);
					var msg = {
						"success":false, "status":"Error",
						"desc":"[saveImages:executeResample]Error Writing",
						"err":err
					};
					process.emit('Shopstyx:logError',msg);
					setImmediate(checkDelete,targetValue,file_info,product_id,req,res);
				}
			});
		});
	}
}
function uploadFile(targetValue,file_info,product_id,req,res)
{
	//upload saved data
	var gcs = GLOBAL.gcloud.storage();
	var bucket = gcs.bucket(GLOBAL.storage_names.products);
	var file = bucket.file('images/'+targetValue+'_'+file_info.new_filename);
	fs.createReadStream(GLOBAL.upload_location+'/'+targetValue+'_'+file_info.new_filename)
		.pipe(file.createWriteStream())
		.on('error', function(err) {
			var msg = {
				"success":false, "status":"Error",
				"desc":"[saveImages:uploadFile]Error in Upload",
				"err":err
			};
			process.emit('Shopstyx:logError',msg);
			setImmediate(checkDelete,targetValue,file_info,product_id,req,res);
			//deleteSourceFile(GLOBAL.upload_location+'/'+targetValue+'_'+filename);
		})
		.on('finish', function() {
			// The file upload is complete.
			// The file upload is complete.
			console.log("Successfully uploaded "+targetValue+'_'+file_info.new_filename);
			setImmediate(checkDelete,targetValue,file_info,product_id,req,res);
			//deleteSourceFile(GLOBAL.upload_location+'/'+targetValue+'_'+filename);
		});
}
function checkDelete(targetValue,file_info,product_id,req,res)
{
	/*if( util.isFunction(res.json)==false)
	{
		//console.log("****************** [checkDelete] res.json NOT A FUNCTION ****************");
	}*/
	if(targetValue==GLOBAL.product_sizes[GLOBAL.product_sizes.length-1])//based on the last value of targetWidth array
	{
		console.log('Deleting file:'+file_info.single_file.path);
		//setImmediate(deleteSourceFile,GLOBAL.upload_location+'/'+filename);
		setImmediate(saveProductImages, product_id,file_info);
		setTimeout(deleteSourceFile,60000,file_info.single_file.path);
		for(var xx=0;xx<GLOBAL.product_sizes.length;xx++)
		{
			var iteratedValue = GLOBAL.product_sizes[xx];
			var default_delay = 60000;
			if(iteratedValue==GLOBAL.product_sizes[GLOBAL.product_sizes.length-1])
			{
				default_delay = 120000;
			}
			//setImmediate(deleteSourceFile,GLOBAL.upload_location+'/'+iteratedValue+'_'+filename);
			setTimeout(deleteSourceFile,default_delay,GLOBAL.upload_location+'/'+iteratedValue+'_'+file_info.new_filename);
		}
		if(file_info.sort_order!=file_info.max)
		{
			var i = file_info.sort_order;
			readSrc(i,product_id,req,res);
		}
	}else{
		var sample_counter=GLOBAL.product_sizes.indexOf(targetValue);
		sample_counter=sample_counter+1;
		resampleImages(sample_counter,file_info,product_id,req,res)
	}
}
function deleteSourceFile(file)
{
	try
	{
		console.log("now executing delete file : "+file);
		if(util.isNullOrUndefined(fs)==true)
		{
			var fs=require('fs');
		}
		
		fs.access(file,fs.F_OK,function(err){
			if(err==null)
			{
				console.log('file found '+file);
				fs.access(file, fs.R_OK | fs.W_OK, function (err) {
					if(err==null)
					{
						console.log('can delete file'+file);
						//setTimeout(fs.unlinkSync,10000,GLOBAL.upload_location+'/'+filename);
						fs.unlink(file, function (err) {
							if(err!=null){
								console.log('Error Deleting '+file);
							}else{
								console.log('successfully deleted '+file);
							}
						});
					}else{
						console.log('no access to '+file);
					}
				});
			}else{
				console.log('not found file: '+file);
			}
		});
	}catch(err){
		console.log("FS NO LONGER VIABLE");
	}
	
}

function saveProductImages(product_id,file_info)
{
	console.log("saving image information to DB");
	var product_default_image = (file_info.default_image==true)?1:0;
	var query = "INSERT INTO `product_images` SET `product_id`="+parseInt(product_id)+", `product_default_image`="+product_default_image+",`product_image_filename`='"+file_info.new_filename+"', `product_image_url`='"+GLOBAL.storage_url+GLOBAL.storage_names.products+"/images/"+"',`product_sort_order`="+parseInt(file_info.sort_order)+",`product_prefixes`='"+GLOBAL.product_sizes.join(',')+"';";
	console.log("query :"+query);
	var mysql = new GLOBAL.mysql.mysql_connect(query);
	console.log("executing query INSERT");
	mysql.results_ss_common().then(function(results){
		if(util.isError(results)==true){
			var msg = {
				"success":false, "status":"Error",
				'desc':'[saveProductImageRecord:start]DB Error Connection',
				'query':query,
				'message':results.message,
				'stack':results.stack||null
			};
			process.emit('Shopstyx:logError',msg);
		}else{
			//results.insertId - product_id
			//no response
			console.log("Success Insert");
			console.log(query);
		}
	});
}
module.exports={
	start:start
}