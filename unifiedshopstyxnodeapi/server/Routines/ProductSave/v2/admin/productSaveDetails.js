const respond = require(GLOBAL.server_location+'/EventListeners/errorListener');
const insertProductInformation = require(__dirname+"/insertProductInformation");
const overwriteProductInformation = require(__dirname+"/overwriteProductInformation");
const insertVariant = require(__dirname+"/insertVariant");
const overwriteVariant = require(__dirname+"/overwriteVariant");
const insertSEO = require(__dirname+"/insertSEO");
const overwriteSEO = require(__dirname+"/overwriteSEO");
// var insertOptionalInformation = require(__dirname+"/insertOptionalInformation");
const overwriteOptionalInformation = require(__dirname+"/overwriteOptionalInformation");
const insertAddedFromWebInformation = require(__dirname+"/insertAddedFromWebInformation");
const overwriteAddedFromWebInformation = require(__dirname+"/overwriteAddedFromWebInformation");
const util = require('util');


var processRequest=function(dataJSON,dataStruct,req,res)
{
	var error = false;
	var error_msg = [];
	switch(((dataJSON.request.type).toLowerCase()).replace(/\s/g, ''))
	{
		case "saveproduct":
			if(util.isNullOrUndefined(dataJSON.request.product_info)==false)
			{
				insertProductInformation.start(dataJSON,dataStruct,success,req,res);
			}else{
				error=true;
				error_msg.push("No product_info found");
			}
			break;
		case "overwriteproduct":
			if(util.isNullOrUndefined(dataJSON.request.product_info)==false)
			{
				overwriteProductInformation.start(dataJSON,dataStruct,success,req,res);
			}else{
				error=true;
				error_msg.push("No product_info found");
			}
			
			break;
		case "savevariant":
			insertVariant.start(dataJSON,dataStruct,success,req,res);
			break;
		case "overwritevariant":
			overwriteVariant.start(dataJSON,dataStruct,success,req,res);
			break;
		case "saveseo":
			insertSEO.start(dataJSON,dataStruct,success,req,res);
			break;
		case "overwriteseo":
			overwriteSEO.start(dataJSON,dataStruct,success,req,res);
			break;
		case "saveoptionalinfo":
		case "overwrite optional info":
			overwriteOptionalInformation.start(dataJSON,dataStruct,success,req,res);
			break;
		case "saveaddedfromweb":
			insertAddedFromWebInformation.start(dataJSON,dataStruct,success,req,res);
			break;
		case "overwriteaddedfromweb":
			overwriteAddedFromWebInformation.start(dataJSON,dataStruct,success,req,res);
			break;
		default:
			var msg = {
				"success":false, "status":"Error",
				"desc":"[productSaveDetails:processRequest:IdentificationOfRequest]Unknown Request"
			};
			respond.respondError(msg,res,req);
						
						
			break;
	}
	if(error==true)
	{
		var msg = {
			"success":false, "status":"Error",
			"desc":"[productSaveDetails:processRequest]Missing Requirements Found",
			"additional_info":error_msg
		};
		respond.respondError(msg,res,req);
	}
};

function success(dataJSON,dataStruct,req,res)
{
	var msg = {
		"success":true, "status":"Success",
		"desc":"[productSaveDetails:success]Product Data Saved"
	};
	respond.respondError(msg,res,req);
						
						
}
module.exports={
	processRequest:processRequest
};