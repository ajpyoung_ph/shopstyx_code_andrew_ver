const util = require('util');
const respond = require(GLOBAL.server_location+'/EventListeners/errorListener');
const overwriteSEO = require(__dirname+"/overwriteSEO");
const overwriteOptionalInformation = require(__dirname+"/overwriteOptionalInformation");
const overwriteAddedFromWebInformation = require(__dirname+"/overwriteAddedFromWebInformation");
const saveImages = require('./saveImages');
//finding the commonnodeconfig

var mysqlConv = require(GLOBAL.server_location+'/helpers/mysql_convertions');


const start = function(dataJSON,dataStruct,cb,req,res)
{
	/*
	UPDATE [LOW_PRIORITY] [IGNORE] table_reference
    SET col_name1={expr1|DEFAULT} [, col_name2={expr2|DEFAULT}] ...
    [WHERE where_condition]
    [ORDER BY ...]
    [LIMIT row_count]
	*/
	var query = "UPDATE `products` SET ";

	//check validity of JSON variables
	if(typeof(dataJSON.request.product_info)!='undefined')
	{
		dataJSON['current_check']='optional_info';
		var product_info_error = false;
		var product_info_NaN = false;
		if(util.isNullOrUndefined(dataJSON.request.product_info.product_id)==true || util.isNullOrUndefined(dataJSON.request.product_info.owner_user_id)==true)
		{
			product_info_error=true;
		}
		//check if any id is NaN
		// if(isNaN(dataJSON.request.product_info.product_owner_id)==true || isNaN(dataJSON.request.product_info.category_id)==true || isNaN(dataJSON.request.product_info.group_id)==true || isNaN(dataJSON.request.product_info.product_id)==true)
		// {
		// 	product_info_NaN=true;
		// }
		if(product_info_error==false && product_info_NaN==false)
		{
			// var shop_id = 0;
			// if(typeof(dataJSON.request.product_info.store_id)!='undefined')
			// {
			// 	shop_id=parseInt(dataJSON.request.product_info.shop_id);
			// }
			for (var k in dataJSON.request.product_info){
			    if (dataJSON.request.product_info.hasOwnProperty(k)) {
			         console.log( k + "=" + dataJSON.request.product_info[k]);
			         if(k.trim()!='group_id')
			         {
				         if(isNaN(parseInt(dataJSON.request.product_info[k]))==true)
				         {
				         	if(k.trim()!='product_id')
				         	{
				         		query = query + "`"+k+"`='"+mysqlConv.mysql_real_escape_string(dataJSON.request.product_info[k])+"',";
				         	}
				         }else{
				         	if(k.trim()!='product_id')
				         	{
				         		query = query + "`"+k+"`="+dataJSON.request.product_info[k]+",";
				         	}
				         }
			         }
			    }
			}
			query = query.slice(0, -1);
			// query = query + "`owner_user_id`="+parseInt(dataJSON.request.product_info.product_owner_id)+", ";
			// query = query + "`owner_shop_id`="+parseInt(shop_id)+", ";
			// query = query + "`product_name`='"+mysqlConv.mysql_real_escape_string(dataJSON.request.product_info.product_name)+"', ";
			// query = query + "`product_description`='"+mysqlConv.mysql_real_escape_string(dataJSON.request.product_info.product_description)+"', ";
			// query = query + "`category_id`="+parseInt(dataJSON.request.product_info.category_id)+", ";
			// query = query + "`is_added_from_web`="+parseInt(dataJSON.request.product_info.is_added_from_web)+" ";
			query = query + " WHERE `product_id`="+parseInt(dataJSON.request.product_info.product_id)+";";

			var mysql = new GLOBAL.mysql.mysql_connect(query);
			mysql.results_ss_common().then(function(results){
				if(util.isError(results)==true){
					var msg = {
						"success":false, "status":"Error",
						'desc':'[overwriteProductInformation:start]DB Error Connection',
						'query':query,
						'message':results.message,
						'stack':results.stack||null
					};
					respond.respondError(msg,res,req);
				}else{
					setImmediate(saveGroupInformation,dataJSON,dataStruct,parseInt(dataJSON.request.product_info.product_id),req,res);
					dataJSON.current_check='optional_info';
					setImmediate(processOtherData,dataJSON,dataStruct,req,res);
					saveImages.start(dataJSON,dataStruct,parseInt(dataJSON.request.product_info.product_id),"update",req,res);
				}
			});
		}else{
			var msg = {
				"success":false, "status":"Error",
				"desc":"[overwriteProductInformation:start:LackProdInfo]Data Structure not valid",
				"product_info_error":product_info_error,
				"product_info_NaN":product_info_NaN
			};
			respond.respondError(msg,res,req);
						
						
		}
	}else{
		var msg = {
			"success":false, "status":"Error",
			"desc":"[overwriteProductInformation:start:NoProdInfo]Data Structure not valid"
		};
		respond.respondError(msg,res,req);
						
						
	}
};

function processOtherData(dataJSON,dataStruct,req,res)
{
	switch(dataJSON.current_check)
	{
		case 'optional_info':
			dataJSON.current_check='added_from_web_info';
			if(typeof(dataJSON.request.optional_info)!='undefined')
			{
				overwriteOptionalInformation.start(dataJSON,dataStruct,processOtherData,req,res);
			}else{
				processOtherData(dataJSON,dataStruct,req,res);
			}
			break;
		case 'added_from_web_info':
			dataJSON.current_check='seo_info';
			if(typeof(dataJSON.request.added_from_web_info)!='undefined')
			{
				overwriteAddedFromWebInformation.start(dataJSON,dataStruct,processOtherData,req,res);
			}else{
				processOtherData(dataJSON,dataStruct,req,res);
			}
			break;
		case 'seo_info':
			dataJSON.current_check='default';
			if(typeof(dataJSON.request.seo_info)!='undefined')
			{
				overwriteSEO.start(dataJSON,dataStruct,processOtherData,req,res);
			}else{
				processOtherData(dataJSON,dataStruct,req,res);
			}
		default:
			if(typeof(cb)=='function')
			{
				cb(dataJSON,dataStruct,req,res);
			}else{
				var msg = {
					"success":true, "status":"Success",
					"desc":"[overwriteProductInformation:processOtherData]Product Saved"
				};
				respond.respondError(msg,res,req);
						
						
			}
			break;
			
	}
}
function saveGroupInformation(dataJSON,dataStruct,product_id,req,res)
{
	var query = 'SELECT * FROM `product_groupings_product_members` WHERE `product_id`='+parseInt(product_id)+' AND `group_owner_user_id`='+parseInt(dataJSON.request.product_info.owner_user_id)+';';
	var mysql = new GLOBAL.mysql.mysql_connect(query);
	mysql.results_ss_common().then(function(results){
		if(util.isError(results)==true){
			var msg = {
				"success":false, "status":"Error",
				'desc':'[overwriteProductInformation:saveGroupInformation]DB Error Connection',
				'query':query,
				'message':results.message,
				'stack':results.stack||null
			};
			respond.respondError(msg,res,req);
		}else{
			if(results.length==0)
			{
				if(parseInt(dataJSON.request.product_info.group_id)>0)
				{
					var query2 = 'INSERT INTO `product_groupings_product_members` SET `grouping_id`='+parseInt(dataJSON.request.product_info.group_id)+', `product_id`='+parseInt(product_id)+', `group_owner_user_id`='+parseInt(dataJSON.request.product_info.owner_user_id)+', `group_owner_shop_id`=0;';
					var mysql2 = new GLOBAL.mysql.mysql_connect(query2);
					mysql2.results_ss_common().then(function(results){
						if(util.isError(results)==true){
							var msg = {
								"success":false, "status":"Error",
								'desc':'[overwriteProductInformation:saveGroupInformation]DB Error Connection',
								'query':query2,
								'message':results.message,
								'stack':results.stack||null
							};
							respond.respondError(msg,res,req);
						}else{
							var msg = {
								"success":true, "status":"Success",
								"desc":"[overwriteProductInformation:saveGroupInformation]Product Saved"
							};
							respond.respondError(msg,res,req);
						}
					});
				}
			}else{
				if(parseInt(dataJSON.request.product_info.group_id)>0)
				{
					var query2 = 'UPDATE `product_groupings_product_members` SET `grouping_id`='+parseInt(dataJSON.request.product_info.group_id)+' WHERE `product_id`='+parseInt(product_id)+' AND `group_owner_user_id`='+parseInt(dataJSON.request.product_info.owner_user_id)+';';
					var mysql2 = new GLOBAL.mysql.mysql_connect(query2);
					mysql2.results_ss_common().then(function(results){
						if(util.isError(results)==true){
							var msg = {
								"success":false, "status":"Error",
								'desc':'[overwriteProductInformation:saveGroupInformation]DB Error Connection',
								'query':query2,
								'message':results.message,
								'stack':results.stack||null
							};
							respond.respondError(msg,res,req);
						}else{
							var msg = {
								"success":true, "status":"Success",
								"desc":"[overwriteProductInformation:saveGroupInformation]Product Saved"
							};
							respond.respondError(msg,res,req);
						}
					});
				}
			}
			if(parseInt(dataJSON.request.product_info.group_id)==0)
			{
				var query2 = 'DELETE FROM `product_groupings_product_members` WHERE `product_id`='+parseInt(product_id)+' AND `group_owner_user_id`='+parseInt(dataJSON.request.product_info.owner_user_id)+';';
				var mysql2 = new GLOBAL.mysql.mysql_connect(query2);
				mysql2.results_ss_common().then(function(results){
					if(util.isError(results)==true){
						var msg = {
							"success":false, "status":"Error",
							'desc':'[overwriteProductInformation:saveGroupInformation]DB Error Connection',
							'query':query2,
							'message':results.message,
							'stack':results.stack||null
						};
						respond.respondError(msg,res,req);
					}else{
						var msg = {
							"success":true, "status":"Success",
							"desc":"[overwriteProductInformation:saveGroupInformation]Product Saved"
						};
						respond.respondError(msg,res,req);
					}
				});
			}
			console.log(query2);
		}
	});
}

module.exports={
	start:start
};