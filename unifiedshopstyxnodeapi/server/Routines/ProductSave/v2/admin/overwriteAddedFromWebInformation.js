const respond = require(GLOBAL.server_location+'/EventListeners/errorListener');
const util = require('util');

const mysqlConv = require(GLOBAL.server_location+'/helpers/mysql_convertions');

const start = function(dataJSON,dataStruct,cb,req,res)
{
	/*
	INSERT [LOW_PRIORITY | DELAYED | HIGH_PRIORITY] [IGNORE]
    [INTO] tbl_name
    [PARTITION (partition_name,...)]
    SET col_name={expr | DEFAULT}, ...
    [ ON DUPLICATE KEY UPDATE
      col_name=expr
        [, col_name=expr] ... ]
	*/
	var query = "UPDATE `product_added_from_web_info` SET ";

	//check validity of JSON variables
	if(typeof(dataJSON.request.added_from_web_info)!='undefined' && typeof(dataJSON.request.product_info)!='undefined')
	{
		var product_info_error = false;
		var product_info_NaN = false;
		if(typeof(dataJSON.request.product_info.product_id)=='undefined' || typeof(dataJSON.request.added_from_web_info.added_by_user_id)=='undefined' || typeof(dataJSON.request.added_from_web_info.url_source)=='undefined' || typeof(dataJSON.request.added_from_web_info.canonical_url)=='undefined' || typeof(dataJSON.request.added_from_web_info.domain_name)=='undefined')
		{
			product_info_error=true;
		}
		//check if any id is NaN
		if(isNaN(dataJSON.request.product_info.product_id)==true || isNaN(dataJSON.request.added_from_web_info.added_by_user_id))
		{
			product_info_NaN=true;
		}
		if(product_info_error==false && product_info_NaN==false)
		{
			query = query + "`added_by_user_id`="+parseInt(dataJSON.request.added_from_web_info.added_by_user_id)+", ";
			query = query + "`url_source`='"+mysqlConv.mysql_real_escape_string(dataJSON.request.added_from_web_info.url_source)+"', ";
			query = query + "`canonical_url`='"+mysqlConv.mysql_real_escape_string(dataJSON.request.added_from_web_info.canonical_url)+"', ";
			query = query + "`domain_name`='"+mysqlConv.mysql_real_escape_string(dataJSON.request.added_from_web_info.domain_name)+"' ";
			query = query + "WHERE `product_id`="+parseInt(dataJSON.request.product_info.product_id)+";";

			var mysql = new GLOBAL.mysql.mysql_connect(query);
			mysql.results_ss_common().then(function(results){
				if(util.isError(results)==true){
					var msg = {
						"success":false, "status":"Error",
						'desc':'[overwriteAddedFromWebInformation:start]DB Error Connection',
						'query':query,
						'message':results.message,
						'stack':results.stack||null
					};
					process.emit('Shopstyx:logError',msg);
					if(typeof(cb)=='function')
					{
						cb(dataJSON,dataStruct,req,res);
					}
				}else{
					if(typeof(cb)=='function')
					{
						cb(dataJSON,dataStruct,req,res);
					}else{
						var msg = {
							"success":true, "status":"Success",
							"desc":"[overwriteAddedFromWebInformation:start:SuccessSaveSEO]Saved Add from Web Info"
						};
						process.emit('Shopstyx:logError',msg);
					}
				}
			});
		}else{
			var msg = {
				"success":false, "status":"Error",
				"desc":"[overwriteAddedFromWebInformation:start:LackProdInfo]Data Structure not valid",
				"product_info_error":product_info_error,
				"product_info_NaN":product_info_NaN
			};
			respond.respondError(msg,res,req);

						
						
		}
	}else{
		var msg = {
			"success":false, "status":"Error",
			"desc":"[overwriteAddedFromWebInformation:start:NoProdInfo]Data Structure not valid"
		};
		respond.respondError(msg,res,req);
		
						
						
	}
};

module.exports={
	start:start
};