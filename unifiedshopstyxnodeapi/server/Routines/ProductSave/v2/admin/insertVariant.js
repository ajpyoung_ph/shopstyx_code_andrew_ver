const respond = require(GLOBAL.server_location+'/EventListeners/errorListener');
const util = require('util');


const mysqlConv = require(GLOBAL.server_location+'/helpers/mysql_convertions');

const start = function(dataJSON,dataStruct,cb,req,res)
{
	/*
	INSERT [LOW_PRIORITY | DELAYED | HIGH_PRIORITY] [IGNORE]
    [INTO] tbl_name
    [PARTITION (partition_name,...)]
    SET col_name={expr | DEFAULT}, ...
    [ ON DUPLICATE KEY UPDATE
      col_name=expr
        [, col_name=expr] ... ]
	*/
	

	//check validity of JSON variables
	if(typeof(dataJSON.request.variant_info)!='undefined' && typeof(dataJSON.request.product_info)!='undefined')
	{
		var product_info_error = false;
		var product_info_NaN = false;
		if(typeof(dataJSON.request.product_info.product_id)=='undefined' || typeof(dataJSON.request.variant_info)=='object')
		{
			product_info_error=true;
		}
		//check if any id is NaN
		if(isNaN(dataJSON.request.product_info.product_id)==true || isNaN(dataJSON.request.variant_info.added_by_user_id))
		{
			product_info_NaN=true;
		}
		if(product_info_error==false && product_info_NaN==false)
		{
			var x =0;
			saveData(x,dataJSON,dataStruct,cb,req,res);
			//save the variant name in the products table
		}else{
			var msg = {
				"success":false, "status":"Error",
				"desc":"[insertVariant:start:LackProdInfo]Data Structure not valid",
				"product_info_error":product_info_error,
				"product_info_NaN":product_info_NaN
			};
			respond.respondError(msg,res,req);

						
						
		}
	}else{
		var msg = {
			"success":false, "status":"Error",
			"desc":"[insertVariant:start:NoProdInfo]Data Structure not valid"
		};
		respond.respondError(msg,res,req);

						
						
	}
};

function saveData(x,dataJSON,dataStruct,cb,req,res)
{
	var error=false;
	try{
		var query = "INSERT INTO `product_variant` SET ";
		query = query + "`product_id`="+parseInt(dataJSON.request.product_info.product_id)+", ";
		query = query + "`variant_qty`="+parseInt(dataJSON.request.variant_info[x]["variant_qty"])+", ";
		query = query + "`variant_name`='"+mysqlConv.mysql_real_escape_string(dataJSON.request.variant_info[x]["variant_name"])+"'; ";
	}catch(err){

		var msg = {
			"success":false, "status":"Error",
			"desc":"[insertVariant:saveData:ErrorAccessVariable]Data Structure not valid",
			"product_info_error":product_info_error,
			"product_info_NaN":product_info_NaN
		};
		respond.respondError(msg,res,req);

						
						
		error=true;
	}
	if(error==false)
	{
		addVariantName(dataJSON,dataStruct,x);
		var mysql = new GLOBAL.mysql.mysql_connect(query);
		mysql.results_ss_common().then(function(results){
			if(util.isError(results)==true){
				var msg = {
					"success":false, "status":"Error",
					'desc':'[insertVariant:saveData]DB Error Connection',
					'query':query,
					'message':results.message,
					'stack':results.stack||null
				};
				process.emit('Shopstyx:logError',msg);
				x=x+1;
				saveData(x,dataJSON,dataStruct,cb)
			}else{
				//check if length information is existing
				if(typeof(dataJSON.request.variant_info[x]["variant_length"])!='undefined' && typeof(dataJSON.request.variant_info[x]["variant_length"])!='undefined' && typeof(dataJSON.request.variant_info[x]["variant_length"])!='undefined' && typeof(dataJSON.request.variant_info[x]["variant_length"])!='undefined' && typeof(dataJSON.request.variant_info[x]["variant_length"])!='undefined' && typeof(dataJSON.request.variant_info[x]["variant_length"])!='undefined' )
				{
					saveVariantLength(x,dataJSON,dataStruct,cb,results.insertId,req,res);
				}else{
					if(x==dataJSON.request.variant_info.length-1)
					{
						if(typeof(cb)=='function')
						{
							cb(dataJSON,dataStruct,req,res);
						}else{
							var msg = {
								"success":true, "status":"Success",
								"desc":"[insertVariant:saveData:SuccessSaveSEO]Saved Add from Web Info"
							};
							process.emit('Shopstyx:logError',msg);
						}
					}else{
						x=x+1;
						saveData(x,dataJSON,dataStruct,cb)
					}
				}
			}
		});
	}
}

function saveVariantLength(x,dataJSON,dataStruct,cb,variant_id,req,res)
{
	var error=false;
	try{
		var query = "INSERT INTO `product_variant_sizes` SET ";
		query = query + "`variant_id`="+parseInt(variant_id)+", ";
		query = query + "`variant_length`="+parseInt(dataJSON.request.variant_info[x]["variant_length"])+", ";
		query = query + "`variant_width`="+parseInt(dataJSON.request.variant_info[x]["variant_width"])+", ";
		query = query + "`variant_height`="+parseInt(dataJSON.request.variant_info[x]["variant_height"])+", ";
		query = query + "`variant_lwh_unit_id`="+parseInt(dataJSON.request.variant_info[x]["variant_lwh_unit_id"])+", ";
		query = query + "`variant_weight`="+parseInt(dataJSON.request.variant_info[x]["variant_weight"])+", ";
		query = query + "`variant_weight_unit_id`="+parseInt(dataJSON.request.variant_info[x]["variant_weight_unit_id"])+"; ";
	}catch(err){

		var msg = {
			"success":false, "status":"Error",
			"desc":"[insertVariant:saveVariantLength:ErrorAccessVariable]Data Structure not valid",
			"product_info_error":product_info_error,
			"product_info_NaN":product_info_NaN
		};
		respond.respondError(msg,res,req);
		
						
						
		error=true;
	}
	if(error==false)
	{
		var mysql = new GLOBAL.mysql.mysql_connect(query);
		mysql.results_ss_common().then(function(results){
			if(util.isError(results)==true){
				var msg = {
					"success":false, "status":"Error",
					'desc':'[insertVariant:saveVariantLength]DB Error Connection',
					'query':query,
					'message':results.message,
					'stack':results.stack||null
				};
				process.emit('Shopstyx:logError',msg);
				x=x+1;
				saveData(x,dataJSON,dataStruct,cb,req,res)
			}else{
				if(x==dataJSON.request.variant_info.length-1)
				{
					if(typeof(cb)=='function')
					{
						cb(dataJSON,dataStruct,req,res);
					}else{
						var msg = {
							"success":true, "status":"Success",
							"desc":"[insertVariant:saveVariantLength:saveVariantLength]Saved Add from Web Info"
						};
						process.emit('Shopstyx:logError',msg);
					}
				}else{
					x=x+1;
					saveData(x,dataJSON,dataStruct,cb,req,res);
				}	
			}
		});
	}
}
//,req,res
function addVariantName(dataJSON,dataStruct,x)
{
	var query = "SELECT * FROM `products` WHERE `product_id`="+parseInt(dataJSON.request.product_info.product_id)+";";
	var mysql = new GLOBAL.mysql.mysql_connect(query);
	mysql.results_ss_common().then(function(results){
		if(util.isError(results)==true){
			var msg = {
				"success":false, "status":"Error",
				'desc':'[insertVariant:addVariantName]DB Error Connection',
				'query':query,
				'message':results.message,
				'stack':results.stack||null
			};
			process.emit('Shopstyx:emailError',msg);
		}else{
			var rows = results;
			var q2 = '';
			if(rows.length>0)
			{
				var data=rows[0].variant_titles_csv;
				for(var x=0;x<data.length;x++)
				{
					data[x]=mysqlConv.mysql_real_escape_string(data[x].trim());
				}
				var arr_data = data.split(',');
				arr_data.push(dataJSON.request.variant_info[x]["variant_name"].toLowerCase().trim());
				data = arr_data.join(',');
				q2 = 'UPDATE `products` SET `variant_titles_csv`="'+data+'" WHERE `product_id`='+parseInt(dataJSON.request.product_info.product_id)+';';
			}else{
				var data = mysqlConv.mysql_real_escape_string(dataJSON.request.variant_info[x]["variant_name"].toLowerCase().trim());
				q2 = 'INSERT INTO `products` SET `variant_titles_csv`="'+data+'" WHERE `product_id`='+parseInt(dataJSON.request.product_info.product_id)+';';
			}
			var mysql2 = new GLOBAL.mysql.mysql_connect(q2);
			mysql2.results_ss_common().then(function(results2){
				if(util.isError(results2)==true){
					var msg = {
						"success":false, "status":"Error",
						'desc':'[insertVariant:addVariantName]DB Error Connection2',
						'query':q2,
						'message':results2.message,
						'stack':results2.stack||null
					};
					process.emit('Shopstyx:logError',msg);
				}
			});
		}
	});
}
module.exports={
	start:start
};