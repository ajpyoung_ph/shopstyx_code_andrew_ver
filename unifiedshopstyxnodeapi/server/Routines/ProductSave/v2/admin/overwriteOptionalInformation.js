const util = require('util');
const respond = require(GLOBAL.server_location+'/EventListeners/errorListener');
const mysqlConv = require(GLOBAL.server_location+'/helpers/mysql_convertions');

const start = function(dataJSON,dataStruct,cb,req,res)
{
	/*
	INSERT [LOW_PRIORITY | DELAYED | HIGH_PRIORITY] [IGNORE]
    [INTO] tbl_name
    [PARTITION (partition_name,...)]
    SET col_name={expr | DEFAULT}, ...
    [ ON DUPLICATE KEY UPDATE
      col_name=expr
        [, col_name=expr] ... ]
	*/
	var query = "UPDATE `products` SET ";

	//check validity of JSON variables
	if(typeof(dataJSON.request.optional_info)!='undefined' && typeof(dataJSON.request.product_info)!='undefined')
	{
		var product_info_error = false;
		var product_info_NaN = false;
		if(typeof(dataJSON.request.product_info.product_id)=='undefined' || typeof(dataJSON.request.optional_info.department_id)=='undefined' || typeof(dataJSON.request.optional_info.shipping_class_id)=='undefined' || typeof(dataJSON.request.optional_info.length)=='undefined' || typeof(dataJSON.request.optional_info.width)=='undefined' || typeof(dataJSON.request.optional_info.height)=='undefined' || typeof(dataJSON.request.optional_info.lwh_unit_id)=='undefined' || typeof(dataJSON.request.optional_info.weight)=='undefined' || typeof(dataJSON.request.optional_info.weight_unit_id)=='undefined')
		{
			product_info_error=true;
		}
		//check if any id is NaN
		if(isNaN(dataJSON.request.product_info.product_id)==true || isNaN(dataJSON.request.optional_info.department_id)==true || isNaN(dataJSON.request.optional_info.shipping_class_id)==true  || isNaN(dataJSON.request.optional_info.length)==true  || isNaN(dataJSON.request.optional_info.width)==true  || isNaN(dataJSON.request.optional_info.height)==true  || isNaN(dataJSON.request.optional_info.lwh_unit_id)==true  || isNaN(dataJSON.request.optional_info.weight)==true  || isNaN(dataJSON.request.optional_info.weight_unit_id)==true )
		{
			product_info_NaN=true;
		}
		if(product_info_error==false && product_info_NaN==false)
		{
			query = query + "`department_id`='"+parseInt(dataJSON.request.optional_info.department_id)+"', ";
			query = query + "`shipping_class_id`='"+parseInt(dataJSON.request.optional_info.shipping_class_id)+"', ";
			query = query + "`length`='"+parseFloat(dataJSON.request.optional_info.length)+"', ";
			query = query + "`width`='"+parseFloat(dataJSON.request.optional_info.width)+"', ";
			query = query + "`height`='"+parseFloat(dataJSON.request.optional_info.height)+"', ";
			query = query + "`lwh_unit_id`='"+parseInt(dataJSON.request.optional_info.lwh_unit_id)+"', ";
			query = query + "`weight`='"+parseFloat(dataJSON.request.optional_info.weight)+"', ";
			query = query + "`weight_unit_id`='"+parseInt(dataJSON.request.optional_info.weight_unit_id)+"' ";
			query = query + "WHERE `product_id`="+parseInt(dataJSON.request.product_info.product_id)+";";

			var mysql = new GLOBAL.mysql.mysql_connect(query);
			mysql.results_ss_common().then(function(results){
				if(util.isError(results)==true){
					var msg = {
						"success":false, "status":"Error",
						'desc':'[overwriteOptionalInformation:start]DB Error Connection',
						'query':query,
						'message':results.message,
						'stack':results.stack||null
					};
					process.emit('Shopstyx:logError',msg);
					if(typeof(cb)=='function')
					{
						cb(dataJSON,dataStruct,req,res);
					}
				}else{
					if(typeof(cb)=='function')
					{
						cb(dataJSON,dataStruct,req,res);
					}else{
						var msg = {
							"success":true, "status":"Success",
							"desc":"[overwriteOptionalInformation:start:SuccessSaveOptionalInfo]Saved Optional Info"
						};
						process.emit('Shopstyx:logError',msg);
					}
				}
			});
		}else{
			var msg = {
				"success":false, "status":"Error",
				"desc":"[overwriteOptionalInformation:start:LackProdInfo]Data Structure not valid",
				"product_info_error":product_info_error,
				"product_info_NaN":product_info_NaN
			};
			respond.respondError(msg,res,req);
			
						
						
		}
	}else{
		var msg = {
			"success":false, "status":"Error",
			"desc":"[overwriteOptionalInformation:start:NoProdInfo]Data Structure not valid"
		};
		respond.respondError(msg,res,req);

						
						
	}
};

module.exports={
	start:start
};