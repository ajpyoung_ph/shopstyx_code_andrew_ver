const util = require('util');
const respond = require(GLOBAL.server_location+'/EventListeners/errorListener');

const insertSEO = require(__dirname+"/insertSEO");
const overwriteOptionalInformation = require(__dirname+"/overwriteOptionalInformation");
const insertAddedFromWebInformation = require(__dirname+"/insertAddedFromWebInformation");
const mysqlConv = require(GLOBAL.server_location+'/helpers/mysql_convertions');
const saveImages = require('./saveImages');

const start = function(dataJSON,dataStruct,cb,req,res)
{
	/*
	INSERT [LOW_PRIORITY | DELAYED | HIGH_PRIORITY] [IGNORE]
    [INTO] tbl_name
    [PARTITION (partition_name,...)]
    SET col_name={expr | DEFAULT}, ...
    [ ON DUPLICATE KEY UPDATE
      col_name=expr
        [, col_name=expr] ... ]
	*/
	var query = "INSERT INTO `products` SET ";

	//check validity of JSON variables
	if(typeof(dataJSON.request.product_info)!='undefined')
	{
		dataJSON['current_check']='optional_info';
		var product_info_error = false;
		var product_info_NaN = false;
		if(util.isNullOrUndefined(dataJSON.request.product_info.product_id)==true || util.isNullOrUndefined(dataJSON.request.product_info.owner_user_id)==true)
		{
			product_info_error=true;
		}
		//check if any id is NaN
		// if(isNaN(dataJSON.request.product_info.product_owner_id)==true || isNaN(dataJSON.request.product_info.category_id)==true || isNaN(dataJSON.request.product_info.group_id)==true)
		// {
		// 	product_info_NaN=true;
		// }
		if(product_info_error==false && product_info_NaN==false)
		{
			for (var k in dataJSON.request.product_info){
			    if (dataJSON.request.product_info.hasOwnProperty(k)) {
			         console.log( k + "=" + dataJSON.request.product_info[k]);
			         if(k.trim()!='group_id')
			         {
				         if(isNaN(parseInt(dataJSON.request.product_info[k]))==true)
				         {
				         	if(k.trim()!='product_id')
				         	{
				         		query = query + "`"+k+"`='"+mysqlConv.mysql_real_escape_string(dataJSON.request.product_info[k])+"',";
				         	}
				         }else{
				         	if(k.trim()!='product_id')
				         	{
				         		query = query + "`"+k+"`="+dataJSON.request.product_info[k]+",";
				         	}
				         }
			         }
			    }
			}
			query = query.slice(0, -1);
			query = query +";";
			// var shop_id = 0;
			// if(typeof(dataJSON.request.product_info.store_id)!='undefined')
			// {
			// 	shop_id=parseInt(dataJSON.request.product_info.shop_id);
			// }
			// query = query + "`owner_user_id`="+parseInt(dataJSON.request.product_info.product_owner_id)+", ";
			// query = query + "`owner_shop_id`="+parseInt(shop_id)+", ";
			// query = query + "`product_name`='"+mysqlConv.mysql_real_escape_string(dataJSON.request.product_info.product_name)+"', ";
			// query = query + "`product_description`='"+mysqlConv.mysql_real_escape_string(dataJSON.request.product_info.product_description)+"', ";
			// query = query + "`category_id`="+parseInt(dataJSON.request.product_info.category_id)+", ";
			// query = query + "`is_added_from_web`="+parseInt(dataJSON.request.product_info.is_added_from_web)+"; ";
			//query = query + "`group_id`="+parseInt(dataJSON.request.product_info.product_owner_id)+";";
			var mysql = new GLOBAL.mysql.mysql_connect(query);
			mysql.results_ss_common().then(function(results){
				if(util.isError(results)==true){
					var msg = {
						"success":false, "status":"Error",
						'desc':'[insertProductInformation:start]DB Error Connection',
						'query':query,
						'message':results.message,
						'stack':results.stack||null
					};
					respond.respondError(msg,res,req);
				}else{
					if(typeof(dataJSON.request.product_info)=='undefined')
					{
						dataJSON.request.product_info={
							'product_id':parseInt(results.insertId)
						};
					}else{
						dataJSON.request.product_info.product_info=parseInt(results.insertId);
					}
					dataJSON.request.product_info.product_id=results.insertId;
					dataJSON.current_check='optional_info';
					setImmediate(processOtherData,dataJSON,dataStruct,req,res);
					if(parseInt(dataJSON.request.product_info.group_id)!=0 && parseInt(dataJSON.request.product_info.group_id)>0)
					{
						setImmediate(saveGroupInformation,dataJSON,dataStruct,results.insertId,req,res);
					}
					setImmediate(saveImages.start,dataJSON,dataStruct,results.insertId,"insert",req,res);
				}
			});
		}else{
			var msg = {
				"success":false, "status":"Error",
				"desc":"[insertProductInformation:start:LackProdInfo]Data Structure not valid",
				"product_info_error":product_info_error,
				"product_info_NaN":product_info_NaN
			};
			respond.respondError(msg,res,req);
		}
	}else{
		var msg = {
			"success":false, "status":"Error",
			"desc":"[insertProductInformation:start:NoProdInfo]Data Structure not valid"
		};
		respond.respondError(msg,res,req);
	}
};

function processOtherData(dataJSON,dataStruct,req,res)
{
	switch(dataJSON.current_check)
	{
		case 'optional_info':
			dataJSON.current_check='added_from_web_info';
			if(typeof(dataJSON.request.optional_info)!='undefined')
			{
				overwriteOptionalInformation.start(dataJSON,dataStruct,processOtherData,req,res);
			}else{
				processOtherData(dataJSON,dataStruct,req,res);
			}
			break;
		case 'added_from_web_info':
			dataJSON.current_check='seo_info';
			if(typeof(dataJSON.request.added_from_web_info)!='undefined')
			{
				insertAddedFromWebInformation.start(dataJSON,dataStruct,processOtherData,req,res);
			}else{
				processOtherData(dataJSON,dataStruct,req,res);
			}
			break;
		case 'seo_info':
			dataJSON.current_check='default';
			if(typeof(dataJSON.request.seo_info)!='undefined')
			{
				insertSEO.start(dataJSON,dataStruct,processOtherData,req,res);
			}else{
				processOtherData(dataJSON,dataStruct,req,res);
			}
		default:
			if(typeof(cb)=='function')
			{
				cb(dataJSON,dataStruct,req,res);
			}else{
				var msg = {
					"success":true, "status":"Success",
					"desc":"[insertProductInformation:processOtherData]Product Saved"
				};
				respond.respondError(msg,res,req);
						
						
			}
			break;
			
	}
}

function saveGroupInformation(dataJSON,dataStruct,product_id,req,res)
{
	var query = 'INSERT INTO `product_groupings_product_members` SET `grouping_id`='+parseInt(dataJSON.request.product_info.group_id)+', `product_id`='+parseInt(product_id)+',`group_owner_user_id`='+parseInt(dataJSON.request.product_info.owner_user_id)+';';
	var mysql = new GLOBAL.mysql.mysql_connect(query);
	mysql.results_ss_common().then(function(results){
		if(util.isError(results)==true){
			var msg = {
				"success":false, "status":"Error",
				'desc':'[insertProductInformation:processOtherData]DB Error Connection',
				'query':query,
				'message':results.message,
				'stack':results.stack||null
			};
			respond.respondError(msg,res,req);
		}else{
			var msg = {
				"success":true, "status":"Success",
				"desc":"[insertProductInformation:processOtherData]Product Saved"
			};
			respond.respondError(msg,res,req);
		}
	});
}
module.exports={
	start:start
};