const respond = require(GLOBAL.server_location+'/EventListeners/errorListener');
const listHome = require(__dirname+'/listHome');
const listProfile = require(__dirname+'/listProfile');
const parseDataJSON = require(GLOBAL.server_location+'/helpers/parseDataJSON');
const uuid = require('uuid');
const util = require('util');
//Documentation : http://mybugserver.shopstyx.com/wiki/index.php/Product_Listing
var processRequest = function(dataJSON,dataStruct,req,res)
{
	var error = false;
	var error_msg = [];
	/*
	"request":{
        "type":["ListHome"/"ListProfile"/"ListCollection"/"ListShop"/"ListShopCategories"/"Search"],
               (array type;only Search can be combined with the other type values)
        "page_number":<int>,(starts at page 0)
        "number_items":<int>,(the number of items to return),
        "sort_algorithm":["trending*"/"latest"/"OwnedByUserID"/"ProdNameASC"/"ProdNameDESC"/"ProdIDASC"/"ProdIDDESC"],
                         (array type and optional;default is always trending, if you specify another sort algorithm it will be mixed with the trending algorithm)
        "filter":{ (optional)
           "color":<string>,(White/Silver/Gray/Black/Brown/Red/Pink/Orange/Yellow/Lime/Green/Teal/Cyan/Blue/Indigo/Purple/Violet)
           "price_range":{ 
                "min":<int> (in USD),
                "max":<int> (in USD)
           },
           "department_id":<int>,
           "category_id":<int>
        },
        "additional_info":{
            "user_id":<int>(product listing user_id owner, required by type:ListProfile and filter:OwnedByUserID),
            "store_id":<int>(for ListProfile ListShop ListShopCategories),
            "collection_id":<int>(for ListProfile ListCollection),
            "category_id":<int>(for ListShop ListShopCategories), (same as collection_id)
            "search_key":<string> (for Search only)
        }
    }
	*/
	//check if all required variables are set
	// if(typeof(dataJSON.security)!='undefined')
	// {
	// 	if(typeof(dataJSON.security.user_id)=='undefined')
	// 	{
	// 		error=true;
	// 		error_msg.push('no member id found in the security area');
	// 	}
	// }
	if(typeof(dataJSON.request)!='undefined')
	{
		//check minimum request requirements
		if(typeof(dataJSON.request.type)=='undefined' && typeof(dataJSON.request.page_number)=='undefined')
		{
			error=true;
			error_msg.push("Request Type or Page Number missing");
		}
		//if search was specified
		if(util.isNullOrUndefined(dataJSON.request.type)==false)
		{
			if(Array.isArray(dataJSON.request.type)==true)
			{
				if(dataJSON.request.type.join("`").toLowerCase().split("`").indexOf("search")>-1)
				{
					if(typeof(dataJSON.request.additional_info)=='undefined')
					{
						error=true;
						error_msg.push("SEARCH specified with no additional_info");
					}else{
						if(typeof(dataJSON.request.additional_info.search_key)=='undefined')
						{
							error=true;
							error_msg.push("SEARCH specified with no search key value");
						}
					}
				}
			}else{
				error=true;
				error_msg.push("Request Type is not an Array");
				////console.log(dataJSON.request);
			}
		}else{
			error=true;
			error_msg.push("Type Request not found");
		}
		
		//if filter was specified
		if(typeof(dataJSON.request.filter)!='undefined')
		{
			if(typeof(dataJSON.request.filter.price_range)!='undefined')
			{
				if(typeof(dataJSON.request.filter.price_range.min)=='undefined' || typeof(dataJSON.request.filter.price_range.max)=='undefined')
				{
					error=true;
					error_msg.push("Price Filter specified without proper price range values");
				}
			}
		}

	}else{
		error=true;
		error_msg.push("No Request Made");
	}
	if(error==true)
	{
		var msg = {
			"success":false, "status":"Error",
			"desc":"[productListRoutines:processRequest]Error Processing Data",
			"additional_info":error_msg.join(','),
			"request_type":dataJSON.request.type,
			"err":dataJSON
		};
		respond.respondError(msg,res,req);

						
						
	}
	if(error==false)
	{
		//try{
			//set process tracker
			dataJSON['uuid']=uuid.v1();	

			if(dataJSON.request.type.join("`").toLowerCase().replace(/\s/g, '').split("`").indexOf("listhome")>-1)
			{
				try{
					if(typeof(dataJSON.security.user_id)=='undefined')
					{
						var msg = {
							"success":false, "status":"Error",
							"desc":"[ProductListing:processRequest]Missing Required Information",
							"err":"code 1",
							"additional_info":"ID of login user information is missing"
						};
						respond.respondError(msg,res,req);

						
						
					}else{
						////console.log("executing listHome");
						listHome.generateQuery(dataJSON,dataStruct,req,res);
					}
				}catch(err){
					var msg = {
						"success":false, "status":"Error",
						"desc":"[ProductListing:processRequest]Missing Required Information",
						"err":err,
						"additional_info":"code 2 try failed ID of user"
					};
					respond.respondError(msg,res,req);

						
						
				}
				
			}else if(dataJSON.request.type.join("`").toLowerCase().replace(/\s/g, '').split("`").indexOf("listprofile")>-1)//ListProfile 
			{
				if(typeof(dataJSON.request.additional_info.user_id)=='undefined')
				{
					var msg = {
						"success":false, "status":"Error",
						"desc":"[ProductListing:processRequest]Missing Required Information",
						"err":"code 3",
						"additional_info":"ID of user missing in required Information of Addition"
					};
					respond.respondError(msg,res,req);
				}else{
					////console.log("executing listProfile");
					listProfile.generateQuery(dataJSON,dataStruct,req,res);
					//listProfile.generateQuery(dataJSON,dataStruct);
				}
				
			// }else if(dataJSON.request.type.join("`").toLowerCase().split("`").indexOf("listshop")>-1)
			// {
			// 	listShop.generateQuery(dataJSON,dataStruct);
			// }else if(dataJSON.request.type.join("`").toLowerCase().split("`").indexOf("listcollection")>-1)
			// {
			// 	listCollection.generateQuery(dataJSON,dataStruct);
			// }else if(dataJSON.request.type.join("`").toLowerCase().split("`").indexOf("listshopcategories")>-1)
			// {
			// 	listShopCategories.generateQuery(dataJSON,dataStruct);
			}else
			{
				var msg = {
					"success":false, "status":"Error",
					"desc":"[ProductListing:processRequest]Uknown type request",
					"err":"code 1"
				};
				respond.respondError(msg,res,req);
				
						
						
			}
		// }catch(err){
		// 	var msg = {
		// 		"success":false, "status":"Error",
		// 		"desc":"[ProductListing:processRequest]Error Processing Data",
		// 		"err":err
		// 	};
		// 	respond.respondError(msg,res,req);
		// }
	}
};


module.exports={
	processRequest:processRequest
};