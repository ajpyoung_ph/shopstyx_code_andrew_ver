// var server_dir_array = (__dirname).toString().split("unifiedshopstyxnodeapi");
// GLOBAL.server_location = server_dir_array[0]+"commonnodeconfig";
delete server_dir_array;
const respond = require(GLOBAL.server_location+'/EventListeners/errorListener');
const listProductsAtHome = require('../../../QueryGenerator/listProductsAtHome');
// var listSearchedProductsAtHome = require('../../../QueryGenerator/listProductsByActivities');
const findImage = require('../../../ProductInfo/v2/client/findImage');
const findSEO = require("../../../ProductInfo/v2/client/findSEO");
const findAddFromWebInfo = require('../../../ProductInfo/v2/client/findAddFromWebInfo');
const findMonetaryUnit = require('../../../ProductInfo/v2/client/findMonetaryUnit');
const findLWHunit = require('../../../ProductInfo/v2/client/findLWHunit');
const findWeightUnit = require("../../../ProductInfo/v2/client/findWeightUnit");
const findShopInformation = require("../../../ProductInfo/v2/client/findShopInformation");

const encodeDecode = require (GLOBAL.server_location+'/helpers/encodeDecode');

const util = require('util');

const generateQuery = function(dataJSON,dataStruct,req,res)
{
	//////console.log("jumping to listProductsAtHome");
	listProductsAtHome.start(dataJSON,dataStruct,executeQuery,req,res);
}

const executeQuery = function(dataJSON,dataStruct,req,res)
{
	//////console.log("executingQuery");
	var query = dataStruct['formed_query'];
	//console.log(query);
	dataStruct['count']=0;
	var now = Date.now();
	//console.log("Getting Data for ListHome ============ "+Date());
	//console.log(query);
	var mysql = new GLOBAL.mysql.mysql_connect(query);
	mysql.results_ss_common().then(function(results){
		//console.log("response from MySQL ListHome ============= "+Date());
		//console.log("MySQL response ListHome =========> "+(Date.now()-now)+" ms");
		if(util.isError(results)==true){
			var msg = {
				"success":false, "status":"Error",
				'desc':'[readCollection:start]DB Error Connection',
				'query':query,
				'message':results.message,
				'stack':results.stack||null
			};
			respond.respondError(msg,res,req); 
		}else{
			var rows=results;
			dataJSON['resVal']={
				"success":true, "status":"Success",
				"desc":"[readCollection:start]Collections Found",
				"data":[]
			};
			dataJSON.rows=rows;
			if(dataJSON.rows.length>0)
			{
				setImmediate(breakdownProdIds,dataJSON,dataStruct,req,res);
			}else{
				respond.respondError(dataJSON['resVal'],res,req);
			}
		}
	});
}

const breakdownProdIds=function(dataJSON,dataStruct,req,res)
{
	var rows = dataJSON.rows;
	var prod_ids = [];
	var shop_ids = [];
	var variant_product_ids = [];
	var variant_color_names = [];
	for(var x = 0 ;x<rows.length;x++)
	{
		if(isNaN(parseInt(rows[x].product_id))==false)
		{
			prod_ids.push(parseInt(rows[x].product_id));
		}else{
			//console.log("product id NaN "+rows[x].product_id);
			//console.log(parseInt(rows[x].product_id));
			//console.log(isNaN(parseInt(rows[x].product_id)));
		}
		
		shop_ids.push(parseInt(rows[x].owner_shop_id));
		if(typeof(dataJSON.request.filter)!='undefined')
		{
			if(typeof(dataJSON.request.filter.color)!='undefined')
			{
				if(row[x].product_color.toLowerCase()!=dataJSON.request.filter.color.toLowerCase().trim())
				{
					variant_product_ids.push(rows[x].product_id);
					if(variant_color_names.length==0)
					{
						variant_color_names.push(dataJSON.request.filter.color.toLowerCase().trim());
					}
				}
			}
		}
	}
	// //console.log("**************************** prod_ids ***********************");
	// //console.log(prod_ids);
	dataStruct['returnMax']=4;
	dataStruct['currentCounter']=0;
	// if(variant_color_names.length>0)
	// {
	// 	dataStruct['returnMax']=5;
	// }
	//using prod_ids
	setImmediate(findImage.findPrimaryImage,prod_ids,dataJSON,dataStruct,formResponse,req,res);
	setImmediate(findAddFromWebInfo.findAddFromWebInfo,prod_ids,dataJSON,dataStruct,formResponse,req,res);
	setImmediate(findSEO.start,prod_ids,dataJSON,dataStruct,formResponse,req,res);
	//using shop_ids
	setImmediate(findShopInformation.start,shop_ids,dataJSON,dataStruct,formResponse,req,res);
	//using variant_color_names
	// if(variant_color_names.length>0)
	// {
	// 	setImmediate(findImage.findVariantImage,prod_ids,variant_color_names,dataJSON,dataStruct,formResponse,req,res);
	// }
}

function formResponse(additional_rows,dataJSON,dataStruct,req,res)
{
	//var rows = dataJSON.rows;
	var error =false;
	////console.log('Processing '+dataStruct['currentCounter']+":"+dataStruct['returnMax']);
	// if(additional_rows.length>0 && util.isNullOrUndefined(dataJSON.rows)==false && dataJSON.rows.length>0)
	// {
		//identify if AddFromWeb
		try{
			if(util.isNullOrUndefined(additional_rows[0].url_source)==false)
			{
				//dataJSON.rows[x]['additional_info']=[];
				//process AddFromWeb
				for(var x=0;x<dataJSON.rows.length;x++)
				{
					for(var y=0;y<additional_rows.length;y++)
					{
						if(dataJSON.rows[x].product_id==additional_rows[y].product_id)
						{
							dataJSON.rows[x]['additional_info']=additional_rows[y];
							break;
						}
					}
				}
			}
		}catch(err){
			//console.log(err.message);
			//console.log(err.stack);
			//console.log('Processing '+dataStruct['currentCounter']+":"+dataStruct['returnMax']);
		}
		
		//identify if SEO
		try{
			if(util.isNullOrUndefined(additional_rows[0].seo_title)==false)
			{
				//dataJSON.rows[x]['SEO']=[];
				//process SEO
				for(var x=0;x<dataJSON.rows.length;x++)
				{
					for(var y=0;y<additional_rows.length;y++)
					{
						if(dataJSON.rows[x].product_id==additional_rows[y].product_id)
						{
							dataJSON.rows[x]['SEO']=additional_rows[y];
							break;
						}
					}
				}
			}
		}catch(err){
			//console.log(err.message);
			//console.log(err.stack);
			//console.log('Processing '+dataStruct['currentCounter']+":"+dataStruct['returnMax']);
		}
		
		//identify if shopInformation
		try{
			if(util.isNullOrUndefined(additional_rows[0].store_id)==false)
			{
				//dataJSON.rows[x]['shop_information']=[];
				//process SEO
				for(var x=0;x<dataJSON.rows.length;x++)
				{
					for(var y=0;y<additional_rows.length;y++)
					{
						if(dataJSON.rows[x].store_id==additional_rows[y].store_id)
						{
							dataJSON.rows[x]['shop_information']=additional_rows[y];
							break;
						}
					}
				}
			}
		}catch(err){
			//console.log(err.message);
			//console.log(err.stack);
			//console.log('Processing '+dataStruct['currentCounter']+":"+dataStruct['returnMax']);
		}
		
		//identify if Primary Images
		try{
			if(util.isNullOrUndefined(additional_rows[0].product_image_id)==false)
			{
				//process Primary Images
				////console.log("found images");
				for(var x=0;x<dataJSON.rows.length;x++)
				{
					dataJSON.rows[x]['product_images']=[];
					for(var y=0;y<additional_rows.length;y++)
					{
						if(dataJSON.rows[x].product_id==additional_rows[y].product_id)
						{
							////console.log("pushing primary images");
							try{
								dataJSON.rows[x]['product_images'].push(additional_rows[y]);
							}catch(err){
								
								if(util.isNullOrUndefined(dataJSON.rows[x])==true)
								{
									//console.log("dataJSON.rows[x] is undefined");
									//console.log(x);
								}
								// //console.log(err.message);
								// //console.log(err.stack);
								break;
							}
							
						}
					}
				}
			}
		}catch(err){
			//console.log(err.message);
			//console.log(err.stack);
			//console.log('Processing '+dataStruct['currentCounter']+":"+dataStruct['returnMax']);
		}

	dataStruct['currentCounter']=dataStruct['currentCounter']+1;

	if(dataStruct['currentCounter']==dataStruct['returnMax'])
	{
		//populate defaults
		for(var x=0;x<dataJSON.rows.length;x++)
		{
			for(var y=0;y<GLOBAL.default_length_units.length;y++)
			{
				if(dataJSON.rows[x].length_unit_id==GLOBAL.default_length_units[y].length_unit_id)
				{
					dataJSON.rows[x]['lwh_unit']=GLOBAL.default_length_units[y].length_unit_shortcut;
					break;
				}
			}
			for(var y=0;y<GLOBAL.default_monetary_units.length;y++)
			{
				if(dataJSON.rows[x].monetary_unit_id==GLOBAL.default_monetary_units[y].money_id)
				{
					dataJSON.rows[x]['monetary_unit']=GLOBAL.default_monetary_units[y].money_sign;
					break;
				}
			}
			for(var y=0;y<GLOBAL.default_weight_units.length;y++)
			{
				if(dataJSON.rows[x].weight_unit_id==GLOBAL.default_weight_units[y].weight_units_id)
				{
					dataJSON.rows[x]['weight_unit']=GLOBAL.default_weight_units[y].weight_units_shortcut;
					break;
				}
			}
		}
		dataJSON['resVal'].data=dataJSON.rows;
		if(util.isNullOrUndefined(dataStruct['from_ProductInfo'])==false)
		{
			
			try{
				var newDataReturn = {
					"success":true, "status":"Success",
					"data":[]
				};
				for(var x = 0;x<dataJSON.request.product_id.length;x++)
				{
					for(var y = 0;y<dataJSON['resVal']['data'].length;y++)
					{
						if(parseInt(dataJSON.request.product_id[x])==dataJSON['resVal']['data'][y]['product_id'])
						{
							newDataReturn.data.push(dataJSON['resVal']['data'][y]);
						}
					}
				}
				respond.respondError(newDataReturn,res,req);
			}catch(err_populateNewData){
				respond.respondError(dataJSON['resVal'],res,req);
			}
			
		}else{
			respond.respondError(dataJSON['resVal'],res,req);
		}
		delete dataJSON;
		delete dataStruct;

	}
}



module.exports={
	generateQuery:generateQuery,
	executeQuery:executeQuery,
	breakdownProdIds:breakdownProdIds
};