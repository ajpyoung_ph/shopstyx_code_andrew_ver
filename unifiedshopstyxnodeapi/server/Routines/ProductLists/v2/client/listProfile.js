const listHome = require(__dirname+"/listHome");
const listProfileProducts = require('../../../QueryGenerator/listProfileProducts');

var generateQuery = function(dataJSON,dataStruct,req,res)
{
	
	//listProfileProducts.start(dataJSON,dataStruct,listHome.executeQuery);
	setImmediate(listProfileProducts.start,dataJSON,dataStruct,listHome.executeQuery,req,res);
};

module.exports={
	generateQuery:generateQuery
};