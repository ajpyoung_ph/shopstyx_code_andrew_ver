const respond = require(GLOBAL.server_location+'/EventListeners/errorListener');

const parseDataJSON = require(GLOBAL.server_location+'/helpers/parseDataJSON');
const util = require('util');

var processRequest = function(dataStruct,req,res)
{
	var dataJSON = parseDataJSON.parseJSON(dataStruct,req,res);
	if(util.isNullOrUndefined(dataJSON)==false)
	{
		try{
			/*
			"request":{
		        "product_id":<int>,
		        "product_owner_user_id":<int>,
		        "store_id":<int>
		    }
			*/
			var where_clause = '';
			if(typeof(dataJSON.request.product_id)!='undefined' && isNaN(dataJSON.request.product_id) == false)
			{
				if(where_clause.length!=0);
				{
					where_clause = where_clause + ' AND ';
				}
				where_clause = where_clause + 'product_id='+parseInt(dataJSON.request.product_id);
			}else{
				throw "product_id is not valid";
			}

			if(typeof(dataJSON.request.product_owner_user_id)!='undefined' && isNaN(dataJSON.request.product_owner_user_id) == false)
			{
				if(where_clause.length!=0);
				{
					where_clause = where_clause + ' AND ';
				}
				where_clause = where_clause + 'user_id='+parseInt(dataJSON.request.product_owner_user_id);
			}else{
				throw "product_owner_user_id is not valid";
			}

			if(typeof(dataJSON.request.store_id)!='undefined' && isNaN(dataJSON.request.store_id) == false)
			{
				if(where_clause.length!=0);
				{
					where_clause = where_clause + ' AND ';
				}
				where_clause = where_clause + 'store_id='+parseInt(dataJSON.request.store_id);
			}else{
				throw "store_id is not valid";
			}

			
		}catch(err){
			var msg = {
				"success":false, "status":"Error",
				"desc":"[ProductListingAdmin:processRequest]No data to process",
				"err":err
			};
			respond.respondError(msg,res,req);

						
						
		}

	}else{
		var msg = {
			"success":false, "status":"Error",
			"desc":"[ProductListingAdmin:processRequest]No data to process",
			"code":0
		};
		respond.respondError(msg,res,req);
		
						
						
	}
};

module.exports={
	processRequest:processRequest
};