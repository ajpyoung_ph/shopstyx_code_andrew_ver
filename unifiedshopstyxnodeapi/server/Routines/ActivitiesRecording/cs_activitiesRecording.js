// var recordSearch = function(dataJSON,dataStruct)
// {
// 	try{
// 		var findQuery = '';
// 		//find if user is logged in or not
// 		if(typeof(dataJSON.security.user_id)!='undefined')
// 		{
// 			//meaning user is logged in
// 			findQuery='SELECT * FROM `activities` WHERE `user_id`='+parseInt(dataJSON.security.user_id)+' AND `activity_type`="RS" LIMIT 1;'
// 		}else{
// 			//meaning user is not logged in and use uuid
// 			findQuery='SELECT * FROM `activities` WHERE `fingerprint_id`="'+dataJSON.security.uuid+'"" AND `activity_type`="RS" LIMIT 1;'
// 		}
// 		//find if data for user is available
// 		GLOBAL.db_ss_transaction_dumps.query(findQuery,function(err,rows, fields){
// 			if(err==null)
// 			{
// 				var recordQuery='';
// 				var search_history=[];
// 				if(rows.length>0)
// 				{
// 					//Update record
					
// 					var obj = JSON.parse(rows[0].json_object);
// 					if(typeof(obj)==='object')
// 					{
// 						search_history=obj.keyword.slice();//clone array
// 					}else{
// 						search_history.push(obj);//push value
// 					}
// 					if(search_history.length>4)
// 					{
// 						search_history.shift();
// 					}
// 					search_history.push(dataJSON.request.additional_info.search_key);
// 					var keyword = {
// 						"keyword":search_history
// 					};
// 					/*
// 					UPDATE [LOW_PRIORITY] [IGNORE] table_reference
// 				    SET col_name1={expr1|DEFAULT} [, col_name2={expr2|DEFAULT}] ...
// 				    [WHERE where_condition]
// 					*/
// 					if(typeof(dataJSON.security.user_id)!='undefined')
// 					{
// 						//meaning user is logged in
// 						recordQuery='UPDATE activities SET `json_object`='+JSON.stringify(keyword)+' WHERE `user_id`='+parseInt(dataJSON.security.user_id)+' AND `activity_type`="RS";'
// 					}else{
// 						//meaning user is not logged in and use uuid
// 						recordQuery='UPDATE activities SET `json_object`='+JSON.stringify(keyword)+' WHERE `fingerprint_id`="'+dataJSON.security.uuid+'" AND `activity_type`="RS" LIMIT 1;'
// 					}
// 				}else{
// 					//Insert record
// 					var search_history = [];
// 					search_history.push(dataJSON.request.additional_info.search_key);
// 					var keyword = {
// 						"keyword":search_history
// 					};
// 					/*
// 					INSERT [LOW_PRIORITY | DELAYED | HIGH_PRIORITY] [IGNORE]
// 				    [INTO] tbl_name
// 				    [PARTITION (partition_name,...)]
// 				    SET col_name={expr | DEFAULT}, ...
// 				    [ ON DUPLICATE KEY UPDATE
// 				      col_name=expr
// 				        [, col_name=expr] ... ]
// 					*/
// 					if(typeof(dataJSON.security.user_id)!='undefined')
// 					{
// 						//meaning user is logged in
// 						recordQuery='INSERT activities SET `json_object`='+JSON.stringify(keyword)+', `user_id`='+parseInt(dataJSON.security.user_id)+', `activity_type`="RS", `verb`="recent-search", `activity_id`=1, fingerprint_id='';';
// 					}else{
// 						//meaning user is not logged in and use uuid
// 						recordQuery='INSERT activities SET `json_object`='+JSON.stringify(keyword)+', `fingerprint_id`="'+dataJSON.security.uuid+'", `activity_type`="RS", `verb`="recent-search", `activity_id`=1, `user_id`=0;';
// 					}
// 				}
// 				GLOBAL.db_ss_transaction_dumps.query(recordQuery,function(err2,rows2,fields2){
// 					if(err2!=null)
// 					{
// 						var msg = {
// 							"success":false, "status":"Error",
// 							"desc":"[cs_activitiesRecording:recordSearch]Error Processing Data",
// 							"code":2,
// 							"err":err2
// 						};
						
// 						process.emit('Shopstyx:logError',msg);
// 					}
// 				});
// 			}else{
// 				var msg = {
// 					"success":false, "status":"Error",
// 					"desc":"[cs_activitiesRecording:recordSearch]Error Processing Data",
// 					"code":1,
// 					"err":err
// 				};
				
// 				process.emit('Shopstyx:logError',msg);
// 			}
// 		});
// 	}catch(err3){
// 		var msg = {
// 			"success":false, "status":"Error",
// 			"desc":"[cs_activitiesRecording:recordSearch]Error Processing Data",
// 			"err":err3
// 		};
		
// 		process.emit('Shopstyx:logError',msg);
// 	}
// };



// var recordInterest = function(dataJSON,dataStruct)
// {
// 	try{
// 		var findQuery = '';
// 		//find if user is logged in or not
// 		if(typeof(dataJSON.security.user_id)!='undefined')
// 		{
// 			//meaning user is logged in
// 			findQuery='SELECT * FROM `activities` WHERE `user_id`='+parseInt(dataJSON.security.user_id)+' AND `activity_type`="P" ORDER BY timestamp ASC;'
// 		}else{
// 			//meaning user is not logged in and use uuid
// 			findQuery='SELECT * FROM `activities` WHERE `fingerprint_id`="'+dataJSON.security.uuid+'" AND `activity_type`="P" ORDER BY timestamp ASC;'
// 		}
// 		//find if data for user is available
// 		GLOBAL.db_ss_transaction_dumps.query(findQuery,function(err1,rows, fields){
// 			if(err1==null)
// 			{
// 				if(rows.length>0)
// 				{
// 					//process data
// 					checkInterests(rows,dataJSON,dataStruct);
// 				}else{
// 					//directly insert data
// 					/*
// 					INSERT [LOW_PRIORITY | DELAYED | HIGH_PRIORITY] [IGNORE]
// 				    [INTO] tbl_name
// 				    [PARTITION (partition_name,...)]
// 				    SET col_name={expr | DEFAULT}, ...
// 				    [ ON DUPLICATE KEY UPDATE
// 				      col_name=expr
// 				        [, col_name=expr] ... ]
// 					*/
// 					var keyword = {
// 						"keyword":dataJSON['current_clicked_product_name'],
// 						"department_id":dataJSON['current_clicked_product_department_id']
// 					};
// 					if(typeof(dataJSON.security.user_id)!='undefined')
// 					{
// 						//meaning user is logged in
// 						recordQuery='INSERT activities SET `json_object`='+JSON.stringify(keyword)+', `user_id`='+parseInt(dataJSON.security.user_id)+', `activity_type`="P", `verb`="view-product", `activity_id`=1, fingerprint_id='';';
// 					}else{
// 						//meaning user is not logged in and use uuid
// 						recordQuery='INSERT activities SET `json_object`='+JSON.stringify(keyword)+', `fingerprint_id`="'+dataJSON.security.uuid+'", `activity_type`="P", `verb`="view-product", `activity_id`=1, `user_id`=0;';
// 					}
// 					GLOBAL.db_ss_transaction_dumps.query(recordQuery,function(err2,rows2,fields2){
// 						if(err2!=null)
// 						{
// 							var msg = {
// 								"success":false, "status":"Error",
// 								"desc":"[cs_activitiesRecording:recordInterest]Error Processing Data",
// 								"code":2,
// 								"err":err2
// 							};
							
// 							process.emit('Shopstyx:logError',msg);
// 						}
// 					});
// 				}
// 			}else{
// 				var msg = {
// 					"success":false, "status":"Error",
// 					"desc":"[cs_activitiesRecording:recordInterest]Error Processing Data",
// 					"err":err1
// 				};
				
// 				process.emit('Shopstyx:logError',msg);
// 			}
// 		});
// 	}catch(err){
// 		var msg = {
// 			"success":false, "status":"Error",
// 			"desc":"[cs_activitiesRecording:recordInterest]Error Processing Data",
// 			"err":err
// 		};
		
// 		process.emit('Shopstyx:logError',msg);
// 	}
// };

// function checkInterests(rows,dataJSON,dataStruct)
// {
// 	var keyword = {
// 		"keyword":dataJSON['current_clicked_product_name'],
// 		"department_id":dataJSON['current_clicked_product_department_id']
// 	};
// 	//check if the number of rows is greater than 4
// 	if(rows.length>4)
// 	{
// 		/*
// 		UPDATE [LOW_PRIORITY] [IGNORE] table_reference
// 	    SET col_name1={expr1|DEFAULT} [, col_name2={expr2|DEFAULT}] ...
// 	    [WHERE where_condition]
// 		*/
// 		//replace oldest record and update json_object
// 		if(typeof(dataJSON.security.user_id)!='undefined')
// 		{
// 			//meaning user is logged in
// 			recordQuery='UPDATE activities SET `json_object`='+JSON.stringify(keyword)+' WHERE `user_id`='+parseInt(dataJSON.security.user_id)+' AND `activity_type`="P" AND `verb`="view-product" AND `activity_id`='+parseInt(rows[0].activity_id)+' AND `fingerprint_id`='';';
// 		}else{
// 			//meaning user is not logged in and use uuid
// 			recordQuery='UPDATE activities SET `json_object`='+JSON.stringify(keyword)+' WHERE `fingerprint_id`="'+dataJSON.security.uuid+'" AND `activity_type`="P" AND `verb`="view-product" AND `activity_id`='+parseInt(rows[0].activity_id)+' AND `user_id`=0;';
// 		}
		
// 	}else{
// 		//if not then insert new data
// 		var new_activity_id = parseInt(rows[rows.length-1].activity_id + 1);
// 		/*
// 		INSERT [LOW_PRIORITY | DELAYED | HIGH_PRIORITY] [IGNORE]
// 	    [INTO] tbl_name
// 	    [PARTITION (partition_name,...)]
// 	    SET col_name={expr | DEFAULT}, ...
// 	    [ ON DUPLICATE KEY UPDATE
// 	      col_name=expr
// 	        [, col_name=expr] ... ]
// 		*/
// 		if(typeof(dataJSON.security.user_id)!='undefined')
// 		{
// 			//meaning user is logged in
// 			recordQuery='INSERT activities SET `json_object`='+JSON.stringify(keyword)+', `user_id`='+parseInt(dataJSON.security.user_id)+', `activity_type`="P", `verb`="view-product", `activity_id`='+parseInt(new_activity_id)+', fingerprint_id='';';
// 		}else{
// 			//meaning user is not logged in and use uuid
// 			recordQuery='INSERT activities SET `json_object`='+JSON.stringify(keyword)+', `fingerprint_id`="'+dataJSON.security.uuid+'", `activity_type`="P", `verb`="view-product", `activity_id`='+parseInt(new_activity_id)+', `user_id`=0;';
// 		}
// 	}
	
// 	GLOBAL.db_ss_transaction_dumps.query(recordQuery,function(err2,rows2,fields2){
// 		if(err2!=null)
// 		{
// 			var msg = {
// 				"success":false, "status":"Error",
// 				"desc":"[cs_activitiesRecording:checkInterests]Error Processing Data",
// 				"code":2,
// 				"err":err2
// 			};
			
// 			process.emit('Shopstyx:logError',msg);
// 		}
// 	});
// }
// module.exports = {
// 	recordSearch:recordSearch,
// 	recordInterest:recordInterest
// };