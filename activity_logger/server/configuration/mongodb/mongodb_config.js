const mongojs = require('mongojs');

var start = function(dataStruct)
{
	/*
	GLOBAL.mongodb = mongojs('transactionUser:s#0pst!cks@'+GLOBAL.locationString+'/shopstyx_transactions', ['transactionRecords','disputeRecords']);
	*/
	switch(process.env.NODE_ENV.toLowerCase())
	{
		case 'local':
		case 'development':
		case 'testing':
		case 'staging':
    //console.log('ugcUser:s#0pst!cks@'+dataStruct.mongo_db_location+'/ugc');
			GLOBAL.mongodb = mongojs('activityUser:s#0pst!cks@'+dataStruct.mongo_db_location+'/activityLogs', ['requestLogs,loginLogs,failedLogs','visitLogs','viewLogs'], {authMechanism: 'ScramSHA1'});
			break;

		case 'production':
		case 'final':
    //console.log('ugcUser:s#0pst!cks@'+dataStruct.mongo_db_location+'/ugc?replicaSet=styx01');
			//GLOBAL.mongodb = mongojs('likeUser:s#0pst!cks@'+dataStruct.mongo_db_location+'/likefollowmoods?replicaSet=styx01', ['likeRecords','followRecords','moodRecords'], {authMechanism: 'ScramSHA1'});
      GLOBAL.mongodb = mongojs('activityUser:s#0pst!cks@'+dataStruct.mongo_db_location+'/activityLogs', ['requestLogs,loginLogs,failedLogs','visitLogs','viewLogs'], {authMechanism: 'ScramSHA1'});
			break;
	}
};

module.exports={
	start:start
};

/*
use shopstyx_transactions
db.createUser(
    {
      user: "transactionUser",
      pwd: "s#0pst!cks",
      roles: [
         { role: "readWrite", db: "shopstyx_transactions" }
      ]
    }
)

use shopstyx_notification
db.createUser(
    {
      user: "notificationUser",
      pwd: "s#0pst!cks",
      roles: [
         { role: "readWrite", db: "shopstyx_notification" }
      ]
    }
)

use shopstyx_bulkEmail
db.createUser(
    {
      user: "bulkEmailUser",
      pwd: "s#0pst!cks",
      roles: [
         { role: "readWrite", db: "shopstyx_bulkEmail" }
      ]
    }
)

use ugc
db.createUser(
    {
      user: "ugcUser",
      pwd: "s#0pst!cks",
      roles: [
         { role: "readWrite", db: "ugc" }
      ]
    }
)

use shopstyx_disputeRobot
db.createUser(
    {
      user: "disputeRobotUser",
      pwd: "s#0pst!cks",
      roles: [
         { role: "readWrite", db: "shopstyx_disputeRobot" }
      ]
    }
)

use admin
db.createUser(
    {
      user: "pinakuratUser",
      pwd: "s#0pst!ckspagkakurata",
      roles: [
         { role: "userAdminAnyDatabase", db: "admin" }
      ]
    }
)

use admin
db.createUser( {
user: "ugatUser",
pwd: "s#0pst!cksp!#akaugat",
roles: [ { role: "root", db: "admin" } ]
});
*/