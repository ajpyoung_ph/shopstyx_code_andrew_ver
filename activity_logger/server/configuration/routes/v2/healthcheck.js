var configRoutes = function(dataStruct)
{	
	var router = dataStruct.router;

	router.route('/v2/healthcheck')
		.get(function(req,res){
			res.sendStatus(200);
		});

	return dataStruct;
}


module.exports = {
	configRoutes:configRoutes
};