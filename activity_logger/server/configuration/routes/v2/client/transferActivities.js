const transferActivitiesRoutine = require('../../../../Routine/TransferActivities/transferActivitiesRoutine');
const securityListener = require(GLOBAL.server_location+'/EventListeners/securityListener');

const multer  = require('multer');
console.log("temp_dir_location: "+GLOBAL.temp_dir_location);
const upload = multer({ dest: GLOBAL.temp_dir_location });
const mkdirp = require('mkdirp');
const cpUpload = upload.fields([{ name: 'prevpic', maxCount: 10 }, { name: 'src', maxCount: 10 }]);
const respond = require(GLOBAL.server_location+'/EventListeners/errorListener');
const util = require('util');


/*
"security": {
         "type": "web/app",
         "uuid": <string>,
         "user_id":<int>, Must Not Be ZERO
         "token": "robot token"
    }
*/
var configRoutes = function(dataStruct)
{   
    var router = dataStruct.router;
    
    router.route('/v2/server/request/activity/transfer/')
        .post(cpUpload,function(req,res){
            mkdirp(GLOBAL.temp_dir_location, function (err) {
                if (err){
                    console.log("Error creating folder");
                    var msg = {
                        'status':'Error',
                        'desc':'Error creating folder'
                    };
                    respond.respondError(msg,res,req);  
                }else{
                    try{
                        console.log('Starting: '+req.protocol + '://' + req.get('host') + req.originalUrl + " -------- "+Date());
                        securityListener.verifyRobot(dataStruct.routeData,transferActivitiesRoutine.processRequest,req,res);
                    }catch(err){
                        var msg={
                            "success":false, "status":"Error",
                            "err":"Error Code Root 1",
                            "url":"",
                            "error_report":err,
                            "stack":err.stack
                        }
                        respond.respondError(err,res,req);
                    }
                }
            });
        });
        
    return dataStruct;
}


module.exports = {
    configRoutes:configRoutes
};