
const respond = require(GLOBAL.server_location+'/EventListeners/errorListener');

var configRoutes = function(dataStruct)
{	
	var router = dataStruct.router;

	router.route('/v2/testEcho/:echo')
		.get(function(req,res){
			// dataStruct.routeData.req = req;
			// res = res;
			msg = {
				"status":"TestRoute",
				"echo":req.params.echo
			};
			respond.respondError(msg,res,req);
		});

	return dataStruct;
}


module.exports = {
	configRoutes:configRoutes
};