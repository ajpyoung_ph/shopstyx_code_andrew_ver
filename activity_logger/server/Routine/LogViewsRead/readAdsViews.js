const util = require('util');

const respond = require(GLOBAL.server_location+'/EventListeners/errorListener');
/*
"request":{
        "type":["Product"/"Advertisement"/"Summary"/"List"],(Summary would just show the total views and the average view duration of the page; List would show the who viewed and for how long and when they viewed)
        (Combine Summary and List with the other keywords; You cannot mix both Summary and List keywords; You cannot use Summary and List by themselves only)
        "owner_id":Int,
        "product_id":<int>, (if type Product)
        "styx_id":<string>,(if type Advertisement)
        (if List is mixed with the other allowable keywords, you must use page_number and number_items)
        "page_number":<int>,(starts at page 0)
        "number_items":<int>,(the number of items to return)
    }

    {
        "type":"Product"/"Advertisement"/"Profile",
        "owner_id":Int,
        "view_duration":Int,(in seconds)
        "page_url":<string>,
        "product_id":<int>, (if type Product)
        "styx_id":<string>,(if type Advertisement)
        "user_id":<string>,(if type Profile)
    }


*/

const start = function(dataJSON,dataStruct,req,res)
{
	if(dataJSON.request.type.join("`").toLowerCase().replace(/\s/g, '').split("`").indexOf("list")>-1)
	{
		giveList(dataJSON,dataStruct,req,res);
	}else if(dataJSON.request.type.join("`").toLowerCase().replace(/\s/g, '').split("`").indexOf("summary")>-1)
	{
		giveSummary(dataJSON,dataStruct,req,res);
	}else{
		var msg = {
			"success":false, "status":"Error",
			"desc":"[readAdsViews:start]No data to process"
		};
		respond.respondError(msg,res,req);
	}
};

function giveList(dataJSON,dataStruct,req,res)
{
	var skip = parseInt(dataJSON.request.number_items) * parseInt(dataJSON.request.page_number);
	var limit =  parseInt(dataJSON.request.number_items);
	var query = {
		"type":"Advertisement",
        "styx_id":dataJSON.request.styx_id
	};
	GLOBAL.mongodb.viewLogs.find(query).sort({"date_unix":-1}).limit(limit).skip(skip,function(err,docs){
		if(err==null)
		{
			var msg = {
				"success":true, "status":"Success",
				"desc":"[readAdsViews:giveList]Data Found",
				"data":docs
			};

			respond.respondError(msg,res,req);
		}else{
			var msg = {
				"success":false, "status":"Error",
				"desc":"[readAdsViews:giveList]Error in DB",
				"err":err,
				"query":query
			};

			respond.respondError(msg,res,req);
		}
	});
}
function giveSummary(dataJSON,dataStruct,req,res)
{
	var query = 'SELECT * FROM `activity_stykables_views` WHERE `stykable_id`="'+dataJSON.request.styx_id+'";';
	var mysql = new GLOBAL.mysql.mysql_connect(query);
	mysql.results_ss_activities().then(function(results){
		if(util.isError(results)==true){
			var msg = {
				"success":false,'status':'Error',
				'desc':'[readAdsViews:giveSummary]Error saving in DB',
				'query':query,
				'message':results.message,
				'stack':results.stack||null
			};
			respond.respondError(msg,res,req);
		}else{
			var msg = {
				"success":true, "status":"Success",
				"desc":"[readAdsViews:giveSummary]Data Found",
				"data":results
			};
			respond.respondError(msg,res,req);
		}
	});
}
module.exports={
	start:start
};