
const respond = require(GLOBAL.server_location+'/EventListeners/errorListener');
const readProductViews = require(__dirname+'/readProductViews');
const readAdsViews = require(__dirname+'/readAdsViews');
const parseDataJSON = require(GLOBAL.server_location+'/helpers/parseDataJSON');
const util = require('util');
/*
"request":{
        "type":["Product"/"Advertisement"/"Summary"/"List"],(Summary would just show the total views and the average view duration of the page; List would show the who viewed and for how long and when they viewed)
        (Combine Summary and List with the other keywords; You cannot mix both Summary and List keywords; You cannot use Summary and List by themselves only)
        "owner_id":Int,
        "product_id":<int>, (if type Product)
        "styx_id":<string>,(if type Advertisement)
        (if List is mixed with the other allowable keywords, you must use page_number and number_items)
        "page_number":<int>,(starts at page 0)
        "number_items":<int>,(the number of items to return)
    }


*/
const processRequest = function(dataJSON,dataStruct,req,res)
{
	var error=false;
	var error_mgs=[];
	var list_flag=false;
	if(typeof(dataJSON.request)=='undefined')
	{
		error=true;
		error_mgs.push('No Request Found');
	}else{
		if(typeof(dataJSON.request.type)=='undefined')
		{
			error=true;
			error_mgs.push('Request Type not found');
		}else{
			
			if(typeof(dataJSON.request.owner_id)=='undefined')
			{
				error=true;
				error_mgs.push('Owner User Information is not found');
			}
			if(dataJSON.request.type.join("`").toLowerCase().replace(/\s/g, '').split("`").indexOf("product")>-1)
			{
				if(dataJSON.request.type.join("`").toLowerCase().replace(/\s/g, '').split("`").indexOf("list")>-1)
				{
					list_flag=true;
				}
				// if(dataJSON.request.type.join("`").toLowerCase().replace(/\s/g, '').split("`").indexOf("summary")>-1)
				// {
					
				// }
			}
			if(dataJSON.request.type.join("`").toLowerCase().replace(/\s/g, '').split("`").indexOf("advertisement")>-1)
			{
				if(dataJSON.request.type.join("`").toLowerCase().replace(/\s/g, '').split("`").indexOf("list")>-1)
				{
					list_flag=true;
				}
			}
			if(list_flag=true)
			{
				if(typeof(dataJSON.request.page_number)=='undefined')
				{
					error=true;
					error_mgs.push('Page Number Required');
				}
				if(typeof(dataJSON.request.number_items)=='undefined')
				{
					error=true;
					error_mgs.push('Item quantity request Required');
				}
			}
		}
	}
	
	if(error==false)
	{
		/*
		"request":{
		        "type":"Product"/"Advertisement"/"Profile",
		        "owner_id":Int,
		        "view_duration":Int,(in seconds)
		        "page_url":<string>,
		        "product_id":<int>, (if type Product)
		        "styx_id":<string>,(if type Advertisement)
		        "user_id":<string>,(if type Profile)
		    }
		*/
		if(dataJSON.request.type.join("`").toLowerCase().replace(/\s/g, '').split("`").indexOf("product")>-1)
		{
			readProductViews.start(dataJSON,dataStruct,req,res);
		}else if(dataJSON.request.type.join("`").toLowerCase().replace(/\s/g, '').split("`").indexOf("advertisement")>-1)
		{
			readAdsViews.start(dataJSON,dataStruct,req,res);
		}else{
			var msg = {
				"success":false, "status":"Error",
				"desc":"[readViewRoutine:processRequest]Unknown Request Type"
			};
			respond.respondError(msg,res,req);
		}
	}else{
		var msg = {
			"success":false, "status":"Error",
			"desc":"[readViewRoutine:processRequest]Missing Requirements",
			"err":error_mgs
		};
		respond.respondError(msg,res,req);
	}
};

module.exports={
	processRequest:processRequest
};
