const mysqlConv = require(GLOBAL.server_location+'/helpers/mysql_convertions');
/*
	Ads list
	"additional_info":{
                     "user_id":<int>(product listing user_id owner(the one who created the advert), required by type:List and filter:OwnedByUserID),
                     "store_id":<int>(for OwnedByShopID),
                     "product_id":<int>(for SearchProd),
                     "search_key":<string> (for Search only)
                }
    Product list
    "additional_info":{
            "user_id":<int>(product listing user_id owner, required by type:ListProfile and filter:OwnedByUserID),
            "store_id":<int>(for ListProfile ListShop ListShopCategories),
            "collection_id":<int>(for ListProfile ListCollection),
            "category_id":<int>(for ListShop ListShopCategories), (same as collection_id)
            "search_key":<string> (for Search only)
        }
*/
const util = require('util');

const save = function(hash_id,dataJSON)
{
	if(util.isNullOrUndefined(dataJSON.request.additional_info)==false)
	{
		if(util.isNullOrUndefined(dataJSON.request.additional_info.search_key)==false)
		{
			logSearch(hash_id,dataJSON);
			for(var x=0;x<GLOBAL.default_colors.length;x++)
			{
				var patternTest = new RegExp(GLOBAL.default_colors[x]['color_name'],'gi');
				if(patternTest.test(dataJSON.request.additional_info.search_key)==true)
				{
					logColor(GLOBAL.default_colors[x],hash_id,dataJSON);
				}
			}
		}
	}
}

const logSearch = function(hash_id,dataJSON)
{
	var error = false;
	var error_msg=[];
	if(util.isNullOrUndefined(dataJSON.security.user_id)==false)
	{
		//find if search_phase exists
		var query = 'SELECT * FROM `activity_search_history` WHERE `search_phase` LIKE "'+mysqlConv.mysql_real_escape_string(dataJSON.request.additional_info.search_key)+'" AND `user_id`='+parseInt(dataJSON.security.user_id)+';';
		var mysql = new GLOBAL.mysql.mysql_connect(query);
		mysql.results_ss_activities().then(function(rows){
			if(util.isError(rows)==true){
				var msg = {
					'status':'Error',
					'desc':'[activity_search_history:logSearch]DB Error Connection',
					'query':query,
					'message':results.message,
					'stack':results.stack||null
				};
				process.emit('Shopstyx:logError',msg);
			}else{
				if(rows.length>0)
				{
					updateSearch(hash_id,dataJSON);//hash_id,rec_id,dataJSON,dataStruct
				}else{
					insertSearch(hash_id,dataJSON);
				}
			}
		});
	}	
};

const insertSearch = function(hash_id,dataJSON)
{
	var query = 'INSERT INTO `activity_search_history` SET `user_id`='+parseInt(dataJSON.security.user_id)+',`hash_tracking_key`="'+hash_id+'", `search_phrase`="'+mysqlConv.mysql_real_escape_string(dataJSON.request.additional_info.search_key)+'", `search_frequency`=1, `date_updated`="'+dataJSON.new_simple_timestamp+'";';
	var mysql = new GLOBAL.mysql.mysql_connect(query);
	mysql.results_ss_activities().then(function(results){
		if(util.isError(results)==true){
			var msg = {
				'status':'Error',
				'desc':'[activity_search_history:insertSearch]DB Error Connection',
				'query':query,
				'message':results.message,
				'stack':results.stack||null
			};
			process.emit('Shopstyx:logError',msg);
		}else{
			var msg = {
				'status':'Success',
				'desc':'[activity_search_history:insertSearch]Save Search History'
			};
			process.emit('Shopstyx:logError',msg);
		}
	});
};

const updateSearch = function(hash_id,rec_id,dataJSON)
{
	var query = 'UPDATE `activity_search_history` SET `search_frequency`=`search_frequency`+1, `hash_tracking_key`="'+hash_id+'" WHERE `search_phase` LIKE "'+mysqlConv.mysql_real_escape_string(dataJSON.request.additional_info.search_key)+'" AND `user_id`='+parseInt(dataJSON.security.user_id)+';';
	var mysql = new GLOBAL.mysql.mysql_connect(query);
	mysql.results_ss_activities().then(function(results){
		if(util.isError(results)==true){
			var msg = {
				'status':'Error',
				'desc':'[activity_search_history:updateSearch]DB Error Connection',
				'query':query,
				'message':results.message,
				'stack':results.stack||null
			};
			process.emit('Shopstyx:logError',msg);
		}else{
			var msg = {
				'status':'Success',
				'desc':'[activity_search_history:updateSearch]Updated Search History'
			};
			process.emit('Shopstyx:logError',msg);
		}
	});
};



//logColor,GLOBAL.default_colors[x],hash_id,dataJSON,dataStruct)
const logColor = function(color_index,hash_id,dataJSON)
{
	if(util.isNullOrUndefined(dataJSON.security.user_id)==false)
	{
		//find if color exists
		var query = 'SELECT * FROM `activity_color_search` WHERE `color_id`='+parseInt(color_index.color_id)+' AND `user_id`='+parseInt(dataJSON.security.user_id)+';';
		var mysql = new GLOBAL.mysql.mysql_connect(query);
		mysql.results_ss_activities().then(function(results){
			if(util.isError(results)==true){
				var msg = {
					'status':'Error',
					'desc':'[activity_search_history:logColor]DB Error Connection',
					'query':query,
					'message':results.message,
					'stack':results.stack||null
				};
				process.emit('Shopstyx:logError',msg);
			}else{
				if(results.length>0)
				{
					updateColor(color_index,hash_id,dataJSON);//hash_id,rec_id,dataJSON,dataStruct
				}else{
					insertColor(color_index,hash_id,dataJSON);
				}
			}
		});
	}
};

const insertColor = function(color_index,hash_id,dataJSON)
{
	var query = 'INSERT INTO `activity_color_search` SET `user_id`='+parseInt(dataJSON.security.user_id)+',`hash_tracking_key`="'+hash_id+'", `color_id`='+parseInt(color_index.color_id)+', `search_frequency`=1, `date_updated`="'+dataJSON.new_simple_timestamp+'";';
	var mysql = new GLOBAL.mysql.mysql_connect(query);
	mysql.results_ss_activities().then(function(results){
		if(util.isError(results)==true){
			var msg = {
				'status':'Error',
				'desc':'[activity_search_history:insertSearch]DB Error Connection',
				'query':query,
				'message':results.message,
				'stack':results.stack||null
			};
			process.emit('Shopstyx:logError',msg);
		}else{
			var msg = {
				'status':'Error',
				'desc':'[activity_search_history:insertSearch]Error reading DB',
				'err':err
			};
			process.emit('Shopstyx:logError',msg);
		}
	});
};

const updateColor = function(color_index,hash_id,dataJSON)
{
	var query = 'UPDATE `activity_color_search` SET `search_frequency`=`search_frequency`+1, `hash_tracking_key`="'+hash_id+'"WHERE `color_id`='+parseInt(color_index.color_id)+' AND `user_id`='+parseInt(dataJSON.security.user_id)+';';
	var mysql = new GLOBAL.mysql.mysql_connect(query);
	mysql.results_ss_activities().then(function(results){
		if(util.isError(results)==true){
			var msg = {
				"success":false, "status":"Error",
				'desc':'[activity_search_history:updateColor]DB Error Connection',
				'query':query,
				'message':results.message,
				'stack':results.stack||null
			};
			process.emit('Shopstyx:logError',msg);
		}else{
			var msg = {
				'status':'Success',
				'desc':'[activity_search_history:updateColor]Updated Search History'
			};
			process.emit('Shopstyx:logError',msg);
		}
	});
};

module.exports={
	save:save,
	logSearch:logSearch,
	insertSearch:insertSearch,
	updateSearch:updateSearch,
	logColor:logColor,
	insertColor:insertColor,
	updateColor:updateColor
};