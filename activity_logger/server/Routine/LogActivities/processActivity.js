//const activity_products_visited = required(__dirname+'/activity_products_visited');
const activity_search_history = require(__dirname+'/activity_search_history');
const saveToMongoDB = require(__dirname+'/saveToMongoDB');
const util = require('util');
var start = function(dataJSON)
{
	//identify the request first
	//inital common method of request is data.request.type as an array with Search
	//also note that identification of request type can also be done in the fullURL information (dataJSON.fullUrl)

	//read product information (url would have '/product/client/details/request/')

	//pattern '/product/client/details/request/' capture
	// var readProductPattern = new RegExp('\/product\/client\/details\/request','gi');
	// if(readProductPattern.test(dataJSON.fullUrl)==true)
	// {
	// 	//log information for ss_activities.activity_products_visited
	// 	//activity_products_visited
	// 	            //                          (cb,hash_id,dataJSON);
	// 	saveToMongoDB.start(activity_products_visited.save,hash_id,dataJSON);
	// }
	var readProductPattern = new RegExp('\/styx\/pahinumdum\/list','gi');
	if(readProductPattern.test(dataJSON.fullUrl)==true)
	{
		//this is List Stykables
		if(util.isNullOrUndefined(dataJSON.request)==false)
		{
			if(util.isNullOrUndefined(dataJSON.request.type)==false)
			{
				switch(dataJSON.request.type.trim().toLowerCase())
				{
					case 'search ad name':
						//ads? should we save?
						break;
					case 'search product exist':
						saveToMongoDB.start(activity_search_history.save,dataJSON);
						break;
					default:
						break;
				}
			}
		}
	}
	readProductPattern = new RegExp('\/product\/client\/list\/request','gi');
	if(readProductPattern.test(dataJSON.fullUrl)==true)
	{
		if(util.isNullOrUndefined(dataJSON.request)==false)
		{
			if(typeof(dataJSON.request.type)!='object')
			{
				if(dataJSON.request.type.join("`").toLowerCase().split("`").indexOf("search")>-1)
				{
						saveToMongoDB.start(activity_search_history.save,dataJSON);
				}
			}
		}
	}
	//look for Search keyword
	//activity_search_history
	//look for color keyword
	//activity_color_search
};

module.exports={
	start:start
};