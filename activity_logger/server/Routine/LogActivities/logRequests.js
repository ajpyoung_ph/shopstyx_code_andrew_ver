var server_dir_array = (__dirname).toString().split("activity_logger");

var server_location = server_dir_array[0]+"commonnodeconfig";
delete server_dir_array;

const parseDataJSON = require(server_location+'/helpers/parseDataJSON');
const processActivity = require(__dirname+'/processActivity');
const util = require('util');

var start = function(dataJSON)
{
	if(util.isNullOrUndefined(dataJSON)==false)
	{
		var fullUrl = dataJSON.fullUrl;
		console.log("Logging : "+fullUrl);
		processActivity.start(dataJSON);
		// GLOBAL.mongodb_Logs.requestLogs.insert(dataJSON,function(err,reply){
		// 	if(err!=null)
		// 	{
		// 		var msg = {
		// 			'status':'Error',
		// 			'desc':'[logRequests:start]Error saving request',
		// 			'err':err,
		// 			'query':query
		// 		};
		// 		process.emit('Shopstyx:logError',msg);
		// 	}else{
		// 		var msg = {
		// 			'status':'Success',
		// 			'desc':'[logRequests:start]Saved request from user'
		// 		};
		// 		process.emit('Shopstyx:logError',msg);
		// 		var hash_id='';
		// 		if(typeof(reply._id)!='undefined')
		// 		{
		// 			hash_id = reply._id.toString();
		// 		}
		// 		if(typeof(reply[0])!='undefined')
		// 		{
		// 			if(typeof(reply[0]['_id'])!='undefined')
		// 			{
		// 				hash_id = reply[0]['_id'].toString();
		// 			}
		// 		}
		// 		processActivity.start(dataJSON,dataStruct,hash_id);
		// 	}
		// });
	}else{
		//log that logging failed
		var msg = {
			'status':'Error',
			'desc':'dataJSON is null',
			'err':'Could not log the request'
		};
		process.emit('Shopstyx:logError',msg);
	}
};

module.exports={
	start:start
};