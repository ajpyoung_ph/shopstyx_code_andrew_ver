const util = require('util');
var save = function(hash_id,dataJSON)
{
	/*
	user_id
	hash_tracking_key
	product_owner_user_id
	product_id
	product_visit_frequency
	date_updated
	*/

	/*
	"security": {
                "type": "web/app",
                "token": "JWT token (logged in/non logged in depends on the API call)",
                "user_id":<int>
        },
	"dev_identity": {
		"geometry": {
			"type": "Point",
			"coordinates": [
				123.8897752761841, (longitude; I think)
				10.31576371999243(latitude; I think)
			]
		},
		(optional
			if web browser allows; mobile mandatory)
		"ip_address": < string > ,
		"referrer": "http://domain",
		(web only)
	},
	"request": {
		"type": "List"/"Search Ad Name"/"Search Product Exist",
        "page_number":<int>,(starts at page 0)
        "number_items":<int>,(the number of items to return),
        "sort_algorithm":["latest"/"AdvertNameASC"/"AdvertNameDESC"],
                 //(array type and optional;default is always latest)
        "additional_info":{
             "user_id":<int>(product listing user_id owner(the one who created the advert), required by type:List and filter:OwnedByUserID),
             "store_id":<int>(for OwnedByShopID),
             "product_id":<int>(for SearchProd),
             "search_key":<string> (for Search only)
        }
	}
	*/
	//check if keyword exists
	var query = 'SELECT * FROM `activity_products_visited` WHERE `product_id`='+parseInt(dataJSON.request.additional_info.product_id)+' AND `user_id`='+parseInt(dataJSON.security.user_id)+' LIMIT 1';
	var mysql = new GLOBAL.mysql.mysql_connect(query);
	mysql.results_ss_activities().then(function(rows){
		if(util.isError(rows)==true){
			var msg = {
				"success":false, "status":"Error",
				'desc':'[activity_products_visited:save]DB Error Connection',
				'query':query,
				'message':results.message,
				'stack':results.stack||null
			};
			process.emit('Shopstyx:logError',msg);
		}else{
			if(rows.length>0)
			{
				var query = 'UPDATE `activity_products_visited` SET `product_visit_frequency`=`product_visit_frequency`+1, `hash_tracking_key`='+hash_id+', `date_updated`="'+dataJSON.new_simple_timestamp+'" WHERE `user_id`='+parseInt(dataJSON.security.user_id)+' AND `product_owner_user_id`='+parseInt(dataJSON.request.additional_info.user_id)+' AND `product_id`='+parseInt(dataJSON.request.additional_info.product_id)+';';
			}else{
				var query = 'INSERT INTO `activity_products_visited` SET `product_visit_frequency`=1, `hash_tracking_key`='+hash_id+', `date_updated`="'+dataJSON.new_simple_timestamp+'", `user_id`='+parseInt(dataJSON.security.user_id)+' AND `product_owner_user_id`='+parseInt(dataJSON.request.additional_info.user_id)+' AND `product_id`='+parseInt(dataJSON.request.additional_info.product_id)+';';
			}
			var mysql2 = new GLOBAL.mysql.mysql_connect(query);
			mysql2.results_ss_activities().then(function(results){
				if(util.isError(results)==true)
				{
					var msg = {
						"success":false, "status":"Error",
						'desc':'[activity_products_visited:save]DB Error Connection in mysql2',
						'query':query,
						'message':results.message,
						'stack':results.stack||null
					};
					process.emit('Shopstyx:logError',msg);
				}
			});
		}
	});
};

module.exports={
	save:save
};