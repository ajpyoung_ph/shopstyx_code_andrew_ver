//                    (activity_products_visited.save,dataJSON,dataStruct)
const util = require('util');
const start = function(cb,dataJSON)
{
	GLOBAL.mongodb_Logs.requestLogs.insert(dataJSON,function(err,reply){
		if(err!=null)
		{
			var msg = {
				'status':'Error',
				'desc':'[saveToMongo:start]Error saving request',
				'err':err,
				'query':query
			};
			process.emit('Shopstyx:logError',msg);
		}else{
			var msg = {
				'status':'Success',
				'desc':'[saveToMongo:start]Saved request from user'
			};
			process.emit('Shopstyx:logError',msg);
			var hash_id='';
			if(util.isNullOrUndefined(reply._id)==false)
			{
				hash_id = reply._id.toString();
			}
			if(util.isNullOrUndefined(reply[0])==false)
			{
				if(util.isNullOrUndefined(reply[0]['_id'])==false)
				{
					hash_id = reply[0]['_id'].toString();
				}
			}
			cb(hash_id,dataJSON);
		}
	});
}

module.exports={
	start:start
};