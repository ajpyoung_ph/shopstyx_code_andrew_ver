
const respond = require(GLOBAL.server_location+'/EventListeners/errorListener');
const logProductView = require(__dirname+'/logProductView');
const logAdsView = require(__dirname+'/logAdsView');
// const logProfileView = require(__dirname+'/logProfileView');
const parseDataJSON = require(GLOBAL.server_location+'/helpers/parseDataJSON');
const util = require('util');
/*
"request":{
        "type":"Product"/"Advertisement"/"Profile",
        "owner_id":Int,
        "view_duration":Int,(in seconds)
        "page_url":<string>,
        "product_id":<int>, (if type Product)
        "styx_id":<string>,(if type Advertisement)
        "user_id":<string>,(if type Profile)
    }


*/ //logViewsRoutine.processRequest(dataJSON,dataStruct,req,res);
const processRequest = function(dataJSON,dataStruct,req,res)
{
	var error=false;
	var error_mgs=[];
	if(util.isNullOrUndefined(dataJSON.request)==true)
	{
		error=true;
		error_mgs.push('No Request Found');
	}else{
		if(util.isNullOrUndefined(dataJSON.request.type)==true)
		{
			error=true;
			error_mgs.push('Request Type not found');
		}else{
			var request_type = dataJSON.request.type.toLowerCase().replace(/\s/g, '');
			if(util.isNullOrUndefined(dataJSON.request.owner_id)==true)
			{
				error=true;
				error_mgs.push('Owner User Information is not found');
			}
			if(util.isNullOrUndefined(dataJSON.request.view_duration)==true)
			{
				error=true;
				error_mgs.push('Duration of View Information is not found');
			}
			switch(request_type)
			{
				case 'product':
					if(typeof(dataJSON.request.product_id)=='undefined')
					{
						error=true;
						error_mgs.push('Product Information not found');
					}
					break;
				case 'advertisement':
					if(typeof(dataJSON.request.styx_id)=='undefined')
					{
						error=true;
						error_msg.push('Advertisement Information not found');
					}
					break;
				// case 'profile':
				// 	if(typeof(dataJSON.request.user_id)=='undefined')
				// 	{
				// 		error=true;
				// 		error_msg.push('Profile Information not found');
				// 	}
				// 	break;
				default:
					break;						
			}
		}
	}
	
	if(error==false)
	{
		/*
		"request":{
		        "type":"Product"/"Advertisement"/"Profile",
		        "owner_id":Int,
		        "view_duration":Int,(in seconds)
		        "page_url":<string>,
		        "product_id":<int>, (if type Product)
		        "styx_id":<string>,(if type Advertisement)
		        "user_id":<string>,(if type Profile)
		    }
		*/
		console.log('comparing request_type');
		switch(request_type)
		{
			case 'product':
				logProductView.start(dataJSON,dataStruct,req,res);
				break;
			case 'advertisement':
				console.log('starting logAdsView');
				//function(dataJSON,dataStruct,req,res)
				logAdsView.start(dataJSON,dataStruct,req,res);
				break;
			// case 'profile':
			// 	logProfileView.start(dataJSON,dataStruct);
			// 	break;
			default:
				var msg = {
					"success":false, "status":"Error",
					"desc":"[logViewsRoutine:processRequest]Unknown Request Type"
				};
				respond.respondError(msg,res,req);
				break;						
		}
	}else{
		var msg = {
			"success":false, "status":"Error",
			"desc":"[logViewsRoutine:processRequest]Missing Requirements",
			"err":error_mgs
		};
		respond.respondError(msg,res,req);
	}
};

module.exports={
	processRequest:processRequest
};
