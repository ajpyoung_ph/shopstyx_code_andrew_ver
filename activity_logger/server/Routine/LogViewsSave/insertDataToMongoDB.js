const util = require('util');
/*
data={
        "type":"Product"/"Advertisement"/"Profile",
        "owner_id":Int,
        "view_duration":Int,(in seconds)
        "page_url":<string>,
        "product_id":<int>, (if type Product)
        "styx_id":<string>,(if type Advertisement)
        "user_id":<string>,(if type Profile)
}
should save as 
{
        "type":"Product"/"Advertisement"/"Profile",
        "owner_id":Int,
        "uuid_hash_tracking":<string>,
        "view_duration":Int,(in seconds)
        "page_url":<string>,
        "product_id":<int>, (if type Product)
        "styx_id":<string>,(if type Advertisement)
        "user_id":<string>,(if type Profile)
}
*/ //start(dataJSON.request,dataJSON,dataStruct);
const start = function(data,dataJSON,dataStruct)
{
	history(data,dataJSON,dataStruct);
	summary(data,dataJSON,dataStruct);
}

function history(data,dataJSON,dataStruct)
{
	data['uuid_hash_tracking']=dataJSON.security.uuid;
	data['date_unix']=dataStruct.new_unix_timestamp;
	data['date_simple']=dataStruct.new_simple_timestamp;
	GLOBAL.mongodb.viewLogs.insert(data,function(err,retVal){
		if(err!=null)
		{
			var msg = {
				"success":false, "status":"Error",
				"desc":"[insertDataToMongoDB:start]Error Saving View History",
				"err":err,
				"query":data
			};
			process.emit('Shopstyx:logError',msg);
		}
	});
}

function summary(data,dataJSON,dataStruct)
{
	switch(dataJSON.request.type.toLowerCase().replace(/\s/g, ''))
	{
		case 'product':
			saveProductSummary(data,dataJSON,dataStruct);
			break;
		case 'advertisement':
			saveAdsSummary(data,dataJSON,dataStruct);
			break;
		default:
			break;
	}
}

function saveProductSummary(data,dataJSON,dataStruct)
{
	/*
{
        "type":"Product"/"Advertisement"/"Profile",
        "owner_id":Int,
        "view_duration":Int,(in seconds)
        "page_url":<string>,
        "product_id":<int>, (if type Product)
        "styx_id":<string>,(if type Advertisement)
        "user_id":<string>,(if type Profile)
}
	*/
	console.log("setting up MySQL Query for saveProductSummary");
	//console.log("dataJSON",dataJSON);
	var query = "INSERT INTO `activity_product_views_per_user` SET `unique_id`=\""+parseInt(dataJSON.request.owner_id)+"-"+parseInt(dataJSON.request.product_id)+"-"+parseInt(dataJSON.security.uuid)+"\", `user_id`="+parseInt(dataJSON.request.owner_id)+", `uuid_hash_tracking_key`=\""+parseInt(dataJSON.security.uuid)+"\", `product_id`="+parseInt(dataJSON.request.product_id)+",`view_frequency`=`view_frequency`+1,`average_view_duration`=(((`average_view_duration`*`view_frequency`)+"+parseInt(dataJSON.request.view_duration)+")/(`view_frequency`)),`date_updated`=\""+dataStruct.new_simple_timestamp.toString()+"\" ON DUPLICATE KEY UPDATE `view_frequency`=`view_frequency`+1,`average_view_duration`=(((`average_view_duration`*`view_frequency`)+"+parseInt(dataJSON.request.view_duration)+")/(`view_frequency`+1)), `date_updated`=\""+dataStruct.new_simple_timestamp.toString()+"\";";
	console.log('executing saveProductsSummary Query');
	var mysql = new GLOBAL.mysql.mysql_connect(query.toString());
	mysql.results_ss_activities().then(function(results){
		if(util.isError(results)==true){
			var msg = {
				"success":false, "status":"Error",
				'desc':'[insertDataToMongoDB:saveProductSummary]DB Error Connection',
				'query':query,
				'message':results.message,
				'stack':results.stack||null
			};
			process.emit('Shopstyx:logError',msg);
		}else{
			var msg = {
				"success":true, "status":"Success",
				"desc":"[insertDataToMongoDB:saveProductSummary]Updated Views"
			};
			process.emit('Shopstyx:logError',msg);
		}
	});
}

function saveAdsSummary(data,dataJSON,dataStruct)
{
	/*
{
        "type":"Product"/"Advertisement"/"Profile",
        "owner_id":Int,
        "view_duration":Int,(in seconds)
        "page_url":<string>,
        "product_id":<int>, (if type Product)
        "styx_id":<string>,(if type Advertisement)
        "user_id":<string>,(if type Profile)
}
	*/
	console.log("setting up MySQL Query for saveAdsSummary");
	//console.log("dataJSON",dataJSON);
	var query = "INSERT INTO `activity_stykable_viewed_per_user` SET `unique_id`=\""+parseInt(dataJSON.request.owner_id)+"-"+dataJSON.request.styx_id+"-"+parseInt(dataJSON.security.uuid)+"\", `user_id`="+parseInt(dataJSON.request.owner_id)+", `uuid_hash_tracking_key`=\""+parseInt(dataJSON.security.uuid)+"\", `stykable_id`=\""+dataJSON.request.styx_id+"\",`view_frequency`=`view_frequency`+1,`average_view_duration`=(((`average_view_duration`*`view_frequency`)+"+parseInt(dataJSON.request.view_duration)+")/(`view_frequency`)),`date_updated`=\""+(dataStruct.new_simple_timestamp).toString()+"\" ON DUPLICATE KEY UPDATE `view_frequency`=`view_frequency`+1,`average_view_duration`=(((`average_view_duration`*`view_frequency`)+"+parseInt(dataJSON.request.view_duration)+")/(`view_frequency`+1)), `date_updated`=\""+(dataStruct.new_simple_timestamp).toString()+"\";";
	console.log(query);
	console.log('executing saveAdsSummary Query');
	var mysql = new GLOBAL.mysql.mysql_connect(query);
	mysql.results_ss_activities().then(function(results){
		if(util.isError(results)==true){
			var msg = {
				"success":false, "status":"Error",
				'desc':'[insertDataToMongoDB:saveAdsSummary]DB Error Connection',
				'query':query,
				'message':results.message,
				'stack':results.stack||null
			};
			process.emit('Shopstyx:logError',msg);
		}else{
			var msg = {
				"success":true, "status":"Success",
				"desc":"[insertDataToMongoDB:saveAdsSummary]Updated Views"
			};
			process.emit('Shopstyx:logError',msg);
		}
	});
}

module.exports={
	start:start
};