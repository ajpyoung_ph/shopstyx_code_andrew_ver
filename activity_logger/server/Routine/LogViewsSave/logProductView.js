
const util = require('util');
const respond = require(GLOBAL.server_location+'/EventListeners/errorListener');
const insertDataToMongoDB = require(__dirname+'/insertDataToMongoDB');

/*
"request":{
        "type":"Product"/"Advertisement"/"Profile",
        "owner_id":Int,
        "view_duration":Int,(in seconds)
        "page_url":<string>,
        "product_id":<int>, (if type Product)
        "styx_id":<string>,(if type Advertisement)
        "user_id":<string>,(if type Profile)
    }
*/

const start = function(dataJSON,dataStruct,req,res)
{
	//save request to MongoDB
	insertDataToMongoDB.start(dataJSON.request,dataJSON,dataStruct);
	//breakdown data to MySQL
	var query = "INSERT INTO `activity_product_views` SET `product_id`="+parseInt(dataJSON.request.product_id)+",`view_frequency`=`view_frequency`+1,`average_view_duration`=(((`average_view_duration`*`view_frequency`)+"+parseInt(dataJSON.request.view_duration)+")/(`view_frequency`)),`date_updated`="+dataStruct.new_simple_timestamp+" ON DUPLICATE KEY UPDATE `view_frequency`=`view_frequency`+1,`average_view_duration`=(((`average_view_duration`*`view_frequency`)+"+parseInt(dataJSON.request.view_duration)+")/(`view_frequency`+1)), `date_updated`=\""+dataStruct.new_simple_timestamp+"\";";
	var mysql = new GLOBAL.mysql.mysql_connect(query);
	mysql.results_ss_activities().then(function(results){
		if(util.isError(results)==true){
			var msg = {
				"success":false, "status":"Error",
				'desc':'[logProductsView:start]DB Error Connection',
				'query':query,
				'message':results.message,
				'stack':results.stack||null
			};
			respond.respondError(msg,res,req);
		}else{
			var msg = {
				"success":true, "status":"Success",
				"desc":"[logProductsView:start]Updated Views"
			};
			respond.respondError(msg,res,req);
		}
	});
};

module.exports={
	start:start
};