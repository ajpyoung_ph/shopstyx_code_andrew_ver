const util = require('util');
const mysqlConv = require(GLOBAL.server_location+'/helpers/mysql_convertions');
/*
"security": {
         "type": "web/app",
         "uuid": <string>,
         "user_id":<int>, Must Not Be ZERO
         "token": "robot token"
    }
*/

const start = function(dataJSON,dataStruct)
{
	//read all `user_id`=0 AND `uuid_hash_tracking_key`="'+mysqlConv.mysql_real_escape_string(dataJSON.security.uuid)+'"
	var query = 'SELECT * FROM `activity_stykable_viewed_per_user` WHERE `user_id`=0 AND `uuid_hash_tracking_key`="'+mysqlConv.mysql_real_escape_string(dataJSON.security.uuid)+'";';
	var mysql = new GLOBAL.mysql.mysql_connect(query);
	mysql.results_ss_activities().then(function(results){
		if(util.isError(results)==true){
			var msg = {
				"success":false, "status":"Error",
				'desc':'[transferAdViews:start]DB Error Connection',
				'query':query,
				'message':results.message,
				'stack':results.stack||null
			};
			process.emit('Shopstyx:logError',msg);
		}else{
			if(results.length>0)
			{
				dataStruct['transferActivity_findingDuplicate_x']=0;
				findDuplicate(results,dataJSON,dataStruct);
			}
		}
	});
};
//from the results of user_id=0 and `uuid_hash_tracking_key`
//find if the targetted user_id=parseInt(dataJSON.security.user_id) and color_id exists
//if not then update existing record to user_id
//if exists then merge
function findDuplicate(rows,dataJSON,dataStruct)
{
	var x=dataStruct['transferActivity_findingDuplicate_x'];
	if(x<rows.length)
	{
		var query = 'SELECT * FROM `activity_stykable_viewed_per_user` WHERE `user_id`='+parseInt(dataJSON.security.user_id)+' AND `stykable_id`="'+rows[x]['stykable_id']+'";';
		var mysql = new GLOBAL.mysql.mysql_connect(query);
		mysql.results_ss_activities().then(function(results){
			if(util.isError(results)==true){
				var msg = {
					"success":false, "status":"Error",
					'desc':'[transferAdViews:findDuplicate]DB Error Connection',
					'query':query,
					'message':results.message,
					'stack':results.stack||null
				};
				process.emit('Shopstyx:logError',msg);
			}else{
				dataStruct['transferActivity_findingDuplicate_x']=0;
				if(results.length>0)
				{
					mergeDuplicate(rows,results,dataJSON,dataStruct);
					deleteDuplicate(rows,results,dataJSON,dataStruct);
				}else{
					//nothing to transfer
					updateRows(rows,dataJSON,dataStruct);
				}
			}
		});
	}else{
		var msg = {
			"success":true, "status":"Success",
			"desc":"[transferSearchHistory:start:executingQuery]Finished updating records",
			"err":err,
			"query":query
		};
		process.emit('Shopstyx:logError',msg);
	}
}
//this is where we update the existing record to user_id
function updateRows(rows,dataJSON,dataStruct)
{
	var x=dataStruct['transferActivity_findingDuplicate_x'];
	var query = 'UPDATE `activity_stykable_viewed_per_user` SET `user_id`='+parseInt(dataJSON.security.user_id)+' WHERE `user_id`=0 AND `uuid_hash_tracking_key`="'+mysqlConv.mysql_real_escape_string(dataJSON.security.uuid)+'" AND `stykable_id`="'+rows[x]['stykable_id']+'";';
	var mysql = new GLOBAL.mysql.mysql_connect(query);
	mysql.results_ss_activities().then(function(results){
		if(util.isError(results)==true){
			var msg = {
				"success":false, "status":"Error",
				'desc':'[transferAdViews:updateRows]DB Error Connection',
				'query':query,
				'message':results.message,
				'stack':results.stack||null
			};
			process.emit('Shopstyx:emailError',msg);
			dataStruct['transferActivity_findingDuplicate_x']=parseInt(dataStruct['transferActivity_findingDuplicate_x'])+1;
			findDuplicate(rows,dataJSON,dataStruct);
		}else{
			dataStruct['transferActivity_findingDuplicate_x']=parseInt(dataStruct['transferActivity_findingDuplicate_x'])+1;
			findDuplicate(rows,dataJSON,dataStruct);
		}
	});
}

function mergeDuplicate(rows,duplicate_row,dataJSON,dataStruct)
{
	var x=dataStruct['transferActivity_findingDuplicate_x'];
	//add the view_frequency from the duplicate to the existing and delete the duplicate row
	//calculate the new average_view_duration
	var old_view_duration_total = duplicate_row[0]['average_view_duration']*duplicate_row[0]['view_frequency'];
	var new_view_duration_total = rows[x]['average_view_duration']*rows[x]['view_frequency'];
	var new_duration = parseFloat((old_view_duration_total+new_view_duration_total)/(duplicate_row[0]['view_frequency']+rows[x]['view_frequency']));

	var query = 'UPDATE `activity_stykable_viewed_per_user` SET `view_frequency`=`view_frequency`+'+parseInt(rows[x]['view_frequency'])+', `average_view_duration`='+new_duration+' WHERE `user_id`='+parseInt(dataJSON.security.user_id)+' AND `stykable_id`="'+rows[x]['stykable_id']+'";';
	var mysql = new GLOBAL.mysql.mysql_connect(query);
	mysql.results_ss_activities().then(function(results){
		if(util.isError(results)==true){
			var msg = {
				"success":false, "status":"Error",
				'desc':'[transferAdViews:mergeDuplicate]DB Error Connection',
				'query':query,
				'message':results.message,
				'stack':results.stack||null
			};
			process.emit('Shopstyx:emailError',msg);
			dataStruct['transferActivity_findingDuplicate_x']=parseInt(dataStruct['transferActivity_findingDuplicate_x'])+1;
			findDuplicate(rows,dataJSON,dataStruct);
		}else{
			dataStruct['transferActivity_findingDuplicate_x']=parseInt(dataStruct['transferActivity_findingDuplicate_x'])+1;
			findDuplicate(rows,dataJSON,dataStruct);
		}
	});
}
function deleteDuplicate(rows,duplicate_rows,dataJSON,dataStruct)
{
	var x=dataStruct['transferActivity_findingDuplicate_x'];
	//add the search_frequency from the duplicate to the existing and delete the duplicate row

	var query = 'DELETE FROM `activity_stykable_viewed_per_user` WHERE `user_id`=0 AND `uuid_hash_tracking_key`="'+mysqlConv.mysql_real_escape_string(dataJSON.security.uuid)+'" AND `stykable_id`="'+rows[x]['stykable_id']+'";';
	var mysql = new GLOBAL.mysql.mysql_connect(query);
	mysql.results_ss_activities().then(function(results){
		if(util.isError(results)==true){
			var msg = {
				"success":false, "status":"Error",
				'desc':'[transferAdViews:deleteDuplicate]DB Error Connection',
				'query':query,
				'message':results.message,
				'stack':results.stack||null
			};
			process.emit('Shopstyx:emailError',msg);
			dataStruct['transferActivity_findingDuplicate_x']=parseInt(dataStruct['transferActivity_findingDuplicate_x'])+1;
			findDuplicate(rows,dataJSON,dataStruct);
		}else{
			dataStruct['transferActivity_findingDuplicate_x']=parseInt(dataStruct['transferActivity_findingDuplicate_x'])+1;
			findDuplicate(rows,dataJSON,dataStruct);
		}
	});
}
module.exports={
	start:start
};