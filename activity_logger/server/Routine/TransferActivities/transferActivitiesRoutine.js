
const respond = require(GLOBAL.server_location+'/EventListeners/errorListener');
const transferSearchHistory = require(__dirname+'/transferSearchHistory');
const transferColorSearch = require(__dirname+'/transferColorSearch');
const transferProductViews = require(__dirname+'/transferProductViews');
const transferAdViews = require(__dirname+'/transferAdViews');

const parseDataJSON = require(GLOBAL.server_location+'/helpers/parseDataJSON');
const util = require('util');


/*
"security": {
         "type": "web/app",
         "uuid": <string>,
         "user_id":<int>, Must Not Be ZERO
         "token": "robot token"
    }
*/
const processRequest = function(dataStruct)
{
	console.log("parsing dataJSON");
	var dataJSON = parseDataJSON.parseJSON(dataStruct,req,res);
	
	if(util.isNullOrUndefined(dataJSON)==false)
	{
		var error=false;
		var error_mgs=[];
		// no checking needed since everything we need is already checked in security Listener
		if(error==false)
		{
			transferSearchHistory.start(dataJSON,dataStruct);
			transferColorSearch.start(dataJSON,dataStruct);
			transferProductViews.start(dataJSON,dataStruct);
			transferAdViews.start(dataJSON,dataStruct);
		}else{
			var msg = {
				"success":false, "status":"Error",
				"desc":"[transferActivitiesRoutine:processRequest]Missing Requirements",
				"err":error_mgs
			};
			respond.logError(msg);
		}
	}else{
		var msg = {
			"success":false, "status":"Error",
			"desc":"[transferActivitiesRoutine:processRequest]No data to process"
		};
		respond.logError(msg);
	}
};

module.exports={
	processRequest:processRequest
};
