const util = require('util');
const mysqlConv = require(GLOBAL.server_location+'/helpers/mysql_convertions');
/*
"security": {
         "type": "web/app",
         "uuid": <string>,
         "user_id":<int>, Must Not Be ZERO
         "token": "robot token"
    }
*/

const start = function(dataJSON,dataStruct)
{
	//read all `user_id`=0 AND `uuid_hash_tracking_key`="'+mysqlConv.mysql_real_escape_string(dataJSON.security.uuid)+'"
	var query = 'SELECT * FROM `activity_search_history` WHERE `user_id`=0 AND `uuid_hash_tracking_key`="'+mysqlConv.mysql_real_escape_string(dataJSON.security.uuid)+'";';
	var mysql = new GLOBAL.mysql.mysql_connect(query);
	mysql.results_ss_activities().then(function(results){
		if(util.isError(results)==true){
			var msg = {
				"success":false, "status":"Error",
				'desc':'[transferSearchHistory:start]DB Error Connection',
				'query':query,
				'message':results.message,
				'stack':results.stack||null
			};
			process.emit('Shopstyx:emailError',msg);
		}else{
			if(results.length>0)
			{
				dataStruct['transferActivity_findingDuplicate_x']=0;
				findDuplicate(results,dataJSON,dataStruct);
			}
		}
	});
};
//from the results of user_id=0 and `uuid_hash_tracking_key`
//find if the targetted user_id=parseInt(dataJSON.security.user_id) and search_phrase exists
//if not then update existing record to user_id
//if exists then merge
function findDuplicate(rows,dataJSON,dataStruct)
{
	var x=dataStruct['transferActivity_findingDuplicate_x'];
	if(x<rows.length)
	{
		var query = 'SELECT * FROM `activity_search_history` WHERE `user_id`='+parseInt(dataJSON.security.user_id)+' AND `search_phrase` LIKE "'+rows[x]['search_phrase']+'";';
		var mysql = new GLOBAL.mysql.mysql_connect(query);
		mysql.results_ss_activities().then(function(results){
			if(util.isError(results)==true){
				var msg = {
					"success":false, "status":"Error",
					'desc':'[transferSearchHistory:findDuplicate]DB Error Connection',
					'query':query,
					'message':results.message,
					'stack':results.stack||null
				};
				process.emit('Shopstyx:logError',msg);
			}else{
				dataStruct['transferActivity_findingDuplicate_x']=0;
				if(results.length>0)
				{
					mergeDuplicate(rows,results,dataJSON,dataStruct);
				}else{
					//nothing to transfer
					updateRows(rows,dataJSON,dataStruct);
				}
			}
		});
		
	}else{
		var msg = {
			"success":true, "status":"Success",
			"desc":"[transferSearchHistory:start:executingQuery]Finished updating records",
			"err":err,
			"query":query
		};
		process.emit('Shopstyx:logError',msg);
	}
}
//this is where we update the existing record to user_id
function updateRows(rows,dataJSON,dataStruct)
{
	var x=dataStruct['transferActivity_findingDuplicate_x'];
	var query = 'UPDATE `activity_search_history` SET `user_id`='+parseInt(dataJSON.security.user_id)+' WHERE `user_id`=0 AND `uuid_hash_tracking_key`="'+mysqlConv.mysql_real_escape_string(dataJSON.security.uuid)+'" AND `search_phrase` LIKE "'+rows[x]['search_phrase']+'";';
	var mysql = new GLOBAL.mysql.mysql_connect(query);
	mysql.results_ss_activities().then(function(results){
		if(util.isError(results)==true){
			var msg = {
				"success":false, "status":"Error",
				'desc':'[transferSearchHistory:updateRows]DB Error Connection',
				'query':query,
				'message':results.message,
				'stack':results.stack||null
			};
			process.emit('Shopstyx:emailError',msg);
			dataStruct['transferActivity_findingDuplicate_x']=parseInt(dataStruct['transferActivity_findingDuplicate_x'])+1;
			findDuplicate(rows,dataJSON,dataStruct);
		}else{
			dataStruct['transferActivity_findingDuplicate_x']=parseInt(dataStruct['transferActivity_findingDuplicate_x'])+1;
			findDuplicate(rows,dataJSON,dataStruct);
		}
	});
}
//this is where we add the 
function mergeDuplicate(rows,duplicate_row,dataJSON,dataStruct)
{
	var x=dataStruct['transferActivity_findingDuplicate_x'];
	//add the search_frequency from the duplicate to the existing and delete the duplicate row

	var query = 'UPDATE `activity_search_history` SET `search_frequency`=`search_frequency`+'+parseInt(rows[x]['search_frequency'])+' WHERE `user_id`='+parseInt(dataJSON.security.user_id)+' AND `search_phrase` LIKE "'+rows[x]['search_phrase']+'";';
	var mysql = new GLOBAL.mysql.mysql_connect(query);
	mysql.results_ss_activities().then(function(results){
		if(util.isError(results)==true){
			var msg = {
				"success":false, "status":"Error",
				'desc':'[transferSearchHistory:mergeDuplicate]DB Error Connection',
				'query':query,
				'message':results.message,
				'stack':results.stack||null
			};
			process.emit('Shopstyx:emailError',msg);
			dataStruct['transferActivity_findingDuplicate_x']=parseInt(dataStruct['transferActivity_findingDuplicate_x'])+1;
			findDuplicate(rows,dataJSON,dataStruct);
		}else{
			dataStruct['transferActivity_findingDuplicate_x']=parseInt(dataStruct['transferActivity_findingDuplicate_x'])+1;
			findDuplicate(rows,dataJSON,dataStruct);
		}
	});
}
function deleteDuplicate(rows,duplicate_rows,dataJSON,dataStruct)
{
	var x=dataStruct['transferActivity_findingDuplicate_x'];
	//add the search_frequency from the duplicate to the existing and delete the duplicate row

	var query = 'DELETE FROM `activity_search_history` WHERE `user_id`=0 AND `uuid_hash_tracking_key`="'+mysqlConv.mysql_real_escape_string(dataJSON.security.uuid)+'" AND `search_phrase` LIKE "'+rows[x]['search_phrase']+'";';
	var mysql = new GLOBAL.mysql.mysql_connect(query);
	mysql.results_ss_activities().then(function(results){
		if(util.isError(results)==true){
			var msg = {
				"success":false, "status":"Error",
				'desc':'[transferSearchHistory:deleteDuplicate]DB Error Connection',
				'query':query,
				'message':results.message,
				'stack':results.stack||null
			};
			process.emit('Shopstyx:emailError',msg);
			dataStruct['transferActivity_findingDuplicate_x']=parseInt(dataStruct['transferActivity_findingDuplicate_x'])+1;
			findDuplicate(rows,dataJSON,dataStruct);
		}else{
			dataStruct['transferActivity_findingDuplicate_x']=parseInt(dataStruct['transferActivity_findingDuplicate_x'])+1;
			findDuplicate(rows,dataJSON,dataStruct);
		}
	});
}
module.exports={
	start:start
};