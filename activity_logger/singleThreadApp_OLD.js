

// if(process.env.NODE_ENV.toLowerCase()=='development')
// {
// 	const cluster = require('cluster');
// 	var cpu_val=4;
// 	if(typeof(process.env.NODE_CPU)!='undefined')
// 	{
// 		cpu_val=parseInt(process.env.NODE_CPU);
// 	}
// 	const numCPUs = cpu_val;//require('os').cpus().length;

// 	if (cluster.isMaster) 
// 	{
// 		// Fork workers.
// 		console.log('numCPUs: '+numCPUs);
// 		for (var i = 0; i < numCPUs; i++) 
// 		{
// 			cluster.fork();
// 		}

// 		cluster.on('listening', (worker, address) => {
// 			//console.log(address);
// 			//{ addressType: 4, address: null, port: 7771, fd: undefined }
// 			console.log('A worker is now connected to '+address.address+':'+address.port);
// 		});

// 		cluster.on('exit', (worker, code, signal) => {
// 			if( signal ) {
				
// 				console.log("worker was killed by signal: "+signal);
// 			} else if( code !== 0 ) {
				
// 				console.log("worker exited with error code: "+code);
// 			} else {
				
// 				console.log("worker success!");
// 			}
// 			//console.log(`worker ${worker.process.pid} died`);
// 			console.log('worker %d died (%s). restarting...',worker.process.pid, signal || code);
// 			clearTimeout(timeouts[worker.id]);
// 			cluster.fork();
// 		});
// 		var timeouts = [];
// 		function errorMsg() 
// 		{
// 			console.error('Something must be wrong with the connection ...');
// 		}

// 		cluster.on('fork', (worker) => {
// 			timeouts[worker.id] = setTimeout(errorMsg, 2000);
// 		});
// 		cluster.on('listening', (worker, address) => {
// 			clearTimeout(timeouts[worker.id]);
// 		});
// 		// cluster.on('exit', (worker, code, signal) => {
// 		// 	clearTimeout(timeouts[worker.id]);
// 		// 	errorMsg();
// 		// });
// 	} else {
// 	  // Workers can share any TCP connection
// 	  // In this case it is an HTTP server
// 	  // http.createServer((req, res) => {
// 	  //   res.writeHead(200);
// 	  //   res.end('hello world\n');
// 	  // }).listen(8000);
// 		start_up();
// 	}
// }else{
// 	start_up();
// }


// function start_up()
// {
	// inital setup
	// var pmx = require('pmx');
	// //Global Exception Logging
	// pmx.init();
	// var probe = pmx.probe();


	const express = require('express');
	const timeout = require('connect-timeout');
	//finding the commonnodeconfig
	var server_dir_array = (__dirname).toString().split("activity_logger");

	GLOBAL.server_location = server_dir_array[0]+"commonnodeconfig";
	delete server_dir_array;

	console.log("setting temp_dir_location");
	if(process.env.NODE_ENV.toLowerCase()!='production')
	{
		var temp_dir_array = (__dirname).toString().split("server");
		GLOBAL.temp_dir_location = temp_dir_array[0]+"/uploads";
		delete temp_dir_array;
	}else{
		GLOBAL.temp_dir_location = '/opt/temp/activity_logger';
	}
	console.log("init temp_dir_location: "+GLOBAL.temp_dir_location);

	const envConfig = require(GLOBAL.server_location + '/configuration/environment/environmentConfig');
	const cors = require(GLOBAL.server_location + '/configuration/environment/CORs');
	const sqlConnect = require(GLOBAL.server_location + '/configuration/mysql/db_conf');

	const conv = require(GLOBAL.server_location + '/helpers/mysql_convertions');
	const routes = require(__dirname + '/server/configuration/routes/v2/mainRoute');
	const mongodb_config = require(__dirname+'/server/configuration/mongodb/mongodb_config');

	//var testCodes = require('./testCodes/initEmitTest');
	const initListeners = require(__dirname + '/server/EventListeners/initializeListeners');

	initListeners.start(); //initialize all known listeners for the system


	// var shutdown_counter = probe.counter({
	//   name : 'unifiedAPI shutdown'
	// });

	var MainDataStruct = {
		app:express(),
		express:express,
		HTTP_location:'',
		mongo_db_location:'',
		mysql_db_init:{
			connectionLimit : 200,
			host     : '',
			user     : '',
			password : '',
			database : ''
		},
		gcloud_db_location:{
			projectId: '',
			keyFilename:''
		},
		port:process.env.PORT,
		router:express.Router(),
		routeData:{
			"user_id":'',
			"token":'',
			"req": '',
			"res": '',
			"new_unix_timestamp":'',
			"new_simple_timestamp":'',
			"timezone":"America/Los_Angeles"
		},
		port:parseInt(7775),
		rootPath:__dirname//,
		//shutdown_counter:shutdown_counter
	};
	//console.log(MainDataStruct);
	
	MainDataStruct.app.use(timeout(2000));
	//Start Configuration of system
	initListeners.start();
	
	MainDataStruct = cors.CORs(MainDataStruct);
	
	MainDataStruct = envConfig.envConf(MainDataStruct);
	
	MainDataStruct = routes.mainRoute(MainDataStruct);
	
	//connect to sql
	sqlConnect.mysqlConnect(MainDataStruct.mysql_db_init);
	
	//connect to GCloud DataStore


	//connect to MongoDB specific Collections

	mongodb_config.start(MainDataStruct);
	
	//exitProt.initExitProtocols(MainDataStruct);


	//End Configuration of system
	//GLOBAL variables specific for this program
	GLOBAL.number_list_items=50;
	GLOBAL.start_list_page=0;
	//End GLOBAL variables specific for this program

	// Add the error middleware at the end (after route declaration)
	//MainDataStruct.app.use(pmx.expressErrorHandler());
	//MainDataStruct.port=7773;
	GLOBAL.server = MainDataStruct.app.listen(MainDataStruct.port);
	console.log(MainDataStruct.HTTP_location);
	console.log('port:'+MainDataStruct.port);
	console.log(process.env.NODE_ENV+' mode running');
	process.emit('Shopstyx:logError',"New Log Entry ==== "+conv.convertUnixToMySQL(Math.floor(new Date().getTime()/1000)));
	//process.emit('Shopstyx:logError',MainDataStruct);
	//startMe.start();
// }
