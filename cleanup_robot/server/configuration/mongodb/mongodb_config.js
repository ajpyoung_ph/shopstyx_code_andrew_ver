const mongojs = require('mongojs');

var start = function(dataStruct)
{
	/*
	GLOBAL.mongodb = mongojs('transactionUser:s#0pst!cks@'+GLOBAL.locationString+'/shopstyx_transactions', ['transactionRecords','disputeRecords']);
	*/
	switch(process.env.NODE_ENV.toLowerCase())
	{
		case 'local':
		case 'development':
		case 'testing':
		case 'staging':
    //console.log('ugcUser:s#0pst!cks@'+dataStruct.mongo_db_location+'/ugc');
			GLOBAL.mongodbUGC = mongojs('ugcUser:s#0pst!cks@'+dataStruct.mongo_db_location+'/ugc', ['styxRecords','uploadProcess','filesThatNeedDeletion','pooledProducts'], {authMechanism: 'ScramSHA1'});
			GLOBAL.mongodbActivityLogs = mongojs('activityUser:s#0pst!cks@'+dataStruct.mongo_db_location+'/activityLogs', ['requestLogs,loginLogs,failedLogs','visitLogs','viewLogs'], {authMechanism: 'ScramSHA1'});
			GLOBAL.mongodbLikes = mongojs('likeUser:s#0pst!cks@'+dataStruct.mongo_db_location+'/likefollowmoods', ['likeRecords','followRecords','moodRecords'], {authMechanism: 'ScramSHA1'});
			break;

		case 'production':
		case 'final':
    //console.log('ugcUser:s#0pst!cks@'+dataStruct.mongo_db_location+'/ugc?replicaSet=styx01');
			//GLOBAL.mongodb = mongojs('ugcUser:s#0pst!cks@'+dataStruct.mongo_db_location+'/ugc?replicaSet=styx01', ['styxRecords','uploadProcess','filesThatNeedDeletion','pooledProducts'], {authMechanism: 'ScramSHA1'});
      		GLOBAL.mongodbUGC = mongojs('ugcUser:s#0pst!cks@'+dataStruct.mongo_db_location+'/ugc', ['styxRecords','uploadProcess','filesThatNeedDeletion','pooledProducts'], {authMechanism: 'ScramSHA1'});
      		GLOBAL.mongodbActivityLogs = mongojs('activityUser:s#0pst!cks@'+dataStruct.mongo_db_location+'/activityLogs', ['requestLogs,loginLogs,failedLogs','visitLogs','viewLogs'], {authMechanism: 'ScramSHA1'});
      		GLOBAL.mongodbLikes = mongojs('likeUser:s#0pst!cks@'+dataStruct.mongo_db_location+'/likefollowmoods', ['likeRecords','followRecords','moodRecords'], {authMechanism: 'ScramSHA1'});
			break;
	}
};

module.exports={
	start:start
};