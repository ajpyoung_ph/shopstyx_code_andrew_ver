//const util = require('util');
const mongojs = require('mongojs');

GLOBAL.mongodb = mongojs('ugcUser:s#0pst!cks@localhost/ugc', ['styxRecords','uploadProcess','filesThatNeedDeletion','pooledProducts'], {authMechanism: 'ScramSHA1'});

var query = {
	'uuid_v1':"6d800fd0-bb5d-11e5-a25a-0d27cfcadca7dondo"
};

const mysql = require('mysql');
//const mytime = require('../../helpers/mysql_convertions');
const fs = require('fs');

const db_init = {
	host     : 'localhost',
	user     : 'root',
	password : '',
	database : ''
};


var mysqlConnect = function(db_init){
	connectSsCommon(JSON.parse(JSON.stringify(db_init)));
	connectSsTransactionDumps(JSON.parse(JSON.stringify(db_init)));
	connectSsActivities(JSON.parse(JSON.stringify(db_init)));

	//setImmediate(populateDefaults);
};

function connectSsCommon(db_init)
{
	// db_init.database = 'cs_search_results';
	// GLOBAL.db_cs_search_results = mysql.createConnection(db_init);
	try{
		GLOBAL.db_ss_common.end();
		//console.log("ending SS_COMMON MySQL connection");
	}catch(err){
		//console.log("No MySQL SS_COMMON Connection to Close");
	}
	//console.log("creating new SS_COMMON mysql connection");
	////console.log(mytime.MySQLDateTimeNOW());
	db_init.database = 'ss_common';
	db_init=ssl_set(db_init);
	//GLOBAL.db_ss_common = mysql.createConnection(db_init);
	GLOBAL.db_ss_common = mysql.createPool(db_init);

	// GLOBAL.db_ss_common.connect(function(err) {              // The server is either down
 //    if(err) {                                     // or restarting (takes a while sometimes).
	//       //console.log('error when connecting to db:', err);
	//       setTimeout(connectCsCommon, 2000, db_init); // We introduce a delay before attempting to reconnect,
	//     }                                     // to avoid a hot loop, and to allow our node script to
	//   });                                     // process asynchronous requests in the meantime.
	//                                           // If you're also serving http, display a 503 error.
	// GLOBAL.db_ss_common.on('error', function(err) {
	// 	//console.log('db error', err);
	// 	if(err.code === 'PROTOCOL_CONNECTION_LOST') { // Connection to the MySQL server is usually
	// 		PROTOCOL_CONNECTION_LOST_counter.inc();
	// 		connectCsCommon(db_init);                         // lost due to either server restart, or a
	// 	} else {                                      // connnection idle timeout (the wait_timeout
	// 	  throw err;                                  // server variable configures this)
	// 	}
	// });
}

function connectSsTransactionDumps(db_init)
{
	//connect to SS_TRANSACTION_DUMPS
	try{
		GLOBAL.db_ss_transaction_dumps.end();
		//console.log("ending SS_TRANSACTION_DUMPS MySQL connection");
	}catch(err){
		//console.log("No MySQL SS_TRANSACTION_DUMPS Connection to Close");
	}
	//console.log("creating new SS_TRANSACTION_DUMPS mysql connection");
	////console.log(mytime.MySQLDateTimeNOW());
	db_init.database = 'ss_transaction_dumps';
	db_init=ssl_set(db_init);
	//GLOBAL.db_ss_transaction_dumps = mysql.createConnection(db_init);
	GLOBAL.db_ss_transaction_dumps = mysql.createPool(db_init);
}

function connectSsActivities(db_init)
{
	//connect to SS_TRANSACTION_DUMPS
	try{
		GLOBAL.db_ss_activities.end();
		//console.log("ending SS_TRANSACTION_DUMPS MySQL connection");
	}catch(err){
		//console.log("No MySQL SS_TRANSACTION_DUMPS Connection to Close");
	}
	//console.log("creating new SS_TRANSACTION_DUMPS mysql connection");
	////console.log(mytime.MySQLDateTimeNOW());
	db_init.database = 'ss_activities';
	db_init=ssl_set(db_init);
	//GLOBAL.db_ss_transaction_dumps = mysql.createConnection(db_init);
	GLOBAL.db_ss_activities = mysql.createPool(db_init);
}

function ssl_set(db_init)
{
	switch('development')
	{
		case 'local':
		case 'development':
			break;

		case 'testing':
		case 'staging':
			break;
		// case 'cloud_staging':
		// 	db_init.ssl={
		// 		ca : fs.readFileSync(server_location + 'certificates/MySQL/server-ca.pem'),
		// 		key : fs.readFileSync(server_location + 'certificates/MySQL/client-key.pem'),
		// 		cert : fs.readFileSync(server_location + 'certificates/MySQL/client-cert.pem')
		// 	};
		// 	break;

		case 'production':
		case 'final':
			db_init.ssl={
				ca : fs.readFileSync(GLOBAL.server_location + '/certificates/MySQL/server-ca.pem'),
				key : fs.readFileSync(GLOBAL.server_location + '/certificates/MySQL/client-key.pem'),
				cert : fs.readFileSync(GLOBAL.server_location + '/certificates/MySQL/client-cert.pem')
			};
			break;
	}
	return db_init;
}

// function populateDefaults()
// {
// 	GLOBAL.default_colors=[];
// 	var query = "SELECT * FROM `default_colors` ORDER BY `color_id`;";
// 	var default_colors=[];
// 	var alternative_colors=[];
// 	var mysql = new GLOBAL.mysql.mysql_connect(query);
// 	mysql.results_ss_common().then(function(results){
// 		if(util.isError(results)==true){
// 			var msg = {
// 				"success":false, "status":"Error",
// 				'desc':'[testReadItem:populateDefaults]DB Error Connection',
// 				'query':query,
// 				'message':results.message,
// 				'stack':results.stack||null
// 			};
// 			process.emit('Shopstyx:logError',msg);
// 		}else{
// 			var rows=results;
// 			default_colors=rows;
// 			query = "SELECT * FROM `default_colors_alternative` ORDER BY `color_id`;";
// 			var mysql2 = new GLOBAL.mysql.mysql_connect(query);
// 			mysql2.results_ss_common().then(function(results2){
// 				if(util.isError(results2)==true){
// 					var msg = {
// 						"success":false, "status":"Error",
// 						'desc':'[testReadItem:populateDefaults]DB Error Connection2',
// 						'query':query,
// 						'message':results2.message,
// 						'stack':results2.stack||null
// 					};
// 					process.emit('Shopstyx:logError',msg);
// 				}else{
// 					var rows2=results2;
// 					alternative_colors=rows2;
// 					GLOBAL.default_colors=default_colors.concat(alternative_colors);
// 				}
// 			});
// 		}
// 	});
// }

// GLOBAL.mongodb.styxRecords.findOne(query,function(err,doc){
// 	if(err==null)
// 	{
// 		x=0;
// 		//console.log(doc);
// 		//console.log("doc.items");
// 		//console.log(typeof(doc.items));
// 		//console.log("doc.items[x].prod");
// 		//console.log(typeof(doc.items[x].prod));
// 		//console.log(doc.items[x].prod);
// 		//console.log(parseInt(doc.items[x].prod.product_id));
// 	}else{
// 		//console.log("error found");
// 		//console.log(err);
// 	}

// });
const util = require('util');

// var start = function()
// {
// 	////console.log("inside Read");
// 	//var hash_id = dataJSON.request.styx_id;
// 	var query = {
// 		'uuid_v1':"6d800fd0-bb5d-11e5-a25a-0d27cfcadca7dondo"
// 	};
// 	//find advertisement by uuid_v1
// 	////console.log("starting query");
// 	GLOBAL.mongodb.styxRecords.findOne(query,function(err,doc){
// 		if(err==null)
// 		{
// 			////console.log('mongo read successful');
// 			if(doc!=null)
// 			{
// 				////console.log('doc not null');
// 				doc['owner_info']=[];
// 				if(util.isNullOrUndefined(doc.owner)==false)
// 				{
// 					extractOwnerInfo(doc);
// 				}else if(util.isNullOrUndefined(doc.items)==false)
// 				{
// 					extractProductInfo(doc,0);
// 				}else{
// 					var msg={
// 						"success":true, "status":"Success",
// 						"data":doc
// 					};
// 					//console.log(msg);
// 				}
// 			}else{
// 				//if we cannot find the uuid_v1 normally then check for another uuid_v1 based on the temporary upload process (new data)
// 				GLOBAL.mongodb.uploadProcess.findOne(query,function(err2,doc2){
// 					if(err2==null)
// 					{
// 						if(doc2!=null)
// 						{
// 							doc=doc2.additional_info.new_data;
// 							doc['owner_info']=[];
// 							if(typeof(doc.owner)!='undefined')
// 							{
// 								////console.log('executing extractOwnerInfo');
// 								extractOwnerInfo(doc);
// 							}else if(typeof(doc.items)!='undefined')
// 							{	
// 								////console.log('executing extractProductInfo');
// 								extractProductInfo(doc,0);
// 							}else{
// 								var msg={
// 									"success":true, "status":"Success",
// 									"data":doc
// 								};
// 								//console.log(msg);
// 							}
// 						}else{
// 							////console.log('executing extractProductInfo code 2');
// 							extractProductInfo(doc,0);
// 						}
						
// 					}else{
// 						var msg = {
// 							"success":false, "status":"Error",
// 							"desc":"[readAdvert:start]No valid request2",
// 							"err":err2
// 						};
// 						//console.log(msg);
// 					}
// 				});
// 			}
// 		}else{
// 			var msg = {
// 				"success":false, "status":"Error",
// 				"desc":"[readAdvert:start]No valid request",
// 				'err':err
// 			};
// 			//console.log(msg);
// 		}
// 	});
// }

// var extractOwnerInfo = function(doc)
// {
// 	////console.log('inside extractOwnerInfo');
// 	if(doc!=null)
// 	{
// 		//console.log('before owner extraction');
// 		//console.log(doc.items);
// 		var query = "SELECT * FROM `users` WHERE `user_id`="+parseInt(doc.owner)+" LIMIT 1";
// 		GLOBAL.db_ss_common.getConnection(function(err, connection) {
// 			////console.log('trying to connect extractOwnerInfo');
// 			connection.query(query,function(err,rows,fields){
// 				if(err==null)
// 				{
// 					if(typeof(rows)!='undefined')
// 					{
// 						if(typeof(rows)=='object')
// 						{
// 							try{
// 								delete rows[0]['password'];
// 							}catch(err){
// 								//do nothing
// 							}
// 							try{
// 								delete rows[0]['password_encryption_type'];
// 							}catch(err){
// 								//do nothing
// 							}
// 							////console.log('rows found for extractOwnerInfo');
// 							doc.owner_info.push(rows[0]);
// 						}else{
// 							////console.log('rows not correct format for extractOwnerInfo');
// 						}
// 					}else{
// 						////console.log('rows undefined for extractOwnerInfo');
// 					}
// 				}else{
// 					var msg={
// 						"success":false, "status":"Error",
// 						"desc":"[readAdvert:extractOwnerInfo] Error executing Query",
// 						"data":[],
// 						"doc":doc,
// 						"err":err
// 					};
// 					//console.log(msg);
// 				}
// 				connection.release();
// 				//console.log('extracting products');
// 				extractProductInfo(doc,0);
// 			});
// 		});
// 	}else{
// 		var msg={
// 			"success":true, "status":"Success",
// 			"desc":"[readAdvert:extractOwnerInfo] No Record of User Found",
// 			"data":[],
// 			"doc":doc
// 		};
// 		//console.log(msg);
// 	}
// }
// //extractProductInfo(doc,x);
// var extractProductInfo=function(doc,x)
// {
// 	//////console.log(doc.items);
// 	////console.log('inside extractProductInfo');
// 	if(doc!=null)
// 	{
// 		if(x<doc.items.length)
// 		{
// 				//console.log('looking for product '+x+' of '+doc.items.length);
// 				//console.log("doc ni:");
// 				//console.log(doc);
// 				var query = "SELECT * FROM `products` WHERE `product_id`="+parseInt(doc.items[x].prod.product_id)+" LIMIT 1";
// 				////console.log('query assigned');
// 				////console.log(query);
// 				GLOBAL.db_ss_common.getConnection(function(err, connection) {
// 					////console.log('requesting mysql connection code 1');
// 					if(err==null)
// 					{
// 						////console.log('connecting query for product');
// 						connection.query(query,function(err_con1,rows,fields){
// 							////console.log('connection successful, looking if there were errors in query');
// 				    		if(err_con1==null)
// 				    		{
// 				    			////console.log('no obvious error occurred');
// 				    			if(rows.length<1)
// 				    			{
// 				    				////console.log('rows is less than 1');
// 				    				var msg = {
// 										"success":false, "status":"Error",
// 										"desc":"[readAdvert:extractProductInfo]Product "+parseInt(doc.items[x].prod.product_id)+" not found",
// 										"query":query
// 									};
// 									process.emit('Shopstyx:logError',msg);//changed from logError
// 									//check if x>doc.items.length
						    		
// 						    		checkX(doc,x);
// 				    			}else{
// 				    				//find images
// 				    				////console.log('finding images for product '+x+' of '+doc.items.length);
// 					    			var query2 = "SELECT * FROM `product_images` WHERE `product_id`="+parseInt(rows[0].product_id)+";";
					    			
// 					    			GLOBAL.db_ss_common.getConnection(function(err2, connection2) {
// 					    				////console.log('requesting mysql connection code 2');
// 					    				if(err2==null)
// 					    				{
// 					    					////console.log('no error in second query');
// 					    					connection2.query(query2,function(err_con2,rows2,fields2){
// 					    						////console.log('parsing results');
// 						    					doc.items[x]['prod']['product_info'] = JSON.parse(JSON.stringify(rows[0]));
// 						    					if(err_con2==null)
// 						    					{
// 						    						doc.items[x]['prod']['product_images'] = JSON.parse(JSON.stringify(rows2));
// 						    					}else{
// 						    						doc.items[x]['prod']['product_images'] = [];
// 						    					}
// 						    					//release connection2
// 						    					connection2.release();
// 						    					//check if x>items.length
						    					
// 						    					checkX(doc,x);
// 						    				});
// 					    				}else{
// 					    					////console.log('error in connection code 2');
// 					    					var msg={
// 												"success":false, "status":"Error",
// 												"data":doc,
// 												'desc':'[readAdvert:extractProductInfo] MySQL Error Connection Code 2',
// 												'err':err2,
// 												'docItems':doc.items
// 											};
// 											process.emit('Shopstyx:logError',msg);
// 											checkX(doc,x);
// 					    				}
					    				
// 					    			});
// 				    			}
				    			
// 				    		}else{
// 				    			//log error and skip insert
// 				    			var msg = {
// 									"success":false, "status":"Error",
// 									"desc":"[readAdvert:extractProductInfo]error in DB products",
// 									"err":err_con1,
// 									"query":query
// 								};
// 								process.emit('Shopstyx:logError',msg);//changed from logError
// 								//check if x>doc.items.length
					    		
// 					    		checkX(doc,x);
// 				    		}
// 				    		//release connection
// 				    		connection.release();
// 				    	});
// 					}else{
// 						////console.log('MySQL Error Connection Occurred');
// 						var msg={
// 							"success":true, "status":"Success",
// 							"data":doc,
// 							'desc':'[readAdvert:extractProductInfo] MySQL Error Connection',
// 							'err':err,
// 							'docItems':doc.items
// 						};
// 						//console.log(msg);
// 					}
// 			    });
// 		}else{
// 			checkX(doc,x);
// 		}
// 	}else{
// 		var msg={
// 			"success":true, "status":"Success",
// 			"desc":"[readAdvert:extractProductInfo] No Records Found",
// 			"data":[],
// 			"doc":doc
// 		};
// 		//console.log(msg);
// 	}
// }

// function checkX(doc,x)
// {
// 	if(x<doc.items.length)
// 	{
// 		////console.log('incrementing product list');
// 		x=x+1;
// 		////console.log("parsing x = "+x);
// 		extractProductInfo(doc,x);
// 	}else{
// 		var msg={
// 			"success":true, "status":"Success",
// 			"data":doc
// 		};
// 		//console.log(msg);
// 	}
// }


// module.exports={
// 	start:start,
// 	extractProductInfo:extractProductInfo
// };
mysqlConnect(db_init);
//start();

var query = 'SELECT afw.*, p.*, pseo.*  FROM `product_added_from_web_info` afw LEFT JOIN `products` p ON afw.product_id = p.product_id LEFT JOIN `product_seo` pseo ON p.product_id = pseo.product_id WHERE (afw.url_source = "http://otteny.com/en/jia-jia-embroidered-top.html" OR afw.canonical_url="http://otteny.com/en/jia-jia-embroidered-top.html") LIMIT 1;'
GLOBAL.db_ss_common.getConnection(function(err, connection) {
	if(err==null)
	{
		connection.query(query,function(err,rows,fields){
			var msg = util.inspect(rows,{showHidden: false, depth: null});
			//console.log(msg);
		});
	}else{
		//console.log("Error Connecting to SQL ");
		//console.log(err);
	}
});