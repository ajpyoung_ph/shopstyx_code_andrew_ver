//from https://googlecloudplatform.github.io/gcloud-ruby/docs/master/Gcloud/Storage/Bucket.html

var server_dir_array = (__dirname).toString().split("advertisement_ugc_api");

GLOBAL.server_location = server_dir_array[0]+"commonnodeconfig";
delete server_dir_array;


GLOBAL.gcloud = require('gcloud')({
	projectId: 'shopstyx',
	keyFilename: GLOBAL.server_location + '/certificates/GDataStore/Shopstyx-5595286364f0.json'
});
GLOBAL.kind={
	styxName:"local_styxDB",
	defaultLogName:"local_logs"
};

GLOBAL.storage_names = {
	'advertisements':'shopstyx-advertisements-local',
	'products':'shopstyx-products-local',
	'profiles':'shopstyx-profiles-local'
};

// require "gcloud"

// gcloud = Gcloud.new
// storage = gcloud.storage

// bucket = storage.bucket "my-bucket"
// file = bucket.file "path/to/my-file.ext"

var gcs = GLOBAL.gcloud.storage();
var bucket = gcs.bucket(GLOBAL.storage_names.advertisements);
//var file = bucket.file('images/'+targetValue+'_'+filename);
//target delete file : 266b38c7-65b8-4c1f-8625-678d1e2d07bf-original.jpeg
var file = bucket.file('images/266b38c7-65b8-4c1f-8625-678d1e2d07bf-original.jpeg');


file.delete(function(err, apiResponse) {
	if(err==null)
	{
		//console.log("delete success");
	}else{
		//console.log("Error :");
		//console.log(err);
		/*
		{ 
			[ApiError: Not Found]
			errors: [ { domain: 'global', reason: 'notFound', message: 'Not Found' } ],
			code: 404,
			message: 'Not Found',
			response: undefined 
		}

		*/
	}
});