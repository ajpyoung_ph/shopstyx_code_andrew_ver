
const respond = require(GLOBAL.server_location+'/EventListeners/errorListener');
const saveAdvert = require(__dirname+'/saveAdvert');
const updateAdvert = require(__dirname+'/updateAdvert');
const deleteAdvert = require(__dirname+'/deleteAdvert');
const readAdvert = require(__dirname+'/readAdvert');
const listAdvert = require(__dirname+'/listAdvert');
const saveMedia = require(__dirname+'/saveMedia');
const searchAd = require(__dirname+'/searchAd');
const readPooled = require(__dirname+'/readPooled');
const relatedProcess = require(__dirname+'/relatedRoutines/relatedProcess');
const parseDataJSON = require(GLOBAL.server_location+'/helpers/parseDataJSON');
const uuid = require('uuid');
const util = require('util');

var start = function(dataJSON,dataStruct,req,res)
{
	var error_mgs = [];
	//validate data here
	//end validate data
	//check if dataStruct.file_flag==true
	//if true then move file from temp and resample to 3 pics for upload
	var dataJSON2 = JSON.parse(JSON.stringify(dataJSON));
	var my_uuid = uuid.v1();
	//var dataStruct2 = JSON.parse(JSON.stringify(dataStruct));
	/*
	"request": {
		"type": "Save",
		"data": {
            "products":[1,2,3,4,5],
            "product_pool":[1,2,3,4,5,6,7,8,9],
			"styx_name": 'Andrew and IHOP friends',
			"short_description": 'One fine weekend, Andrew and his cuddly little muchkin friends went to IHOP to reward themselves for doing a great job over the week.',
			"src": 'uploads/andrewihop.mp4',
			"styx_type": 'V', //'V' for Video, 'P' for Photo, 'S' for Slideshow, 'B' for Banner
			"duration": 150, //by seconds <- for Video only
			"width": 720, //width of media <- Video/Photo
			"height": 480, //height of media <- Video/Photo
			"owner": 12, //shopstyx user_id
			"color_theme": { //color theme (using google’s paper-style system)
				color1: "#4CBB17", //--default-primary-color
				color2: "#f7f6f2", //--accent-color
				color3: "#ffffff" //--primary-background-color
			},
			"pin_icon": "maps:local-offer", //use any element for this
			...

		}
	}
	*/
	var size_array = GLOBAL.advertisement_sizes;
	//save Advertisement Info
	//console.log("saving data");
	//check request type
	processRequest(size_array,dataJSON,dataStruct,my_uuid,req,res);
	
	if(util.isNullOrUndefined(dataJSON.request.data)==false)
	{
		if(util.isNullOrUndefined(dataJSON.request.data.styx_type)==false)
		{
			//console.log("saving files");
			switch(dataJSON.request.data.styx_type.toLowerCase().toString())
			{
				case 'v':
					saveMedia.video(dataJSON2,req,res,GLOBAL.storage_names.advertisements,my_uuid,req,res);
					break;
				case '3':
					saveMedia.third_party_video(dataJSON2,req,res,GLOBAL.storage_names.advertisements,my_uuid,req,res);
					break;
				case 'p':
					saveMedia.photo(size_array,dataJSON2,req,res,GLOBAL.storage_names.advertisements,my_uuid,req,res);
					break;
				case 'c'://Collage here
					saveMedia.collage(size_array,dataJSON2,req,res,GLOBAL.storage_names.advertisements,my_uuid,req,res);
					break;
				case 's':
					//console.log("No Images expected");
					saveMedia.slideshow(size_array,dataJSON2,req,res,GLOBAL.storage_names.advertisements,my_uuid,req,res);
					break;
				default:
					//no media to upload
					break;
			}
		}
	}
};

function processRequest(size_array,dataJSON,dataStruct,my_uuid,req,res)
{
	var error_flag = false;
	var error_mgs=[];
	// if(typeof(dataJSON.pictures)!='object')
	// {
	// 	error_mgs.push("Saving Photos Error");
	// 	error_flag=true;
	// }
	// if(typeof(dataJSON.vids)!='object')
	// {
	// 	error_mgs.push("Saving Videos Error");
	// 	error_flag=true;
	// }
	if(error_flag==false)
	{
		//console.log('"'+dataJSON.request.type.toLowerCase().replace(/\s/g, '')+'"');
		switch(dataJSON.request.type.toLowerCase().replace(/\s/g, ''))
		{
			case 'read':
				//console.log("executing Read");
				readAdvert.start(size_array,dataJSON,dataStruct,my_uuid,req,res);
				break;
			case 'save':
				dataJSON.request.data['uuid_v1']=my_uuid.toString();
				dataJSON.request.data['date_simple']=dataStruct.new_simple_timestamp;
				dataJSON.request.data['date_unix']=dataStruct.new_unix_timestamp;
				saveAdvert.start(size_array,dataJSON,dataStruct,my_uuid,req,res);
				break;
			case 'update'://no uuid_v1 because it should be there na
				dataJSON.request.data['date_simple']=dataStruct.new_simple_timestamp;
				dataJSON.request.data['date_unix']=dataStruct.new_unix_timestamp;
				updateAdvert.start(size_array,dataJSON,dataStruct,my_uuid,req,res);
				break;
			case 'delete':
				deleteAdvert.start(size_array,dataJSON,dataStruct,my_uuid,req,res);
				break;
			case 'list':
				listAdvert.start(dataJSON,dataStruct,req,res);
				break;
			case 'searchadname':
				searchAd.searchName(dataJSON,dataStruct,req,res);
				break;
			case 'searchproductexist':
				searchAd.searchProductIdExist(dataJSON,dataStruct,req,res);
				break;
			case 'status':
				readAdvert.readStatus(dataJSON,dataStruct,req,res);
				break;
			case 'pooledproductssummary':
				readPooled.summary(dataJSON,dataStruct,req,res);
				break;
			case 'pooledproductshistory':
				readPooled.history(dataJSON,dataStruct,req,res);
				break;
			case 'related':
				relatedProcess.start(dataJSON,dataStruct,req,res);
				break;
			default:
				var msg = {
					"success":false, "status":"Error",
					"desc":"[advertisementRoutine:processRequest]No valid request"
				};
				respond.respondError(msg,res,req);
				break;
		}
	}else{
		var msg = {
			"success":false, "status":"Error",
			"desc":"[advertisementRoutine:processRequest]Error processing media",
			"err":error_mgs.join(',')
		};
		respond.respondError(msg,res,req);
		//process request
	}
}
module.exports={
	start:start
};