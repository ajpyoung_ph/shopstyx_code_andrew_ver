const util = require('util');

var start = function(dataJSON)
{
	var sort = {
        "date_updated":1
    };

	if(typeof(dataJSON.request.sort_algorithm)!='undefined')
    {
        //dataJSON.request.sort_algorithm.join("`").toLowerCase().replace(/\s/g, '').split("`").indexOf('prodnameasc')>-1
        if(dataJSON.request.sort_algorithm.join("`").toLowerCase().replace(/\s/g, '').split("`").indexOf('latest')>-1)
        {
            if(typeof(sort.date_updated)=='undefined')
            {
                sort.date_updated=-1;
            }
        }
        if(dataJSON.request.sort_algorithm.join("`").toLowerCase().replace(/\s/g, '').split("`").indexOf('advertnameasc')>-1)
        {
            if(typeof(sort.styx_name)=='undefined')
            {
                sort.styx_name=1;
            }
        }
        //AdvertNameDESC
        if(dataJSON.request.sort_algorithm.join("`").toLowerCase().replace(/\s/g, '').split("`").indexOf('advertnamedesc')>-1)
        {
            if(typeof(sort.styx_name)=='undefined')
            {
                sort.styx_name=-1;
            }
        }
    }
    
    return sort;
}

module.exports={
	start:start
};