const mongojs = require('mongojs');
/*
{ 
    "_id" : ObjectId("56a0724f1e30590545825b41"), 
    "user_id" : NumberInt(1), 
    "type" : "pooled product summary", 
    "date_simple" : "2016-01-21 13:53:18", 
    "date_unix" : NumberInt(1453355598), 
    "data" : {
        "product_ids" : [
            NumberInt(1)
        ], 
        "current_count" : NumberInt(1)
    }
}

{ 
    "_id" : ObjectId("56a0724f1e30590545825b40"), 
    "user_id" : NumberInt(1), 
    "type" : "pooled product history", 
    "date_simple" : "2016-01-21 13:53:18", 
    "date_unix" : NumberInt(1453355598), 
    "product_id" : NumberInt(1)
}

*/
var start = function(user_id,pooledProducts,new_simple_date,new_unix_date)
{
	//query if information in pooled product exists in summary
	var query = {
		"user_id":parseInt(user_id),
		"type":"pooled product summary"
	};

	GLOBAL.mongodb.pooledProducts.findOne(query,function(err,doc){
		if(err==null)
		{
			if(doc==null)
			{
				//no data found so save all data
				saveAll(user_id,pooledProducts,new_simple_date,new_unix_date);
			}else{
				//data found and then look for products not in the summary
				saveSelected(user_id,pooledProducts,doc,new_simple_date,new_unix_date);
			}
		}else{
			var msg = {
				"success":false, "status":"Error",
				"desc":"[collectPooledProducts:start]DB Error",
				"err":err
			};
			process.emit("Shopstyx:logError",msg);
		}
	});
};

function saveAll(user_id,pooledProducts,new_simple_date,new_unix_date)
{
	//make sure all data in pooledProducts are numbers
	var holder = [];
	var bulk = GLOBAL.mongodb.pooledProducts.initializeUnorderedBulkOp();
	var z=0;
	for(var x = 0;x<pooledProducts.length;x++)
	{
		var bulk_save=true;
		if(isNaN(pooledProducts[x])!=true)
		{
			
			//compare holder[x] with the other numbers
			for(var y=0;y<holder.length;y++)
			{
				if(holder[z]==holder[y])
				{
					bulk_save=false;
					break;
				}
			}
			if(bulk_save==true)
			{
				holder[z]=parseInt(pooledProducts[x]);
				var dataQuery = {
					"user_id":parseInt(user_id),
					"type":"pooled product history",
					"date_simple":new_simple_date,
					"date_unix":new_unix_date,
					"product_id":parseInt(pooledProducts[x])
				};
				bulk.insert(dataQuery);
				//savePool(dataQuery);
			}
		}		
	}
	bulk.execute(function (err, res) {
	  //console.log('saved pooled product history!')
	});
	var summaryData = {
		"user_id":parseInt(user_id),
		"type":"pooled product summary",
		"date_simple":new_simple_date,
		"date_unix":new_unix_date,
		"data":{
			"product_ids":holder,
			"current_count":parseInt(holder.length)
		}
	};
	GLOBAL.mongodb.pooledProducts.insert(summaryData,function(err,returnData){
		if(err!=null)
		{
			var msg = {
				"success":false, "status":"Error",
				"desc":"[collectPooledProducts:saveAll]DB Error",
				"err":err,
				"query":summaryData
			};
			process.emit('Shopstyx:emailError',msg);
		}
	});
}

function savePool(query)
{
	GLOBAL.mongodb.pooledProducts.insert(query,function(err,returnData){
		//nevermind lang gud
	});
}
function saveSelected(user_id,pooledProducts,doc,new_simple_date,new_unix_date)
{
	//compare pooledProducts with the existing document product_ids
	var holder = [];
	var z = 0;
	//var bulk = GLOBAL.mongodb.pooledProducts.initializeUnorderedBulkOp();
	for(var x=0;x<pooledProducts.length;x++)
	{
		var found_duplicate=false;
		for(var y=0;y<doc.data.product_ids.length;y++)
		{
			if(parseInt(pooledProducts[x])==doc.data.product_ids[y])
			{
				found_duplicate=true;
				break;
			}
		}
		if(found_duplicate==false)
		{
			doc.data.product_ids.push(parseInt(pooledProducts[x]));
			var dataQuery = {
				"user_id":parseInt(user_id),
				"type":"pooled product history",
				"date_simple":new_simple_date,
				"date_unix":new_unix_date,
				"product_id":parseInt(pooledProducts[x])
			};
			savePool(dataQuery);
			//bulk.insert(dataQuery);
		}
	}
	// bulk.execute(function (err, res) {
	//   //console.log('saved selected!')
	// });
	var update_query = {
		"_id":mongojs.ObjectId((doc._id).toString())
	};
	delete doc._id;

	GLOBAL.mongodb.pooledProducts.update(update_query,doc,function(err,returnData){
		if(err!=null)
		{
			var msg = {
				"success":false, "status":"Error",
				"desc":"[collectPooledProducts:saveSelected]DB Error",
				"err":err,
				"query":doc,
				"additional_infomation":update_query
			};
			process.emit('Shopstyx:emailError',msg);
		}
	});
}

module.exports={
	start:start
};