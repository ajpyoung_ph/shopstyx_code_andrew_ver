db.styxRecords.aggregate(
  {$match:{"product_pool":{$in:[1, 2,6]}}}, // filter to the ones that match
  {$unwind:"$product_pool"}, // unwinds the array so we can match the items individually
  {$group: { // groups the array back, but adds a count for the number of matches
    _id:"$_id", 
    matches:{
      $sum:{
        $cond:[
          {$eq:["$product_pool", 1]}, 
          1, 
          {$cond:[
            {$eq:["$product_pool", 2]}, 
            1, 
            {$cond:[
              {$eq:["$product_pool", 6]}, 
              1, 
              0
              ]
            }
            ]
          }
          ]
        }
      }, 
    uuid_v1:{$first:"$uuid_v1"}, 
    "product_pool":{$push:"$product_pool"}
    }
  }, 
  {$sort:{matches:-1}}, // sorts by the number of matches descending
  {$project:{matches:1, array:{uuid_v1:"$uuid_v1", product_pool:"$product_pool"}}} // rebuilds the original structure
);