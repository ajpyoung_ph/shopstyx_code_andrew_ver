const util = require('util');
const updateProgress = require(__dirname+'/updateProgress');

/*
db.collection.update(
   <query>,
   <update>,
   {
     upsert: <boolean>,
     multi: <boolean>,
     writeConcern: <document>
   }
)
*/
const start = function(uuid_v1,docs,dataStruct)
{
	var query = {
		"uuid_v1":uuid_v1
	};
	var setQ = {
		"$set":{
			"related_advertisement.data":docs
		}
	};
	var options = {
		upsert: false,
		multi: false
	};
	GLOBAL.mongodb.styxRecords.update(query,setQ,options,function(err,retVal){
		if(err!=null)
		{
			//console.log("ERROR updating process");
			//console.log("err : "+err);
			//console.log("query : "+query);
			//console.log("set : "+setQ);
			if(util.isNullOrUndefined(err.stack)==false)
			{
				//console.log("err.stack : "+err.stack);
			}
		}else{
			updateProgress.start(uuid_v1,'ready',dataStruct);
		}
	});
	updatePooledProductsOLDData(uuid_v1,docs,dataStruct);
	updatePooledProductsNEWData(uuid_v1,docs,dataStruct);
};

const updatePooledProductsOLDData = function(uuid_v1,docs,dataStruct)
{
	var query = {
		"additional_info.old_data.uuid_v1":uuid_v1
	};
	var setQ = {
		"$set":{
			"additional_info.old_data.related_advertisement.data":docs
		}
	};
	var options = {
		upsert: false,
		multi: false
	};
	GLOBAL.mongodb.pooledProducts.update(query,setQ,options,function(err,retVal){
		if(err!=null)
		{
			//console.log("ERROR updating process");
			//console.log("err : "+err);
			//console.log("query : "+query);
			//console.log("set : "+setQ);
			if(util.isNullOrUndefined(err.stack)==false)
			{
				//console.log("err.stack : "+err.stack);
			}
		}else{
			updateProgress.startOLD(uuid_v1,'ready',dataStruct);
		}
	});
};
const updatePooledProductsNEWData = function(uuid_v1,docs,dataStruct)
{
	var query = {
		"additional_info.new_data.uuid_v1":uuid_v1
	};
	var setQ = {
		"$set":{
			"additional_info.new_data.related_advertisement.data":docs
		}
	};
	var options = {
		upsert: false,
		multi: false
	};
	GLOBAL.mongodb.pooledProducts.update(query,setQ,options,function(err,retVal){
		if(err!=null)
		{
			//console.log("ERROR updating process");
			//console.log("err : "+err);
			//console.log("query : "+query);
			//console.log("set : "+setQ);
			if(util.isNullOrUndefined(err.stack)==false)
			{
				//console.log("err.stack : "+err.stack);
			}
		}else{
			updateProgress.startNEW(uuid_v1,'ready',dataStruct);
		}
	});
}

module.exports={
	start:start,
	updatePooledProductsOLDData:updatePooledProductsOLDData,
	updatePooledProductsNEWData:updatePooledProductsNEWData
};