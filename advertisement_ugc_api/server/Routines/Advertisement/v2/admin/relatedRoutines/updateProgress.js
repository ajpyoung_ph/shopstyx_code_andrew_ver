const util = require('util');

/*
db.collection.update(
   <query>,
   <update>,
   {
     upsert: <boolean>,
     multi: <boolean>,
     writeConcern: <document>
   }
)
*/
const start = function(uuid_v1,status_msg,dataStruct)
{
	var query = {
		"uuid_v1":uuid_v1
	};
	var setQ = {
		"$set":{
			"related_advertisement.progress":status_msg,//"processing","ready"
			"related_advertisement.progress_date_simple":dataStruct.new_simple_timestamp,
			"related_advertisement.progress_date_unix":dataStruct.new_unix_timestamp
		}
	};
	var options = {
		upsert: false,
		multi: false
	};
	GLOBAL.mongodb.styxRecords.update(query,setQ,options,function(err,retVal){
		if(err!=null)
		{
			//console.log("ERROR updating process");
			//console.log("err : "+err);
			//console.log("query : "+query);
			//console.log("set : "+setQ);
			if(util.isNullOrUndefined(err.stack)==false)
			{
				//console.log("err.stack : "+err.stack);
			}
		}
	});
};

const startOLD = function(uuid_v1,status_msg,dataStruct)
{
	var query = {
		"additional_info.old_data.uuid_v1":uuid_v1
	};
	var setQ = {
		"$set":{
			"additional_info.old_data.related_advertisement.progress":status_msg,//"processing","ready"
			"additional_info.old_data.related_advertisement.progress_date_simple":dataStruct.new_simple_timestamp,
			"additional_info.old_data.related_advertisement.progress_date_unix":dataStruct.new_unix_timestamp
		}
	};
	var options = {
		upsert: false,
		multi: false
	};
	GLOBAL.mongodb.styxRecords.update(query,setQ,options,function(err,retVal){
		if(err!=null)
		{
			//console.log("ERROR updating process");
			//console.log("err : "+err);
			//console.log("query : "+query);
			//console.log("set : "+setQ);
			if(util.isNullOrUndefined(err.stack)==false)
			{
				//console.log("err.stack : "+err.stack);
			}
		}
	});
};

const startNEW = function(uuid_v1,status_msg,dataStruct)
{
	var query = {
		"additional_info.new_data.uuid_v1":uuid_v1
	};
	var setQ = {
		"$set":{
			"additional_info.new_data.related_advertisement.progress":status_msg,//"processing","ready"
			"additional_info.new_data.related_advertisement.progress_date_simple":dataStruct.new_simple_timestamp,
			"additional_info.new_data.related_advertisement.progress_date_unix":dataStruct.new_unix_timestamp
		}
	};
	var options = {
		upsert: false,
		multi: false
	};
	GLOBAL.mongodb.styxRecords.update(query,setQ,options,function(err,retVal){
		if(err!=null)
		{
			//console.log("ERROR updating process");
			//console.log("err : "+err);
			//console.log("query : "+query);
			//console.log("set : "+setQ);
			if(util.isNullOrUndefined(err.stack)==false)
			{
				//console.log("err.stack : "+err.stack);
			}
		}
	});
};

module.exports={
	start:start,
	startOLD:startOLD,
	startNEW:startNEW
};