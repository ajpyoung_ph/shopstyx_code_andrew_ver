const util = require('util');
const updateProgress = require(__dirname+'/updateProgress');
const attachRelatedAdvertisement = require(__dirname+'/attachRelatedAdvertisement');
const respond = require(GLOBAL.server_location+'/EventListeners/errorListener');

const start = function(dataJSON,dataStruct,res,req)
{
  var msg = {
    "success":true, "status":"Success",
    "desc":"[relatedProcess:start]Process activated"
  };
  respond.respondError(msg,res,req);
  robotStart(dataJSON.request.styx_id,dataJSON.request.product_pool,dataStruct);
};

const robotStart = function(uuid_v1,product_pool,dataStruct)
{
  //check if uuid_v1 has related_advertisement.progress="progress" and if date is less than current date -10 days
  var query = {
    "uuid_v1":uuid_v1
  };
  GLOBAL.mongodb.styxRecords.findOne(query,function(err,doc){
    if(err==null)
    {
      if(doc!=null)
      {
        //check related_advertisement.progress value and the date
        if(util.isNullOrUndefined(doc.related_advertisement)==true)
        {
          //meaning no related_advertisement has been made
          startProcess(uuid_v1,product_pool,dataStruct);
        }else if(util.isNullOrUndefined(doc.related_advertisement.progress)==true){
          //meaning no progress is made
          startProcess(uuid_v1,product_pool,dataStruct);
        }else{
          if(doc.related_advertisement.progress=='progress')
          {
            //check the date
            var diffinsecods = dataStruct.new_unix_timestamp - doc.related_advertisement.progress_date_unix;   //86400 = 24 hours
            if(diffinseconds>86400)
            {
              startProcess(uuid_v1,product_pool,dataStruct);
            }else{
              //do nothing since it's not 24 hours later (maybe something happened and it's still in progress)
            }
          }else if(doc.related_advertisement.progress=='ready'){
            //check the date
            var diffinsecods = dataStruct.new_unix_timestamp - doc.related_advertisement.progress_date_unix;   //604800 = 1 week or 7 days
            if(diffinseconds>604800)
            {
              startProcess(uuid_v1,product_pool,dataStruct);
            }else{
              //do nothing since it's not 1 week later (maybe something happened and it's still in progress)
            }
          }else{
            //since the progress value is not within the required parameters then we trigger the process
            startProcess(uuid_v1,product_pool,dataStruct);
          }
        }
      }
    }else{
      var msg = {
        "success":false, "status":"Error",
        "desc":"[relatedProcess:robotStart] error in MongoDB read",
        "err":err
      };
      respond.logError(msg);
    }
  });
}
function startProcess(uuid_v1,product_pool,dataStruct)
{
  updateProgress.start(uuid_v1,"progress",dataStruct);
	var query = [{"$match":{"product_pool":{"$in":product_pool}}}, 
	  {"$unwind":"$product_pool"}, 
	  {"$group": { 
	    _id:"$_id", 
	    matches:{
	      "$sum":sumGenerator(product_pool)
	      }, 
	    "uuid_v1":{"$first":"$uuid_v1"}, 
	    "product_pool":{"$push":"product_pool"}
	    }
	  },
	  {"$sort":{"matches":-1}},
	  {"$limit":20},  
	  {"$project":{"matches":1, "array":{"uuid_v1":"$uuid_v1", "product_ids":"$product_ids"}}}];

    GLOBAL.mongodb.styxRecords.aggregate(query,function(err,docs){
      if(err==null)
      {
        attachRelatedAdvertisement.start(uuid_v1,docs,dataStruct);
      }
    });

}

function sumGenerator(product_pool)
{
	var startVar = '';
	var endVar = '';
	for(var x=0;x<product_pool.length;x++)
	{
		startVar=startVar + '{"$cond":[{"$eq":["$product_pool", '+product_pool[x]+']},1,';
		endVar=endVar + ']}'; 
	}
	startVar = startVar +'0'+endVar;
	return JSON.parse(startVar);
	/*
	{"$cond":[{"$eq":["$product_ids", 1]},1, 
		{"$cond":[{"$eq":["$product_ids", 5]},1, 
			{"$cond":[{"$eq":["$product_ids", 3]},1,0
			]}
		]}
	]}
	*/
}
module.exports={
	start:start,
  robotStart:robotStart
};

/*
db.TestCollection.aggregate(
  {$match:{"product_ids":{$in:[1,5,3]}}}, // filter to the ones that match
  {$unwind:"$product_ids"}, // unwinds the array so we can match the items individually
  {$group: { // groups the array back, but adds a count for the number of matches
    _id:"$_id", 
    matches:{
      $sum:{
        $cond:[
          {
          	$eq:["$product_ids", 1]
          },1, 
          {
          	$cond:[
            	{
            		$eq:["$product_ids", 5]
            	},1, 
            {
            	$cond:[
              		{
              			$eq:["$product_ids", 3]
              		},1,0
              	]
            }
            ]
          }
          ]
        }
      }, 
    item:{$first:"$prod_id"}, 
    "product_ids":{$push:"$product_ids"}
    }
  },
  {$sort:{matches:-1}}, // sorts by the number of matches descending
  {$skip:0},
  {$limit:2},  
  {$project:{matches:1, array:{"prod_id":"$item", "product_ids":"$product_ids"}}} // rebuilds the original structure
);
*/