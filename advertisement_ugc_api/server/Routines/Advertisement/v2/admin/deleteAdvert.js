
const util = require('util');
const respond = require(GLOBAL.server_location+'/EventListeners/errorListener');
const mongojs = require('mongojs');
const reportMsg = require(__dirname+'/reportToStories/reportMsg');

var start = function(size_array,dataJSON, dataStruct,my_uuid,req,res)
{
	//get record
	//identify styx type
		//pull out cdns if type V and type P
		//have Clint's API delete the files
	//then just delete records MongoDB and MySQL
	////console.log("execute start find advert for delete");
	var hash_id = dataJSON.request.styx_id.toString();
	////console.log(typeof(hash_id));
	var query = {
		"uuid_v1":hash_id
	};
	////console.log(typeof(query._id));
	GLOBAL.mongodb.styxRecords.findOne(query,function(err,doc){
		if(err==null)
		{
			if(doc!=null)
			{
				////console.log("found record");
				
				if(typeof(doc)=='object')
				{
					////console.log("going to try");
					try{
						switch(doc.styx_type.toUpperCase().replace(/\s/g, ''))
						{
							case "V":
							case "P":
								////console.log("getting media");
								getMedia(hash_id,doc,dataJSON,dataStruct,req,res);
								break;
							default:
								//no image uploaded
								//delete records
								////console.log("deleting record");
								deleteRecords(hash_id,doc,dataJSON,dataStruct,req,res);
								break;
						}
					}catch(err2){
						var msg = {
							"success":false, "status":"Error",
							"desc":"[deleteAdvert:start]Error Reading Record",
							"err":err2,
							"doc":doc,
							"query":query
						};
						respond.respondError(msg,res,req);
					}

				}
			}else{
				var msg = {
					"success":false, "status":"Error",
					"desc":"[deleteAdvert:start]Request Invalid",
					"query":query
				};
				respond.respondError(msg,res,req);
			}
			
		}else{
			var msg = {
				"success":false, "status":"Error",
				"desc":"[deleteAdvert:start]No valid request"
			};
			respond.respondError(msg,res,req);
		}
	});
};


var getMedia=function(hash_id,doc,dataJSON,dataStruct,req,res)
{
	var cdn = [];
	var temp_array = [];
	switch(doc.styx_type.toUpperCase().replace(/\s/g, ''))
	{
		case 'V':
			//just src and prevpic
			temp_array=doc.src.split('/');
			cdn.push(temp_array[temp_array.length-1]);
			temp_array=[];
			temp_array=doc.prevpic.split('/');
			cdn.push(temp_array[temp_array.length-1]);
			
			deleteRecords(hash_id,doc,dataJSON,dataStruct,req,res);
			deleteMedia(cdn,0);
			break;
		case 'P':
			//just src and src_array
			// //console.log('src_array');
			// //console.log(doc.src_array);
			temp_array = doc.src.split(doc.src_array[0]+"_");
			// //console.log('temp_array');
			// //console.log(temp_array);
			var filename = temp_array[temp_array.length-1];
			// //console.log('filename : '+filename);
			// //console.log('length : '+doc.src_array.length);
			for(var x=0;x<doc.src_array.length;x++)
			{
				// //console.log("x :"+x);
				// //console.log("max : "+(parseInt(doc.src_array.length)-1));
				////console.log(parseInt(x)+" : "+parseInt(doc.src_array.length)-1);
				cdn.push(doc.src_array[x]+'_'+filename);
				////console.log(cdn);
			}
			deleteRecords(hash_id,doc,dataJSON,dataStruct,req,res);
			// //console.log('CDN');
			// //console.log(cdn);
			deleteMedia(cdn,0);
			break;
		default:
			// var msg = {
			// 	"success":false, "status":"Error",
			// 	"desc":"[deleteAdvert:getMedia]error in delete media process"
			// };
			// process.emit('Shopstyx:logError',msg);
			break;
	}	
}

function deleteMedia(cdn,x)
{
	//cdn is an array
	//insert Clint's delete media API here
	var gcs = GLOBAL.gcloud.storage();
	var bucket = gcs.bucket(GLOBAL.storage_names.advertisements);
	//var file = bucket.file('images/'+targetValue+'_'+filename);
	//target delete file : 266b38c7-65b8-4c1f-8625-678d1e2d07bf-original.jpeg
	var file = bucket.file('images/'+cdn[x]);
	//console.log("trying ot delete : "+'images/'+cdn[x]);


	file.delete(function(err, apiResponse) {
		if(err==null)
		{
			//console.log("delete success for file : "+'images/'+cdn[x]);
		}else{
			//console.log("Error :");
			//console.log(err);
			/*
			{ 
				[ApiError: Not Found]
				errors: [ { domain: 'global', reason: 'notFound', message: 'Not Found' } ],
				code: 404,
				message: 'Not Found',
				response: undefined 
			}

			*/
		}
		if(x<cdn.length)
		{
			x=x+1;
			deleteMedia(cdn,x);
		}
	});
}

function deleteRecords(hash_id,doc,dataJSON,dataStruct,req,res)
{
	if(typeof(dataStruct.fromUpdate)=='undefined')
	{
		//deleteMySQL(hash_id,dataStruct,req,res);
		deleteMongoDB(hash_id,dataJSON,dataStruct,req,res);
	}
}

function deleteMySQL(hash_id,dataStruct,req,res)
{
	/*
	DELETE [LOW_PRIORITY] [QUICK] [IGNORE] FROM tbl_name
    [PARTITION (partition_name,...)]
    [WHERE where_condition]
    [ORDER BY ...]
    [LIMIT row_count]
	*/
	var query = 'DELETE FROM `stykable_advertisement_info_hash` WHERE `advertisement_datastore_id`="'+hash_id+'";';
	var mysql = new GLOBAL.mysql.mysql_connect(query);
	mysql.results_ss_common().then(function(results){
		if(util.isError(results)==true){
			var msg = {
				"success":false, "status":"Error",
				'desc':'[deleteAdvert:deleteMySQL]DB Error Connection',
				'query':query,
				'message':results.message,
				'stack':results.stack||null
			};
			process.emit('Shopstyx:logError',msg);
		}else{
			var msg = {
				'status':'Success',
				'desc':'[deleteAdvert:deleteMySQL] Record deleted'
			};
			respond.respondError(msg,res,req);
		}
	});
}
function deleteMongoDB(hash_id,dataJSON,dataStruct,req,res)
{
	var query = {
		'uuid_v1':hash_id
	};
	var options = {
		justOne: true
	};
	GLOBAL.mongodb.styxRecords.findOne(query,function(err,results){
		if(err==null)
		{
			dataJSON['reportForm']=[];
            dataJSON['reportForm']['type']=[];
            dataJSON['reportForm']['action']=[];
            dataJSON['reportForm']['to']=[];
            dataJSON.reportForm.type='profile';
            dataJSON.reportForm.action='delete styx';
            dataJSON.reportForm.to.push(parseInt(results.owner));
			reportMsg.getRobotApiKey(results,dataJSON,dataStruct);
		}
		GLOBAL.mongodb.styxRecords.remove(query,options,function(err,results){
			if(err!=null)
			{
				//console.log("MongoDB Error");
				//console.log(err);
				
			}else{
				var msg = {
						'status':'Success',
						'desc':'[deleteAdvert:deleteMongoDB] Record deleted'
					};
					respond.respondError(msg,res,req);
			}
		});
	});
}

module.exports={
	start:start,
	getMedia:getMedia
};