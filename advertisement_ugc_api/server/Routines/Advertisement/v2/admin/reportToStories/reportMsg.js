const util = require('util');
const request = require('request');
const respond = require(GLOBAL.server_location+'/EventListeners/errorListener');

var findAdOwner=function(doc,dataJSON,dataStruct)
{

    var query = {
        "uuid_v1":dataJSON.request.styx_id
    };
    GLOBAL.mongodb.styxRecords.findOne(query,function(err,resultData){
        if(err==null)
        {
            dataJSON.reportForm.to=[];
            dataJSON.reportForm.to.push=parseInt(dataJSON.request.owner_id);
            dataJSON.reportForm.to.push=parseInt(resultData.owner);
            getRobotApiKey(doc,dataJSON,dataStruct);
            var copy = JSON.parse(JSON.stringify(dataJSON));
            delete copy.reportForm.to;
            copy.reportForm.type='stykable';
            copy.reportForm.styx_id=dataJSON.request.styx_id;
            getRobotApiKey(doc,copy,dataStruct);
        }else{
            var msg = {
                "success":false, "status":"Error",
                "desc":"[likeAds:findAdOwner]Error in DB",
                "err":err,
                "query":query
            };
            respond.logError(msg);
        }
    });
}

var findProdOwner=function(doc,dataJSON,dataStruct)
{
    var query = "SELECT * FROM `products` WHERE `product_id`="+parseInt(dataJSON.request.product_id)+" LIMIT 1;";
    var mysql = new GLOBAL.mysql.mysql_connect(query);
    mysql.results_ss_common().then(function(results){
        if(util.isError(results)==true){
            var msg = {
                "success":false, "status":"Error",
                'desc':'[likeProduct:findProdOwner]DB Error Connection',
                'query':query,
                'message':results.message,
                'stack':results.stack||null
            };
            process.emit('Shopstyx:logError',msg);
        }else{
            dataJSON.reportForm.to=[];
            dataJSON.reportForm.to.push(parseInt(dataJSON.request.owner_id));
            dataJSON.reportForm.to.push(parseInt(results[0].owner_user_id));
            getRobotApiKey(doc,dataJSON,dataStruct);
        }
    });
}

var getRobotApiKey = function(dataSavedToMongoDB,dataJSON,dataStruct)
{
    var query = 'SELECT * FROM `default_settings` WHERE `group`="api" AND `key`="webservice_signature";';

    var mysql = new GLOBAL.mysql.mysql_connect(query);
    mysql.results_ss_common().then(function(results){
        if(util.isError(results)==true){
            var msg = {
                "success":false, "status":"Error",
                'desc':'[callCreateStyx:start]DB Error Connection',
                'query':query,
                'message':results.message,
                'stack':results.stack||null
            };
            respond.logError(msg);
        }else{
            var resultDoc = results;
            var key = resultDoc[0].value;
            reportMsgProfile(key,dataSavedToMongoDB,dataJSON,dataStruct,0);
        }
    });
}

function reportMsgProfile(key,dataSavedToMongoDB,dataJSON,dataStruct,x)
{
    if( util.isNullOrUndefined(dataJSON.reportForm.to)==false)
    {
        var myForm = {
            "form":{
                "key":key,
                "type":dataJSON.reportForm.type,
                "action":dataJSON.reportForm.action,
                "message":dataSavedToMongoDB
            }
        };
        myForm['form']['to']=[];
        myForm.form.to=JSON.stringify(dataJSON.reportForm.to);
        //console.log(myForm);
        request.post(GLOBAL.HTTP_location+'/api/v2/stories/add_stories', myForm, function(err,httpResponse,body){
            try{
                var sentData = '';
                ////console.log("{{{{{{{{{{{{{{{{{{{{{{{ Return FROM /api/v2/notifications/createstyx }}}}}}}}}}}}}}");
                ////console.log(body);
                if(typeof(body)=='string')
                {
                    sentData = JSON.parse(body);
                }else{
                    if(typeof(body)=='object')
                    {
                        sentData = JSON.parse(JSON.stringify(body));
                    }
                }
                if(sentData.success==false)
                {
                    //don't do anything yet
                    x=x+1;
                    if(x>10)
                    {
                        var msg = {
                            "success":false, "status":"Error",
                            "desc":"[reportMsg:reportMsgProfile] Clint's API keep saying error"
                        };
                        respond.logError(msg);
                    }else{
                        //console.log("Error in API");
                        //console.log(sentData);
                        setTimeout(reportMsgProfile,2000,key,dataSavedToMongoDB,dataJSON,dataStruct,x);
                    }
                }
             }catch(error_here){
                var msg = {
                    "success":false, "status":"Error",
                    "desc":"[reportMsg:reportMsgProfile] Error occurred code 1",
                    "err":error_here,
                    "stack":error_here.stack,
                    "body":body,
                    "myForm":myForm||"No Form sent"
                };
                respond.logError(msg);
             }
        });
    }
    if(util.isNullOrUndefined(dataJSON.reportForm.styx_id)==false)
    {
        reportMsgStyx(key,dataSavedToMongoDB,dataJSON,dataStruct,0);
    }
}

function reportMsgStyx(key,dataSavedToMongoDB,dataJSON,dataStruct,x)
{
    var myForm = {
        "form":{
            "key":key,
            "type":dataJSON.reportForm.type,
            "action":dataJSON.reportForm.action,
            "message":dataSavedToMongoDB,
            "styx_id":dataJSON.reportForm.styx_id
        }
    };
    console.log(myForm);
    request.post(GLOBAL.HTTP_location+'/api/v2/stories/add_stories', myform, function(err,httpResponse,body){
        try{
            var sentData = '';
            ////console.log("{{{{{{{{{{{{{{{{{{{{{{{ Return FROM /api/v2/notifications/createstyx }}}}}}}}}}}}}}");
            ////console.log(body);
            if(typeof(body)=='string')
            {
                sentData = JSON.parse(body);
            }else{
                if(typeof(body)=='object')
                {
                    sentData = JSON.parse(JSON.stringify(body));
                }
            }
            if(sentData.success==false)
            {
                //don't do anything yet
                x=x+1;
                if(x>10)
                {
                    var msg = {
                        "success":false, "status":"Error",
                        "desc":"[reportMsg:reportMsgProfile] Clint's API keep saying error"
                    };
                    respond.logError(msg);
                }else{
                    setTimeout(reportMsgStyx,2000,key,dataSavedToMongoDB,dataJSON,dataStruct,x);
                }
            }
         }catch(error_here){
            var msg = {
                "success":false, "status":"Error",
                "desc":"[callCreateStyx:start] Error occurred code 1",
                "err":error_here,
                "stack":error_here.stack
            };
            respond.logError(msg);
         }
    });
}

module.exports={
    getRobotApiKey:getRobotApiKey,
    findAdOwner:findAdOwner,
    findProdOwner:findProdOwner
};