
const respond = require(GLOBAL.server_location+'/EventListeners/errorListener');
const textMan = require(GLOBAL.server_location+'/helpers/text_manipulation');
const uuid = require('uuid');
const mysqlStuff = require(GLOBAL.server_location+'/helpers/mysql_convertions');
const collectPooledProducts = require(__dirname+'/collectionRoutines/collectPooledProducts');
const parseUri = require(GLOBAL.server_location+'/helpers/parseUri');
const relatedProcess = require(__dirname+'/relatedRoutines/relatedProcess');
const callCreateStyx = require(__dirname+'/callCreateStyx');
const util = require('util');
const reportMsg = require(__dirname+'/reportToStories/reportMsg');
const saveUploadProcess = require(__dirname+'/saveUploadProcess');

var start = function(size_array,dataJSON, dataStruct,my_uuid,req,res)
{
	var copy = JSON.parse(JSON.stringify(dataJSON.request.data));
	var files = req.files;
	//at this point the actual picture and videos are already uploaded to the system at the advertisementRoutine:processRequest location

	var styx_type = '';
	var error=false;
	var error_msg=[];
	////console.log("Getting filenames");
	if(util.isNullOrUndefined(dataJSON.request.data.styx_type)==false)
	{
		styx_type = dataJSON.request.data.styx_type.toLowerCase();
		dataJSON['prevpic_url'] = [];
		dataJSON['src_url'] = [];
		var product_ids=[];
		//console.log("processing types");
		switch(styx_type.toString())
		{
			case 'v'://videostyx
				dataJSON['prevpic_url'] = [];
				if(util.isNullOrUndefined(files)==false)
				{
					if(util.isNullOrUndefined(files.prevpic)==false)
					{
						var prevpic_extension = textMan.getFileExtenstion(files.prevpic[0].originalname);
						var prevpic_filename = 'prev_pic_'+my_uuid+'.'+prevpic_extension;
						copy.prevpic = GLOBAL.storage_url+GLOBAL.storage_names.advertisements+'/images/'+prevpic_filename;
						
						dataJSON.prevpic_url.push(copy.prevpic);
					}else{
						error=true;
						error_msg.push('No Video File Found');
					}
				}

				dataJSON['src_url'] = [];
				if(util.isNullOrUndefined(files)==false)
				{
					if(util.isNullOrUndefined(files.src)==false)
					{
						//copy.prevpic_array = size_array.slice();
						var src_extension = textMan.getFileExtenstion(files.src[0].originalname);
						var src_filename = 'src_'+my_uuid+'.'+src_extension;
						copy.src = GLOBAL.storage_url+GLOBAL.storage_names.advertisements+'/images/'+src_filename;
						
						dataJSON.src_url.push(copy.src);
					}else{
						error=true;
						error_msg.push('No Video File Found');
					}
				}else{
					error=true;
					error_msg.push('No Video File Found');
				}
				//collect product_ids
				break;
			case '3':
				dataJSON['prevpic_url'] = [];
				if(util.isNullOrUndefined(files)==false)
				{
					if(util.isNullOrUndefined(files.prevpic)==false)
					{
						var prevpic_extension = textMan.getFileExtenstion(files.prevpic[0].originalname);
						var prevpic_filename = 'prev_pic_'+my_uuid+'.'+prevpic_extension;
						copy.prevpic = GLOBAL.storage_url+GLOBAL.storage_names.advertisements+'/images/'+prevpic_filename;
						
						dataJSON.prevpic_url.push(copy.prevpic);
					}
				}
				break;
			case 'p'://photostyx
				dataJSON['prevpic_url'] = [];
				dataJSON['src_url'] = [];
				if(util.isNullOrUndefined(files)==false)
				{
					if(util.isNullOrUndefined(files.src)==false)
					{
						var src_extension = textMan.getFileExtenstion(files.src[0].originalname);
						var src_filename = 'src_'+my_uuid+'.'+src_extension;
						copy.src = GLOBAL.storage_url+GLOBAL.storage_names.advertisements+'/images/'+size_array[0]+'_'+src_filename;
						copy.src_array = size_array.slice();
						for(var x = 0;x<size_array.length;x++)
						{
							dataJSON.src_url.push(GLOBAL.storage_url+GLOBAL.storage_names.advertisements+'/images/'+size_array[x]+'_'+src_filename);
						}
					}else{
						error=true;
						error_msg.push('No Photos Found');
					}
				}else{
					error=true;
					error_msg.push('No Photos Found');
				}

				break;
			case 'c'://collage
				dataJSON['prevpic_url'] = [];
				dataJSON['src_url'] = [];
				var filename = '';
				//console.log("collage parsing");
				if(util.isNullOrUndefined(files)==false)
				{
					if(util.isNullOrUndefined(files.src)==false)
					{
						//console.log("processing different files");
						for(var x=0;x<files.src.length;x++)
						{
							for(var y=0;y<dataJSON.request.data.items.length;y++)
							{
								var pattern = new RegExp(files.src[x].originalname,'i');
								var ext_arr = files.src[x].mimetype.split('/');
								var ext = ext_arr[ext_arr.length-1];
								if(pattern.test(copy.request.data.items[y].cover_product_image)==true)
								{	
									filename = y+"_"+my_uuid+'.'+ext;
									copy.request.data.items[y].cover_product_image=GLOBAL.storage_url+GLOBAL.storage_names.advertisements+'/images/'+filename;
									break;
								}
							}
						}
					}
				}
				//console.log("finishing processing files");
				break;
			case 's':
				dataJSON['prevpic_url'] = [];
				dataJSON['src_url'] = [];
				//there are no files but 
				break;
			default:
				//no media to upload
				break;
		}
	}
	//collect product_ids
	//console.log("collecting items");
	//console.log(dataJSON);
	if(dataJSON.request.data.items.length>0)
	{
		//console.log("there are items");
		dataJSON.request.data.items.forEach((data)=>{
			//console.log(data.prod.product_id);
			product_ids.push(parseInt(data.prod.product_id));
		});
	}
	//console.log("product_ids");
	//console.log(product_ids);
	if(util.isNullOrUndefined(dataStruct.new_simple_timestamp)==true)
	{
		dataStruct.new_simple_timestamp = mysqlStuff.populateMySQLTimeStamp(dataStruct);
	}
	copy.date_updated = dataStruct.new_unix_timestamp;
	copy.date_updated_simple = dataStruct.new_simple_timestamp;
	copy['uuid_v1']=my_uuid.toString();
	//save pooled products
	if(util.isNullOrUndefined(copy.product_pool)==false)
	{
		collectPooledProducts.start(parseInt(copy.owner),copy.product_pool,copy.date_updated_simple,copy.date_updated);
	}
	
	////console.log("Storing Adstyx in MongoDB");
	if(error==false)
	{
		GLOBAL.mongodb.styxRecords.insert(copy,function(err,docsInserted){
			if(err==null)
			{
				var hash_id = '';
				if(util.isNullOrUndefined(docsInserted._id)==false)
				{
					hash_id = docsInserted._id.toString();
				}
				if(util.isNullOrUndefined(docsInserted[0])==false)
				{
					if(util.isNullOrUndefined(docsInserted[0]['_id'])==false)
					{
						hash_id = docsInserted[0]['_id'].toString();
					}
				}
				var msg = {
				    "success":true, "status": "Success",
				    "desc": "[saveAdvert:start]Data Saved",
				    "data":{
				        "styx_id":hash_id.toString(),
				        "uuid_v1":my_uuid.toString(),
				        "prevpic_url":dataJSON.prevpic_url,
				        "src_url":dataJSON.src_url,
				        "styx_type":styx_type
				    }
				};
				respond.respondError(msg,res,req);
				saveUploadProcess.updateStatus(my_uuid,'Ready');
				dataJSON['reportForm']=[];
	            dataJSON['reportForm']['type']=[];
	            dataJSON['reportForm']['action']=[];
	            dataJSON['reportForm']['to']=[];
	            dataJSON.reportForm.type='profile';
	            dataJSON.reportForm.action='create styx';
	            dataJSON.reportForm.to.push(parseInt(dataJSON.request.data.owner));
				reportMsg.getRobotApiKey(dataJSON.request.data,dataJSON,dataStruct);
				//call clint's /api/v2/notifications/createstyx
				setTimeout(callCreateStyx.start,60000,my_uuid.toString(),product_ids,0);//to call 2m later
				relatedProcess.robotStart(my_uuid.toString(),copy.product_pool,dataStruct);
				// var msg = {
				// 	"success":true, "status":"Success",
				// 	"desc":"[saveAdvert:start]Saved NoSQL Data",
				// 	"data":{
				//         "styx_id":hash_id
				//     }
				// };
				// process.emit('Shopstyx:logError',msg);
				// //save in MySQL
				// saveAdvertInMySQL(hash_id,dataJSON,dataStruct,my_uuid); //no need anymore since we're using MongoDB and therefore complex searching can be created
			}else{
				var msg = {
					"success":false, "status":"Error",
					"desc":"[saveAdvert:start]No valid request",
					"err":err
				};
				respond.respondError(msg,res,req);
			}
		});
	}else{
		var msg = {
			"success":false, "status":"Error",
			"desc":"[saveAdvert:start]Required Information Missing",
			"err":error_msg
		};
		respond.respondError(msg,res,req);
	}
	
};

function saveAdvertInMySQL(hash_id,dataJSON,dataStruct,uuid_v1,req,res)
{
	/*
	INSERT [LOW_PRIORITY | DELAYED | HIGH_PRIORITY] [IGNORE]
    [INTO] tbl_name
    [PARTITION (partition_name,...)]
    SET col_name={expr | DEFAULT}, ...
    [ ON DUPLICATE KEY UPDATE
      col_name=expr
        [, col_name=expr] ... ]
	*/
	////console.log("Saving Adstyx in MySQL");
	var query = 'INSERT INTO `stykable_advertisement_info_hash` SET `uuidv1`="'+uuid_v1+'", `advertisement_owner_user_id`='+parseInt(dataJSON.security.user_id)+', `advertisement_type`="'+dataJSON.request.data.styx_type.toUpperCase()+'", `advertisement_name`="'+dataJSON.request.data.styx_name+'", `advertisement_datastore_id`="'+hash_id+'";';
	var mysql = new GLOBAL.mysql.mysql_connect(query);
	mysql.results_ss_common().then(function(results){
		if(util.isError(results)==true){
			var msg = {
				"success":false, "status":"Error",
				'desc':'[saveAdvert:saveAdvertInMySQL]DB Error Connection',
				'query':query,
				'message':results.message,
				'stack':results.stack||null
			};
			process.emit('Shopstyx:logError',msg);
		}else{
			var msg = {
			    "success":true, "status": "Success",
			    "desc": "[saveAdvert:saveAdvertInMySQL]Data Saved",
			    "data":{
			        "styx_id":hash_id.toString(),
			        "uuid_v1":uuid_v1.toString(),
			        "prevpic_url":dataJSON.prevpic_url,
			        "src_url":dataJSON.src_url
			    }
			};
			respond.respondError(msg,res,req);
		}
	});
}

module.exports={
	start:start
};