
const respond = require(GLOBAL.server_location+'/EventListeners/errorListener');
const mongojs = require('mongojs');
//var deleteAdvert = require(__dirname+'/deleteAdvert');

const saveUploadProcess = require(__dirname+'/saveUploadProcess');
const reportUploadFinished = require(__dirname+'/reportUploadFinished');
const textMan = require(GLOBAL.server_location+'/helpers/text_manipulation');
const parseUri = require(GLOBAL.server_location+'/helpers/parseUri');
const relatedProcess = require(__dirname+'/relatedRoutines/relatedProcess');
const callCreateStyx = require(__dirname+'/callCreateStyx');
const util = require('util');
const reportMsg = require(__dirname+'/reportToStories/reportMsg');

var start = function(size_array,dataJSON, dataStruct,my_uuid,req,res)//size_array,dataJSON,dataStruct,my_uuid
{
	try{
		//copy is the new data
		var copy = JSON.parse(JSON.stringify(dataJSON.request.data));
		var old_uuid_v1 = copy['uuid_v1'].toString();
		copy['uuid_v1']=my_uuid.toString();
		//var hash_id = copy._id.toString();
		if(util.isNullOrUndefined(copy._id)==false)
		{
			delete copy._id;
		}
		var query = {
			'uuid_v1':old_uuid_v1
		};
		var product_ids=[];
		////console.log(query);
		saveUploadProcess.updateStatus_withOldInfo(my_uuid,'In Progress',null,copy);
		GLOBAL.mongodb.styxRecords.findOne(query,function(err,doc){
			if(err==null)
			{
				if(doc!=null)
				{
					//////console.log("found record");
					
					if(typeof(doc)=='object')
					{
						//////console.log("going to try");
						// try{
							var files = req.files;
							var styx_type = doc.styx_type.toUpperCase().replace(/\s/g, '');
							var files_found = false;
							dataJSON['prevpic_url'] = [];
							dataJSON['src_url'] = [];
							// if(typeof(files.src)!='undefined')
							// {
								
								switch(styx_type.toString())
								{
									case 'V'://videostyx
										dataJSON['prevpic_url'] = [];
										if(util.isNullOrUndefined(files)==false)
										{
											if(util.isNullOrUndefined(files.prevpic)==false)
											{
												var prevpic_extension = textMan.getFileExtenstion(files.prevpic[0].originalname);
												var prevpic_filename = 'prev_pic_'+my_uuid+'.'+prevpic_extension;
												copy.prevpic = GLOBAL.storage_url+GLOBAL.storage_names.advertisements+'/images/'+prevpic_filename;
												
												dataJSON.prevpic_url.push(copy.prevpic);
												files_found=true;
											}
										}else{
											//check if prevpic_url is a URL
											if(util.isNullOrUndefined(copy.prevpic))
											{
												if(copy.prevpic!='')
												{
													if(parseUri.isUrl(copy.prevpic))
													{
														dataJSON['prevpic_url'].push(copy.prevpic);
													}
												}
											}
										}
										//copy.prevpic_array = size_array.slice();
										dataJSON['src_url'] = [];
										if(util.isNullOrUndefined(files)==false)
										{
											if(util.isNullOrUndefined(files.src)==false)
											{
												var src_extension = textMan.getFileExtenstion(files.src[0].originalname);
												var src_filename = 'src_'+my_uuid+'.'+src_extension;
												copy.src = GLOBAL.storage_url+GLOBAL.storage_names.advertisements+'/images/'+src_filename;
												
												dataJSON.src_url.push(copy.src);
												files_found=true;
											}
										}else{
											//check if prevpic_url is a URL
											if(util.isNullOrUndefined(copy.src))
											{
												if(copy.src!='')
												{
													if(parseUri.isUrl(copy.src))
													{
														dataJSON['src_url'].push(copy.src);
													}
												}
											}
										}
										
										break;
									case '3':
										dataJSON['src_url'] = [];
										dataJSON['prevpic_url'] = [];
										if(util.isNullOrUndefined(files)==false)
										{
											if(util.isNullOrUndefined(files.prevpic)==false)
											{
												var prevpic_extension = textMan.getFileExtenstion(files.prevpic[0].originalname);
												var prevpic_filename = 'prev_pic_'+my_uuid+'.'+prevpic_extension;
												copy.prevpic = GLOBAL.storage_url+GLOBAL.storage_names.advertisements+'/images/'+prevpic_filename;
												
												dataJSON.prevpic_url.push(copy.prevpic);
												files_found=true;
											}
										}else{
											//check if prevpic_url is a URL
											if(util.isNullOrUndefined(copy.prevpic))
											{
												if(copy.prevpic!='')
												{
													if(parseUri.isUrl(copy.prevpic))
													{
														dataJSON['prevpic_url'].push(copy.prevpic);
													}
												}
											}
										}
										break;
									case 'P'://photostyx
										dataJSON['prevpic_url'] = [];
										dataJSON['src_url'] = [];
										if(util.isNullOrUndefined(files)==false)
										{
											if(util.isNullOrUndefined(files.src)==false)
											{
												////console.log("*********************** UPDATE FOUND FILES ********************************");
												var src_extension = textMan.getFileExtenstion(files.src[0].originalname);
												var src_filename = 'src_'+my_uuid+'.'+src_extension;
												copy.src = GLOBAL.storage_url+GLOBAL.storage_names.advertisements+'/images/'+size_array[0]+'_'+src_filename;
												copy.src_array = size_array.slice();
												files_found=true;
												for(var x = 0;x<size_array.length;x++)
												{
													dataJSON.src_url.push(GLOBAL.storage_url+GLOBAL.storage_names.advertisements+'/images/'+size_array[x]+'_'+src_filename);
												}
											}
										}else{
											//check if prevpic_url is a URL
											////console.log("*********************** UPDATE NO NEW FILES ********************************");
											
											if(util.isNullOrUndefined(copy.src)==false)
											{
												////console.log("*********************** SRC NOT NULL ********************************");
												if(copy.src!='')
												{
													////console.log("*********************** SRC NOT EMPTY ********************************");
													if(parseUri.isUrl(copy.src))
													{
														////console.log("*********************** SRC IS URL ********************************");
														dataJSON['src_url'].push(copy.src);
													}
												}
											}
										}
										
										break;
									case 'C'://collage
										var filename = '';
										if(util.isNullOrUndefined(files)==false)
										{
											if(util.isNullOrUndefined(files.src)==false)
											{
												files_found=true;
												for(var x=0;x<files.src.length;x++)
												{
													for(var y=0;y<dataJSON.request.data.items.length;y++)
													{
														var pattern = new RegExp(files.src[x].originalname,'i');
														var ext_arr = files.src[x].mimetype.split('/');
														var ext = ext_arr[ext_arr.length-1];
														if(pattern.test(copy.request.data.items[y].cover_product_image)==true)
														{	
															filename = y+"_"+my_uuid+'.'+ext;
															copy.request.data.items[y].cover_product_image=GLOBAL.storage_url+GLOBAL.storage_names.advertisements+'/images/'+filename;
															break;
														}
													}
												}
											}
										}
										
										break;
									default:
										//no media to upload
										break;
								}
							//}
							//waiting for upload to finish
							/*saveUploadProcess.updateStatus_withOldInfo(my_uuid,'Waiting to Switch places with old record',doc,copy);
							saveUploadProcess.updateStatus(doc.uuid_v1,'Waiting to Switch with new updated record');
							var msg = {
							    "success":true, "status": "Success",
							    "desc": "[updateAdvert:start]Data Saved and waiting for uploaded file",
							    "data":{
							        "uuid_v1_old":old_uuid_v1,
							        "uuid_v1_new":my_uuid.toString(),
							        "prevpic_url":dataJSON.prevpic_url,
							        "src_url":dataJSON.src_url,
							        "styx_type":styx_type
							    }
							};
							respond.respondError(msg,res,req);*/
							// if(files_found==false)
							// {
								/*reportUploadFinished.start(my_uuid,0);*/
							//}
							//setTimeout(callCreateStyx.start,120000,my_uuid.toString(),0);
							if(copy.items.length>0)
							{
								//console.log("there are items");
								copy.items.forEach((data)=>{
									//console.log(data.prod.product_id);
									product_ids.push(parseInt(data.prod.product_id));
								});
							}
							//console.log("product_ids");
							//console.log(product_ids);
							setTimeout(callCreateStyx.start,60000,old_uuid_v1.toString(),product_ids,0);
							relatedProcess.robotStart(my_uuid.toString(),copy.product_pool,dataStruct);
							updateAdvertInMongoDB(old_uuid_v1,copy,doc,dataJSON,dataStruct,res,req);
							// switch(styx_type)
							// {
							// 	case "V":
							// 	case "P":
							// 		//////console.log("getting media");
							// 		(updateAdvertInMongoDB,hash_id,dataJSON,dataStruct,copy,req,res);
							// 		dataStruct.fromUpdate = true;
							// 		(deleteAdvert.getMedia,hash_id,doc,dataJSON,dataStruct);
							// 		break;
							// 	default:
							// 		//no image uploaded
							// 		//delete records
							// 		//////console.log("deleting record");
							// 		(updateAdvertInMongoDB,hash_id,dataJSON,dataStruct,copy,req,res);
							// 		break;
							// }
						// }catch(err2){
						// 	var msg = {
						// 		"success":false, "status":"Error",
						// 		"desc":"[updateAdvert:start]Error Reading Record",
						// 		"err":err2,
						// 		"doc":doc,
						// 		"query":query
						// 	};
						// 	respond.respondError(msg,res,req);
						// }

					}
				}else{
					var msg = {
						"success":false, "status":"Error",
						"desc":"[updateAdvert:start]Request Invalid",
						'query':query
					};
					respond.respondError(msg,res,req);
				}
				
			}else{
				var msg = {
					"success":false, "status":"Error",
					"desc":"[updateAdvert:start]No valid request"
				};
				respond.respondError(msg,res,req);
			}
		});
	}catch(err){
		var msg = {
			"success":false, "status":"Error",
			"desc":"[updateAdvert:start]Fatal Error Occurred",
			"err":err,
			"stack":err.stack
		};
		respond.respondError(msg,res,req);
	}
}

function updateAdvertInMongoDB(target_uuid_v1,new_data,old_data,dataJSON,dataStruct,res,req)
{
	var styx_type = old_data.styx_type.toUpperCase().replace(/\s/g, '');
	var query = {
		"uuid_v1":target_uuid_v1
	};
	new_data.uuid_v1=target_uuid_v1;
	var set = {
		"$set":new_data
	};
	var options={
		upsert: false,
		multi: false
	};
	GLOBAL.mongodb.styxRecords.update(query,set,options,function(err,docs){
		if(err==null)
		{
			var msg = {
			    "success":true, "status": "Success",
			    "desc": "[updateAdvert:start]Data Saved and waiting for uploaded file",
			    "data":{
			        "uuid_v1_old":target_uuid_v1,
			        "uuid_v1_new":new_data.uuid_v1.toString(),
			        "prevpic_url":dataJSON.prevpic_url,
			        "src_url":dataJSON.src_url,
			        "styx_type":styx_type
			    }
			};
			saveUploadProcess.updateStatus(old_data.uuid_v1,'Ready');
			compareFiles(new_data,old_data);
			dataJSON['reportForm']=[];
            dataJSON['reportForm']['type']=[];
            dataJSON['reportForm']['action']=[];
            dataJSON['reportForm']['to']=[];
            dataJSON.reportForm.type='profile';
            dataJSON.reportForm.action='update styx';
            dataJSON.reportForm.to.push(parseInt(new_data.owner));
			reportMsg.getRobotApiKey(dataJSON.request.data,dataJSON,dataStruct);
			
		}else{
			var msg = {
			    "success":false, "status": "Error",
			    "desc": "[updateAdvert:start]Data is not saved",
			    "data":{
			        "uuid_v1_old":target_uuid_v1,
			        "uuid_v1_new":new_data.uuid_v1.toString(),
			        "prevpic_url":dataJSON.prevpic_url,
			        "src_url":dataJSON.src_url,
			        "styx_type":styx_type
			    }
			};
			saveUploadProcess.updateStatus(old_data.uuid_v1,'Ready');
		}
		respond.respondError(msg,res,req);
	});
}

function compareFiles(new_data,old_data)
{
	switch(old_data.styx_type.toLowerCase().replace(/\s/g, ''))
	{
		case'v':
			//assume prevpic and src
			//console.log("deleting VideoStyx media");
			schedule_delete_prevpic(new_data,old_data);
			schedule_delete_src(new_data,old_data);
			break;
		case 'p':
			//assume src only
			//console.log("deleting Photostyx image");
			schedule_delete_src(new_data,old_data);
			break;
		case 'c':
			//console.log("deleting collage images");
			schedule_delete_cover_product_image(new_data,old_data);
			break;
		default:
			//make switch and delete old record
			break;
	}
}

function schedule_delete_prevpic(new_data,old_data)
{
	if(old_data.prevpic!=new_data.prevpic)
	{
		var arrayHolder = old_data.prevpic.split('/');
		var filename = arrayHolder[arrayHolder.length-1];
		// var query = {
		// 	'filename':filename,
		// 	'date_simple':dataStruct.new_simple_timestamp,
		// 	'date_unix':dataStruct.new_unix_timestamp
		// };
		// GLOBAL.mongodb.filesThatNeedDeletion.insert(query,function(err,retVal){
		// 	//don't care here
		// });

		//delete filename here
		var cdn = [filename];
		deleteMedia(cdn,0);
	}
}

function schedule_delete_src(new_data,old_data)
{
	if(old_data.src!=new_data.src)
	{
		var cdn=[];
		var arrayHolder = old_data.src.split('/');
		var filename = arrayHolder[arrayHolder.length-1];
		if(util.isNullOrUndefined(old_data.src_array)==false)
		{
			var src_array=[];
			if(typeof(old_data.src_array)=='string')
			{
				src_array=old_data.src_array.split(',');
			}
			if(typeof(old_data.src_array)=='object')
			{
				src_array=old_data.src_array;
			}
			var prefix_holder = filename.split(src_array[0]+'_');
			filename=prefix_holder[prefix_holder.length-1];
			
			for(var x=0;x<src_array.length;x++)
			{
				// var query = {
				// 	'filename':src_array[x]+'_'+filename,
				// 	'date_simple':dataStruct.new_simple_timestamp,
				// 	'date_unix':dataStruct.new_unix_timestamp
				// };
				// GLOBAL.mongodb.filesThatNeedDeletion.insert(query,function(err,retVal){
				// 	//don't care here
				// });
				cdn.push(src_array[x]+'_'+filename);
			}
		}else{
			// var query = {
			// 	'filename':filename,
			// 	'date_simple':dataStruct.new_simple_timestamp,
			// 	'date_unix':dataStruct.new_unix_timestamp
			// };
			// GLOBAL.mongodb.filesThatNeedDeletion.insert(query,function(err,retVal){
			// 	//don't care here
			// });
			var cdn=[filename];
		}
		//console.log("Photostyx delete list:");
		//console.log(cdn);
		deleteMedia(cdn,0);
	}
}

function schedule_delete_cover_product_image(new_data,old_data)
{
	if(old_data.items>new_data.items)
	{
		var max_items = old_data.items.length;
	}else{
		var max_items = new_data.items.length;
	}
	x=0;
	var cdn=[];
	insert_cover_product_image_for_deletion(old_data,new_data,max_items,x,cdn);
}

function insert_cover_product_image_for_deletion(old_data,new_data,max_items,x,cdn)
{
	if(x<max_items)
	{
		if(util.isNullOrUndefined(old_data)==false && util.isNullOrUndefined(new_data)==false)
		{
			var move_on=false;
			if(util.isNullOrUndefined(old_data.items[x]) == false && util.isNullOrUndefined(new_data.items[x])==false)
			{
				if(util.isNullOrUndefined(old_data.items[x].cover_product_image) == false && util.isNullOrUndefined(new_data.items[x].cover_product_image)==false)
				{
					if( (old_data.items[x].cover_product_image!=new_data.items[x].cover_product_image) && (old_data.items[x].cover_product_image!='') )
					{
						// var query = {
						// 	'filename':old_data.items[x].cover_product_image,
						// 	'date_simple':dataStruct.new_simple_timestamp,
						// 	'date_unix':dataStruct.new_unix_timestamp
						// };
						// GLOBAL.mongodb.filesThatNeedDeletion.insert(query,function(err,retVal){
						// 	x=x+1;
						// 	insert_cover_product_image_for_deletion(uuid_v1,dataStruct,doc,old_data,new_data,max_items,x);
						// });
						cdn.push(old_data.items[x].cover_product_image);
						x=x+1;
						insert_cover_product_image_for_deletion(old_data,new_data,max_items,x,cdn);
					}else{
						move_on=true;
					}
				}else{
					move_on=true;
				}
			}else{
				move_on=true;
			}
			if(move_on == true)
			{
				x=x+1;
				insert_cover_product_image_for_deletion(old_data,new_data,max_items,x,cdn);
			}
		}else{
			if(util.isNullOrUndefined(old_data)==false)
			{
				// var query = {
				// 	'filename':old_data.items[x].cover_product_image,
				// 	'date_simple':dataStruct.new_simple_timestamp,
				// 	'date_unix':dataStruct.new_unix_timestamp
				// };
				// GLOBAL.mongodb.filesThatNeedDeletion.insert(query,function(err,retVal){
				// 	x=x+1;
				// 	insert_cover_product_image_for_deletion(uuid_v1,dataStruct,doc,old_data,new_data,max_items,x)
				// });
				x=x+1;
				cdn.push(old_data.items[x].cover_product_image)
				insert_cover_product_image_for_deletion(old_data,new_data,max_items,x,cdn);
			}else{
				x=x+1;
				insert_cover_product_image_for_deletion(old_data,new_data,max_items,x,cdn);
			}
		}
	}else{
		if(x==max_items)
		{
			deleteMedia(cdn,0);
		}
	}
}

function deleteMedia(cdn,x)
{
	if(util.isNullOrUndefined(cdn[x])==false)
	{
		//cdn is an array
		var gcs = GLOBAL.gcloud.storage();
		var bucket = gcs.bucket(GLOBAL.storage_names.advertisements);
		//var file = bucket.file('images/'+targetValue+'_'+filename);
		//target delete file : 266b38c7-65b8-4c1f-8625-678d1e2d07bf-original.jpeg
		var file = bucket.file('images/'+cdn[x]);
		////console.log("trying ot delete : "+'images/'+cdn[x]);


		file.delete(function(err, apiResponse) {
			if(err==null)
			{
				//console.log("delete success for file : "+'images/'+cdn[x]);
			}else{
				//console.log("Error deleting file: "+cdn[x]);
				//console.log(err);
				/*
				{ 
					[ApiError: Not Found]
					errors: [ { domain: 'global', reason: 'notFound', message: 'Not Found' } ],
					code: 404,
					message: 'Not Found',
					response: undefined 
				}

				*/
			}
			if(x<cdn.length)
			{
				x=x+1;
				deleteMedia(cdn,x);
			}
		});
	}
}
module.exports={
	start:start
};