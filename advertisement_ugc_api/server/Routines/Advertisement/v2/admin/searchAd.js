
const respond = require(GLOBAL.server_location+'/EventListeners/errorListener');
const sortAlgo = require(__dirname+'/sortAlgo');

var searchName = function(dataJSON, dataStruct,req,res)
{
	/*
	"request": {
		"type": "List"/"Search Ad Name"/"Search Product Exist"
        "page_number":<int>,(starts at page 0)
        "number_items":<int>,(the number of items to return),
        "sort_algorithm":["latest"/"OwnedByUserID"/ "OwnedByShopID"/"AdvertNameASC"/"AdvertNameDESC"],
                 //(array type and optional;default is always trending, if you specify another sort algorithm it will be mixed with the trending algorithm)
        "additional_info":{
             "user_id":<int>(product listing user_id owner(the one who created the advert), required by type:List and filter:OwnedByUserID),
             "store_id":<int>(for OwnedByShopID),
             "product_id":<int>(for SearchProd),
             "search_key":<string> (for Search only)
        }
	}
	*/
    var pattern = new RegExp(dataJSON.request.additional_info.search_key,"ig")
    var query = {
        "styx_name":pattern
    };
    
    var sort = sortAlgo.start(dataJSON);
    var skip = parseInt(dataJSON.request.number_items*dataJSON.request.page_number);
    var limit = parseInt(dataJSON.request.number_items);

    GLOBAL.mongodb.styxRecords.find(query).sort(sort).skip(skip).limit(limit,function(err,doc){
        if(err==null)
        {
            var msg={
                "success":true, "status":"Success",
                "data":doc
            };
            respond.respondError(msg,res,req);
        }else{
            var msg = {
                "success":false, "status":"Error",
                "desc":"[listAdvert:start]DB Error",
                "err":err
            };
            respond.respondError(msg,res,req);
        }
    });
}

var searchProductIdExist=function(dataJSON,dataStruct,req,res)
{
    //product_id
    var query = {
        "$or":[
            {
                "products":parseInt(dataJSON.request.additional_info.product_id)
            },
            {
                "products":(dataJSON.request.additional_info.product_id).toString()
            }
        ]
        
    };
    
    var sort = sortAlgo.start(dataJSON);
    var skip = parseInt(dataJSON.request.number_items*dataJSON.request.page_number);
    var limit = parseInt(dataJSON.request.number_items);

    GLOBAL.mongodb.styxRecords.find(query).sort(sort).skip(skip).limit(limit,function(err,doc){
        if(err==null)
        {
            var msg={
                "success":true, "status":"Success",
                "data":doc
            };
            respond.respondError(msg,res,req);
        }else{
            var msg = {
                "success":false, "status":"Error",
                "desc":"[listAdvert:start]DB Error",
                "err":err
            };
            respond.respondError(msg,res,req);
        }
    });
}

module.exports={
    searchName:searchName,
    searchProductIdExist:searchProductIdExist
};