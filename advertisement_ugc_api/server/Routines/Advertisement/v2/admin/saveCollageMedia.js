const fs = require('fs');
const textMan = require(GLOBAL.server_location+'/helpers/text_manipulation');


/*
 data
 {
	styx_id: 15,
	styx_name: 'Andrew and IHOP friends',
	styx_type: 'C',
	...
	items: [{
	x: 56,
	y: 60,
cover_product_image: 'bkmeal1_cover.jpg',
      prod: {
	    product_id: 1, 
	    
    ...
	}}, {
x: 56,
	y: 60,
	cover_product_image: '',
      prod: {
	    product_id: 5, 
	   
    ...
	}}, ...]
}

*/
var start = function(dataJSON,req,res,targetBucket,my_uuid)
{
	processFiles(dataJSON,req,res,targetBucket,my_uuid,0);
};

function processFiles(dataJSON,req,res,targetBucket,my_uuid,x)
{
	var files = req.files;
	//size_array is of no use since there is no resampling
	if(x<files.src.length)
	{
		//upload to cloud using <int position number in array>_myuuid.ext
		var filename = '';
		for(var y=0;y<dataJSON.request.data.items.length;y++)
		{
			var pattern = new RegExp(files.src[x].originalname,'i');

			if(pattern.test(dataJSON.request.data.items[y].cover_product_image)==true)
			{
				var ext_arr = files.src[x].mimetype.split('/');
				var ext = ext_arr[ext_arr.length-1];
				filename = y+"_"+my_uuid+'.'+ext;
				fs.renameSync(files.src[x].path, files.src[x].destination+'/'+filename);
				//files.src[x]['already_used']=true;
				break;
			}
		}
		var gcs = GLOBAL.gcloud.storage();
		var bucket = gcs.bucket(GLOBAL.storage_names.advertisements);
		
		var options = {
		  destination: 'images/'+filename,
		  resumable: true
		};
		
		bucket.upload(files.src[x].destination+filename, options, function(err, file) {
			//fs.unlinkSync(files.src[x].destination+filename);
			setTimeout(deleteFile,120000,files.src[x].destination+filename);
			if(err!=null)
			{
				var msg = {
					'status':'Error saving file '+filename,
					'desc':'error occurred while pushing file to the cloud',
					'err':err
				};
				process.emit('Shopstyx:logError',msg);
				process.emit('Shopstyx:emailError',msg);
			}else{
				var msg = {
					'status':'Saved file '+filename
				};
				process.emit('Shopstyx:logError',msg);
			}
			x=x+1;
			processFiles(dataJSON,req,res,targetBucket,my_uuid,x);
		});
	}
}

function deleteFile(file)
{
	fs.unlinkSync(file);
}

module.exports={
	start:start
};