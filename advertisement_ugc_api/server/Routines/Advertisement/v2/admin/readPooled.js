var productInfoArray = (__dirname).toString().split("advertisement_ugc_api");
var productInfoLocation = productInfoArray[0];
delete productInfoArray;
const ProductInfo = require(productInfoLocation+"unifiedshopstyxnodeapi/server/Routines/ProductInfo/v2/client/ProductInfo");

var summary = function(dataJSON,dataStruct,req,res)
{
	var query = {
		"user_id":parseInt(dataJSON.request.data.owner),
		"type":"pooled product summary"
	};
	var returnVal = {
		"status":"",
		"type":dataJSON.request.type,
		"data":[]
	};
	
	GLOBAL.mongodb.pooledProducts.findOne(query,function(err,doc){
		if(err==null)
		{
			/*
			doc
			{
				"user_id":parseInt(user_id),
				"type":"pooled product summary",
				"date_simple":new_simple_date,
				"date_unix":new_unix_date,
				"data":{
					"product_ids":holder,
					"current_count":parseInt(holder.length)
				}
			};
			*/
			if(doc!=null)
			{
				returnVal=doc;
				returnVal['status']='';
			}
			returnVal.status="Success";
			process.emit("Shopstyx:resError",returnVal,res);
		}else{
			var msg = {
				"success":false, "status":"Error",
				"desc":"[readPooled:summary]DB Error",
				"err":err
			};
			process.emit("Shopstyx:resError",msg,res);
		}
	});
}

var history = function(dataJSON,dataStruct,req,res)
{
	var query = {
		"user_id":parseInt(dataJSON.request.data.owner),
		"type":"pooled product history"
	};
	if(typeof(dataJSON.request.number_items)=='undefined' || typeof(dataJSON.request.page_number))
	{
		var msg = {
			"success":false, "status":"Error",
			"desc":"[readPooled:history]Insufficient parameters to execute request"
		};
		process.emit("Shopstyx:resError",msg,res);
	}else{
		var skip = parseInt(dataJSON.request.number_items*dataJSON.request.page_number);
	    var limit = parseInt(dataJSON.request.number_items);

	    GLOBAL.mongodb.pooledProducts.find(query).limit(limit).skip(skip,function(err,docs){

	    	if(err==null)
	    	{
	    		if(docs.length>0)
	    		{
	    			var copy = JSON.parse(JSON.stringify(dataJSON));
	    			var products = [];
	    			for(var x =0;x<docs.length;x++)
	    			{
	    				products.push(parseInt(docs[x].product_id));
	    			}
	    			var request = {
	    				"product_id":products
	    			};
	    			copy.request=request;
	    			req.body.data=JSON.parse(JSON.stringify(copy));
	    			ProductInfo.processRequest(dataStruct,req,res);
	    		}else{
	    			var response={
						"success":true, "status": "Success",
						"desc":"[readPooled:history] No docs found",
					    "type":dataJSON.request.type,
					    "data":[]
					};
					process.emit("Shopstyx:resError",msg,res);
	    		}
	    	}else{
	    		var msg = {
					"success":false, "status":"Error",
					"desc":"[readPooled:history]DB Error",
					"err":err
				};
				process.emit("Shopstyx:resError",msg,res);
	    	}
	    });
	}
	
}