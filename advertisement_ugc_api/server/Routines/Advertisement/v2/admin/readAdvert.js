const mongojs = require('mongojs');
const util = require('util');
const respond = require(GLOBAL.server_location+'/EventListeners/errorListener');
const saveUploadProcess = require(__dirname+'/saveUploadProcess');
const reportUploadFinished = require(__dirname+'/reportUploadFinished');

var start = function(size_array,dataJSON, dataStruct,my_uuid,req,res)
{
	////console.log("inside Read");
	var hash_id = dataJSON.request.styx_id;
	var query = {
		'uuid_v1':hash_id
	};
	//find advertisement by uuid_v1
	////console.log("starting query");
	GLOBAL.mongodb.styxRecords.findOne(query,function(err,doc){
		if(err==null)
		{
			////console.log('mongo read successful');
			if(doc!=null)
			{
				////console.log('doc not null');
				doc['owner_info']=[];
				if(util.isNullOrUndefined(doc.owner)==false)
				{
					extractOwnerInfo(doc,dataStruct,req,res);
				}else if(util.isNullOrUndefined(doc.items)==false)
				{
					extractProductInfo(doc,0,dataStruct,req,res);
				}else{
					var msg={
						"success":true, "status":"Success",
						"desc":"[readAdvert:start]doc not null",
						"data":doc
					};
					respond.respondError(msg,res,req);
				}
			}else{
				//if we cannot find the uuid_v1 normally then check for another uuid_v1 based on the temporary upload process (new data)
				GLOBAL.mongodb.uploadProcess.findOne(query,function(err2,doc2){
					if(err2==null)
					{
						if(doc2!=null)
						{
							doc=doc2.additional_info.new_data;
							doc['owner_info']=[];
							if(util.isNullOrUndefined(doc.owner)==false)
							{
								////console.log('executing extractOwnerInfo');
								extractOwnerInfo(doc,dataStruct,req,res);
							}else if(util.isNullOrUndefined(doc.items)==false)
							{	
								////console.log('executing extractProductInfo');
								extractProductInfo(doc,0,dataStruct,req,res);
							}else{
								var msg={
									"success":true, "status":"Success",
									"desc":"[readAdvert:start]doc null doc2 not null",
									"data":doc
								};
								respond.respondError(msg,res,req);
							}
						}else{
							////console.log('executing extractProductInfo code 2');
							extractProductInfo(doc,0,dataStruct,req,res);
						}
						
					}else{
						var msg = {
							"success":false, "status":"Error",
							"desc":"[readAdvert:start]No valid request2",
							"err":err2
						};
						respond.respondError(msg,res,req);
					}
				});
			}
		}else{
			var msg = {
				"success":false, "status":"Error",
				"desc":"[readAdvert:start]No valid request",
				'err':err
			};
			respond.respondError(msg,res,req);
		}
	});
}

var extractOwnerInfo = function(doc,dataStruct,req,res)
{
	////console.log('inside extractOwnerInfo');
	if(doc!=null)
	{
		var query = "SELECT * FROM `users` WHERE `user_id`="+parseInt(doc.owner)+" LIMIT 1";
		var mysql = new GLOBAL.mysql.mysql_connect(query);
		mysql.results_ss_common().then(function(results){
			if(util.isError(results)==true){
				var msg = {
					"success":false, "status":"Error",
					'desc':'[readAdvert:extractOwnerInfo]DB Error Connection',
					'query':query,
					'message':results.message,
					'stack':results.stack||null
				};
				process.emit('Shopstyx:logError',msg);
			}else{
				var rows=results;
				if(util.isNullOrUndefined(rows)==false)
				{
					if(typeof(rows)=='object')
					{
						try{
							delete rows[0]['password'];
						}catch(err){
							//do nothing
						}
						try{
							delete rows[0]['password_encryption_type'];
						}catch(err){
							//do nothing
						}
						////console.log('rows found for extractOwnerInfo');
						doc.owner_info.push(rows[0]);
					}
				}
			}
			extractProductInfo(doc,0,dataStruct,req,res);
		});
	}else{
		var msg={
			"success":true, "status":"Success",
			"desc":"[readAdvert:extractOwnerInfo] No Record of User Found",
			"data":[],
			"doc":doc
		};
		respond.respondError(msg,res,req);
	}
}
//extractProductInfo(doc,x,dataStruct);
var extractProductInfo=function(doc,x,dataStruct,req,res)
{
	//////console.log(doc.items);
	////console.log('inside extractProductInfo');
	if(doc!=null)
	{
		if(x<doc.items.length)
		{
			//console.log('looking for product '+x+' of '+doc.items.length);
			//console.log("doc ni:");
			//console.log(doc);
			var query = "SELECT * FROM `products` WHERE `product_id`="+parseInt(doc.items[x].prod.product_id)+" LIMIT 1";
			////console.log('query assigned');
			////console.log(query);
			var mysql = new GLOBAL.mysql.mysql_connect(query);
			mysql.results_ss_common().then(function(results){
				if(util.isError(results)==true){
					var msg = {
						"success":false, "status":"Error",
						'desc':'[readAdvert:extractProductInfo]DB Error Connection',
						'query':query,
						'message':results.message,
						'stack':results.stack||null
					};
					process.emit('Shopstyx:logError',msg);
					checkX(doc,x,dataStruct,req,res);
				}else{
					var rows=results;
					if(rows.length<1)
	    			{
	    				////console.log('rows is less than 1');
	    				var msg = {
							"success":false, "status":"Error",
							"desc":"[readAdvert:extractProductInfo]Product "+parseInt(doc.items[x].prod.product_id)+" not found",
							"query":query
						};
						process.emit('Shopstyx:logError',msg);//changed from logError
						//check if x>doc.items.length
			    		
			    		checkX(doc,x,dataStruct,req,res);
	    			}else{
	    				//find images
	    				////console.log('finding images for product '+x+' of '+doc.items.length);
		    			var query2 = "SELECT * FROM `product_images` WHERE `product_id`="+parseInt(rows[0].product_id)+";";
		    			var mysql2 = new GLOBAL.mysql.mysql_connect(query2);
		    			mysql2.results_ss_common().then(function(results2){
		    				if(util.isError(results2)==true){
		    					var msg = {
		    						"success":false, "status":"Error",
		    						'desc':'[readAdvert:extractProductInfo]DB Error Connection2',
		    						'query':query2,
		    						'message':results2.message,
		    						'stack':results2.stack||null
		    					};
		    					process.emit('Shopstyx:logError',msg);
		    					doc.items[x]['prod']['product_images'] = [];
								checkX(doc,x,dataStruct,req,res);
		    				}else{
		    					var rows2=results2;
		    					doc.items[x]['prod']= JSON.parse(JSON.stringify(rows[0]));
		    					doc.items[x]['prod']['product_images'] = JSON.parse(JSON.stringify(rows2));
		    					checkX(doc,x,dataStruct,req,res);
		    				}
		    			});
	    			}
				}
			});
		}else{
			checkX(doc,x,dataStruct,req,res);
		}
	}else{
		var msg={
			"success":true, "status":"Success",
			"desc":"[readAdvert:extractProductInfo] No Records Found",
			"data":[],
			"doc":doc
		};
		respond.respondError(msg,res,req);
	}
}

function checkX(doc,x,dataStruct,req,res)
{
	if(x<doc.items.length)
	{
		////console.log('incrementing product list');
		x=x+1;
		////console.log("parsing x = "+x);
		extractProductInfo(doc,x,dataStruct,req,res);
	}else{
		// if(x==doc.items.length)
		// {
			var msg={
				"success":true, "status":"Success",
				"desc":"[readAdvert:checkX]x is equal or greater then items",
				"data":doc
			};
			respond.respondError(msg,res,req);
			delete dataStruct;
		//}
	}
}

var readStatus = function(dataJSON,dataStruct,req,res)
{
	//read the status of the file uploaded
	if(util.isNullOrUndefined(dataJSON.request.uuid_v1)==false)
	{
		var query={
			"uuid_v1":dataJSON.request.uuid_v1.toString()
		};
		var projection={
			'uuid_v1':true,
			'status':true,
			'additional_info.old_data.uuid_v1':true,
			'additional_info.new_data.uuid_v1':true,
		};
		GLOBAL.mongodb.uploadProcess.findOne(query,projection,function(err,retVal){
			if(err!=null)
			{
				var msg = {
					"success":false, "status":"Error",
					"desc":"[readAdvert:readStatus]Error Reading Record",
				    "err":err
				};
				respond.respondError(msg,res,req);
			}else{
				if(retVal!=null)
				{
					var query2={
						"additional_info.old_data.uuid_v1":dataJSON.request.uuid_v1.toString()
					};
					respond.respondError(retVal,res,req);
				}else{
					var msg = {
						"success":true, "status":"Success",
						"uuid_v1":dataJSON.request.uuid_v1.toString(),
						"desc":"[readAdvert:readStatus]Record not in system"
					};
					respond.respondError(msg,res,req);
				}
			}
		});
	}else{
		var msg = {
			"success":false, "status":"Error",
			"desc":"[readAdvert:readStatus]tracking number not found",
		    "err":"Trying to get a status of processed advertisement without valid tracking number"
		};
		respond.respondError(msg,res,req);
	}
};

module.exports={
	start:start,
	extractProductInfo:extractProductInfo,
	readStatus:readStatus
};