const mysqlStuff = require(GLOBAL.server_location+'/helpers/mysql_convertions');
const saveUploadProcess = require(__dirname+'/saveUploadProcess');
const util = require('util');

var start = function(uuid_v1,tries)
{
	//console.log("Updating :"+uuid_v1);
	var query = {
		'uuid_v1':uuid_v1.toString()//,
		//'status':'Waiting to Switch places with old record'
	};
	var dataStruct = {
		new_unix_timestamp:'',
		new_simple_timestamp:''
	};
	dataStruct.new_simple_timestamp = mysqlStuff.populateMySQLTimeStamp(dataStruct);
	GLOBAL.mongodb.uploadProcess.findOne(query,function(err,doc){
		if(err==null)
		{
			if(doc!=null)
			{
				var reloadme=false;
				if(util.isNullOrUndefined(doc.additional_info)==false)
				{	
					if(util.isNullOrUndefined(doc.additional_info.old_data)==false && util.isNullOrUndefined(doc.additional_info.new_data)==false)
					{
						//console.log("doc found");
						//console.log(doc);
						//dataJSON.request.type.data.styx_type.toLowerCase().replace(/\s/g, '')
						switch(doc.additional_info.old_data.styx_type.toLowerCase().replace(/\s/g, ''))
						{
							case'v':
								//assume prevpic and src
								schedule_delete_prevpic(uuid_v1,dataStruct,doc);
								schedule_delete_src(uuid_v1,dataStruct,doc);
								break;
							case 'p':
								//assume src only
								schedule_delete_src(uuid_v1,dataStruct,doc);
								break;
							case 'c':
								schedule_delete_cover_product_image(uuid_v1,dataStruct,doc);
								break;
							default:
								//make switch and delete old record
								break;
						}
						switch_old_data_with_new(uuid_v1,dataStruct,doc);
					}else{
						reloadme=true;
					}
				}else{
					reloadme=true;
				}
				if(reloadme==true)
				{
					//setTimeout(function(){ alert("Hello"); }, 3000);
					if(tries<10)
					{
						tries=tries+1;
						setTimeout(start,5000,uuid_v1,tries);
					}else{
						if(util.isNullOrUndefined(doc)==true)
						{
							var msg = {
								"success":false, "status":"Error",
								"desc":"[reportUploadFinished:start]doc is NULL",
								"data":{
							        "uuid_v1":uuid_v1.toString()
							    },
							    "err":err
							};
						}else if(util.isNullOrUndefined(doc.additional_info)==true){
							var msg = {
								"success":false, "status":"Error",
								"desc":"[reportUploadFinished:start]No Additional Info",
								"data":{
							        "uuid_v1":uuid_v1.toString()
							    },
							    "err":err
							};
						}else{
							if(util.isNullOrUndefined(doc.additional_info.old_data)==true)
							{
								var msg = {
									"success":false, "status":"Error",
									"desc":"[reportUploadFinished:start]Can't find OLD DATA OR NEW DATA",
									"data":{
								        "uuid_v1":uuid_v1.toString()
								    },
								    "err":err
								};
							}else if(util.isNullOrUndefined(doc.additional_info.new_data)==true){
								var msg = {
									"success":false, "status":"Error",
									"desc":"[reportUploadFinished:start]Can't find NEW DATA OR OLD DATA",
									"data":{
								        "uuid_v1":uuid_v1.toString()
								    },
								    "err":err
								};
							}
						}
						process.emit('Shopstyx:logError',msg);
					}
				}
			}else{
				// var msg = {
				// 	"success":false, "status":"Error",
				// 	"desc":"[reportUploadFinished:start]No Record Found",
				// 	"data":{
				//         "uuid_v1":uuid_v1.toString()
				//     }
				// };
				// process.emit('Shopstyx:logError',msg);

				//try again in a few seconds
				// //console.log("Null document");
				// //console.log("query:");
				// //console.log(query);
				// //console.log("retry in 5 seconds");
				tries=tries+1;
				if(tries>10)
				{
					var msg = {
						"success":false, "status":"Error",
						"desc":"[reportUploadFinished:start]NULL DOCUMENT",
						"data":{
					        "uuid_v1":uuid_v1.toString()
					    },
					    "err":err
					};
					process.emit('Shopstyx:logError',msg);
				}else{
					//check document if status ready
					var chkQ={
				        "uuid_v1":uuid_v1.toString()
				    };
					GLOBAL.mongodb.uploadProcess.findOne(query,function(err,doc){
						if(err==null)
						{
							if(doc!=null)
							{
								if(doc.status=='Ready')
								{
									var msg = {
										"success":true, "status":"Success",
										"desc":"[reportUploadFinished:start]DOCUMENT SAVED",
										"data":{
									        "uuid_v1":uuid_v1.toString()
									    }
									};
									process.emit('Shopstyx:logError',msg);
								}
							}else{
								//console.log("retry in 5 seconds");
								//console.log(doc);
								setTimeout(start,5000,uuid_v1,tries);
							}
						}else{
							//console.log("retry in 5 seconds");
							//console.log(doc);
							setTimeout(start,5000,uuid_v1,tries);	
						}
					});
				}
			}
			
		}else{
			var msg = {
				"success":false, "status":"Error",
				"desc":"[reportUploadFinished:start]DB Error",
				"data":{
			        "uuid_v1":uuid_v1.toString()
			    },
			    "err":err
			};
			process.emit('Shopstyx:logError',msg);
		}
	});
};

function schedule_delete_prevpic(uuid_v1,dataStruct,doc)
{
	if(util.isNullOrUndefined(doc.additional_info)==false)
	{
		if(doc.additional_info.old_data.prevpic!=doc.additional_info.new_data.prevpic)
		{
			var arrayHolder = doc.additional_info.old_data.prevpic.split('/');
			var filename = arrayHolder[arrayHolder.length-1];
			var query = {
				'filename':filename,
				'date_simple':dataStruct.new_simple_timestamp,
				'date_unix':dataStruct.new_unix_timestamp
			};
			GLOBAL.mongodb.filesThatNeedDeletion.insert(query,function(err,retVal){
				//don't care here
			});
		}
	}else{
		var msg = {
			"success":false, "status":"Error",
			"desc":"[reportUploadFinished:schedule_delete_prevpic]No additional_info found",
			"data":{
		        "uuid_v1":uuid_v1.toString()
		    }
		};
		process.emit('Shopstyx:logError',msg);
	}
}

function schedule_delete_src(uuid_v1,dataStruct,doc)
{
	if(util.isNullOrUndefined(doc.additional_info)==false)
	{
		if(doc.additional_info.old_data.src!=doc.additional_info.new_data.src)
		{
			var arrayHolder = doc.additional_info.old_data.src.split('/');
			var filename = arrayHolder[arrayHolder.length-1];
			if(util.isNullOrUndefined(doc.additional_info.old_data.src_array)==false)
			{
				var src_array=[];
				if(typeof(doc.additional_info.old_data.src_array)=='string')
				{
					src_array=doc.additional_info.old_data.src_array.split(',');
				}
				if(typeof(doc.additional_info.old_data.src_array)=='object')
				{
					src_array=doc.additional_info.old_data.src_array;
				}
				var prefix_holder = filename.split(src_array[0]+'_');
				filename=prefix_holder[prefix_holder.length-1];
				for(var x=0;x<src_array.length;x++)
				{
					var query = {
						'filename':src_array[x]+'_'+filename,
						'date_simple':dataStruct.new_simple_timestamp,
						'date_unix':dataStruct.new_unix_timestamp
					};
					GLOBAL.mongodb.filesThatNeedDeletion.insert(query,function(err,retVal){
						//don't care here
					});
				}
			}else{
				var query = {
					'filename':filename,
					'date_simple':dataStruct.new_simple_timestamp,
					'date_unix':dataStruct.new_unix_timestamp
				};
				GLOBAL.mongodb.filesThatNeedDeletion.insert(query,function(err,retVal){
					//don't care here
				});
			}
			
		}
	}else{
		var msg = {
			"success":false, "status":"Error",
			"desc":"[reportUploadFinished:schedule_delete_src]No additional_info found",
			"data":{
		        "uuid_v1":uuid_v1.toString()
		    }
		};
		process.emit('Shopstyx:logError',msg);
	}
}

function schedule_delete_cover_product_image(uuid_v1,dataStruct,doc)
{
	if(util.isNullOrUndefined(doc.additional_info)==false)
	{
		var old_data = doc.additional_info.old_data;
		var new_data = doc.additional_info.new_data;

		if(old_data.items>new_data.items)
		{
			var max_items = old_data.items.length;
		}else{
			var max_items = new_data.items.length;
		}
		x=0;
		insert_cover_product_image_for_deletion(uuid_v1,dataStruct,doc,old_data,new_data,max_items,x);
	}else{
		var msg = {
			"success":false, "status":"Error",
			"desc":"[reportUploadFinished:schedule_delete_cover_product_image]No additional_info found",
			"data":{
		        "uuid_v1":uuid_v1.toString()
		    }
		};
		process.emit('Shopstyx:logError',msg);
	}
}

function insert_cover_product_image_for_deletion(uuid_v1,dataStruct,doc,old_data,new_data,max_items,x)
{
	if(x<max_items)
	{
		if(util.isNullOrUndefined(old_data)==false && util.isNullOrUndefined(new_data)==false)
		{
			var move_on=false;
			if(util.isNullOrUndefined(old_data.items[x]) == false && util.isNullOrUndefined(new_data.items[x])==false)
			{
				if(util.isNullOrUndefined(old_data.items[x].cover_product_image) == false && util.isNullOrUndefined(new_data.items[x].cover_product_image)==false)
				{
					if( (old_data.items[x].cover_product_image!=new_data.items[x].cover_product_image) && (old_data.items[x].cover_product_image!='') )
					{
						var query = {
							'filename':old_data.items[x].cover_product_image,
							'date_simple':dataStruct.new_simple_timestamp,
							'date_unix':dataStruct.new_unix_timestamp
						};
						GLOBAL.mongodb.filesThatNeedDeletion.insert(query,function(err,retVal){
							x=x+1;
							insert_cover_product_image_for_deletion(uuid_v1,dataStruct,doc,old_data,new_data,max_items,x);
						});
					}else{
						move_on=true;
					}
				}else{
					move_on=true;
				}
			}else{
				move_on=true;
			}
			if(move_on == true)
			{
				x=x+1;
				insert_cover_product_image_for_deletion(uuid_v1,dataStruct,doc,old_data,new_data,max_items,x);
			}
		}else{
			if(util.isNullOrUndefined(old_data)==false)
			{
				var query = {
					'filename':old_data.items[x].cover_product_image,
					'date_simple':dataStruct.new_simple_timestamp,
					'date_unix':dataStruct.new_unix_timestamp
				};
				GLOBAL.mongodb.filesThatNeedDeletion.insert(query,function(err,retVal){
					x=x+1;
					insert_cover_product_image_for_deletion(uuid_v1,dataStruct,doc,old_data,new_data,max_items,x)
				});
			}else{
				x=x+1;
				insert_cover_product_image_for_deletion(uuid_v1,dataStruct,doc,old_data,new_data,max_items,x);
			}
		}
	}
}
function switch_old_data_with_new(uuid_v1,dataStruct,doc)
{
	if(util.isNullOrUndefined(doc.additional_info)==false)
	{
		//doc.additional_info.old_data
		//doc.additional_info.new_data
		var query = {
			"uuid_v1":doc.additional_info.old_data.uuid_v1
		};
		doc.additional_info.new_data['prev_uuid_v1']=doc.additional_info.new_data.uuid_v1;
		doc.additional_info.new_data.uuid_v1 = doc.additional_info.old_data.uuid_v1;
		GLOBAL.mongodb.styxRecords.update(query,doc.additional_info.new_data,function(err,doc){

		});
		saveUploadProcess.updateStatus(doc.additional_info.old_data.uuid_v1,'Ready');
		saveUploadProcess.deleteStatus(doc.additional_info.new_data.prev_uuid_v1);
	}else{
		var msg = {
			"success":false, "status":"Error",
			"desc":"[reportUploadFinished:switch_old_data_with_new]No additional_info found",
			"data":{
		        "uuid_v1":uuid_v1.toString()
		    }
		};
		process.emit('Shopstyx:logError',msg);
	}
}
module.exports={
	start:start
};