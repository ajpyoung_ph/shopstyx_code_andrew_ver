const util = require('util');
const respond = require(GLOBAL.server_location+'/EventListeners/errorListener');
const request = require('request');

const start = function(uuid_v1,product_ids,x)
{
	/*
	$_POST['key'] = robot key
	$_POST['uuid'] = styx uuid
	*/
	var query = 'SELECT * FROM `default_settings` WHERE `group`="api" AND `key`="webservice_signature";';

	var mysql = new GLOBAL.mysql.mysql_connect(query);
	mysql.results_ss_common().then(function(results){
		if(util.isError(results)==true){
			var msg = {
				"success":false, "status":"Error",
				'desc':'[callCreateStyx:start]DB Error Connection',
				'query':query,
				'message':results.message,
				'stack':results.stack||null
			};
			respond.logError(msg);
		}else{
			var resultDoc = results;
			var key = resultDoc[0].value;
			request.post(GLOBAL.HTTP_location+'/api/v2/notifications/createstyx', {form:{"key":key,"uuid":uuid_v1,"product_ids":JSON.stringify(product_ids)}}, function(err,httpResponse,body)
			{
				 /*
					"success": true,
					"data": "https:\/\/storage.googleapis.com\/shopstyx-advertisements-local\/styxrenders\/63472470-b5ab-11e5-b55c-5f1ce2806015.jpg"
				 */
				 try{
				 	var sentData = '';
				 	////console.log("{{{{{{{{{{{{{{{{{{{{{{{ Return FROM /api/v2/notifications/createstyx }}}}}}}}}}}}}}");
				 	////console.log(body);
				 	if(typeof(body)=='string')
				 	{
				 		sentData = JSON.parse(body);
				 	}else{
				 		if(typeof(body)=='object')
				 		{
				 			sentData = JSON.parse(JSON.stringify(body));
				 		}
				 	}
				 	if(sentData.success==false)
				 	{
				 		//don't do anything yet
				 		x=x+1;
				 		if(x>10)
				 		{
				 			var msg = {
				 				"success":false, "status":"Error",
				 				"desc":"[callCreateStyx:start] Clint's API keep saying error"
				 			};
				 			respond.logError(msg);
				 		}else{
				 			setTimeout(start,2000,uuid_v1,x);
				 		}
				 	}else{
				 		//console.log("FLAT FILE SHOULD BE");
				 		//console.log(sentData.data);
				 		saveLocation(sentData.data,uuid_v1);
				 		saveLocationUploadProcessOLD(sentData.data,uuid_v1);
				 		saveLocationUploadProcessNEW(sentData.data,uuid_v1);
				 	}
				 }catch(error_here){
				 	var msg = {
		 				"success":false, "status":"Error",
		 				"desc":"[callCreateStyx:start] Error occurred code 1",
		 				"err":error_here,
		 				"stack":error_here.stack
		 			};
		 			respond.logError(msg);
				 }
			});
		}
	});
};

function saveLocation(flat_ver,uuid_v1)
{
	var query = {
		"uuid_v1":uuid_v1
	};
	var setQ = {
		"$set":{
			"flat_ver":flat_ver
		}
	};
	var options = {
		upsert: false,
		multi: false
	};
	GLOBAL.mongodb.styxRecords.update(query,setQ,options,function(err,retVal){
		if(err!=null)
		{
			var msg = {
				"success":false, "status":"Error",
				"desc":"[securityListener:saveLocation]Error saving in DB",
				"err":err,
				"query":query
			};
			respond.logError(msg);
		}
	});

	
}
function saveLocationUploadProcessOLD(flat_ver,uuid_v1)
{
	var query = {
		"additional_info.old_data.uuid_v1":uuid_v1
	};
	var setQ = {
		"$set":{
			"additional_info.old_data.flat_ver":flat_ver
		}
	};
	var options = {
		upsert: false,
		multi: false
	};
	GLOBAL.mongodb.pooledProducts.update(query,setQ,options,function(err,retVal){
		if(err!=null)
		{
			var msg = {
				"success":false, "status":"Error",
				"desc":"[securityListener:saveLocationUploadProcessOLD]Error saving in DB",
				"err":err,
				"query":query
			};
			respond.logError(msg);
		}
	});
}
function saveLocationUploadProcessNEW(flat_ver,uuid_v1)
{
	var query = {
		"additional_info.new_data.uuid_v1":uuid_v1
	};
	var setQ = {
		"$set":{
			"additional_info.new_data.flat_ver":flat_ver
		}
	};
	var options = {
		upsert: false,
		multi: false
	};
	GLOBAL.mongodb.pooledProducts.update(query,setQ,options,function(err,retVal){
		if(err!=null)
		{
			var msg = {
				"success":false, "status":"Error",
				"desc":"[securityListener:saveLocationUploadProcessNEW]Error saving in DB",
				"err":err,
				"query":query
			};
			respond.logError(msg);
		}
	});
}
module.exports={
	start:start
};