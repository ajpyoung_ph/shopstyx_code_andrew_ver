const fs = require('fs');
//const gm = require('gm').subClass({imageMagick: true});

const saveUploadProcess = require(__dirname+'/saveUploadProcess');
const reportUploadFinished = require(__dirname+'/reportUploadFinished');
/*
	prevpic = true/false - if prevpic is available
	newfilename =  {
		video:'',
		prevpic:''
	},

*/
var uploadMedia  = function(prevpic,newfilename,dataJSON,req,res,targetBucket,myuuid)
{
	var files = req.files;
	var video=false;
	if(typeof(files.src)!='undefined')
	{
		video=true;
	}
	var StatusCounter={
		counter: 0,
		max_items: 1
	};
	saveUploadProcess.updateStatus(myuuid,'Converting & Uploading');
	if(prevpic==true)
	{
		var gcs = GLOBAL.gcloud.storage();
		var bucket = gcs.bucket(GLOBAL.storage_names.advertisements);
		if(video==true)
		{
			StatusCounter.max_items=2;
		}
		
		var options = {
		  destination: 'images/'+newfilename.prevpic,
		  resumable: true
		};
		
		bucket.upload(GLOBAL.temp_dir_location+'/'+newfilename.prevpic, options, function(err, file) {
			//fs.unlinkSync(GLOBAL.temp_dir_location+'/'+newfilename.prevpic);
			setTimeout(deleteFile,120000,GLOBAL.temp_dir_location+'/'+newfilename.prevpic);
			if(err!=null)
			{
				var msg = {
					'status':'Error saving file '+newfilename.prevpic,
					'desc':'error occurred while pushing file to the cloud',
					'err':err
				};
				process.emit('Shopstyx:logError',msg);
				process.emit('Shopstyx:emailError',msg);
			}else{
				var msg = {
					'status':'Saved file '+newfilename.prevpic
				};
				process.emit('Shopstyx:logError',msg);
			}
			StatusCounter.counter=StatusCounter.counter+1;
			reportStatus(StatusCounter,myuuid,dataJSON);
		});
	}
	if(video==true)
	{
		uploadVideo(video,newfilename,dataJSON,req,res,targetBucket,myuuid,StatusCounter);
	}
	
};

var uploadVideo = function(video,newfilename,dataJSON,req,res,targetBucket,myuuid,StatusCounter)
{
	var gcs = GLOBAL.gcloud.storage();
	var bucket = gcs.bucket(GLOBAL.storage_names.advertisements);
	var files = req.files;
	var options = {
	  destination: 'images/'+newfilename.video,
	  resumable: true
	};
	if(video==true)
	{
		bucket.upload(GLOBAL.temp_dir_location+'/'+newfilename.video, options, function(err, file) {
			//fs.unlinkSync(GLOBAL.temp_dir_location+'/'+newfilename.video);
			setTimeout(deleteFile,120000,GLOBAL.temp_dir_location+'/'+newfilename.video);
			if(err!=null)
			{
				var msg = {
					'status':'Error saving file '+newfilename.video,
					'desc':'error occurred while pushing file to the cloud',
					'err':err
				};
				process.emit('Shopstyx:logError',msg);
				process.emit('Shopstyx:emailError',msg);
			}else{
				var msg = {
					'status':'Saved file '+newfilename.video
				};
				process.emit('Shopstyx:logError',msg);
			}
			StatusCounter.counter=StatusCounter.counter+1;
			reportStatus(StatusCounter,myuuid,dataJSON);
		});
	}
}

function reportStatus(StatusCounter,myuuid,dataJSON)
{
	if(StatusCounter.counter==StatusCounter.max_items)
	{
		saveUploadProcess.updateStatus(myuuid,'Ready');
		/*if(dataJSON.request.type.toLowerCase().replace(/\s/g, '')=='update')
		{
			//console.log("triggering update");
			reportUploadFinished.start(myuuid,0);
		}*/
	}
}
function deleteFile(file)
{
	fs.unlinkSync(file);
}
module.exports = {
	uploadMedia:uploadMedia
};