
const respond = require(GLOBAL.server_location+'/EventListeners/errorListener');
const sortAlgo = require(__dirname+'/sortAlgo');
const util = require('util');

var start = function(dataJSON, dataStruct,req,res)
{
	/*
	"request": {
		"type": "List"/"Search Ad Name"/"Search Product Exist"
        "page_number":<int>,(starts at page 0)
        "number_items":<int>,(the number of items to return),
        "sort_algorithm":["latest"/"OwnedByUserID"/ "OwnedByShopID"/"AdvertNameASC"/"AdvertNameDESC"],
                 //(array type and optional;default is always trending, if you specify another sort algorithm it will be mixed with the trending algorithm)
        "additional_info":{
             "user_id":<int>(product listing user_id owner(the one who created the advert), required by type:List and filter:OwnedByUserID),
             "store_id":<int>(for OwnedByShopID),
             "product_id":<int>(for SearchProd),
             "search_key":<string> (for Search only)
        }
	}
	*/
    if(util.isNullOrUndefined(dataJSON.request.additional_info)==true)
    {
        var query = {};
    }else{
        var query = {
            "$or":[
                {
                    "owner":parseInt(dataJSON.request.additional_info.user_id)
                },
                {
                    "owner":(dataJSON.request.additional_info.user_id).toString()
                }
            ]
        };
    }
    var sort = {};
    //["latest"/"AdvertNameASC"/"AdvertNameDESC"]
    if(util.isNullOrUndefined(dataJSON.request.sort_algorithm)==true)
    {
        sort = {
            "date_updated":-1
        };
    }else{
        if(dataJSON.request.sort_algorithm.join("`").toLowerCase().split("`").indexOf("latest")>-1)
        {
            sort = {
                "date_updated":-1
            };
        }
        if(dataJSON.request.sort_algorithm.join("`").toLowerCase().split("`").indexOf("advertnameasc")>-1)
        {
            sort['styx_name']=1;
        }else if(dataJSON.request.sort_algorithm.join("`").toLowerCase().split("`").indexOf("advertnamedesc")>-1)
        {
            sort['styx_name']=-1;
        }

    }
    // //console.log(sort);
    // //console.log(query);
    // //console.log(skip);

	//var sort = sortAlgo.start(dataJSON);
    var skip = parseInt(dataJSON.request.number_items*dataJSON.request.page_number);
    var limit = parseInt(dataJSON.request.number_items);

    GLOBAL.mongodb.styxRecords.find(query).sort(sort).skip(skip).limit(limit,function(err,doc){
        if(err==null)
        {
            var x=0;
            var max = doc.length;
            if(max<1)
            {
                var msg = {
                    "success":true, "status":"Success",
                    "desc":"[listAdvert:start]NO Record Found",
                    "data":[]
                };
                respond.respondError(msg,res,req);
            }else{
                if(util.isNullOrUndefined(doc[x].uuid_v1)==false)
                {
                    insert_status_info(doc,x,max,req,res);
                }else{
                    var msg = {
                        "success":false, "status":"Error",
                        "desc":"[listAdvert:start]Record Data Error",
                        "err":"record specified has no uuid_v1",
                        "query":query
                    };
                    respond.respondError(msg,res,req);
                }
            }
            
        }else{
            var msg = {
                "success":false, "status":"Error",
                "desc":"[listAdvert:start]DB Error",
                "err":err
            };
            respond.respondError(msg,res,req);
        }
    });
}

function insert_status_info(docs,x,max,req,res)
{
    if(x<max)
    {
        var query={
            'uuid_v1':docs[x].uuid_v1.toString()
        };
        var projection={
            'uuid_v1':true,
            'status':true
        };
        GLOBAL.mongodb.uploadProcess.findOne(query,projection,function(err,doc){
            var proceed=true;
            if(err==null && doc!=null)
            {
                if(doc.status!='Waiting to Switch with new updated record')
                {
                    docs[x]['status_readiness_info']=doc;
                }else{
                    proceed=false;
                    findOtherRecord(docs,x,max,doc,req,res);
                }
                
            }else if(doc==null){
                docs[x]['status_readiness_info']={
                    'status':"Success",
                    'uuid_v1':docs[x].uuid_v1.toString()
                };
            }
            if(err!=null)
            {
                docs[x]['status_readiness_info']={
                    'status':"Success",
                    'uuid_v1':docs[x].uuid_v1.toString()
                };
                var msg = {
                    "success":false, "status":"Error",
                    "desc":"[listAdvert:insert_status_info]DB Error",
                    "err":err
                };
                respond.respondError(msg,res,req);
            }
            if(proceed==true)
            {
                x=x+1;
                insert_status_info(docs,x,max,req,res);
            }
            
        });
        
    }else{
        var msg={
            "success":true, "status":"Success",
            "data":docs
        };
        respond.respondError(msg,res,req);
    }
}

function findOtherRecord(docs,x,max,doc,req,res)
{
    var query={
        'additional_info.old_data.uuid_v1':docs[x].uuid_v1.toString()
    };
    var projection={
        'uuid_v1':true,
        'status':true
    };
    GLOBAL.mongodb.uploadProcess.findOne(query,projection,function(err,doc2){
        if(err==null)
        {
             docs[x]['status_readiness_info']=doc2;//if we plan to change from status_readiness_info to just the uuid_v1
        }
        x=x+1;
        insert_status_info(docs,x,max,req,res);
    });
}
module.exports={
    start:start
};