var gd = require('easy-gd');
var uuid = require('node-uuid');
var start = function(dataJSON,dataStruct,cb,targetBucket)
{
	/*
	{
		"files": "{ 
			prevpic:[ 
				{ 
					fieldname: 'prevpic',
					originalname: '12047042_10204679170119053_7349686871162241667_n.jpg',
					encoding: '7bit',
					mimetype: 'image/jpeg',
					destination: '/home/andrew/Projects/NodeProjects/TestCodes/FileUploads/server/uploads/',
					filename: '59c2c238e2d633a0ea5460e6490abe77',
					path: '/home/andrew/Projects/NodeProjects/TestCodes/FileUploads/server/uploads/59c2c238e2d633a0ea5460e6490abe77',
					size: 76410 
				} 
			],
			src:[ 
				{ 
					fieldname: 'src',
					originalname: 'VID_20151117_0818.mp4',
					encoding: '7bit',
					mimetype: 'video/mp4',
					destination: '/home/andrew/Projects/NodeProjects/TestCodes/FileUploads/server/uploads/',
					filename: '7ef6f340476ac32b81d02d97f4458eb5',
					path: '/home/andrew/Projects/NodeProjects/TestCodes/FileUploads/server/uploads/7ef6f340476ac32b81d02d97f4458eb5',
					size: 72992528 
				} 
			] 
		}",
		"body": "{ something: 'asdfalsdfjaf', description: 'afsdaf' }"
	}
	*/
	try{
		var returnValue = [];
		var files = req.files;
		if(typeof(files.prevpic)!='undefined')
		{
			var tempFile = files.prevpic.path;
			var bucket = GLOBAL.storage.bucket(GLOBAL.storage_names.advertisements);
			//sample pictures to different sizes
			/*
			$imgfile_webstore960 = $dir.$new_filename.'_webstore960'.$ext;
			$imgfile_medium650 = $dir.$new_filename.'_medium650'.$ext;
			$imgfile_small300 = $dir.$new_filename.'_small300'.$ext;
			$imgfile_xsmall100 = $dir.$new_filename.'_xsmall100'.$ext;
			*/
			var my_uuid = uuid.v1();
			var image;
			var resized=[];
			var saved_status = false;
			image = gd.open(tempFile);
			resized.push(image.resize({width: 960,resample:true}));
			resized.push(image.resize({width: 650,resample:true}));
			resized.push(image.resize({width: 300,resample:true}));
			resized.push(image.resize({width: 100,resample:true}));
			resized.save('resized.jpg')
			saved_status = upload_cloud(resized);
			

		}
		return returnValue;
	}catch(err){
		return false;
	}
	
};



module.exports={
	start:start
};