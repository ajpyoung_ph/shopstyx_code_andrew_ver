
const respond = require(GLOBAL.server_location+'/EventListeners/errorListener');
if(typeof(process.env.NODE_USER)!='undefined')
{
	const gm = require('gm').subClass({imageMagick: true});
}else{
	const sharp = require('sharp');
}
//

const fs = require('fs');

const saveUploadProcess = require(__dirname+'/saveUploadProcess');
const reportUploadFinished = require(__dirname+'/reportUploadFinished');

var resampleMedia=function(size_array,newfilename,dataJSON,req,res,targetBucket,myuuid)
{
	var x=0;
	resampleImage(size_array,newfilename,dataJSON,req,res,targetBucket,myuuid,x,size_array[x]);
}

function resampleImage(size_array,filename,dataJSON,req,res,targetBucket,myuuid,x,targetValue)
{
	saveUploadProcess.updateStatus(myuuid,'Converting & Uploading '+parseInt(x+1)+' out of '+size_array.length);
	// if(process.env.NODE_ENV.toLowerCase()=='development')
	// {
		if(typeof(process.env.NODE_USER)=='undefined')
		{
			sharp(GLOBAL.temp_dir_location+'/'+filename)
			.resize(targetValue)
			.toFile(GLOBAL.temp_dir_location+'/'+targetValue+'_'+filename, function(err) {
				// output.jpg is a 300 pixels wide and 200 pixels high image
				// containing a scaled and cropped version of input.jpg
				if(err==null)
				{
					//upload saved data
					var gcs = GLOBAL.gcloud.storage();
					var bucket = gcs.bucket(GLOBAL.storage_names.advertisements);
					var file = bucket.file('images/'+targetValue+'_'+filename);
					fs.createReadStream(GLOBAL.temp_dir_location+'/'+targetValue+'_'+filename)
						.pipe(file.createWriteStream())
						.on('error', function(err) {
							var msg = {
								"success":false, "status":"Error",
								"desc":"[saveSrapeData:resampleImage]Error in Upload",
								"err":err
							};
							process.emit('Shopstyx:logError',msg);
							checkDelete(size_array,filename,dataJSON,req,res,targetBucket,myuuid,x);
						})
						.on('finish', function() {
							// The file upload is complete.
							// The file upload is complete.
							//console.log("Successfully uploaded "+targetValue+'_'+filename);
							checkDelete(size_array,filename,dataJSON,req,res,targetBucket,myuuid,x);
						});
				}else{
					var msg = {
						"success":false, "status":"Error",
						"desc":"[saveSrapeData:resampleImage]Error Writing",
						"err":err
					};
					process.emit('Shopstyx:logError',msg);
					checkDelete(size_array,filename,dataJSON,req,res,targetBucket,myuuid,x);
				}
			});
		}else{
			gm(GLOBAL.temp_dir_location+'/'+filename)
			.resize(targetValue)
			.write(GLOBAL.temp_dir_location+'/'+targetValue+'_'+filename,function(err){
				if(err==null)
				{
					//upload saved data
					var gcs = GLOBAL.gcloud.storage();
					var bucket = gcs.bucket(GLOBAL.storage_names.advertisements);
					var file = bucket.file('images/'+targetValue+'_'+filename);
					fs.createReadStream(GLOBAL.temp_dir_location+'/'+targetValue+'_'+filename)
						.pipe(file.createWriteStream())
						.on('error', function(err) {
							var msg = {
								"success":false, "status":"Error",
								"desc":"[saveSrapeData:resampleImage]Error in Upload",
								"err":err
							};
							process.emit('Shopstyx:logError',msg);
							checkDelete(size_array,filename,dataJSON,req,res,targetBucket,myuuid,x);
						})
						.on('finish', function() {
							// The file upload is complete.
							// The file upload is complete.
							//console.log("Successfully uploaded "+targetValue+'_'+filename);
							checkDelete(size_array,filename,dataJSON,req,res,targetBucket,myuuid,x);
						});
				}else{
					var msg = {
						"success":false, "status":"Error",
						"desc":"[saveSrapeData:resampleImage]Error Writing",
						"err":err
					};
					process.emit('Shopstyx:logError',msg);
					checkDelete(size_array,filename,dataJSON,req,res,targetBucket,myuuid,x);
				}
			});
		}
		
		/**/
			/*
			.stream(function (err, stdout, stderr) {
				//var writeStream = fs.createWriteStream('/path/to/my/reformatted.png');
				//stdout.pipe(writeStream);
				var gcs = GLOBAL.gcloud.storage();
				var bucket = gcs.bucket(GLOBAL.storage_names.advertisements);
				var file = bucket.file('images/'+targetValue+'_'+filename);
				stdout.pipe(file.createWriteStream())
					.on('error', function(err) {
						var msg = {
							"success":false, "status":"Error",
							"desc":"[saveSrapeData:resampleImage]Error in Upload",
							"err":err
						};
						process.emit('Shopstyx:logError',msg);
						checkDelete,size_array(filename,dataJSON,req,res,targetBucket,myuuid,x);
					})
					.on('finish', function() {
						// The file upload is complete.
						//console.log("Successfully uploaded "+targetValue+'_'+filename);
						checkDelete(size_array,filename,dataJSON,req,res,targetBucket,myuuid,x);
					});
				
			});
			*/
	// }else{
	// 	gm(GLOBAL.temp_dir_location+'/'+filename)
	// 		.resize(targetValue)
	// 		.stream(function (err, stdout, stderr) {
	// 			//var writeStream = fs.createWriteStream('/path/to/my/reformatted.png');
	// 			//stdout.pipe(writeStream);
	// 			var gcs = GLOBAL.gcloud.storage();
	// 			var bucket = gcs.bucket(GLOBAL.storage_names.advertisements);
	// 			var file = bucket.file('images/'+targetValue+'_'+filename);
	// 			stdout.pipe(file.createWriteStream())
	// 				.on('error', function(err) {
	// 					var msg = {
	// 						"success":false, "status":"Error",
	// 						"desc":"[saveSrapeData:resampleImage]Error in Upload",
	// 						"err":err
	// 					};
	// 					process.emit('Shopstyx:logError',msg);
	// 					checkDelete(size_array,filename,dataJSON,req,res,targetBucket,myuuid,x);
	// 				})
	// 				.on('finish', function() {
	// 					// The file upload is complete.
	// 					//console.log("Successfully uploaded "+targetValue+'_'+filename);
	// 					checkDelete(size_array,filename,dataJSON,req,res,targetBucket,myuuid,x);
	// 				});
				
	// 		});
	// }
}
function checkDelete(size_array,filename,dataJSON,req,res,targetBucket,myuuid,x)
{
	if(x<size_array.length)
	{
		x=x+1;
		if(typeof(size_array[x])!='undefined')
		{
			if(process.env.NODE_ENV.toLowerCase()=='development')
			{
				setTimeout(deleteStraggler,120000,size_array,filename,dataJSON,req,res,targetBucket,myuuid,x);
			}
			resampleImage(size_array,filename,dataJSON,req,res,targetBucket,myuuid,x,size_array[x]);
		}else{
			if(process.env.NODE_ENV.toLowerCase()=='development')
			{
				setTimeout(deleteStraggler,120000,size_array,filename,dataJSON,req,res,targetBucket,myuuid,x);
			}
			checkDelete(size_array,filename,dataJSON,req,res,targetBucket,myuuid,x);
		}
	}else{
		//console.log('Deleting file:'+filename);
		//setTimeout(deleteSourceFile,3000,GLOBAL.temp_dir_location+'/'+filename);
		setTimeout(deleteSourceFile, 120000, GLOBAL.temp_dir_location+'/'+filename);
		if(dataJSON.tempStoreImagesCounter==0)
		{
			var msg = {
				"success":true, "status":"Success",
				"desc":"[savePhotoStyxMedia:checkDelete]Saved Data"
			};
			respond.respondError(msg,res,req);
		}
		dataJSON.tempStoreImagesCounter=dataJSON.tempStoreImagesCounter+1;
		//resampleImage(size_array,filename,dataJSON,req,res,targetBucket,myuuid,x,targetValue);
		saveUploadProcess.updateStatus(myuuid,'Ready');
		/*if(dataJSON.request.type.toLowerCase().replace(/\s/g, '')=='update')
		{
			//console.log("triggering update");
			reportUploadFinished.start(myuuid,0);
		}*/
	}
}
function deleteSourceFile(file)
{
	fs.access(file,fs.F_OK,function(err){
		if(err==null)
		{
			//console.log('file found '+file);
			fs.access(file, fs.R_OK | fs.W_OK, function (err) {
				if(err==null)
				{
					//console.log('can delete file'+file);
					//setTimeout(fs.unlinkSync,10000,upload_location+'/'+filename);
					fs.unlink(file, function (err) {
						if(err!=null){
							//console.log('Error Deleting '+file);
						}else{
							//console.log('successfully deleted '+file);
						}
					});
				}else{
					//console.log('no access to '+file);
				}
			});
		}else{
			//console.log('not found file: '+file);
		}
	});
}

function deleteStraggler(size_array,filename,dataJSON,req,res,targetBucket,myuuid,x)
{
	var my_x = x-1;
	if(typeof(size_array[my_x])!='undefined')
	{
		var targetValue = size_array[my_x];
		var file = GLOBAL.temp_dir_location+'/'+targetValue+'_'+filename
		fs.access(file,fs.F_OK,function(err){
			if(err==null)
			{
				//console.log('Straggler file found '+file);
				fs.access(file, fs.R_OK | fs.W_OK, function (err) {
					if(err==null)
					{
						//console.log('can delete Straggler file'+file);
						//setTimeout(fs.unlinkSync,10000,upload_location+'/'+filename);
						fs.unlink(file, function (err) {
							if(err!=null){
								//console.log('Error Deleting '+file);
							}else{
								//console.log('successfully deleted '+file);
							}
						});
					}else{
						//console.log('no access to '+file);
					}
				});
			}else{
				//console.log('not found file: '+file);
			}
		});
	}
}
module.exports={
	resampleMedia:resampleMedia
};