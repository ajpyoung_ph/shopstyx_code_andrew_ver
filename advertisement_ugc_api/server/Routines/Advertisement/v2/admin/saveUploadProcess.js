const mongojs = require('mongojs');
const util = require('util');
/*
{
	"success":true, "status": "Success",
	"data": {
             "new_uuid_v1":"2c39c190-ad36-11e5-beaf-5b01cef14b41"
	}
}
{
	"status":"Waiting to Switch places with old record"
}
{
	"status":"Waiting to Switch with new updated record"
}
{
	"status": "Converting & Uploading"
}

{
	"status": "In Progress"
}

{
	"status": "Deleted"
}
*/

var updateStatus = function(uuid_v1,status)
{
	var query = {
		'uuid_v1':uuid_v1.toString()
	};
	var options = {
		'upsert':true,
		'multi':false
	};
	var data = {
		'$set':{
			'uuid_v1':uuid_v1.toString(),
			'status':status
		}
	};
	
	GLOBAL.mongodb.uploadProcess.findOne(query,function(err,doc){
		if(err==null)
		{
			if(doc!=null)
			{
				//_id: mongojs.ObjectId('523209c4561c640000000001')
				query = {
					'_id':mongojs.ObjectId(doc._id.toString())
				};
				options = {
					'upsert':false,
					'multi':false
				};
			}
			if(util.isNullOrUndefined(doc)==false)
			{
				if(util.isNullOrUndefined(doc.status)==false)
				{
					if(doc.status=='Ready')
					{
						data = {
							'$set':{
								'uuid_v1':uuid_v1.toString(),
								'status':'Ready'
							}
						};
					}
				}
			}
			
			GLOBAL.mongodb.uploadProcess.update(query,data,options,function(err,retVal){
				if(err!=null)
				{
					var msg = {
						"success":false, "status":"Error",
						"desc":"[saveUploadProcess:updateStatus]Saved NoSQL Data",
						"data":{
					        "uuid_v1":uuid_v1.toString()
					    },
					    "err":err
					};
					process.emit('Shopstyx:logError',msg);
				}
			});
			
		}else{
			var msg = {
				"success":false, "status":"Error",
				"desc":"[saveUploadProcess:updateStatus]Connection NoSQL Data",
				"data":{
			        "uuid_v1":uuid_v1.toString()
			    },
			    "err":err
			};
			process.emit('Shopstyx:logError',msg);
		}
	});
}

var updateStatus_withOldInfo = function(uuid_v1,status,OldData,NewData)
{
	var query = {
		'uuid_v1':uuid_v1.toString()
	};
	var data = {
		'$set':{
			'uuid_v1':uuid_v1.toString(),
			'status':status,
			'additional_info':{
				'old_data':OldData,
				'new_data':NewData
			}
		}
	};
	var options = {
		'upsert':true,
		'multi':false
	};
	GLOBAL.mongodb.uploadProcess.findOne(query,function(err,doc){
		if(err==null)
		{
			if(doc!=null)
			{
				//_id: mongojs.ObjectId('523209c4561c640000000001')
				query = {
					'_id':mongojs.ObjectId(doc._id.toString())
				};
				options = {
					'upsert':false,
					'multi':false
				};
			}
			GLOBAL.mongodb.uploadProcess.update(query,data,options,function(err,retVal){
				if(err!=null)
				{
					var msg = {
						"success":false, "status":"Error",
						"desc":"[saveUploadProcess:updateStatus_withOldInfo]Saved NoSQL Data",
						"data":{
					        "uuid_v1":uuid_v1.toString()
					    },
					    "err":err
					};
					process.emit('Shopstyx:logError',msg);
				}
			});
		}else{
			var msg = {
				"success":false, "status":"Error",
				"desc":"[saveUploadProcess:updateStatus_withOldInfo]Connection NoSQL Data",
				"data":{
			        "uuid_v1":uuid_v1.toString()
			    },
			    "err":err
			};
			process.emit('Shopstyx:logError',msg);
		}
	});
}

var deleteStatus = function(uuid_v1)
{
	var query = {
		'uuid_v1':uuid_v1.toString()
	};
	GLOBAL.mongodb.uploadProcess.remove(query,function(err,retVal){
		if(err!=null)
		{
			var msg = {
				"success":false, "status":"Error",
				"desc":"[saveUploadProcess:deleteStatus]Saved NoSQL Data",
				"data":{
			        "uuid_v1":uuid_v1.toString()
			    },
			    "err":err
			};
			process.emit('Shopstyx:logError',msg);
		}
	});
}
module.exports={
	updateStatus:updateStatus,
	updateStatus_withOldInfo:updateStatus_withOldInfo,
	deleteStatus:deleteStatus
};