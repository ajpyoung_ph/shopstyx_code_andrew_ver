const fs = require('fs');
const textMan = require(GLOBAL.server_location+'/helpers/text_manipulation');
const saveVideoStyxMedia = require(__dirname+'/saveVideoStyxMedia');
const savePhotoStyxMedia = require(__dirname+'/savePhotoStyxMedia');
const saveUploadProcess = require(__dirname+'/saveUploadProcess');
const saveCollageMedia = require(__dirname+'/saveCollageMedia');
const reportUploadFinished = require(__dirname+'/reportUploadFinished');
const util = require('util');
const respond = require(GLOBAL.server_location+'/EventListeners/errorListener');

	/*
	{
		"files": "{ 
			prevpic:[ 
				{ 
					fieldname: 'prevpic',
					originalname: '12047042_10204679170119053_7349686871162241667_n.jpg',
					encoding: '7bit',
					mimetype: 'image/jpeg',
					destination: '/home/andrew/Projects/NodeProjects/TestCodes/FileUploads/server/uploads/',
					filename: '59c2c238e2d633a0ea5460e6490abe77',
					path: '/home/andrew/Projects/NodeProjects/TestCodes/FileUploads/server/uploads/59c2c238e2d633a0ea5460e6490abe77',
					size: 76410 
				} 
			],
			src:[ 
				{ 
					fieldname: 'src',
					originalname: 'VID_20151117_0818.mp4',
					encoding: '7bit',
					mimetype: 'video/mp4',
					destination: '/home/andrew/Projects/NodeProjects/TestCodes/FileUploads/server/uploads/',
					filename: '7ef6f340476ac32b81d02d97f4458eb5',
					path: '/home/andrew/Projects/NodeProjects/TestCodes/FileUploads/server/uploads/7ef6f340476ac32b81d02d97f4458eb5',
					size: 72992528 
				} 
			] 
		}",
		"body": "{ something: 'asdfalsdfjaf', description: 'afsdaf' }"
	}
	*/
	/*
	"request": {
		"type": "Save",
		"data": {
            "products":[1,2,3,4,5],
            "product_pool":[1,2,3,4,5,6,7,8,9],
			"styx_name": 'Andrew and IHOP friends',
			"short_description": 'One fine weekend, Andrew and his cuddly little muchkin friends went to IHOP to reward themselves for doing a great job over the week.',
			"src": 'uploads/andrewihop.mp4',
			"styx_type": 'V', //'V' for Video, 'P' for Photo, 'S' for Slideshow, 'B' for Banner
			"duration": 150, //by seconds <- for Video only
			"width": 720, //width of media <- Video/Photo
			"height": 480, //height of media <- Video/Photo
			"owner": 12, //shopstyx user_id
			"color_theme": { //color theme (using google’s paper-style system)
				color1: "#4CBB17", //--default-primary-color
				color2: "#f7f6f2", //--accent-color
				color3: "#ffffff" //--primary-background-color
			},
			"pin_icon": "maps:local-offer", //use any element for this
			...

		}
	}
	*/
	
			//sample pictures to different sizes
			/*
			$imgfile_webstore960 = $dir.$new_filename.'_webstore960'.$ext;
			$imgfile_medium650 = $dir.$new_filename.'_medium650'.$ext;
			$imgfile_small300 = $dir.$new_filename.'_small300'.$ext;
			$imgfile_xsmall100 = $dir.$new_filename.'_xsmall100'.$ext;
			*/
			

var video = function(dataJSON,req,res,targetBucket,my_uuid,req,res)
{
	//look if prevpic is available... if so, rename to prev_pic+my_uuid and upload to advert bucket
	//upload video directly to bucket with vid_my_uuid

	var files = req.files;
	saveUploadProcess.updateStatus(my_uuid,'In Progress');
	if(util.isNullOrUndefined(files)==false)
	{
		
		var prevpic = false;
		var newfilenames = {
			video:'',
			prevpic:''
		};

		if(util.isNullOrUndefined(files.prevpic)==false)
		{
			var prev_pic_file_ext = textMan.getFileExtenstion(files.prevpic[0].originalname);
			newfilenames.prevpic = 'prev_pic_'+my_uuid+'.'+prev_pic_file_ext;
			fs.renameSync(files.prevpic[0].path, GLOBAL.temp_dir_location+'/'+newfilenames.prevpic);
			prevpic=true;
		}
		if(util.isNullOrUndefined(files.src)==false)
		{
			var src_file_ext = textMan.getFileExtenstion(files.src[0].originalname);
			newfilenames.video = 'src_'+my_uuid+'.'+src_file_ext;
			fs.renameSync(files.src[0].path, GLOBAL.temp_dir_location+'/'+newfilenames.video);
			saveVideoStyxMedia.uploadMedia(prevpic,newfilenames,dataJSON,req,res,targetBucket,my_uuid);
		}else{
			switch(dataJSON.request.type.toLowerCase().replace(/\s/g, ''))
			{
				case 'update':
					saveUploadProcess.updateStatus(my_uuid,'Ready');
					
					break;
				case 'save':
				default:
					saveUploadProcess.deleteStatus(my_uuid);
					var msg = {
						"success":false, "status":"Error",
						"desc":"[saveMedia:video]No video found"
					};
					respond.respondError(msg,res,req);
					break;
			}
		}
	}else{
		switch(dataJSON.request.type.toLowerCase().replace(/\s/g, ''))
		{
			case 'update':
				saveUploadProcess.updateStatus(my_uuid,'Ready');
				
				break;
			case 'save':
			default:
				saveUploadProcess.deleteStatus(my_uuid);
				var msg = {
					"success":false, "status":"Error",
					"desc":"[saveMedia:video]No video found"
				};
				respond.respondError(msg,res,req);
				break;
		}
	}
}

var third_party_video = function(dataJSON,req,res,targetBucket,my_uuid,req,res)
{
	var files = req.files;
	saveUploadProcess.updateStatus(my_uuid,'In Progress');
	if(util.isNullOrUndefined(files)==false)
	{
		var prevpic = false;
		var newfilenames = {
			video:'',
			prevpic:''
		};

		if(util.isNullOrUndefined(files.prevpic)==false)
		{
			var prev_pic_file_ext = textMan.getFileExtenstion(files.prevpic[0].originalname);
			newfilenames.prevpic = 'prev_pic_'+my_uuid+'.'+prev_pic_file_ext;
			fs.renameSync(files.prevpic[0].path, GLOBAL.temp_dir_location+'/'+newfilenames.prevpic);
			prevpic=true;
			saveVideoStyxMedia.uploadMedia(prevpic,newfilenames,dataJSON,req,res,targetBucket,my_uuid);
		}else{
			switch(dataJSON.request.type.toLowerCase().replace(/\s/g, ''))
			{
				case 'save':
					saveUploadProcess.updateStatus(my_uuid,'Ready');
					break;
				case 'update':
					saveUploadProcess.updateStatus(my_uuid,'Ready');
					
					break;
				default:
					break;
			}
		}
	}else{
		switch(dataJSON.request.type.toLowerCase().replace(/\s/g, ''))
		{
			case 'save':
				saveUploadProcess.updateStatus(my_uuid,'Ready');
				break;
			case 'update':
				saveUploadProcess.updateStatus(my_uuid,'Ready');
				
				break;
			default:
				break;
		}
		// saveUploadProcess.updateStatus(my_uuid,'Ready');
		// reportUploadFinished.start(my_uuid,0);
	}
}

var photo = function(size_array,dataJSON,req,res,targetBucket,my_uuid,req,res)
{
	//rename photo to photo_my_uuid
	//resample original 
	//upload resampled to bucket
	//delete original
	var files = req.files;
	saveUploadProcess.updateStatus(my_uuid,'In Progress');
	if(util.isNullOrUndefined(files)==false)
	{
		if(util.isNullOrUndefined(files.src)==false)
		{
			var src_file_ext = textMan.getFileExtenstion(files.src[0].originalname);
			var newfilename = 'src_'+my_uuid+'.'+src_file_ext;
			fs.renameSync(files.src[0].path, GLOBAL.temp_dir_location+'/'+newfilename);
			savePhotoStyxMedia.resampleMedia(size_array,newfilename,dataJSON,req,res,targetBucket,my_uuid);
		}else{
			switch(dataJSON.request.type.toLowerCase().replace(/\s/g, ''))
			{
				case 'update':
					saveUploadProcess.updateStatus(my_uuid,'Ready');
					
					break;
				case 'save':
				default:
					saveUploadProcess.deleteStatus(my_uuid);
					var msg = {
						"success":false, "status":"Error",
						"desc":"[saveMedia:photo]No video found"
					};
					respond.respondError(msg,res,req);
					break;
			}
		}
	}else{
		switch(dataJSON.request.type.toLowerCase().replace(/\s/g, ''))
		{
			case 'update':
				saveUploadProcess.updateStatus(my_uuid,'Ready');
				
				break;
			case 'save':
			default:
				saveUploadProcess.deleteStatus(my_uuid);
				var msg = {
					"success":false, "status":"Error",
					"desc":"[saveMedia:photo]No video found"
				};
				respond.respondError(msg,res,req);
				break;
		}
	}
}

var collage = function(size_array,dataJSON,req,res,targetBucket,my_uuid,req,res)
{
	var files = req.files;
	//console.log("collage media check start");
	if(util.isNullOrUndefined(files)==false)
	{
		//console.log("collage Media in Progress");
		//console.log(files);
		
		if(util.isNullOrUndefined(files.src)==false)
		{
			saveUploadProcess.updateStatus(my_uuid,'In Progress');
			saveCollageMedia.start(dataJSON,req,res,targetBucket,my_uuid);
		}else{
			switch(dataJSON.request.type.toLowerCase().replace(/\s/g, ''))
			{
				case 'save':
					//console.log("collage save media NO FILES");
					saveUploadProcess.updateStatus(my_uuid,'Ready');
					break;
				case 'update':
					//console.log("collage update media NO FILES");
					saveUploadProcess.updateStatus(my_uuid,'Ready');
					
					break;
				default:
					// saveUploadProcess.updateStatus(my_uuid,'Ready');
					saveUploadProcess.deleteStatus(my_uuid);
					// var msg = {
					// 	"success":false, "status":"Error",
					// 	"desc":"[saveMedia:collage]No Collage Photo found"
					// };
					// respond.respondError(msg,res,req);
					break;
			}
		}
	}else{
		switch(dataJSON.request.type.toLowerCase().replace(/\s/g, ''))
		{
			case 'save':
				//console.log("collage save media NO FILES");
				saveUploadProcess.updateStatus(my_uuid,'Ready');
				break;
			case 'update':
				//console.log("collage update media NO FILES");
				saveUploadProcess.updateStatus(my_uuid,'Ready');
				
				break;
			default:
				// saveUploadProcess.updateStatus(my_uuid,'Ready');
				saveUploadProcess.deleteStatus(my_uuid);
				// var msg = {
				// 	"success":false, "status":"Error",
				// 	"desc":"[saveMedia:collage]No Collage Photo found"
				// };
				// respond.respondError(msg,res,req);
				break;
		}
	}
};

var slideshow = function(size_array,dataJSON,req,res,targetBucket,my_uuid,req,res)
{
	//no actual files 
	saveUploadProcess.updateStatus(my_uuid,'In Progress');
	switch(dataJSON.request.type.toLowerCase().replace(/\s/g, ''))
	{
		case 'save':
			saveUploadProcess.updateStatus(my_uuid,'Ready');
			break;
		case 'update':
			saveUploadProcess.updateStatus(my_uuid,'Ready');
			
			break;
		default:
			saveUploadProcess.deleteStatus(my_uuid);
			var msg = {
				"success":false, "status":"Error",
				"desc":"[saveMedia:collage]No Slideshow Photo found"
			};
			respond.respondError(msg,res,req);
			break;
	}
};

module.exports={
	video:video,
	third_party_video:third_party_video,
	photo:photo,
	collage:collage,
	slideshow:slideshow
};