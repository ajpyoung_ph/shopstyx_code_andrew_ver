const checkWeight = require('../Routines/ProductLists/v2/client/checkWeights');

var start = function()
{
	//email folder
	process.on('Shopstyx:checkWeight:collectInterest',checkWeight.collectInterest);
	process.on('Shopstyx:checkWeight:checkPastSearches',checkWeight.checkPastSearches);
	process.on('Shopstyx:checkWeight:checkSearchString',checkWeight.checkSearchString);
};

module.exports={
	start:start
};