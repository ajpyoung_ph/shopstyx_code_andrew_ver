Array.prototype.toLowerCase= function(){
	var L= this.length, tem;
	while(L){
		tem= this[--L] || '';
		if(tem.toLowerCase) this[L]= tem.toLowerCase();		
	}
	return this;
}